ALTER TABLE {$NAMESPACE}_okrs.okrs_keyresult 
CHANGE COLUMN `target` `target` FLOAT NOT NULL,
CHANGE COLUMN `baseValue` `baseValue` FLOAT NOT NULL;
