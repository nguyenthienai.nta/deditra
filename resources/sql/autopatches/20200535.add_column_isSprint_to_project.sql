ALTER TABLE {$NAMESPACE}_project.project 
ADD COLUMN `isSprint` TINYINT(1) NULL DEFAULT NULL AFTER `subtype`,
ADD COLUMN `startDate` INT(10) NULL DEFAULT NULL AFTER `isSprint`,
ADD COLUMN `endDate` INT(10) NULL DEFAULT NULL AFTER `startDate`;