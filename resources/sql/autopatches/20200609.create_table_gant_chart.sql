CREATE TABLE IF NOT EXISTS {$NAMESPACE}_po_toolkit.po_toolkit_gantchart (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `phid` varbinary(64) NOT NULL,
    `projectPHID` varbinary(64) NOT NULL,
    `nameTask` text COLLATE utf8mb4_bin NOT NULL,
    `startDate` int(10) unsigned,
    `endDate` int(10) unsigned,
    `parentTaskPHID` varbinary(64),
    `progress` int,
    `dateCreated` int(10) unsigned NOT NULL,
    `dateModified` int(10) unsigned NOT NULL,
    `isDeleted` tinyint(1) NOT NULL,
     PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
