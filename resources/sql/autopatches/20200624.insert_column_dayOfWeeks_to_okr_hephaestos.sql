ALTER TABLE {$NAMESPACE}_okrs.okrs_keyresult
ADD COLUMN `dayOfWeeks` VARCHAR(255) NULL;

ALTER TABLE {$NAMESPACE}_hephaestos.hephaestos_keyresult
ADD COLUMN `dayOfWeeks` VARCHAR(255) NULL AFTER `frequenceTime`;

