CREATE TABLE IF NOT EXISTS {$NAMESPACE}_po_toolkit.po_toolkit_projects (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `phid` varbinary(64) NOT NULL,
    `name` longtext COLLATE utf8mb4_bin NOT NULL,
    `nameProjectCompany` varbinary(64),
    `icon` varbinary(64),
    `viewPolicy` varbinary(64) NOT NULL,
    `editPolicy` varbinary(64) NOT NULL,
    `dateCreated` int(10) unsigned NOT NULL,
    `dateModified` int(10) unsigned NOT NULL,
    `isArchived` tinyint(1) NOT NULL,
     PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
