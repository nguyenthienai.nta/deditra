ALTER TABLE {$NAMESPACE}_po_toolkit.po_toolkit_projects
ADD `customer` longtext COLLATE utf8mb4_bin AFTER `icon`,
ADD `who` longtext COLLATE utf8mb4_bin AFTER `customer`,
ADD `productName` longtext COLLATE utf8mb4_bin AFTER `who`,
ADD `classify` longtext COLLATE utf8mb4_bin AFTER `productName`,
ADD `benefit` longtext COLLATE utf8mb4_bin AFTER `classify`,
ADD `unlike` longtext COLLATE utf8mb4_bin AFTER `benefit`,
ADD `ourProduct` longtext COLLATE utf8mb4_bin AFTER `unlike`;
