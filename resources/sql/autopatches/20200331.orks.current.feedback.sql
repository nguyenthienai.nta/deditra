ALTER TABLE {$NAMESPACE}_okrs.okrs_current 
ADD COLUMN `feedback` INT(10) UNSIGNED NULL AFTER `color`,
ADD COLUMN `comment` LONGTEXT NULL AFTER `feedback`;