ALTER TABLE {$NAMESPACE}_okrs.okrs_keyresult
ADD COLUMN `startDate` INT(10) UNSIGNED NOT NULL AFTER `frequenceTime`,
ADD COLUMN `endDate` INT(10) UNSIGNED NOT NULL AFTER `startDate`;
