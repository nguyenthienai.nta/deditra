/**
 * @provides javelin-behavior-project-edit
 * @requires javelin-behavior
 *           javelin-dom
 *           javelin-stratcom
 *           javelin-workflow
 */

JX.behavior('project-edit', function (config) {
  var isSprint = $("input[name='isSprint[]'][value='1']");
  $('.aphront-form-control-date').hide();

  $(document).ready(function () {

    // css date input
    $('.aphront-form-time-input-container').hide();
    $('.aphront-form-control-checkbox').insertAfter( $('.aphront-form-control-textarea'));
    var startDate = $("input[name='startDate_d']").parent().parent();
    var endDate = $("input[name='endDate_d']").parent().parent();
    var $spanTo = "<span class = 'spandate' style = 'margin-right: 15px; font-weight: bold; color: #6B748C;'>-</span>";
    var $spanDate = "<span class = 'spandate' style = 'margin-right: 10px; margin-left: 5px; font-weight: bold; color: #6B748C;'>Date</span>";

    isSprint.parent().parent().parent().css("display", "inline-block");
    isSprint.parent().parent().parent().parent().attr('style', 'margin-top: 5px !important; display: inline-table; margin-bottom: 6px !important;');
    startDate.insertAfter(isSprint.parent().parent().parent().parent());
    endDate.insertAfter(startDate);
    $($spanTo).insertAfter(startDate);    
    $($spanDate).insertBefore(startDate);    
    $('.aphront-form-control-date').remove();  

    // end of css date input

    // hide or show when check radio button isSpprint
    isSprint.change(function () {
      if (isSprint.prop('checked')) {
        $('.spandate').show();
        startDate.show();
        endDate.show();
        $('.aphront-form-error').eq(-1).show();
      } else {
        $('.spandate').hide();
        startDate.hide();
        endDate.hide();
        $('.aphront-form-error').eq(-1).hide();
      }
    }).change();
    // end of hide or show when check radio button isSpprint

    // clear data if isSprint not check
    $("button[type='submit']").click(function () {
      if (!isSprint.prop('checked')) {
        $("input[name='startDate_d']").val('');
        $("input[name='endDate_d']").val('');
      }
    });

  });
});
