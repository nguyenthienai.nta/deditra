/**
 * @provides javelin-behavior-kr
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('kr', function () {
  $(document).ready(function () {
    dayOffs = JSON.parse($('input[name$="dayOffs"]').val());
    startDate = $(".startDate");
    endDate = $(".endDate");
    // $(".startDate").change(function () {
    //   if (Date.parse(startDate.val()) >= Date.parse(endDate.val())) {
    //     endDate.css("border", "2px solid red");
    //     $("#save").attr("disabled", true);
    //     $("#save-new").attr("disabled", true);
    //   } else {
    //     endDate.css("border", "");
    //     $("#save").attr("disabled", false);
    //     $("#save-new").attr("disabled", false);
    //   }
    // });

    // $(".endDate").change(function () {
    //   if (Date.parse(startDate.val()) >= Date.parse(endDate.val())) {
    //     endDate.css("border", "2px solid red");
    //     $("#save").attr("disabled", true);
    //     $("#save-new").attr("disabled", true);
    //   } else {
    //     endDate.css("border", "");
    //     $("#save").attr("disabled", false);
    //     $("#save-new").attr("disabled", false);
    //   }
    // });


    typeChartInput = $('select[name$="typeChart"]').parent().parent();
    targetInput = $('input[name$="target"]').parent().parent();
    baseValueInput = $('input[name$="baseValue"]').parent().parent();
    default_totaldev_value = $("#totalDev").val();
    default_basevalue = $('#baseValue').val();
    default_target = $('#target').val();
    default_meansure_by = $('#manual-checkin').val();
    span = ($('#projectPHID').children().children()[0]);

    //KR Detail
    if ($('.phui-header-header').text() == 'KR Detail') {
      $('.aphront-form-label').attr('style', 'text-align: left; padding-left: 20%');
      $('.aphront-form-input').children('div').attr('style', 'padding-left: 50%; word-break: break-all;');
      $('.aphront-form-control-nolabel').attr('style', 'padding: 0px');
    }

    //datepicker
    jQuery.browser = {};
    (function () {
      jQuery.browser.msie = false;
      jQuery.browser.version = 0;
      if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
      }
    })();

    $.datepicker._gotoToday = function(id) {
      $(id).datepicker('setDate', new Date()).datepicker('hide').blur();
    };

    $(".startDate").datepicker({
      showButtonPanel: true,
      closeText: 'Clear',
      onClose: function(e) {
        var ev = window.event;
        if (ev.srcElement.innerHTML == 'Clear')
          this.value = "";
      },
      dateFormat: 'dd/mm/yy',//check change
    });

    $(".endDate").datepicker({
      showButtonPanel: true,
      closeText: 'Clear',
      onClose: function(e) {
        var ev = window.event;
        if (ev.srcElement.innerHTML == 'Clear')
          this.value = "";
      },
      dateFormat: 'dd/mm/yy',//check change
    });

    $('#start').on('click', function () {
      $('#startDate').trigger("select");
    });

    $('#end').on('click', function () {
      $('#endDate').trigger("select");
    });

    if(dayOffs != ''){
      if($(".phui-info-severity-error").length > 0){
        checkError();
      }
      else{
        $('#day_off').multiDatesPicker({
          showButtonPanel: true,
          closeText: 'Clear',
          onClose: function(e) {
            var ev = window.event;
            if (ev.srcElement.innerHTML == 'Clear')
              this.value = "";
          },
          dateFormat: 'dd/mm/yy',
          addDates: dayOffs,
          onSelect: function(value, date) {
            date.dpDiv.find('.ui-datepicker-current-day a')
                .css('background-color', 'black');
            dayOffNewValues = $('#day_off').val();
            localStorage.setItem("dayOffs", dayOffNewValues);
          }
        });
      }
    }
    else{
      if($(".phui-info-severity-error").length > 0){
        checkError();
      }
      else{
        $('#day_off').multiDatesPicker({
          showButtonPanel: true,
          closeText: 'Clear',
          onClose: function(e) {
            var ev = window.event;
            if (ev.srcElement.innerHTML == 'Clear')
              this.value = "";
          },
          dateFormat: 'dd/mm/yy',
          onSelect: function(value, date) {
            date.dpDiv.find('.ui-datepicker-current-day a')
                .css('background-color', 'black');
            dayOffNewValues = $('#day_off').val();
            localStorage.setItem("dayOffs", dayOffNewValues);
          }
        });
      }
    }

    // total dev show hide when select HII HFI
    if ($('#manual-checkin').val() == 0) {
      $("#isAutoCheckin").parent().hide();
    } else {
      $("#isAutoCheckin").parent().show();
    }

    if ($('#manual-checkin').val() == 4 || $('#manual-checkin').val() == 5) {
      $("#txtDev").show();
    }
    else {
      $("#txtDev").hide();
    }

    //checked auto checkin when create
    if($('.phui-header-header').text() === 'Create KR' && $('.phui-info-view-icon').length === 0) {
      $("#isAutoCheckin").attr('checked','true');
    }

    if ($('#chkAutoCheckin').val() == 1) {
      $("#isAutoCheckin").attr('checked','true');
    } else if ($('#manual-checkin').val() == 4 || $('#manual-checkin').val() == 5) {
      $("#txtDev").hide();
    }

    $("#isAutoCheckin").click(function () {
      // $("input[name*='totalDev']").val('');
      if ($('#manual-checkin').val() == 4 || $('#manual-checkin').val() == 5) {
        if($('#isAutoCheckin')[0].checked) {
          $("#txtDev").show();
        } else {
          $("#txtDev").hide();
        }
      }
    });

    // checked date of week
    if($("#dayOfWeeksValue").length > 0) {
      var dateOfWeeks = $("#dayOfWeeksValue").val();

      if (dateOfWeeks.indexOf('1') !== -1) {
        $('#mon').attr('checked', 'true');
      }
      if (dateOfWeeks.indexOf('2') !== -1) {
        $('#tue').attr('checked', 'true');
      }
      if (dateOfWeeks.indexOf('3') !== -1) {
        $('#wed').attr('checked', 'true');
      }
      if (dateOfWeeks.indexOf('4') !== -1) {
        $('#thu').attr('checked', 'true');
      }
      if (dateOfWeeks.indexOf('5') !== -1) {
        $('#fri').attr('checked', 'true');
      }
      if (dateOfWeeks.indexOf('6') !== -1) {
        $('#sat').attr('checked', 'true');
      }
      if (dateOfWeeks.indexOf('7') !== -1) {
        $('#sun').attr('checked', 'true');
      }

      $("#sun").parent().click(function () {
        if ($('#sun')[0].checked) {
          $("#dayOfWeeksValue").val($("#dayOfWeeksValue").val() + '7')
        } else {
          let removeDay = $("#dayOfWeeksValue").val();
          let regex = new RegExp('7', 'g');
          $("#dayOfWeeksValue").val(removeDay.replace(regex, ''));
        }
      });
      $("#mon").parent().click(function () {
        if ($('#mon')[0].checked) {
          $("#dayOfWeeksValue").val($("#dayOfWeeksValue").val() + '1')
        } else {
          let removeDay = $("#dayOfWeeksValue").val();
          let regex = new RegExp('1', 'g');
          $("#dayOfWeeksValue").val(removeDay.replace(regex, ''));
        }
      });
      $("#tue").parent().click(function () {
        if ($('#tue')[0].checked) {
          $("#dayOfWeeksValue").val($("#dayOfWeeksValue").val() + '2')
        } else {
          let removeDay = $("#dayOfWeeksValue").val();
          let regex = new RegExp('2', 'g');
          $("#dayOfWeeksValue").val(removeDay.replace(regex, ''));
        }
      });
      $("#wed").parent().click(function () {
        if ($('#wed')[0].checked) {
          $("#dayOfWeeksValue").val($("#dayOfWeeksValue").val() + '3')
        } else {
          let removeDay = $("#dayOfWeeksValue").val();
          let regex = new RegExp('3', 'g');
          $("#dayOfWeeksValue").val(removeDay.replace(regex, ''));
        }
      });
      $("#thu").parent().click(function () {
        if ($('#thu')[0].checked) {
          $("#dayOfWeeksValue").val($("#dayOfWeeksValue").val() + '4')
        } else {
          let removeDay = $("#dayOfWeeksValue").val();
          let regex = new RegExp('4', 'g');
          $("#dayOfWeeksValue").val(removeDay.replace(regex, ''));
        }
      });
      $("#fri").parent().click(function () {
        if ($('#fri')[0].checked) {
          $("#dayOfWeeksValue").val($("#dayOfWeeksValue").val() + '5')
        } else {
          let removeDay = $("#dayOfWeeksValue").val();
          let regex = new RegExp('5', 'g');
          $("#dayOfWeeksValue").val(removeDay.replace(regex, ''));
        }
      });
      $("#sat").parent().click(function () {
        if ($('#sat')[0].checked) {
          $("#dayOfWeeksValue").val($("#dayOfWeeksValue").val() + '6')
        } else {
          let removeDay = $("#dayOfWeeksValue").val();
          let regex = new RegExp('6', 'g');
          $("#dayOfWeeksValue").val(removeDay.replace(regex, ''));
        }
      });

      if(($('select[name="frequence"]').find(':selected').val() ==='Day') && ($("#dayOfWeeksValue").val() === 'every-weekday')) {
        if($(".phui-info-severity-error").length == 0) {
          $("#frequence_time").val('');
        }
        $("#every-weekday").attr('checked', 'true');
      }

      if(($('select[name="frequence"]').find(':selected').val() ==='Month')) {
        if($("#dayOfWeeksValue").val().indexOf(',') === -1) {
          $("#dayOfMonth").val($("#dayOfWeeksValue").val());
        } else {
          let dayOfWeek = $("#dayOfWeeksValue").val().split(",");
          $("#on_the").attr('checked', 'true');
          $("#pre_dayOfWeek").val(dayOfWeek[0]);
          $("#dayOfWeek").val(dayOfWeek[1]);
        }
      }
    }
    //end


    // clear total dev when total dev = 0
    if($("input[name*='totalDev']").val() == 0) {
      $("input[name*='totalDev']").val('')
    }


    //check Error
    function checkError() {
      if (localStorage.getItem("dayOffs")) {
        if (localStorage.getItem("dayOffs").split(', ') != '') {
          $('#day_off').multiDatesPicker({
            dateFormat: 'dd/mm/yy',
            addDates: localStorage.getItem("dayOffs").split(', '),
            onSelect: function (value, date) {
              date.dpDiv.find('.ui-datepicker-current-day a')
                .css('background-color', 'black');
              dayOffNewValues = $('#day_off').val();
              localStorage.setItem("dayOffs", dayOffNewValues);
            }
          });
        }
      } else {
        if (dayOffs != '') {
          $('#day_off').multiDatesPicker({
            dateFormat: 'dd/mm/yy',
            addDates: dayOffs,
            onSelect: function (value, date) {
              date.dpDiv.find('.ui-datepicker-current-day a')
                .css('background-color', 'black');
              dayOffNewValues = $('#day_off').val();
              localStorage.setItem("dayOffs", dayOffNewValues);
            }
          });
        } else {
          $('#day_off').multiDatesPicker({
            dateFormat: 'dd/mm/yy',
            onSelect: function (value, date) {
              date.dpDiv.find('.ui-datepicker-current-day a')
                .css('background-color', 'black');
              dayOffNewValues = $('#day_off').val();
              localStorage.setItem("dayOffs", dayOffNewValues);
            }
          });
        }
      }
    }

    dateObj = new Date();
    month = dateObj.getMonth() + 1;
    day = String(dateObj.getDate()).padStart(2, '0');
    year = dateObj.getFullYear();
    output = day + '/' + (month > 9 ? month : '0' + month) + '/' + year;

    if($('select[name$="manualCheckin"]').children("option:selected").val() != 2 && dayOffs.indexOf(output) != -1){
      $('#check_in').remove();
    };

    //disable check-in
    $('#check_in').click(function () {
      $(this).attr('href', '#');
    });

    //clear base value, target value if not select manual
    if (!isSelectManually()){
      $('#baseValue').val('');
      $('#target').val('');
    }

    //hidden type chart and check-in if meansure by is not manually or by project point
    if ($('select[name$="manualCheckin"]').children("option:selected").val() == 2) {
      $('#day_off').parent().parent().parent().slideUp();
    } else $('#day_off').parent().parent().parent().slideDown();

    if (isNotNeededCheckin()) {
      typeChartInput.slideUp();
      $('#type_chart').val(0);
      // $('#day_off').parent().parent().parent().slideUp();
    }
    else {
      typeChartInput.slideDown();
      // $('#day_off').parent().parent().parent().slideDown();
    };
    showHideTargetBase();

    // check when create edit kr
      if (isSelectManually()) {
        $('select[name$="confidenceLevel"]').parent().parent().slideUp();
        $('#projectPHID').parent().parent().parent().slideUp();
        $('.confident-scope').parent().parent().slideUp();
      } else if (isNotNeededCheckin()) {
        $("input[name*='meansure']").parent().parent().slideUp();
        $('select[name$="confidenceLevel"]').parent().parent().slideUp();
        $('.confident-scope').parent().parent().slideDown();
        targetInput.slideUp();
        baseValueInput.slideUp();
      }
      if ($('select[name$="manualCheckin"]').children("option:selected").val() == 1) {
        $('.confident-scope').parent().parent().slideUp();
        targetInput.slideUp();
        baseValueInput.slideUp();
        $("input[name*='meansure']").parent().parent().slideUp();
        $('select[name$="confidenceLevel"]').parent().parent().slideUp();

      }

      //change text confident scope
      if($('select[name$="manualCheckin"]').children("option:selected").val() == 2
      || $('select[name$="manualCheckin"]').children("option:selected").val() == 3
      ) {
        $('#yellow-1').css('color','red').text('Red');
        $('#yellow-2').css('color','green').text('Green');
      } else {
        $('#yellow-1').css('color','green').text('Green');
        $('#yellow-2').css('color','red').text('Red');
      }

    //parse scope
    $('.scope1').change(function () {
      $('.scope1').val(parseFloat($('.scope1').val()))
    });
    $('.scope2').change(function () {
      $('.scope2').val(parseFloat($('.scope2').val()))
    });

    // Change Meansure by Handle
    $('select[name$="manualCheckin"]').change(function () {
      $('.scope1').val('');
      $('.scope2').val('');
      span = ($('#projectPHID').children().children()[0]);

      if ($('select[name$="manualCheckin"]').children("option:selected").val() == 2) {
        $('#day_off').parent().parent().parent().slideUp();
      } else $('#day_off').parent().parent().parent().slideDown();

      if (isNotNeededCheckin()) {
        $('.confident-scope').parent().parent().slideDown();
        $('#type_chart').val(0);
        typeChartInput.slideUp();
        $("input[name*='meansure']").parent().parent().slideUp();
        $('select[name$="confidenceLevel"]').parent().parent().slideUp();
        $('#projectPHID').parent().parent().parent().slideDown();
        // $('#day_off').parent().parent().parent().slideUp();
      }
      else {
        typeChartInput.slideDown();
        $('#type_chart').val('');
        $("input[name*='meansure']").val('');
        // $('#day_off').parent().parent().parent().slideDown();
      }
      showHideTargetBase();


      // total dev show hide when select HII HFI
      if ($('#manual-checkin').val() == 0) {
        $("#isAutoCheckin").parent().hide();
      } else {
        $("#isAutoCheckin").parent().show();
      }
      if ($('#manual-checkin').val() == 4 || $('#manual-checkin').val() == 5) {
        if($('#isAutoCheckin')[0].checked) {
          $("#txtDev").show();
        } else {
          $("#txtDev").hide();
        }
      }
      else {
        $("#txtDev").hide();
      }

      // clear base value when select another meansure by
      if (!isSelectManually()){
        $('#baseValue').val('');
        $('#target').val('');
      }
      else{
        isCreatePage() ? $("#baseValue").val('') : $("#baseValue").val(default_basevalue);
        isCreatePage() ? $("#target").val('') : $("#target").val(default_target);
      }

      // clear default_basevalue and default_target
      if(isSelectManually()){
        $('.confident-scope').parent().parent().slideUp();
        $("input[name*='meansure']").parent().parent().slideDown();
        $('#projectPHID').parent().parent().parent().slideUp();
        if(default_meansure_by != 0){
          default_basevalue = $('#baseValue').val('');
          default_target = $('#target').val('');
        }
      }

      if ($('select[name$="manualCheckin"]').children("option:selected").val() == 1) {
        $('.confident-scope').parent().parent() .slideUp();
        $('#projectPHID').parent().parent().parent().slideDown();
        $("input[name*='meansure']").parent().parent().slideUp();
        $('select[name$="confidenceLevel"]').parent().parent().slideUp();
      }

      //change text confident scope
      if($('select[name$="manualCheckin"]').children("option:selected").val() == 2
        || $('select[name$="manualCheckin"]').children("option:selected").val() == 3
      ) {
        $('#yellow-1').css('color','red').text('Red');
        $('#yellow-2').css('color','green').text('Green');
      } else {
        $('#yellow-1').css('color','green').text('Green');
        $('#yellow-2').css('color','red').text('Red');
      }

    });

    function isSelectManually() {
      var selected = $('select[name$="manualCheckin"]').children("option:selected").val();
      return selected == 0;
    }

    function isNotNeededCheckin() {
      var selected = $('select[name$="manualCheckin"]').children("option:selected").val();
      return selected == 2 || selected == 3 || selected == 4 ||  selected == 5;
    }

    function showHideTargetBase() {
      if (isSelectManually()) {
        targetInput.slideDown();
        baseValueInput.slideDown();
        $('#projectPHID').parent().parent().parent().slideUp();
        $('select[name$="confidenceLevel"]').parent().parent().slideUp();
      } else {
        targetInput.slideUp();
        baseValueInput.slideUp();
      };
    }

    // Remove history edit text
    let patt = new RegExp(" edited this Current.");
    let patt1 = new RegExp(" edited this Checkin.");


    $.each($('.phui-timeline-title'), (id, element) => {
      let $element = $(element);
      if(patt.test($element.text()) || patt1.test($element.text())){
        element.remove();
      }
    });

    $.each($('.phui-timeline-shell'), (id, element) => {
      let $element = $(element);
      if($element.text() == ''){
        element.remove();
      }
    });

    //Hide or show meansure by when not select or select project
    if (span.value !== undefined) {
      $("input[name*='meansure']").parent().parent().find('span').css('display', '');
    } else {
      $("input[name*='meansure']").parent().parent().find('span').css('display', 'none');
    }

    //Hide type chart, base value, target, meansure by if not have project
    // if (span.value == 'Type a project name...' || span.value == '') {
    //   typeChartInput.slideDown();
    //   targetInput.slideDown();
    //   baseValueInput.slideDown();
    //   $('#check_in').slideDown();
    // } else {
    //   $('#manual-checkin').parent().parent().slideDown();
    // }

    //HII HFI chart handle
    // $("#totalDev").parent().parent().slideUp();
    // if ($('#manual-checkin').val() == 4 || $('#manual-checkin').val() == 5) {
    //   $("#totalDev").parent().parent().slideDown();
    //   $("#totalDev").css('display', '');
    //   $("#totalDev").val() == 69.69 ? $("#totalDev").val('') : '';
    // }
    // else {
    //   $("#totalDev").css('display', 'none');
    //   $("#totalDev").val(69.69);
    // };

    // Change project handle
    $("div[id = projectPHID]").on('input change select DOMSubtreeModified', "div", function () {
      let url = '';
      span = ($('#projectPHID').children().children()[0]);

      if (span.value == undefined) {
        if ($('#manual-checkin').val() == 4 || $('#manual-checkin').val() == 5) {
          //$("#totalDev").parent().parent().slideDown();
        }
        if (isNotNeededCheckin()) {
          typeChartInput.slideUp();
          $("input[name*='meansure']").parent().parent().slideUp();
          $('select[name$="confidenceLevel"]').parent().parent().slideUp();
        } else if ($('select[name$="manualCheckin"]').children("option:selected").val() == 1) {
          $("input[name*='meansure']").parent().parent().slideUp();
          $('select[name$="confidenceLevel"]').parent().parent().slideUp();
        }
        else {
          typeChartInput.slideDown();
        }
        $("input[name*='meansure']").parent().parent().find('span').css('display', 'none');
        $('#manual-checkin').parent().parent().slideDown();
        showHideTargetBase();
      }

      // if (span.value == 'Type a project name...' || span.value == '') {
      //   $("input[name*='meansure']").parent().parent().slideDown();
      //   $('select[name$="confidenceLevel"]').parent().parent().slideDown();
      // }

    });
    // Href project tag

    endDateParts = $(".endDate").val().split("/");
    startDateParts = $(".startDate").val().split("/");
    yesterday = new Date();
    today = new Date();
    yesterday.setDate(today.getDate() - 1);

    if (yesterday > new Date(+endDateParts[2], endDateParts[1] - 1, +endDateParts[0]) || today < new Date(+startDateParts[2], startDateParts[1] - 1, +startDateParts[0])) {
      $('#check_in').slideUp();
    } else {
      $('#check_in').slideDown();
    }

    if (isCreatePage()) {
      $('#frequence_time').val('');
    }

    function isCreatePage() {
      if ($("span[class$='phui-header-header']").text() == 'Create KR' && $(".phui-info-severity-error").length == 0) {
        return true;
      }
      else {
        return false;
      }
    }

    // Hide and show frequence time
    var selectedEvery = $('select[name="frequence"]').find(':selected').val();
    var everyValue = $('input[name="every"]:checked').val();
    var monthlyValue = $('input[name="month-on"]:checked').val();
    addReadOnly(everyValue);
    addReadOnly(monthlyValue);
    showFrequence(selectedEvery);

    // add attribute readonly for frequence time
    $('input[name="every"]').on('change', function () {
      var everyValue1 = $(this).val();
      addReadOnly(everyValue1);

      if(($('select[name="frequence"]').find(':selected').val() ==='Day') && (everyValue1 === 'every-weekday')) {
        $("#dayOfWeeksValue").val('every-weekday');
      }else {
        $("#dayOfWeeksValue").val('');
      }
    });

    $('input[name="month-on"]').on('change', function () {
      var selectedValue = $(this).val();
      addReadOnly(selectedValue);
    });

    $('select[name="frequence"]').on('change', function () {
      var selectedEvery1 = $(this).children("option:selected").val();
      showFrequence(selectedEvery1);
    });
  });

  function showFrequence(frequenceTime) {
    if (frequenceTime == 'Day') {
      var everyVal = $('input[name="every"]:checked').val();
      $('.monthly').parent().parent().hide();
      $('#txtTime').text('day(s)');
      $('#week').hide();
      $('#labelEvery').show();
      $('#everyWeekDay').show();
      $('.txtEvery').css('padding-right','48px');
      addReadOnly(everyVal);
    }
    else if (frequenceTime == 'Week') {
      $('.monthly').parent().parent().hide();
      $('#txtTime').text('week(s) on');
      $('#week').show();
      $('#labelEvery').hide();
      $('#everyWeekDay').hide();
      $('.txtEvery').css('padding-right','5px');
      $('#frequence_time').removeAttr('readonly');
    }
    else if (frequenceTime == 'Month') {
      var monthlyVal = $('input[name="month-on"]:checked').val();
      $('.monthly').parent().parent().show();
      $('#txtTime').text('month(s)');
      $('#week').hide();
      $('#labelEvery').hide();
      $('#everyWeekDay').hide();
      $('.txtEvery').css('padding-right','5px');
      $('#frequence_time').removeAttr('readonly');
      addReadOnly(monthlyVal);
    }
  }

  function addReadOnly(everyValue) {
    switch (everyValue) {
      case 'every-weekday':
        $('#frequence_time').attr('readonly','true');
        $('#dayOfMonth').removeAttr('disabled');
        break;
      case 'every-day':
        $('#frequence_time').removeAttr('readonly');
        $('#dayOfMonth').removeAttr('disabled');
        break;
      case 'on_day':
        $('#dayOfMonth').removeAttr('disabled');
        $('#dayOfWeek').attr('disabled','true');
        $('#pre_dayOfWeek').attr('disabled','true');
        break;
      case 'on_the':
        $('#dayOfMonth').attr('disabled','true');
        $('#dayOfWeek').removeAttr('disabled');
        $('#pre_dayOfWeek').removeAttr('disabled');
        break;
    }
    // if (everyValue == 'every-weekday')
    //   $('#frequence_time').attr('readonly','true');
    // else $('#frequence_time').removeAttr('readonly');
  }
});
