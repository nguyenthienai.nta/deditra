/**
 * @provides javelin-behavior-okr
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('okr', function () {
  ids = JSON.parse($('input[name$="ids"]').val());
  colors = JSON.parse($('input[name$="colors"]').val());
  contents = JSON.parse($('input[name$="contents"]').val());
  chartData = JSON.parse($('input[name$="data"]').val());
  hidden_icon_editOKR = JSON.parse($('input[name$="hidden_icon_editOKR"]').val());
  label = [];
  notes = [];
  data = [];
  var url = '';
  //set href
  var href = window.location.href;
  if(href.indexOf('okrs') === -1){
    url = 'hephaestos';
  } else {
    url = 'okrs';
  };

  pointColors = [
    'rgba(255, 99, 132, 1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)',
    'rgba(255, 0, 0, 1)',
    'rgba(188, 74, 225, 1)',
    'rgba(102, 238, 41, 1)',
    'rgba(5, 238, 136, 1)',
    'rgba(182, 118, 161, 1)',
    'rgba(225, 216, 217, 1)'
  ],
  colorBackground = [
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)',
    'rgba(255, 0, 0, 0.2)',
    'rgba(188, 74, 225, 0.2)',
    'rgba(102, 238, 41, 0.2)',
    'rgba(5, 238, 136, 0.2)',
    'rgba(182, 118, 161, 0.2)',
    'rgba(225, 216, 217, 0.2)'
  ];
  $.each(chartData, (id, values) => {
    $.each(values, (id, value) => {
      label.push(Object.keys(value));
      notes.push(Object.keys(value));
      data.push(Object.values(value));
    });
  });

  function truncate(source, size) {
    return source.length > size ? source.slice(0, size - 1) + "…" : source;
  }

  $.each(label, (id, values) => {
    if (values.length !== 0) {
      $.each(values, (id, value) => {
        values[id] = '';
      });
    }
  });

  $.each(ids, (id, values) => {
    $.each(values, (id, value) => {
      $('.phui-pinboard-item-header_' + value).css({
        'background-color': '#2E89C5',
      });
      $('.phui-pinboard-item-header_' + value).children().css({
        'font-weight': '600',
        'text-align': 'left',
        'position': 'relative',
        'top': '22px',
        'word-break': 'break-all',
        'margin-left': '5px',
        'color': 'white',
        'width': '30ch',
        'display' : 'inline-block',
        'text-overflow' : 'ellipsis',
        'white-space' : 'nowrap',
        'overflow' : 'hidden'
      });
      $('.phui-pinboard-item-header_' + value).append(
        `<p class='tooltip${id}' style="
        position: absolute;
        border: 1px solid #333;
        background-color: black;
        color: white;
        border-radius: 3px;
        padding: 2px 6px;
        z-index:999" class="tooltiptext tooltip${id}">${contents['contents'][id]}</p>`
      );
      $('.tooltip' + id).hide();

      $('.phui-pinboard-item-image-link_' + value).hover(function () {
        $('.tooltip' + id).show();
      }, function () {
        $('.tooltip' + id).hide();
      });
      colors['colors'][id] == 'red' ? color = '#ff3300' : (colors['colors'][id] == 'green' ? color = '#53ff1a' : color = 'yellow');
      $('.phui-pinboard-item-header_' + value).append(
        `<div class="phui-object-icon-pane"><ul class="phui-oi-icons"><li class="phui-oi-icon"><span class="visual-only phui-icon-view phui-font-fa fa-circle ${colors['colors'][id]} phui-oi-icon-image" data-meta="0_7" aria-hidden="true"  style="color: ${color}; position: relative; left: -20px; top: -1px;"></span><span class="phui-oi-icon-label"></span></li></ul></div>`
      );
      $('.phui-pinboard-item-header_' + value).append(
        `<li class="phui-list-item-view phui-list-item-type-link phui-list-item-has-icon "><a href="/${url}/edit/${value}/" class="phui-list-item-href"><span id="icon-edit-obj" class="visual-only phui-icon-view phui-font-fa ${hidden_icon_editOKR['hidden_icon_editOKR'][id]} phui-list-item-icon white" data-meta="0_9" aria-hidden="true" style="position: relative; left: 263px; top: -23px;"></span></a></li>`
      );

      // Chart render
      var img = document.getElementById('chart' + value);
      var chart_div = document.createElement("div");
      chart_div.setAttribute('id', 'div_chart');

      var canvas = document.createElement('canvas');
      canvas.setAttribute('id', 'chart_render' + value);
      canvas.setAttribute('class', 'chart_render_obj');
      canvas.setAttribute('height', '262.99px');
      canvas.setAttribute('width', '280px');

      img.parentNode.insertBefore(chart_div, img.childNodes[0]);
      chart_div.appendChild(canvas);

      var ctx = document.getElementById('chart_render' + value).getContext('2d');
      $('#chart' + value).remove();
      if((data[id]).length == 0){
        canvas.setAttribute('height', '258.99px');
        canvas.setAttribute('width', '280px');
        canvas.setAttribute('class', 'chart_render_objk');
        ctx.font = "13px Arial";
        ctx.fillStyle = "black";
        ctx.textAlign = "center";
        ctx.fillText("No objective", canvas.width/2, canvas.height/2);
      }
      else{
        if(label[id].length == 1 && data[id].length == 1){
          label[id].push('');
          data[id].push(undefined);
        }
        if(label[id].length == 2 && data[id].length == 2){
          label[id].push('');
          data[id].push(undefined);
        }
        var myChart = new Chart(ctx, {
          type: 'radar',
          data: {
            labels: label[id],
            datasets: [{
              label: '# of Votes',
              data: data[id],
              notes: notes[id],
              backgroundColor: colorBackground,
              borderColor: pointColors,
              pointBackgroundColor: pointColors,
              borderWidth: 2,
              fill: notes[id].length < 3 ? false : true,
            }]
          },
          options: {
            legend: {
              display: false
            },
            tooltips: {
              callbacks: {
                label: function (tooltipItem, data) {
                  var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
                  //This will be the tooltip.body
                  return ': ' + data.datasets[tooltipItem.datasetIndex].notes[tooltipItem.index];
                }
              },
              enabled: false,

              custom: function(tooltipModel) {
                // Tooltip Element
                var tooltipEl = document.getElementById('chartjs-tooltip');

                // Create element on first render
                if (!tooltipEl) {
                  tooltipEl = document.createElement('div');
                  tooltipEl.id = 'chartjs-tooltip';
                  tooltipEl.innerHTML = `<table style = 'background-color: #4c4c4c; border-radius:5px' width="250"></table>`;
                  document.body.appendChild(tooltipEl);
                }

                // Hide if no tooltip
                if (tooltipModel.opacity === 0) {
                  tooltipEl.style.opacity = 0;
                  return;
                }

                // Set caret Position
                tooltipEl.classList.remove('above', 'below', 'no-transform');
                if (tooltipModel.yAlign) {
                  tooltipEl.classList.add(tooltipModel.yAlign);
                } else {
                  tooltipEl.classList.add('no-transform');
                }

                function getBody(bodyItem) {
                  return bodyItem.lines;
                }

                // Set Text
                if (tooltipModel.body) {
                  var titleLines = tooltipModel.title || [];
                  var bodyLines = tooltipModel.body.map(getBody);
                  var innerHtml = '<tbody>';

                  bodyLines.forEach(function(body, i) {
                    var colors = tooltipModel.labelColors[i];
                    var style = 'background:' + colors.borderColor;
                    style += '; border-color:' + colors.borderColor;
                    style += '; border-width: 2px';
                    var span = '<span class="dot-radar chartjs-tooltip-key" style="' + style + '"></span>';
                    innerHtml += '<tr style="width: 50px"><h5 style="font-weight:50 ; margin: 5px; word-break: break-all">' + span + "&nbsp;" + body + '</h5></tr>';
                  });
                  innerHtml += '</tbody>';

                  var tableRoot = tooltipEl.querySelector('table');
                  tableRoot.innerHTML = innerHtml;
                }
                // `this` will be the overall tooltip
                var position = this._chart.canvas.getBoundingClientRect();
                // Display, position, and set styles for font
                tooltipEl.style.opacity = 0.7;
                tooltipEl.style.position = 'absolute';
                tooltipEl.style.color = 'white';
                tooltipEl.style.borderRadius = '10px';
                tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 6 + 'px';
                tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY - 30 + 'px';
                tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
                tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
                tooltipEl.style.fontWeight = '900';
                tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
                tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                tooltipEl.style.pointerEvents = 'none';
              }
            },
            scale: {

              ticks: {
                beginAtZero: true,
                max: 10,
                min: 0,
                stepSize: 1
              }
            }
          }
        });
      }

      var html = `<div style="margin-left: 5px ;width: 97%; height: 62px; overflow: auto;" class="">`;
      for (let i = 0; i < notes[id].length; i++) {
        html += `<h5 style="font-size: 90%; color: black;font-weight: 50; padding-bottom: 5px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; width: 97%">
                    <span style="background-color: ${pointColors[i]}" class="dot-radar"></span>
                    &nbsp;${notes[id][i]}
                </h5>`;
      }
      html += `</div>`;
      $('#chart_render' + value).after(html);
    });
  });


});

