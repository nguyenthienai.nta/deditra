/**
 * @provides javelin-behavior-move-okr
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('move-okr', function () {
  $('.button-grey').removeAttr("data-sigil");
  $('.button-grey').removeAttr("href");

  $('.button-grey').click(function () {
    window.location.reload();
  });
});
