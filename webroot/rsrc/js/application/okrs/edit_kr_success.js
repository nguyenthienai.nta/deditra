/**
 * @provides javelin-behavior-edit_kr_success
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('edit_kr_success', function () {
    $(document).ready(function () {
        localStorage.removeItem("dayOffs");
    });
});
  