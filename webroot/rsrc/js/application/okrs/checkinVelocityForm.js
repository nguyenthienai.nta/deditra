/**
 * @provides javelin-behavior-checkinVelocityForm
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('checkinVelocityForm', function () {
    $(document).ready(function () {
        function checkPosition(){
            if (($("#comment").parent().parent().css('display') == 'none')){
                $('#preview').css('top', '-40px');
                $('#next').css('top', '-40px');
            }
            else{
                $('#preview').css('top', '-150px');
                $('#next').css('top', '-150px');
            }
        }

        checkPosition();
        $('#preview').click(function () {
            checkPosition();
        });
        $('#next').click(function () {
            checkPosition();
        })
    });
  });
  