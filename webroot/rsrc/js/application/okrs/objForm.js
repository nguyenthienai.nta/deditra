/**
 * @provides javelin-behavior-objective-form
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('objective-form', function () {

  $(document).ready(function () {

    let check = 0;

    for (let i = 11; i <= 19; i++) {
      if (parseFloat(document.getElementsByTagName("input")[i].value) < parseFloat(document.getElementsByTagName("input")[i - 1].value)) {
        document.getElementsByTagName("input")[i].style.border = "2px solid red";
      }
    }

    $(".point").change(function () {
      for (let i = 11; i <= 19; i++) {
        if (parseFloat(document.getElementsByTagName("input")[i].value) < parseFloat(document.getElementsByTagName("input")[i - 1].value)) {
          document.getElementsByTagName("input")[i].style.border = "2px solid red";
        } else {
          document.getElementsByTagName("input")[i].style.border = "";
        }
      }
    });
    $(".txtSuccessRule").parent().append(`<p style="color:gray; padding-top: 5px;">(Set&nbsppoint&nbspfor&nbspObjective&nbspbased&nbspon&nbspconfidence&nbspKR&nbsppercent)</p>`);

    for (let i = 10; i <= 19; i++) {
      if (isNaN(parseFloat(document.getElementsByTagName("input")[i].value)) == false) {
        check++;
      }
    }
    if (check == 0) {
      for (let i = 10; i <= 19; i++) {
        if ($("span[class$='phui-header-header']").text() == 'Create Objective') {
          document.getElementsByTagName("input")[i].value = (i - 9) * 10;
        }
      }
    }
    for (let i = 10; i >= 1; i--) {
      if (i === 1) {
        $("#point" + i).parent().parent().parent().prepend(`<span id="text${i}" class ="text1"> ${i + " "} Point </span>`);
      } else {
        $("#point" + i).parent().parent().parent().prepend(`<span id="text${i}" class ="text"> ${i + " "} Points </span>`);
      }
    }
  });
});
