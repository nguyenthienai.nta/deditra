/**
 * @provides javelin-behavior-aphront-basic-tokenizer-kr
 * @requires javelin-behavior
 *           phabricator-prefab-kr
 *           phabricator-prefab
 */

JX.behavior('aphront-basic-tokenizer-kr', function(config) {
  var build = JX.PrefabKR.buildTokenizer(config);
  if (build) {
    build.tokenizer.start();
  }
});
