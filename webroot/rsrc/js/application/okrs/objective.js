/**
 * @provides javelin-behavior-submit-objective
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('submit-objective', function () {
  JX.Stratcom.listen(
    'click',
    'submitObjetiveButton',    // Listen only to this sigil set
    function (e) {
      document.getElementsByName("check")[0].value = 'true';
    });
});
