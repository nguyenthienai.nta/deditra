/**
 * @provides javelin-behavior-chart-render
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('chart-render', function () {
    var edit_form = document.getElementById('form_edit');
    var chart_div = document.createElement("div");
    chart_div.setAttribute('id', 'div_chart');
    var url = '';
    //set href
    var href = window.location.href;
    if(href.indexOf('hephaestos') === -1){
      url = '/kr';
    } else {
      url = '/hephaestos/kr';
    };

    data = JSON.parse($('input[name$="data"]').val());
    velocityChartData = JSON.parse($('input[name$="velocityChartData"]').val());
    chartData_HII = JSON.parse($('input[name$="chartData_HII"]').val());
    chartData_HFI = JSON.parse($('input[name$="chartData_HFI"]').val());
    confidentScope = JSON.parse($('input[name$="confidentScope"]').val());
    lastFeedback =  JSON.parse($('input[name$="lastFeedback"]').val()) - 1;
    effectiveOfDevelopChartData = JSON.parse($('input[name$="effectiveOfDevelopChartData"]').val());

  if (data.id != null && data.target != null) {
      var canvas = document.createElement('canvas');
      canvas.setAttribute('id', 'chart');
      edit_form.insertBefore(chart_div, edit_form.childNodes[0]);
      chart_div.appendChild(canvas);

      canvas.parentNode.style.top = '50%';
      canvas.parentNode.style.left = '50%';
      canvas.parentNode.style.marginRight = '15%';
      canvas.parentNode.style.marginLeft = '15%';

      if ($('.phui-header-header').text() == 'Edit KR') {
        var target = document.getElementsByClassName('aphront-form-input');
        var button = document.createElement('a');
        button.setAttribute('id', 'check_in');
        button.setAttribute('role', 'button');
        button.setAttribute('class',
          'button button-blue has-text icon-last phui-button-default');
        button.setAttribute('href', url + '/checkin/' + data.id + '/?current-id=0');
        button.setAttribute('data-sigil', 'workflow');
        button.innerHTML = "Check-in";
        button.style.marginTop = '10px';
        document.getElementById("btn-checkin").appendChild(button);
        // target[5].appendChild(button);
      }

      //dataTarget
      let targetValue = $('input[name$="targetValue"]').val();

      let step = JSON.parse($('input[name$="labelChart"]').val()).length - 1;
      let value = targetValue / step;
      let arrayTarget = [];

      if (step >= 2) {
        for (let i = 0; i <= step; i++) {
          if (i === 0) {
            arrayTarget[i] = 0;
          } else if (i > 0) {
            arrayTarget[i] = arrayTarget[i - 1] + value;
          } else if (i === (step)) {
            arrayTarget[i] = targetValue;
          }
        }
      } else {
        arrayTarget = [0, targetValue];
      }

      /* src image */

      const happy = new Image();
      happy.src = '/res/phabricator/0d17c6c4/rsrc/image/happy_.png';
      happy.width = 25;
      happy.height = 25;

      const good = new Image();
      good.src = '/res/phabricator/0d17c6c4/rsrc/image/happy.png';
      good.width = 25;
      good.height = 25;

      const normal = new Image();
      normal.src = '/res/phabricator/0d17c6c4/rsrc/image/normal.png';
      normal.width = 25;
      normal.height = 25;

      const unhappy = new Image();
      unhappy.src = '/res/phabricator/0d17c6c4/rsrc/image/unhappy.png';
      unhappy.width = 25;
      unhappy.height = 25;

      const annoying = new Image();
      annoying.src = '/res/phabricator/0d17c6c4/rsrc/image/annoying.png';
      annoying.width = 25;
      annoying.height = 25;

      dates = JSON.parse($('input[name$="labelChart"]').val());
      dataChart = JSON.parse($('input[name$="dataChart"]').val());
      feedback = JSON.parse($('input[name$="emotions"]').val());
      target_chart = JSON.parse($('input[name$="Target"]').val());
      emotions = [annoying, unhappy, normal, good, happy];
      oldMeasure = JSON.parse($('input[name$="oldMeasure"]').val());
    /* Chart render */
      if (meansureByDataChart() == 1) {
        if(oldMeasure == 0 || $('#manual-checkin-detail').text() == "Manually") {
          /* Chart data */
          var arrayChart = [{
            label: JSON.parse($('input[name$="nameCurrent"]').val()),
            data: JSON.parse($('input[name$="dataChart"]').val()),
            fill: false,
            borderColor: 'royalblue',
            pointBackgroundColor: JSON.parse($('input[name$="colorChart"]').val()),
            pointBorderColor: JSON.parse($('input[name$="colorChart"]').val()),
            borderWidth: 1,
            pointRadius: 5,
            spanGaps: true,
          },
            {
              label: 'Target',
              data: target_chart,
              fill: false,
              borderColor: 'green',
              borderWidth: 1,
              pointRadius: 0,
              spanGaps: true,
              fontSize: 10,
            },
            {
              label: 'Scope',
              data: JSON.parse($('input[name$="scopeLine"]').val()),
              fill: false,
              borderColor: 'black',
              borderWidth: 1,
              pointRadius: 0,
              spanGaps: true,
              fontSize: 10,
            }];
          var estimation = JSON.parse($('input[name$="estimation"]').val());
          if (estimation.length > 0) {
            arrayChart.push({
              label: 'Estimation Remaining Effort',
              data: estimation,
              fill: false,
              borderColor: 'orange',
              borderWidth: 0,
              pointRadius: 5,
              spanGaps: true,
              fontSize: 10,
              pointBackgroundColor: 'orange',
            })
          };

          var taskID = JSON.parse($('input[name$="taskID"]').val());

          for (i = 0; i < taskID.length; i++) {
            arrayChart.push({
              label: "T" + taskID[i],
              data: JSON.parse($('input[name$="task' + taskID[i] + '"]').val()),
              backgroundColor: 'transparent',
              borderColor: 'blue',
              borderWidth: 0,
              pointRadius: 5,
              spanGaps: true,
              pointBackgroundColor: 'blue',
              pointStyle: 'rect'
            })
          };

          /* end Chart data */

          /* Chart render */
          var ctx = document.getElementById('chart').getContext('2d');
          var myChart = new Chart(ctx, {
            type: 'line',
            data: {
              labels: JSON.parse($('input[name$="labelDateChart"]').val()),
              datasets: arrayChart
            },
            options: {
              responsive: true,
              elements: {
                line: {
                  tension: 0
                }
              },
              layout: {
                padding: 10,
              },
              legend: {
                display: false,
              },
              title: {
                display: true,
                text: 'Check-in Chart'
              },
              plugins: {
                // Change options for ALL labels of THIS CHART
                datalabels: {
                  display: false
                }
              },
              scales: {
                yAxes: [{
                  gridLines: {
                    drawOnChartArea: true
                  },
                  scaleLabel: {
                    display: true,
                    labelString: $('#type_chart').val() == 0 ? 'Story Point' : 'Remaining effort'
                  },
                  ticks: {
                    beginAtZero: true,
                  },
                }],
                xAxes: [{
                  gridLines: {
                    drawOnChartArea: true
                  },
                  scaleLabel: {
                    display: true,
                    labelString: 'Day of the Year'
                  }
                }]
              },
              tooltips: {
                enabled: false,

                custom: function (tooltipModel) {
                  var tooltipEl = document.getElementById('chartjs-tooltip');

                  if (!tooltipEl) {
                    tooltipEl = document.createElement('div');
                    tooltipEl.id = 'chartjs-tooltip';
                    tooltipEl.innerHTML = '<table></table>';
                    document.body.appendChild(tooltipEl);
                  }

                  if (tooltipModel.opacity === 0) {
                    tooltipEl.style.opacity = 0;
                    return;
                  }

                  tooltipEl.classList.remove('above', 'below', 'no-transform');
                  if (tooltipModel.yAlign) {
                    tooltipEl.classList.add(tooltipModel.yAlign);
                  } else {
                    tooltipEl.classList.add('no-transform');
                  }

                  function getBody(bodyItem) {
                    return bodyItem.lines;
                  }

                  // Set Content and Emotion
                  if (tooltipModel.body) {
                    var titleLines = tooltipModel.title || [];
                    var bodyLines = tooltipModel.body.map(getBody);

                    var innerHtml = '<thead>';

                    titleLines.forEach(function (title) {
                      innerHtml += `<tr><th style='color: #33A2EC'>${title}</th></tr>`;
                    });
                    innerHtml += '</thead><tbody>';

                    bodyLines.forEach(function (body, i) {
                      var colors = tooltipModel.labelColors[i];
                      var style = 'background:' + colors.backgroundColor;
                      style += '; border-color:' + colors.borderColor;
                      style += '; border-width: 2px';

                      var regexp = /^[T][0-9]{1,9}$/;
                      isTask = regexp.test(body[0].slice(0, -3));
                      if (isTask) {
                        var span = '<span style="' + style + '"></span>';
                        innerHtml += `<tr><td  style='color: white'> ${span} ${body[0].slice(0, -3)} </td></tr>`;
                      }
                      else {
                        var span = '<span style="' + style + '"></span>';
                        innerHtml += `<tr><td  style='color: white'> ${span} ${body} </td></tr>`;
                      }
                      titleLines.forEach(function (title) {
                        var patt = new RegExp("Check-in Value");

                        var isCheckInLine = patt.test(body[0]);

                        if (isCheckInLine) {
                          labels = JSON.parse($('input[name$="labelDateChart"]').val());
                          $.each(labels, (id, values) => {
                            if (values == title && feedback[id] > 0) {
                              innerHtml += '<tr><th>' + '<img src="' + emotions[feedback[id] - 1].src + '" height="20" width="20" style="  display: block; margin-left: auto; margin-right: auto;"/>' + '</th></tr>';
                            }
                          });
                        }
                      });
                    });

                    innerHtml += '</tbody>';

                    var tableRoot = tooltipEl.querySelector('table');
                    tableRoot.innerHTML = innerHtml;
                  }
                  var position = this._chart.canvas.getBoundingClientRect();

                  // Tooltip style
                  tooltipEl.style.opacity = 1;
                  tooltipEl.style.position = 'absolute';
                  tooltipEl.style.color = 'white';
                  tooltipEl.style.background = '#4c4c4c';
                  tooltipEl.style.borderRadius = '10px';
                  tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 6 + 'px';
                  tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY - 30 + 'px';
                  tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
                  tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
                  tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
                  tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                  tooltipEl.style.pointerEvents = 'none';
                }
              }
            }
          });

        } else {
          /* Chart data */
          var estimation = JSON.parse($('input[name$="estimation"]').val());
          var resolvedPoint = JSON.parse($('input[name$="resolvedPoint"]').val());
          var pointToResolve = JSON.parse($('input[name$="pointToResolve"]').val());
          var scopeLine = JSON.parse($('input[name$="scopeLine"]').val());
          var dataChart = JSON.parse($('input[name$="dataChart"]').val());
          var colorChart = JSON.parse($('input[name$="colorChart"]').val());
          if (pointToResolve[pointToResolve.length - 1] != null || resolvedPoint[resolvedPoint.length - 1] != null) {
            target_chart.push(null);
            estimation.push(null);
            resolvedPoint.push('');
            pointToResolve.push('');
            scopeLine.push(null);
          }
          if (resolvedPoint[0] != null || pointToResolve[0] != null) {
            target_chart.unshift(null);
            estimation.unshift(null);
            resolvedPoint.unshift('');
            pointToResolve.unshift('');
            scopeLine.unshift(null);
            dataChart.unshift(null);
            colorChart.unshift(null);
          }

            var arrayChart = [{
            label: JSON.parse($('input[name$="nameCurrent"]').val()),
            data: dataChart,
            fill: false,
            borderColor: 'royalblue',
            pointBackgroundColor: colorChart,
            pointBorderColor: colorChart,
            borderWidth: 1,
            pointRadius: 5,
            spanGaps: true,
          },
            {
              label: 'Target',
              data: target_chart,
              fill: false,
              borderColor: 'green',
              borderWidth: 1,
              pointRadius: 0,
              spanGaps: true,
              fontSize: 10,
            },
            {
              label: 'Scope',
              data: scopeLine,
              fill: false,
              borderColor: 'black',
              borderWidth: 1,
              pointRadius: 0,
              spanGaps: true,
              fontSize: 10,
            }];
          if (estimation.length > 0) {
            arrayChart.push({
              label: 'Estimation Remaining Effort',
              data: estimation,
              fill: false,
              borderColor: 'orange',
              borderWidth: 0,
              pointRadius: 5,
              spanGaps: true,
              fontSize: 10,
              pointBackgroundColor: 'orange',
            });
          }
          arrayChart.push(
            {
              label: 'Resolved point',
              data: resolvedPoint,
              borderColor: 'pink',
              backgroundColor: 'rgba(255, 0, 0, 1)',
              // Changes this dataset to become a line
              type: 'bar'
            },
            {
              label: 'Point to resolve',
              data: pointToResolve,
              borderColor: 'pink',
              backgroundColor: 'rgba(240, 255, 0, 1)',
              // Changes this dataset to become a line
              type: 'bar'
            }
          );

          var taskID = JSON.parse($('input[name$="taskID"]').val());

          for (i = 0; i < taskID.length; i++) {
            var task = JSON.parse($('input[name$="task' + taskID[i] + '"]').val());
            if (pointToResolve[pointToResolve.length - 1] != null || resolvedPoint[resolvedPoint.length - 1] != null) {
              task.push(null);
            }
            if (resolvedPoint[0] != null || pointToResolve[0] != null) {
              task.unshift(null);
            }

              arrayChart.push({
              label: "T" + taskID[i],
              data: task,
              backgroundColor: 'transparent',
              borderColor: 'blue',
              borderWidth: 0,
              pointRadius: 5,
              spanGaps: true,
              pointBackgroundColor: 'blue',
              pointStyle: 'rect'
            })
          }

          /* end Chart data */

          /* Chart render */
          var label = JSON.parse($('input[name$="labelDateChart"]').val());
          if (pointToResolve[pointToResolve.length - 1] != null || resolvedPoint[resolvedPoint.length - 1] != null) {
            label.push('');
          }
          if (resolvedPoint[0] != null || pointToResolve[0] != null) {
            label.unshift('');
          }


            var ctx = document.getElementById('chart').getContext('2d');
          var myChart = new Chart(ctx, {
            type: 'line',
            data: {
              labels: label,
              datasets: arrayChart
            },
            options: {
              responsive: true,
              elements: {
                line: {
                  tension: 0
                }
              },
              layout: {
                padding: 10
              },
              legend: {
                display: false,
              },
              title: {
                display: true,
                text: 'Check-in Chart'
              },
              plugins: {
                // Change options for ALL labels of THIS CHART
                datalabels: {
                  display: false
                }
              },
              scales: {
                yAxes: [{
                  gridLines: {
                    drawOnChartArea: true
                  },
                  scaleLabel: {
                    display: true,
                    labelString: $('#type_chart').val() == 0 ? 'Story Point' : 'Remaining effort'
                  },
                  ticks: {
                    beginAtZero: true,
                  },
                }],
                xAxes: [{
                  categoryPercentage: 0.7,
                  gridLines: {
                    drawOnChartArea: true
                  },
                  scaleLabel: {
                    display: true,
                    labelString: 'Day of the Year'
                  }
                }]
              },
              tooltips: {
                enabled: false,

                custom: function (tooltipModel) {
                  var tooltipEl = document.getElementById('chartjs-tooltip');

                  if (!tooltipEl) {
                    tooltipEl = document.createElement('div');
                    tooltipEl.id = 'chartjs-tooltip';
                    tooltipEl.innerHTML = '<table></table>';
                    document.body.appendChild(tooltipEl);
                  }

                  if (tooltipModel.opacity === 0) {
                    tooltipEl.style.opacity = 0;
                    return;
                  }

                  tooltipEl.classList.remove('above', 'below', 'no-transform');
                  if (tooltipModel.yAlign) {
                    tooltipEl.classList.add(tooltipModel.yAlign);
                  } else {
                    tooltipEl.classList.add('no-transform');
                  }

                  function getBody(bodyItem) {
                    return bodyItem.lines;
                  }

                  // Set Content and Emotion
                  if (tooltipModel.body) {
                    var titleLines = tooltipModel.title || [];
                    var bodyLines = tooltipModel.body.map(getBody);

                    var innerHtml = '<thead>';

                    titleLines.forEach(function (title) {
                      innerHtml += `<tr><th style='color: #33A2EC'>${title}</th></tr>`;
                    });
                    innerHtml += '</thead><tbody>';

                    bodyLines.forEach(function (body, i) {
                      var colors = tooltipModel.labelColors[i];
                      var style = 'background:' + colors.backgroundColor;
                      style += '; border-color:' + colors.borderColor;
                      style += '; border-width: 2px';

                      var regexp = /^[T][0-9]{1,9}$/;
                      isTask = regexp.test(body[0].slice(0, -3));
                      if (isTask) {
                        var span = '<span style="' + style + '"></span>';
                        innerHtml += `<tr><td  style='color: white'> ${span} ${body[0].slice(0, -3)} </td></tr>`;
                      }
                      else {
                        var span = '<span style="' + style + '"></span>';
                        innerHtml += `<tr><td  style='color: white'> ${span} ${body} </td></tr>`;
                      }
                      titleLines.forEach(function (title) {
                        var patt = new RegExp("Check-in Value");

                        var isCheckInLine = patt.test(body[0]);

                        if (isCheckInLine) {
                          labels = JSON.parse($('input[name$="labelDateChart"]').val());
                          $.each(labels, (id, values) => {
                            if (values == title && feedback[id] > 0) {
                              innerHtml += '<tr><th>' + '<img src="' + emotions[feedback[id] - 1].src + '" height="20" width="20" style="  display: block; margin-left: auto; margin-right: auto;"/>' + '</th></tr>';
                            }
                          });
                        }
                      });
                    });

                    innerHtml += '</tbody>';

                    var tableRoot = tooltipEl.querySelector('table');
                    tableRoot.innerHTML = innerHtml;
                  }
                  var position = this._chart.canvas.getBoundingClientRect();

                  // Tooltip style
                  tooltipEl.style.opacity = 1;
                  tooltipEl.style.position = 'absolute';
                  tooltipEl.style.color = 'white';
                  tooltipEl.style.background = '#4c4c4c';
                  tooltipEl.style.borderRadius = '10px';
                  tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 6 + 'px';
                  tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY - 30 + 'px';
                  tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
                  tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
                  tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
                  tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                  tooltipEl.style.pointerEvents = 'none';
                }
              }
            }
          });
        }
      }
      else if (meansureByDataChart() == 2 || oldMeasure == 2) {
        var ctx = document.getElementById('chart').getContext('2d');
        // labelData = velocityChartData['label'];
        // lastLabel = labelData[0] ? labelData[0] : 5;
        colorVelocity = [];
        if (confidentScope[0] !== '' && velocityChartData.length !== 0) {
          for (let i = 0; i < velocityChartData['value'].length; i++) {
            if (parseInt(velocityChartData['value'][i]) < parseInt(confidentScope[0])) {
              colorVelocity[i] = 'red';
            } else if (parseInt(velocityChartData['value'][i]) > parseInt(confidentScope[1])) {
              colorVelocity[i] = 'green';
            } else {
              colorVelocity[i] = 'yellow';
            }
          }
          colorVelocity.reverse();
        } else {
          colorVelocity = 'royalblue';
        }

        var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: velocityChartData['label'].reverse(),
            datasets: [{
              label: '# Point',
              data: velocityChartData['value'].reverse(),
              backgroundColor: colorVelocity,
              borderColor: colorVelocity,
              borderWidth: 1,
              barPercentage: 0.25,
              maxBarThickness: 30,
              minBarLength: 2,
            }],
          },

          options: {
            responsive: true,
            layout: {
              padding: 10,
            },
            legend: {
              display: false,
            },
            title: {
              display: true,
              text: 'Velocity Chart'
            },
            plugins: {
              // Change options for ALL labels of THIS CHART
              datalabels: {
                display: false
              }
            },
            scales: {
              yAxes: [{
                gridLines: {
                  drawOnChartArea: true
                },
                scaleLabel: {
                  display: true,
                  labelString: 'Point'
                },
                ticks: {
                  beginAtZero: true,
                  mirror: false,
                },
              }],
              xAxes: [{
                barPercentage: velocityChartData['label'].length == 1 ? 0.1 : (velocityChartData['label'].length == 2 ? 0.2 : 0.4),
                gridLines: {
                  drawOnChartArea: true
                },
                ticks: {
                  beginAtZero: true,
                  mirror: false,
                },
              }]
            },
            // tooltips: {
            //   enabled: false,

            //   custom: function (tooltipModel) {
            //     var tooltipEl = document.getElementById('chartjs-tooltip');

            //     if (!tooltipEl) {
            //       tooltipEl = document.createElement('div');
            //       tooltipEl.id = 'chartjs-tooltip';
            //       tooltipEl.innerHTML = '<table></table>';
            //       document.body.appendChild(tooltipEl);
            //     }

            //     if (tooltipModel.opacity === 0) {
            //       tooltipEl.style.opacity = 0;
            //       return;
            //     }

            //     tooltipEl.classList.remove('above', 'below', 'no-transform');
            //     if (tooltipModel.yAlign) {
            //       tooltipEl.classList.add(tooltipModel.yAlign);
            //     } else {
            //       tooltipEl.classList.add('no-transform');
            //     }

            //     function getBody(bodyItem) {
            //       return bodyItem.lines;
            //     }

            //     // Set Content and Emotion
            //     if (tooltipModel.body) {
            //       var titleLines = tooltipModel.title || [];
            //       var bodyLines = tooltipModel.body.map(getBody);

            //       var innerHtml = '<thead>';

            //       titleLines.forEach(function (title) {
            //         innerHtml += `<tr><th style='color: #33A2EC'>${title}</th></tr>`;
            //       });
            //       innerHtml += '</thead><tbody>';

            //       bodyLines.forEach(function (body, i) {
            //         var colors = tooltipModel.labelColors[i];
            //         var style = 'background:' + colors.backgroundColor;
            //         style += '; border-color:' + colors.borderColor;
            //         style += '; border-width: 2px';

            //         var span = '<span style="' + style + '"></span>';
            //         innerHtml += `<tr><td  style='color: white'> ${span} ${body} </td></tr>`;
            //         titleLines.forEach(function (title) {
            //           if (titleLines == lastLabel && lastFeedback !== -1) {
            //             innerHtml += '<tr><th>' + '<img src="' + emotions[lastFeedback].src + '" height="20" width="20" style="  display: block; margin-left: auto; margin-right: auto;"/>' + '</th></tr>';
            //           }
            //         });
            //       });

            //       innerHtml += '</tbody>';

            //       var tableRoot = tooltipEl.querySelector('table');
            //       tableRoot.innerHTML = innerHtml;
            //     }
            //     var position = this._chart.canvas.getBoundingClientRect();

            //     // Tooltip style
            //     tooltipEl.style.opacity = 1;
            //     tooltipEl.style.position = 'absolute';
            //     tooltipEl.style.color = 'white';
            //     tooltipEl.style.background = '#4c4c4c';
            //     tooltipEl.style.borderRadius = '10px';
            //     tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 6 + 'px';
            //     tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY - 30 + 'px';
            //     tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
            //     tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
            //     tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
            //     tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
            //     tooltipEl.style.pointerEvents = 'none';
            //   }
            // }
          }
        });
      }
      else if (meansureByDataChart() == 3 || oldMeasure == 3) {
        var ctx = document.getElementById('chart').getContext('2d');
        colorEff = [];
        if (confidentScope[0] !== '' && effectiveOfDevelopChartData.length !== 0) {
          for (let i = 0; i < effectiveOfDevelopChartData['value'].length; i++) {
            if (effectiveOfDevelopChartData['value'][i] < parseFloat(confidentScope[0])) {
              colorEff[i] = 'red';
            } else if (effectiveOfDevelopChartData['value'][i] > parseFloat(confidentScope[1])) {
              colorEff[i] = 'green';
            } else {
              colorEff[i] = 'yellow';
            }
          }
        } else {
          colorEff = 'green';
        }
        var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: effectiveOfDevelopChartData['label'],
            datasets: [{
              label: 'Time to develop ',
              yAxisID: "y-axis-2",
              backgroundColor: 'rgba(30, 130, 76, 1)',
              borderColor: 'rgba(30, 130, 76, 1)',
              borderWidth: 1.2,
              pointRadius: 0,
              data: effectiveOfDevelopChartData['totalTimeDEV'],
              type: 'line',
              fill: false,
              datalabels: {
                display: false,
              },
            },
            {
              label: 'Total time',
              yAxisID: "y-axis-2",
              backgroundColor: 'purple',
              borderColor: 'purple',
              borderWidth: 1.2,
              pointRadius: 0,
              data:effectiveOfDevelopChartData['totalTime'],
              type: 'line',
              fill: false,
              datalabels: {
                display: false,
              },
            },
            {
              label: 'Time to develop',
              backgroundColor: colorEff,
              yAxisID: "y-axis-1",
              data: effectiveOfDevelopChartData['totalTimeDEV'],
              datalabels: {
                color: "black",
                formatter: (value, ctx) => {
                  const otherDatasetIndex = ctx.datasetIndex === 2 ? 3 : 2;
                  const total =
                    ctx.chart.data.datasets[otherDatasetIndex].data[ctx.dataIndex] + value;
                  if(total != 0) {
                    return `${Math.round((value / total * 100) * 10) / 10}%`;
                  } else {
                    return null;
                  }
                },
                font: {
                  size: 10,
                },
                align: 'start',
                offset: -11,
              },
              barPercentage: 0.25,
              maxBarThickness: 30,
              minBarLength: 2,
            },
            {
              label: 'Time to fix bug',
              backgroundColor: 'rgba(54, 162, 235, 1)',
              yAxisID: "y-axis-1",
              data: effectiveOfDevelopChartData['totalTimeBUG'],
              datalabels: {
                display: false,
              },
              barPercentage: 0.25,
              maxBarThickness: 30,
              minBarLength: 2,
            },


        ]

        },
        options: {
          responsive: true,
          elements: {
            line: {
              tension: 0
            }
          },
          legend: {
            display: true,
            position: 'bottom',
            onClick: function (e) {
              e.stopPropagation();
            },
            labels: {
              filter: function(legendItem, data) {
                return legendItem.text != 'Time to develop';
            }
          }
          },
          title:{
              display:true,
              text: 'Effective Of Develop Chart'
          },
          tooltips: {
              enabled: false,
              mode: 'index',
              intersect: true,
              filter: function (tooltipItem) {
                return tooltipItem.datasetIndex === 2 || tooltipItem.datasetIndex === 3 || tooltipItem.datasetIndex === 1;
              },
              custom: function (tooltipModel) {
                var tooltipEl = document.getElementById('chartjs-tooltip');

                if (!tooltipEl) {
                  tooltipEl = document.createElement('div');
                  tooltipEl.id = 'chartjs-tooltip';
                  tooltipEl.innerHTML = '<table></table>';
                  document.body.appendChild(tooltipEl);
                }

                if (tooltipModel.opacity === 0) {
                  tooltipEl.style.opacity = 0;
                  return;
                }

                tooltipEl.classList.remove('above', 'below', 'no-transform');
                if (tooltipModel.yAlign) {
                  tooltipEl.classList.add(tooltipModel.yAlign);
                } else {
                  tooltipEl.classList.add('no-transform');
                }

                function getBody(bodyItem) {
                  return bodyItem.lines;
                }

                // Set Content and Emotion
                if (tooltipModel.body) {
                  var titleLines = tooltipModel.title || [];
                  var bodyLines = tooltipModel.body.map(getBody);

                  var innerHtml = '<thead>';

                  titleLines.forEach(function (title) {
                    innerHtml += `<tr><th style='color: #33A2EC'>${title}</th></tr>`;
                  });
                  innerHtml += '</thead><tbody>';

                  bodyLines.forEach(function (body, i) {
                    var colors = tooltipModel.labelColors[i];
                    var style = 'background:' + colors.backgroundColor;
                    style += '; border-color:' + colors.borderColor;
                    style += '; border-width: 2px; display: inline-block';
                    style += '; height: 10px;  width: 10px; margin-top: 3px; margin-right: 2px';
                    var span = '<span style="' + style + '"></span>';
                    innerHtml += `<tr><td  style='color: white'> ${span} ${body} </td></tr>`;
                    titleLines.forEach(function (title) {

                      var patt = new RegExp("Time to fix bug");

                      var isCheckInLine = patt.test(body[0]);

                      if (isCheckInLine) {
                        labels = effectiveOfDevelopChartData['label'];
                        $.each(labels, (id, values) => {
                          if (values == title && effectiveOfDevelopChartData['emotion'][id] > 0) {
                            innerHtml += '<tr><th>' + '<img src="' + emotions[effectiveOfDevelopChartData['emotion'][id] - 1].src + '" height="20" width="20" style="  display: block; margin-left: auto; margin-right: auto;"/>' + '</th></tr>';
                          }
                        });
                      }
                    });
                  });

                  innerHtml += '</tbody>';

                  var tableRoot = tooltipEl.querySelector('table');
                  tableRoot.innerHTML = innerHtml;
                }
                var position = this._chart.canvas.getBoundingClientRect();

                // Tooltip style
                tooltipEl.style.opacity = 1;
                tooltipEl.style.position = 'absolute';
                tooltipEl.style.color = 'white';
                tooltipEl.style.background = '#4c4c4c';
                tooltipEl.style.borderRadius = '10px';
                tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 6 + 'px';
                tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY - 30 + 'px';
                tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
                tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
                tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
                tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                tooltipEl.style.pointerEvents = 'none';
              }
          },
          scales: {
            xAxes: [{
              stacked: true,
            }],
            yAxes: [{
              type: "linear",
              stacked: true,
              display: true,
              position: "left",
              id: "y-axis-1",
              ticks: {
                beginAtZero: true,
                suggestedMin: 0,
                suggestedMax: 10,
                min: 0
              },
              scaleLabel: {
                display: true,
                labelString: "Develop Productivity(hour)"
              },
              },
              {
                type: "linear",
                display: false,
                id: "y-axis-2",
                ticks: {
                beginAtZero: true,
                suggestedMin: 0,
                suggestedMax: 10,
                min: 0,
              }
            }],
          }
      }
        });
      }
      else if (oldMeasure == 4 || $('#manual-checkin-detail').text() == "DEBIT Bug Rate" || oldMeasure == 5 || $('#manual-checkin-detail').text() == "Customer Bug Rate"){
        /* Chart data */
        colorHII = [];
        if (confidentScope[0] !== '') {
          for (let i = 0; i < JSON.parse($('input[name$="dataChart"]').val()).length; i++) {
            if (JSON.parse($('input[name$="dataChart"]').val())[i] < confidentScope[0]) {
              colorHII[i] = 'green';
            } else if (JSON.parse($('input[name$="dataChart"]').val())[i] > confidentScope[1]) {
              colorHII[i] = 'red';
            } else {
              colorHII[i] = 'yellow';
            }
          }
        } else {
          colorHII = 'royalblue';
        }

        var arrayChart = [{
            label: (oldMeasure == 4 || $('#manual-checkin-detail').text() == "DEBIT Bug Rate") ? 'DEBIT Bug Rate' : 'Customer Bug Rate',
            data: JSON.parse($('input[name$="dataChart"]').val()),
            fill: false,
            borderColor: 'royalblue',
            pointBackgroundColor: colorHII,
            pointBorderColor: colorHII,
            borderWidth: 1,
            pointRadius: 5,
            spanGaps: true,
          }];
        /* end Chart data */

          /* Chart render */
          var ctx = document.getElementById('chart').getContext('2d');
          var myChart = new Chart(ctx, {
            type: 'line',
            data: {
              labels: JSON.parse($('input[name$="labelDateChart"]').val()),
              datasets: arrayChart
            },
            options: {
              responsive: true,
              elements: {
                line: {
                  tension: 0
                }
              },
              layout: {
                padding: 10,
              },
              legend: {
                display: true,
                position: 'bottom',
                onClick: function (e) {
                  e.stopPropagation();
                }
              },
              title: {
                display: true,
                text: (oldMeasure == 4 || $('#manual-checkin-detail').text() == "DEBIT Bug Rate") ? 'DEBIT Bug Rate Index Chart' : 'Customer Bug Rate Index Chart'
              },
              plugins: {
                // Change options for ALL labels of THIS CHART
                datalabels: {
                  display: false
                }
              },
              scales: {
                yAxes: [{
                  gridLines: {
                    drawOnChartArea: true
                  },
                  scaleLabel: {
                    display: true,
                    labelString: 'Bug Rate'
                  },
                  ticks: {
                    beginAtZero: true,
                  },
                }],
                xAxes: [{
                  gridLines: {
                    drawOnChartArea: true
                  },
                  scaleLabel: {
                    display: false,
                    labelString: 'Day of the Year'
                  }
                }]
              },
              tooltips: {
                enabled: false,

                custom: function (tooltipModel) {
                  var tooltipEl = document.getElementById('chartjs-tooltip');

                  if (!tooltipEl) {
                    tooltipEl = document.createElement('div');
                    tooltipEl.id = 'chartjs-tooltip';
                    tooltipEl.innerHTML = '<table></table>';
                    document.body.appendChild(tooltipEl);
                  }

                  if (tooltipModel.opacity === 0) {
                    tooltipEl.style.opacity = 0;
                    return;
                  }

                  tooltipEl.classList.remove('above', 'below', 'no-transform');
                  if (tooltipModel.yAlign) {
                    tooltipEl.classList.add(tooltipModel.yAlign);
                  } else {
                    tooltipEl.classList.add('no-transform');
                  }

                  function getBody(bodyItem) {
                    return bodyItem.lines;
                  }

                  // Set Content and Emotion
                  if (tooltipModel.body) {
                    var titleLines = tooltipModel.title || [];
                    var bodyLines = tooltipModel.body.map(getBody);

                    var innerHtml = '<thead>';

                    titleLines.forEach(function (title) {
                      innerHtml += `<tr><th style='color: #33A2EC'>${title}</th></tr>`;
                    });
                    innerHtml += '</thead><tbody>';

                    bodyLines.forEach(function (body, i) {
                      var colors = tooltipModel.labelColors[i];
                      var style = 'background:' + colors.backgroundColor;
                      style += '; border-color:' + colors.borderColor;
                      style += '; border-width: 2px';

                      var regexp = /^[T][0-9]{1,9}$/;
                      isTask = regexp.test(body[0].slice(0, -3));
                      if (isTask) {
                        var span = '<span style="' + style + '"></span>';
                        innerHtml += `<tr><td  style='color: white'> ${span} ${body[0].slice(0, -3)} </td></tr>`;
                      }
                      else {
                        var span = '<span style="' + style + '"></span>';
                        innerHtml += `<tr><td  style='color: white'> ${span} ${body} </td></tr>`;
                      }
                      titleLines.forEach(function (title) {
                        var patt = new RegExp("DEBIT Bug Rate");
                        var patt1 = new RegExp("Customer Bug Rate");

                        var isCheckInLine = patt.test(body[0]);
                        var isCheckInLine1 = patt1.test(body[0]);

                        if (isCheckInLine || isCheckInLine1) {
                          labels = JSON.parse($('input[name$="labelDateChart"]').val());
                          $.each(labels, (id, values) => {
                            if (values == title && feedback[id] > 0) {
                              innerHtml += '<tr><th>' + '<img src="' + emotions[feedback[id] - 1].src + '" height="20" width="20" style="  display: block; margin-left: auto; margin-right: auto;"/>' + '</th></tr>';
                            }
                          });
                        }
                      });
                    });

                    innerHtml += '</tbody>';

                    var tableRoot = tooltipEl.querySelector('table');
                    tableRoot.innerHTML = innerHtml;
                  }
                  var position = this._chart.canvas.getBoundingClientRect();

                  // Tooltip style
                  tooltipEl.style.opacity = 1;
                  tooltipEl.style.position = 'absolute';
                  tooltipEl.style.color = 'white';
                  tooltipEl.style.background = '#4c4c4c';
                  tooltipEl.style.borderRadius = '10px';
                  tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 6 + 'px';
                  tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY - 30 + 'px';
                  tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
                  tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
                  tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
                  tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                  tooltipEl.style.pointerEvents = 'none';
                }
              }
            }
          });
      }
    }

    if (typeof ctx !== "undefined") {
      myChart.options.scales.yAxes[0].ticks.fontSize = 10;
      myChart.options.scales.xAxes[0].ticks.fontSize = 10;
      myChart.update();
    }

    // Legend chart
    left = 27;

    if (estimation.length > 0) {
      left = 0;
      textLeft = 0;
      if (arrayChart[0].label == 'Remaining effort') {
        textLeft -= 26;
      }
      $('#chart').after(`<center style="height: 50px">
      <div id="legend" style="width: 500px; min-height: 50px; height: auto;" class="">
      <div style="background-color: royalblue; margin-right: 5px; width: 15px; height: 12px; border-color: white;
      position: relative; left: ${-255 + left + 2 * textLeft}px; top: -1px;" aria-hidden="true" class=""></div>
      <h5 style="font-weight: 50; position: relative; left: ${-223 + left + textLeft}px; top: -16px; will-change: top, left;" class="">Real line</h5>
      <div style="background-color: green; margin-right: 5px; width: 15px; height: 12px; border-color: white; position: relative; left: ${-174 + left}px; top: -30px;" aria-hidden="true" class=""></div>
      <h5 style="font-weight: 50; position: relative; left: ${-142 + left}px; top: -45px;" class="">Base line</h5>
      <div style="background-color: black; margin-right: 5px; width: 15px; height: 12px; border-color: white; position: relative; left: ${-94 + left}px; top: -59px;" aria-hidden="true" class=""></div>
      <h5 style="font-weight: 50; position: relative; left: ${-69 + left}px; top: -74px;" class="">Scope</h5>
      <div style="background-color: orange; margin-right: 5px; width: 15px; height: 12px; border-color: white; position: relative; left: ${-29 + left}px; top: -89px;" aria-hidden="true" class=""></div>
      <h5 style="font-weight: 50; position: relative; left: ${22 + left}px; top: -104px;" class="">Estimation line</h5>
      <div style="background-color: red; margin-right: 5px; width: 15px; height: 12px; border-color: white; position: relative; left: ${91 + left}px; top: -119px;" aria-hidden="true" class=""></div>
      <h5 style="font-weight: 50; position: relative; left: ${140 + left}px; top: -134px;" class="scope">Resolved point</h5>
      `);
      $('.scope').after(`<div class='est-block' aria-hidden="true" class=""></div>
      <h5 class='est-text'>Point to resolve</h5></div></center>`);
    }
    else {
      textLeft = 0;
      if (arrayChart[0].label == 'Remaining effort') {
        textLeft -= 26;
      }
      $('#chart').after(`<center style="height: 50px">
      <div id="legend" style="width: 500px; min-height: 50px; height: auto;" class="">
      <div style="background-color: royalblue; margin-right: 5px; width: 40px; height: 12px; border-color: white;
      position: relative; left: ${-155 + left + 2 * textLeft}px; top: -1px;" aria-hidden="true" class=""></div>
      <h5 style="font-weight: 50; position: relative; left: ${-111 + left + textLeft}px; top: -17px; will-change: top, left;" class="">Real Line</h5>
      <div style="background-color: green; margin-right: 5px; width: 40px; height: 12px; border-color: white; position: relative; left: ${-39 + left}px; top: -29px;" aria-hidden="true" class=""></div>
      <h5 style="font-weight: 50; position: relative; left: ${6 + left}px; top: -46px;" class="">Base line</h5><div style="background-color: black; margin-right: 5px; width: 40px; height: 12px; border-color: white; position: relative; left: ${69 + left}px; top: -59px;" aria-hidden="true" class=""></div>
      <h5 style="font-weight: 50; position: relative; left: ${107 + left}px; top: -74px;" class="scope">Scope</h5>
      `);
    }

    function meansureByDataChart() {
      if (velocityChartData.label == null && velocityChartData.value == null
        && effectiveOfDevelopChartData.label == null && effectiveOfDevelopChartData.value == null
        && chartData_HII.label == null && chartData_HII.value == null && chartData_HFI.label == null && chartData_HFI.value == null) {
        return 1;
      }
      else if (velocityChartData.label != null && velocityChartData.value != null
        && effectiveOfDevelopChartData.label == null && effectiveOfDevelopChartData.value == null
        && chartData_HII.label == null && chartData_HII.value == null && chartData_HFI.label == null && chartData_HFI.value == null) {
        return 2;
      }
      else if (velocityChartData.label == null && velocityChartData.value == null
        && effectiveOfDevelopChartData.label != null && effectiveOfDevelopChartData.value != null
        && chartData_HII.label == null && chartData_HII.value == null && chartData_HFI.label == null && chartData_HFI.value == null){
        return 3;
      }
      else if (velocityChartData.label == null && velocityChartData.value == null
        && effectiveOfDevelopChartData.label == null && effectiveOfDevelopChartData.value == null
        && chartData_HII.label != null && chartData_HII.value != null && chartData_HFI.label == null && chartData_HFI.value == null){
        return 4;
      }
      else if (velocityChartData.label == null && velocityChartData.value == null
        && effectiveOfDevelopChartData.label == null && effectiveOfDevelopChartData.value == null
        && chartData_HII.label == null && chartData_HII.value == null && chartData_HFI.label != null && chartData_HFI.value != null){
        return 5;
      }
    }
  });
