/**
 * @provides javelin-behavior-checkin
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('checkin', function () {

  $(document).ready(function () {
    let arrayCurrents = JSON.parse($("input[name = arrayCurrents]").val());
    let current_id = $("input[name = current_id]").val();
    const lengthCurrent = arrayCurrents.length - 1;
    let position = arrayCurrents.length - 1;
    let is_new = $("input[name = is_new]").val();
    if($('input[name = timeDevelop]').length){
      arrayCurrents[position]['effective'] =JSON.parse($('input[name = effective]').val());
      arrayCurrents[position][2] = arrayCurrents[position]['effective']['effective'];
    }
    checkCurrent();
    checkFeedBack(lengthCurrent, position, is_new);

    //readonly when kr many project
    checkHIIHFIManyProjects();

    $('#curent').keyup(function () {
      if($('#curent').val() == '') {
        $('button[name ="__submit__"]').prop('disabled', true);
      }else {
        $('button[name ="__submit__"]').prop('disabled', false);
      }
    });

    $('#curent').change(function () {
      if($('#curent').val() == '') {
        $('button[name ="__submit__"]').prop('disabled', true);
      }else {
        $('button[name ="__submit__"]').prop('disabled', false);
      }
    });

    if (($("#feedback").length === 0)){
      $('#preview').css('bottom', '30px');
      $('#next').css('bottom', '30px');
    }

    if (($("#dev").length > 0)){

      if($('#dev').val() == '' || parseFloat($('#dev').val()) < 0.5 || (parseFloat($('#dev').val())%0.5 !== 0)) {
        $('button[name ="__submit__"]').prop('disabled', true);
      }else {
        $('button[name ="__submit__"]').prop('disabled', false);
      }
    }

    $('#dev').keyup(function () {
      if($('#dev').val() == '' || parseFloat($('#dev').val()) < 0.5 || (parseFloat($('#dev').val())%0.5 !== 0)) {
        $('button[name ="__submit__"]').prop('disabled', true);
      }else {
        $('button[name ="__submit__"]').prop('disabled', false);
      }
    });

    $('#dev').change(function () {
      devChange();
    });

    $('.button-grey').removeAttr("data-sigil");
    $('.button-grey').removeAttr("href");

    $('.button-grey').click(function () {
      window.location.reload();
    });

    $('#next').hide();
    if (arrayCurrents.length == 1) {
      $('#preview').hide();
    }
    if (current_id == 0) {
      $('input[name = current_id]').val(arrayCurrents[arrayCurrents.length - 1]);
    }

    /* append icon */

    $("label[for$='feedback']").after(`<div style="style="width:750px; background-color:white; height:120px; overflow:scroll; overflow-x: scroll; overflow-y: hidden;">
    <div style="width: 2000px; height: 90px;" class = 'emotion-ground'>
      <img id='emotion5' src="/res/phabricator/0d17c6c4/rsrc/image/happy_.png" width="25" height="25" alt="bbc news special" style="position: relative; left: 228px; top: 10px;"/>
      <img id='emotion4' src="/res/phabricator/0d17c6c4/rsrc/image/happy.png" width="25" height="25" alt="bbc news special" style="position: relative; left: 265px; top: -15px;"/>
      <img id='emotion3' src="/res/phabricator/0d17c6c4/rsrc/image/normal.png" width="25" height="25" alt="bbc news special" style="position: relative; left: 300px; top: -39px;"/>
      <img id='emotion2' src="/res/phabricator/0d17c6c4/rsrc/image/unhappy.png" width="25" height="25" alt="bbc news special" style="position: relative; left: 334px; top: -64px;"/>
      <img id='emotion1' src="/res/phabricator/0d17c6c4/rsrc/image/annoying.png" width="25" height="25" alt="bbc news special" style="position: relative; left: 370px; top: -89px;"/>
    </div>
  </div>`);

    $("select[name$='feedback']").css('display', 'none');
    keys = [1, 2, 3, 4, 5];
    $.each(keys, (id, i) => {
      $('#emotion' + i).hover(function () {
        $(this).css("width", "35px");
        $(this).css("height", "35px");
      }, function () {
        $(this).css("width", "25px");
        $(this).css("height", "25px");
      });
      $('#emotion' + i).on("click", function () {
        $("select[name$='feedback']").val(i);
        y = jQuery.grep(keys, function (value) {
          return value != i;
        });
        $('#emotion' + i).css('background-color', 'blue');
        $.each(y, (id, i) => {
          $('#emotion' + i).css('background-color', 'white');
        });
      });
    });

    if ($("select[name$='feedback']").val() !== null) {
      $('#emotion' + $("select[name$='feedback']").val()).css('background-color', 'blue');
    }

    $('#preview').click(function () {
      position--;
      processData(arrayCurrents, position, lengthCurrent, is_new);
      checkEmotion();
      checkCurrent();
      if (($("#feedback").length === 0)){
        $('#preview').css('bottom', '30px');
        $('#next').css('bottom', '30px');
      }
      devChange();
      checkHIIHFIManyProjects();
    });
    $('#next').click(function () {
      position++;
      processData(arrayCurrents, position, lengthCurrent, is_new);
      checkEmotion();
      checkCurrent();
      if (($("#feedback").length === 0)){
        $('#preview').css('bottom', '30px');
        $('#next').css('bottom', '30px');
      }
      devChange();
      checkHIIHFIManyProjects();
    })

  });
  function devChange(){
    if($('#dev').length){
      if($('#dev').val() == '' || parseFloat($('#dev').val()) < 0.5 || (parseFloat($('#dev').val())%0.5 !== 0)) {
        $('button[name ="__submit__"]').prop('disabled', true);
      }else {
        $('button[name ="__submit__"]').prop('disabled', false);
      }
    }
  }

  function checkEmotion(){
    if($("select[name$='feedback']").val() !== null){
      $('img').css('background-color', 'white');
      $('#emotion' + $("select[name$='feedback']").val()).css('background-color', 'blue');
    }
    else{
      $('img').css('background-color', 'white')
    }
  }

  function processData(arrayCurrents, position, lengthCurrent, is_new) {
    $('.phui-header-header').text('Check-in KR ' + arrayCurrents[position][1]);
    $('input[name = current]').val(arrayCurrents[position][2]);
    $('select[name = confidenceLevel]').val(arrayCurrents[position][3]);
    $('select[name = feedback]').val(arrayCurrents[position][4]);
    $('textarea[name = comment]').val(arrayCurrents[position][5]);
    $('input[name = current_id]').val(arrayCurrents[position][0]);
    $('input[name = dev]').val(arrayCurrents[position][6]);
    $('#totalBug').text(arrayCurrents[position][7]);
    $('input[name = totalBug]').val(arrayCurrents[position][7]);

    if($('input[name = timeDevelop]').length){
      $('input[name = timeDevelop]').val(arrayCurrents[position]['effective']['totalTimeDEV']);
      $('input[name = timeFixBug]').val(arrayCurrents[position]['effective']['totalTimeBUG']);
      $('input[name = effective]').val(JSON.stringify(arrayCurrents[position]['effective']));
    }

    checkFeedBack(lengthCurrent, position, is_new);

    if (position == 0) {
      $('#preview').hide();
    } else {
      $('#preview').show();
    }
    if (position == arrayCurrents.length - 1) {
      $('#next').hide();
    } else {
      $('#next').show();
    }
  }

  function checkCurrent() {
    if($('#curent').val() == '') {
      $('button[name ="__submit__"]').prop('disabled', true);
    }else {
      $('button[name ="__submit__"]').prop('disabled', false);
    }
  }

  function checkFeedBack(lengthCurrent, position, is_new) {
    if(lengthCurrent == position && is_new == 1){
      $('#comment').parent().parent().css('display', 'none');
      $('label[for=feedback]').parent().css('display', 'none');
      $('#preview').css('bottom', '50px');
      $('#next').css('bottom', '50px');
    }else {
      $('#comment').parent().parent().css('display', 'block');
      $('label[for=feedback]').parent().css('display', 'block');
      $('#preview').css('bottom', '200px');
      $('#next').css('bottom', '200px');
    }
  }

  function checkHIIHFIManyProjects() {
    if($('#projectPHID').children().children('a').length > 1) {
      $('#dev').prop('readonly', true);
      if($('#dev').val() == 0) {
        $('#error-checkin').text('(No checkin data found of these projects)');
      } else {
        $('#error-checkin').text('(Dev must be greater than 0 and valid. Example: 0.5 or 1)');
      }
    }
  }

});
