/**
 * @provides javelin-behavior-po
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('po', function () {
    $(document).ready(function () {
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();

        $(".startDate").datepicker({
            dateFormat: 'dd/mm/yy',//check change
            showButtonPanel: true
        });

        $(function () {
            let dpFunc = $.datepicker._generateHTML;
            $.datepicker._generateHTML = function (inst) {
                let thishtml = $(dpFunc.call($.datepicker, inst));

                thishtml = $('<div />').append(thishtml);
                $('.ui-datepicker-close', thishtml).remove();
                $('.ui-datepicker-buttonpane', thishtml).append(
                    $('<button class="\ ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all\"\>Clear</button>').click(function () {
                        inst.input.val('');
                        inst.input.datepicker('hide');
                    })
                );

                thishtml = thishtml.children();

                return thishtml;
            };
        });

        $.datepicker._gotoToday = function (id) {
            $(id).datepicker('setDate', new Date()).datepicker('hide').blur();
        };

    });
});
