/**
 * @provides javelin-behavior-product-form
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('product-form', function () {
    $(document).ready(function () {
        $("#customer").parent().append(`<p style="color:gray; padding-top: 5px; word-break: break-all;">(Khách&nbsphàng,&nbspngười&nbspdùng&nbspcuối,...&nbspVD:&nbspPO&nbspDEHA)</p>`);
        $("#who").parent().append(`<p style="color:gray; padding-top: 5px; word-break: break-all;">(Nhu&nbspcầu,&nbspMong&nbspmuốn,...&nbspVD:&nbspQuản&nbsplý&nbspdự&nbspán&nbsptrực&nbspquan)</p>`);
        $("#productName").parent().append(`<p style="color:gray; padding-top: 5px; word-break: break-all;">(Tên&nbspsản&nbspphẩm.&nbspVD:&nbspPO&nbspToolkits...)</p>`);
        $("#classify").parent().append(`<p style="color:gray; padding-top: 5px; word-break: break-all;">(Phân&nbsploại,&nbsplĩnh&nbspvực&nbspcủa&nbspsản&nbspphẩm.&nbspVD:&nbspHệ&nbspthống&nbspquản&nbsplý&nbspdự&nbspán&nbspnội&nbspbộ...)</p>`);
        $("#benefit").parent().append(`<p style="color:gray; padding-top: 5px; word-break: break-all;">(Lợi&nbspích,&nbsplý&nbspdoc&nbspmua&nbspsản&nbspphẩm.&nbspVD:&nbspTạo&nbspcác&nbspoutput&nbspcần&nbspcó&nbspđối&nbspvới&nbspPO&nbsptrong&nbspquản&nbsplý&nbspdự&nbspán)</p>`);
        $("#unlike").parent().append(`<p style="color:gray; padding-top: 5px; word-break: break-all;">(Các&nbspsản&nbspsản&nbspphẩm&nbspkhác,&nbspcách&nbsplàm&nbsphiện&nbsptại,...&nbspVD:&nbspquản&nbsplý&nbspoutput&nbspphân&nbsptán)</p>`);
        $("#ourProject").parent().append(`<p style="color:gray; padding-top: 5px; word-break: break-all;">(Sự&nbspkhác&nbspbiệt,&nbspgiá&nbsptrị&nbspcủa&nbspsản&nbspphẩm,...&nbspVD:&nbspTập&nbsptrung&nbspcác&nbspoutput&nbsptại&nbspmột&nbspnơi&nbspduy&nbspnhất)</p>`);


        $last_update = $("#value-day-update").val();

        $(".phui-header-subheader").append("<div class=\"phui-header-subheader\">Last Update : " + $last_update + "</div>");

    });
});
