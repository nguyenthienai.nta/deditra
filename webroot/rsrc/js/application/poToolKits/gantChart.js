/**
 * @provides javelin-behavior-gant-chart
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('gant-chart', function () {
    $(".aphront-table-wrap").append("<div class=\"custom-gantt-chart-view\" id='view-gantt-chart'><svg id=\"gantt\"></svg></div>");

    $(document).ready(function () {

        $last_update = $("#value-day-update").val();

        $(".phui-header-subheader").append("<div class=\"phui-header-subheader\">Last Update : " + $last_update + "</div>");

        $height_table = $(".aphront-table-view").outerHeight() + 40;

        $("#gantt").css({'height': $height_table});

        $("#gantt").find(".bar.bar-invalid").parent().parent().css("visibility", "hidden");

        var tasks = JSON.parse($("#object-tasks").val());

        if (tasks.length == 0) {
            $(".aphront-table-view").css("width", "100%");
        } else {
            $(".aphront-table-view").css("width", "43%");
        }

        $(".task-parent").find(".bar-progress").addClass("background-parent");

    });

});