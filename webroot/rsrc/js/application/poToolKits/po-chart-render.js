/**
 * @provides javelin-behavior-po-chart-render
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('po-chart-render', function () {
    const happy = new Image();
    happy.src = '/res/phabricator/0d17c6c4/rsrc/image/normal.png';
    happy.width = 25;
    happy.height = 25;

    const normal = new Image();
    normal.src = '/res/phabricator/0d17c6c4/rsrc/image/suspicious.png';
    normal.width = 25;
    normal.height = 25;

    const sad = new Image();
    sad.src = '/res/phabricator/0d17c6c4/rsrc/image/sad.png';
    sad.width = 25;
    sad.height = 25;

    let data = [];
    data['label'] = ['', "Mumbai", "Mexico City", "Shanghai", "Sao Paulo", "New York", "Karachi", "Buenos Aires", "Delhi", "Moscow"];
    data['value'] =  [null, 1, 3, 2, 1, 3, 2, 3, 2, 3];
    $(".aphront-table-wrap").before(`<div id="chartDiv" style="top: 50%; left: 50%; margin-right: 15%; margin-left: 15%;">
                                        <canvas id="chart" style="display: block; height: 476px; width: 952px;" width="856" height="428" class="chartjs-render-monitor"></canvas>
                                     </div>`);
    let emotions = [happy, normal, sad];
    Chart.pluginService.register({
        afterUpdate: function (chart) {
            for (i = 0; i<10; i++) {
                chart.config.data.datasets[0]._meta[0].data[i]._model.pointStyle = emotions[Math.floor(Math.random() * emotions.length)];                ;
            }
        }
    }); 
                                  
    let ctx = document.getElementById('chart').getContext('2d');
    let myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: data['label'],
            datasets: [{
                label: 'Point', // Name the series
                data: data['value'], // Specify the data values array
                fill: false,
                borderColor: '#2196f3', // Add custom color border (Line)
                backgroundColor: '#2196f3', // Add custom color background (Points and Fill)
                borderWidth: 1 // Specify bar border width
            }]
        },
        options: {
            responsive: true, // Instruct chart js to respond nicely.
            maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height
            elements: {
                line: {
                tension: 0
                }
            },
            legend: {
                display: false,
              },
            title: {
                display: true,
                text: 'NSS Report'
            },
            scales: {
                yAxes : [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Point'
                    },
                    ticks : {
                        max : 3.5,    
                        min : 0
                    }
                }]
            }
        }
    });
});
