/**
 * @provides javelin-behavior-survey-form
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior('survey-form', function () {
    $(document).ready(function () {
        // hover border emotion block
        $('.feedback').hover(function () {
            $(this).css("border", "2px solid green");
        }, function () {
            $(this).css("border", "1px solid #A1A6B0");
        });
    });
});
