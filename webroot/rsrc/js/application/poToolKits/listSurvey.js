/**
 * @provides javelin-behavior-list-survey
 * @requires javelin-behavior
 *           javelin-dom
 */
JX.behavior("list-survey", function () {
    $(document).ready(function () {
        id = $('#projectId').val();
        all_survey_id = JSON.parse($('#allSurveyId').val());

        let createBtn = `<div class="phui-header-action-links">
                            <a class="create-btn button button-blue has-text icon-last phui-button-default msl phui-header-action-link" href="/po-tools/release-survey/${id}/add" role="button">
                                <div class="phui-button-text">Create Survey</div>
                            </a>
                        </div>`;
        let saveStatusBtn = ` <div class="phui-header-action-links">
                                <a class="save-status-btn button button-green has-text icon-last phui-button-default msl phui-header-action-link" role="button">
                                    <div class="phui-button-text">Save Status</div>
                                </a>
                              </div>`;
        //append btn
        $(".aphront-table-wrap").prepend(saveStatusBtn);
        $(".aphront-table-wrap").prepend(createBtn);

        // Hover copy icon
        $('.fa-copy').hover(function () {
            $(this).css("color", "green");
        }, function () {
            $(this).css("color", "black");
        });
        
        let surveyStatus = [[], []];

        $.each(all_survey_id, (key, survey_id) => {
            // Copy URL
            $(".btn-copy-" + survey_id).append(`<p class='tooltip tooltip${survey_id}'>Copied!!</p>`);
            $('.tooltip' + survey_id).hide();
            $(".btn-copy-" + survey_id).on('click', function(){
                copyToClipboard($('.url-' + survey_id));
                $('.tooltip' + survey_id).show();
                setTimeout(function() { 
                    $('.tooltip' + survey_id).hide();
                }, 1000);
            });

            // Update survey status
            $("#status_" + survey_id).on('change', function(){
                if(surveyStatus[0].indexOf(survey_id) === -1){
                    surveyStatus[0].push(survey_id);
                    surveyStatus[1].push($(this).val());
                }
                else{
                    index = surveyStatus[0].indexOf(survey_id);
                    surveyStatus[1][index] = $(this).val();
                }
                console.log(surveyStatus);
                $('#surveyStatus').val(surveyStatus);
            });
        });
    
        let dialog = `<div class="jx-client-dialog dialog-confirm-update" style="left: 0px; top: 335px; opacity: 0">
                        <form method="GET" class="aphront-dialog-view  aphront-dialog-view-standalone" role="dialog">
                            <div class="aphront-dialog-head">
                                <div class="phui-header-shell ">
                                    <h1 class="phui-header-view">
                                        <div class="phui-header-row">
                                            <div class="phui-header-col2">
                                                <span class="phui-header-header">Update surveys status</span>
                                            </div>
                                            <div class="phui-header-col3"></div>
                                        </div>
                                    </h1>
                                </div>
                            </div>
                            <div class="aphront-dialog-body grouped">All survey will update.</div>
                            <div class="aphront-dialog-tail grouped">
                                <a class="btn-update button button-blue has-text icon-last phui-button-default msl phui-header-action-link">Update
                                    status</a>
                                <a
                                    class="btn-cancel button button-grey has-text icon-last phui-button-default msl phui-header-action-link">Cancel</a>
                            </div>
                        </form>
                    </div>`;

        $('.aphront-table-wrap').before(dialog);

        $('.save-status-btn').on('click', function(){
            $('.dialog-confirm-update').css("opacity", '1');
            $('.dialog-confirm-update').before(`<div class="jx-mask jx-dialog"></div>`);
            $('.jx-dialog').css('background-color', '#292f33'); 
            $('.jx-dialog').css('opacity', '0.7');
        });
        
        $('.btn-cancel').on('click', function(){
            $('.dialog-confirm-update').css("opacity", '0');
            $('.jx-dialog').remove();
        });

        $('.btn-update').on('click', function(){
            $('button[type$="submit"]').trigger("click");
        });
        
        const copyToClipboard = (element) => {
            let $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();
        }
          
    });
});
