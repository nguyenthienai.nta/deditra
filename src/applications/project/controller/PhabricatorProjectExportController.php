<?php


final class PhabricatorProjectExportController
  extends PhabricatorProjectController
{

  public function handleRequest(AphrontRequest $request)
  {

    require_celerity_resource('export-data-css');

    Javelin::initBehavior('jquery');
    Javelin::initBehavior('export-data');

    $viewer = $this->getViewer();
    $projectID = $request->getStr('query');

    $all_formats = PhabricatorExportFormat::getAllExportFormats();

    $available_options = array();
    $unavailable_options = array();
    $formats = array();
    $unavailable_formats = array();
    foreach ($all_formats as $key => $format) {
      if ($format->isExportFormatEnabled()) {
        $available_options[$key] = $format->getExportFormatName();
        $formats[$key] = $format;
      } else {
        $unavailable_options[$key] = pht(
          '%s (Not Available)',
          $format->getExportFormatName());
        $unavailable_formats[$key] = $format;
      }
    }
    $format_options = $available_options + $unavailable_options;

    $format_key = $this->readExportFormatPreference();
    if (!isset($formats[$format_key])) {
      $format_key = head_key($format_options);
    }

    $errors = array();

    $e_format = null;
    if ($request->isFormPost()) {
      $projectID = $request->getStr('projectID');
      $project = id(new PhabricatorProject())->loadOneWhere('id = %s', $projectID);
      $state = id(new PhabricatorOkrsViewState())
        ->setProject($project)
        ->readFromRequest($this->getRequest());
      $tasks = $state->getObjects();
      $format_key = $request->getStr('format');
      if (isset($unavailable_formats[$format_key])) {
        $unavailable = $unavailable_formats[$format_key];
        $instructions = $unavailable->getInstallInstructions();

        $markup = id(new PHUIRemarkupView($viewer, $instructions))
          ->setRemarkupOption(
            PHUIRemarkupView::OPTION_PRESERVE_LINEBREAKS,
            false);

        return $this->newDialog()
          ->setTitle(pht('Export Format Not Available'))
          ->appendChild($markup)
          ->addCancelButton('/project/export/?query=' . $projectID, pht('Done'));
      }

      $format = idx($formats, $format_key);

      if (!$format) {
        $e_format = pht('Invalid');
        $errors[] = pht('Choose a valid export format.');
      }

      if($project->getParentProjectPHID()) {
        $projectName = id(new PhabricatorProject())->loadOneWhere('phid = %s', $project->getParentProjectPHID())->getName() . ' (' .$project->getName() . ')';
      }else {
        $projectName = $project->getName();
      }

      if (!$errors) {
        $export_engine = id(new PhabricatorExportProjectEngine())
          ->setViewer($viewer)
          ->setTitle('Tasks List')
          ->setFilename($projectName . '_' . date('d-m-y'))
          ->setExportFormat($format)
          ->setProjectName($projectName)
          ->setCheckTag($project->getName())
          ->setProjectPHID($project->getPHID());

        $file = $export_engine->exportFile($tasks);

        return $file->newDownloadResponse();
      }
    }

    $index = 0;
    foreach ($format_options as $key => $value) {
      if ($key == 'json') {
        break;
      }
      $index++;
    }
    array_splice($format_options, $index, 1);

    $export_form = id(new AphrontFormView())
      ->setViewer($viewer)
      ->appendControl(
        id(new AphrontFormSelectControl())
          ->setName('format')
          ->setLabel(pht('Format'))
          ->setError(true)
          ->setValue($format_key)
          ->setOptions($format_options))
    ->appendChild(javelin_tag(
      'div',
      array(
        'class' => 'loader'
      ),
      [
        javelin_tag(
          'div',
          array(
            'class' => 'loading1'
          ),'')]));

    return $this->newDialog()
      ->setTitle(pht('Export Results'))
      ->setErrors($errors)
      ->setClass('export-data')
      ->appendForm($export_form)
      ->addHiddenInput('projectID', $projectID)
      ->addCancelButton('/project/board/' . $projectID)
      ->addSubmitButton('Download Data');
  }

  private function readExportFormatPreference()
  {
    $viewer = $this->getViewer();
    $export_key = PhabricatorPolicyFavoritesSetting::SETTINGKEY;
    return $viewer->getUserSetting($export_key);
  }

}
