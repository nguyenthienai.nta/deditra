<?php

final class PhabricatorExportProjectEngine
  extends Phobject
{

  private $viewer;
  private $searchEngine;
  private $savedQuery;
  private $exportFormat;
  private $filename;
  private $title;
  private $projectName;
  private $checkTag;
  private $projectPHID;

  public function setViewer(PhabricatorUser $viewer)
  {
    $this->viewer = $viewer;
    return $this;
  }

  public function getViewer()
  {
    return $this->viewer;
  }

  public function setSearchEngine(
    PhabricatorApplicationSearchEngine $search_engine)
  {
    $this->searchEngine = $search_engine;
    return $this;
  }

  public function getSearchEngine()
  {
    return $this->searchEngine;
  }

  public function setSavedQuery(PhabricatorSavedQuery $saved_query)
  {
    $this->savedQuery = $saved_query;
    return $this;
  }

  public function getSavedQuery()
  {
    return $this->savedQuery;
  }

  public function setExportFormat(
    PhabricatorExportFormat $export_format)
  {
    $this->exportFormat = $export_format;
    return $this;
  }

  public function getExportFormat()
  {
    return $this->exportFormat;
  }

  public function setFilename($filename)
  {
    $this->filename = $filename;
    return $this;
  }

  public function getFilename()
  {
    return $this->filename;
  }

  public function setTitle($title)
  {
    $this->title = $title;
    return $this;
  }

  public function getTitle()
  {
    return $this->title;
  }

  public function setProjectName($projectName)
  {
    $this->projectName = $projectName;
    return $this;
  }

  public function getProjectName()
  {
    return $this->projectName;
  }

  public function setCheckTag($checkTag)
  {
    $this->checkTag = $checkTag;
    return $this;
  }

  public function getCheckTag()
  {
    return $this->checkTag;
  }

  public function setProjectPHID($projectPHID)
  {
    $this->projectPHID = $projectPHID;
    return $this;
  }

  public function getProjectPHID()
  {
    return $this->projectPHID;
  }

  public function newBulkJob(AphrontRequest $request)
  {
    $viewer = $this->getViewer();
    $engine = $this->getSearchEngine();
    $saved_query = $this->getSavedQuery();
    $format = $this->getExportFormat();

    $params = array(
      'engineClass' => get_class($engine),
      'queryKey' => $saved_query->getQueryKey(),
      'formatKey' => $format->getExportFormatKey(),
      'title' => $this->getTitle(),
      'filename' => $this->getFilename(),
    );

    $job = PhabricatorWorkerBulkJob::initializeNewJob(
      $viewer,
      new PhabricatorExportEngineBulkJobType(),
      $params);

    // We queue these jobs directly into STATUS_WAITING without requiring
    // a confirmation from the user.

    $xactions = array();

    $xactions[] = id(new PhabricatorWorkerBulkJobTransaction())
      ->setTransactionType(PhabricatorWorkerBulkJobTransaction::TYPE_STATUS)
      ->setNewValue(PhabricatorWorkerBulkJob::STATUS_WAITING);

    $editor = id(new PhabricatorWorkerBulkJobEditor())
      ->setActor($viewer)
      ->setContentSourceFromRequest($request)
      ->setContinueOnMissingFields(true)
      ->applyTransactions($job, $xactions);

    return $job;
  }

  public function exportFile(array $tasks)
  {
    $viewer = $this->getViewer();
    $format = $this->getExportFormat();
    $title = $this->getTitle();
    $filename = $this->getFilename();
    $extension = $format->getFileExtension();
    $mime_type = $format->getMIMEContentType();
    $filename = $filename . '.' . $extension;
    $stt = 1;

    $columns[0] = 'Backlog';
    $columnsHeader[0] = 'Time on column Backlog (hours)';
    $arrTransactions = array();

    if(id(new PhabricatorProject())->loadOneWhere('phid = %s', $this->projectPHID)->getHasMilestones() == 0) {
      $projectColumns = id(new PhabricatorProjectColumn())->loadAllWhere('projectPHID = %s', $this->projectPHID);
      foreach ($projectColumns as $projectColumn) {
        $columns[] = $projectColumn->getName() ? $projectColumn->getName() : 'Backlog';
        $columnsHeader[] = 'Time on column ' .
          ($projectColumn->getName() ?
            $projectColumn->getName() : 'Backlog') . ' (hours)';
      }
    } else {
      foreach ($tasks as $task_phid => $task) {
        $transactions = id(new ManiphestTransaction())->loadAllWhere('objectPHID = %s', $task->getPHID());

        foreach ($transactions as $transaction) {
          if (isset($transaction->getNewValue()[0]['columnPHID'])) {
            //Header Column
            $columns[] = $this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getName() ?
              $this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getName() : 'Backlog';
            $columnsHeader[] = 'Time on column ' .
              ($this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getName() ?
                $this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getName() : 'Backlog') . ' (hours)';
          }
        }
      }
    }

    $columns = array_intersect_key($columns, array_unique(array_map('strtolower', $columns)));
    $columnsHeader = array_intersect_key($columnsHeader, array_unique(array_map('strtolower', $columnsHeader)));

    $format = id(clone $format)
      ->setViewer($viewer)
      ->setTitle($title);

    $header = array(
      'No',
      'Project, Tag',
      'Mã',
      'Title',
      'Description',
      'Start Date',
      'End Date',
      'Assign To',
      'Status',
      'Priority',
      'Story Points',
      'Estimated Hours',
      'Current Column'
    );

    $format->addHeaders(array_merge($header, $columnsHeader, array('Recidivism Rate')));

    foreach ($tasks as $task_phid => $task) {
      $arrayTime = array();

      $transactions = id(new ManiphestTransaction())->loadAllWhere('objectPHID = %s', $task->getPHID());
      $arrTransactions[$task_phid]['recidivism'] = 0;
      $arrTransactions[$task_phid]['currentColumn'] = 'Backlog';
      $arrTransactions[$task_phid][$this->getProjectPHID()] = 'Backlog';

      $key[$task_phid] = array();
      foreach ($transactions as $key_transaction => $transaction) {
        if (isset($transaction->getNewValue()[0]['columnPHID']) || (strpos($transaction->getNewValue()[0], '-PROJ-'))) {
          $key[$task_phid][] = $key_transaction;

          if (isset($transaction->getNewValue()[0]['columnPHID'])) {

            if ($this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getName() ||
              (($this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getName() == '') && $this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getSequence() == 0)) {

              $arrTransactions[$task_phid][$transaction->getNewValue()[0]['boardPHID']] = $this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getName() ?
                $this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getName() : 'Backlog';

              $fromSequence = key($transaction->getNewValue()[0]['fromColumnPHIDs']) == null
                ? '0' : $this->getColumn(key($transaction->getNewValue()[0]['fromColumnPHIDs']))->getSequence();

              $arrTransactions[$task_phid][$this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getPHID()] = $this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getName() ?
                $this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getName() : 'Backlog';

              $arrTransactions[$task_phid]['column'][] = $this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getName() ?
                $this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getName() : 'Backlog';
              $arrTransactions[$task_phid]['createAt'][] = $transaction->getDateCreated();

              //Recidivism Rate
              if ($this->getColumn($transaction->getNewValue()[0]['columnPHID'])->getSequence() < $fromSequence) {
                $arrTransactions[$task_phid]['recidivism']++;
              }
            }
          }
          if (strpos($transaction->getNewValue()[0], '-PROJ-')) {

            if ($arrTransactions[$task_phid][$transaction->getNewValue()[0]]) {
              $arrTransactions[$task_phid]['column'][] = $arrTransactions[$task_phid][$transaction->getNewValue()[0]];
              $arrTransactions[$task_phid]['createAt'][] = $transaction->getDateCreated();
            } else {
              $arrTransactions[$task_phid]['column'][] = 'Backlog';
              $arrTransactions[$task_phid]['createAt'][] = $transaction->getDateCreated();
            }
          }
        } else {
          continue;
        }
      }

      if (!isset($arrTransactions[$task_phid]['column'])) {
        $arrTransactions[$task_phid]['column'][] = 'Backlog';
        foreach ($transactions as $transaction) {
          $arrTransactions[$task_phid]['createAt'][] = $transaction->getDateCreated();
          break;
        }
      }

      foreach ($columns as $key => $column) {
        for ($i = 0; $i < count($arrTransactions[$task_phid]['column']); $i++) {
          if (strtolower($arrTransactions[$task_phid]['column'][$i]) == strtolower($column)) {
            if (isset($arrTransactions[$task_phid]['column'][$i + 1])) {
              $arrayTime[$column] += round(abs($arrTransactions[$task_phid]['createAt'][$i] - $arrTransactions[$task_phid]['createAt'][$i + 1]) / 3600,1);
            } else {
              $arrayTime[$column] += round(abs($arrTransactions[$task_phid]['createAt'][$i] - time()) / 3600,1);
            }
          } else {
            $arrayTime[$column] += 0;
          }
        }
      }

      $row = [
        $stt,
        $this->projectName .
        ($this->getProjectTags($task) ? (', ' . $this->getProjectTags($task)) : ''),
        'T' . $task->getID(),
        $task->getTitle(),
        $task->getDescription(),
        $task->getStartDate() !== null ? date('d/m/Y', $task->getStartDate()) : null,
        $task->getEndDate() !== null ? date('d/m/Y', $task->getEndDate()) : null,
        id(new PhabricatorUser())->loadOneWhere('PHID = %s', $task->getOwnerPHID()) ?
          id(new PhabricatorUser())->loadOneWhere('PHID = %s', $task->getOwnerPHID())->getUserName() : '',
        $task->getStatus(),
        $this->getPriorityName($task),
        $task->getPoints(),
        $this->estimatedHours($viewer, $task->getID()),
        $arrTransactions[$task_phid]['column'][count($arrTransactions[$task_phid]['column']) - 1]
      ];

      $timeOnColumns = array_values($arrayTime);
      $row = array_merge($row, $timeOnColumns, array($arrTransactions[$task_phid]['recidivism']));
      $format->addObject('custom', array(), $row);
      $stt++;
    }

    $export_result = $format->newFileData();

    $iterator = new ArrayIterator(array($export_result));

    $source = id(new PhabricatorIteratorFileUploadSource())
      ->setName($filename)
      ->setViewPolicy(PhabricatorPolicies::POLICY_NOONE)
      ->setMIMEType($mime_type)
      ->setRelativeTTL(phutil_units('60 minutes in seconds'))
      ->setAuthorPHID($viewer->getPHID())
      ->setIterator($iterator);

    return $source->uploadFile();
  }

  private function estimatedHours($viewer, $taskId)
  {
    $task = id(new ManiphestTaskQuery())
      ->setViewer($viewer)
      ->withIDs(array($taskId))
      ->needSubscriberPHIDs(true)
      ->executeOne();
    if (!$task) {
      return new Aphront404Response();
    }

    $field_list = PhabricatorCustomField::getObjectFields(
      $task,
      PhabricatorCustomField::ROLE_VIEW);
    $field_list
      ->setViewer($viewer)
      ->readFieldsFromStorage($task);

    return $this->buildPropertyView($field_list);
  }

  private function buildPropertyView(PhabricatorCustomFieldList $field_list)
  {
    return $field_list->getEstimatedHours();
  }

  private function getPriorityName($task)
  {
    switch ($task->getPriority()) {
      case 0 :
        return 'Wishlist';
      case 25:
        return 'Low';
      case 50:
        return 'Normal';
      case 80:
        return 'Hight';
      case 90:
        return 'Needs Triage';
      case 100:
        return 'Unbreak Now!';
      default :
        return '';
    }
  }

  private function getProjectTags($task)
  {
    if (json_encode($task->edgeProjectPHIDs)) {
      $text = json_encode($task->edgeProjectPHIDs);
      $replacements = [
        '"' => "",
        "[" => "",
        "]" => ""
      ];
      $arrTags = explode(',', strtr($text, $replacements));
      $projectTag = '';
      for ($i = 0; $i < count($arrTags); $i++) {
        if (
          trim(id(new PhabricatorProject())->loadOneWhere('phid = %s', $arrTags[$i])->getName()) == trim($this->checkTag)
        ) {
          continue;
        }
        $projectTag .= id(new PhabricatorProject())->loadOneWhere('phid = %s', $arrTags[$i])->getName() . ' ,';
      }
      return rtrim($projectTag, ",");
    } else {
      return null;
    }
  }

  private function getColumn($columnPHID)
  {
    return id(new PhabricatorProjectColumn())->loadOneWhere('phid = %s', $columnPHID);
  }

}
