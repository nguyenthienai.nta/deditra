<?php

final class PhabricatorProjectIsSprintTransaction
  extends PhabricatorProjectTransactionType {

  const TRANSACTIONTYPE = 'project:isSprint';

  public function generateOldValue($object) {
    return $object->getIsSprint();
  }

  public function generateNewValue($object, $value)
  {
    return $value[0];
  }

  public function applyInternalEffects($object, $value) {
    $object->setIsSprint($value[0]);
  }

  public function getTitle() {
    $new = $this->getNewValue();
    if ($new == 1) {
      return pht(
        "%s update this project is sprint.",
        $this->renderAuthor());
    } else {
      return pht(
        "%s update this project is not sprint.",
        $this->renderAuthor());
    }
  }

  public function getTitleForFeed() {
    $new = $this->getNewValue();
    if ($new == 1) {
      return pht(
        "%s update this project is a sprint.",
        $this->renderAuthor());
    } else {
      return pht(
        "%s update this project is not a sprint.",
        $this->renderAuthor());
    }
  }

  public function validateTransactions($object, array $xactions) {
    $errors = array();
    return $errors;
  }
}
