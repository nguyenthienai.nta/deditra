<?php

final class PhabricatorProjectStartDateTransaction
  extends PhabricatorProjectTransactionType {

  const TRANSACTIONTYPE = 'project:startdate';

  public function generateOldValue($object) {
    return $object->getStartDate() == null ? null : (int)$object->getStartDate();
  }

  public function generateNewValue($object, $value) {

    if($value->getValueDate() == null || $value->getValueDate() == "" ) {
      return null;
    } else {
      return  $value->newPhutilDateTime()
        ->newAbsoluteDateTime()
        ->getEpoch();
    }
  }

  public function applyInternalEffects($object, $value) {
    $object->setStartDate($value);
  }

  public function getTitle() {
    return pht(
      '%s updated the start date from %s to %s.',
      $this->renderAuthor(),
      $this->renderOldDate(),
      $this->renderNewDate());
  }

  public function getTitleForFeed() {
    return pht(
      '%s updated the start date for %s from %s to %s.',
      $this->renderAuthor(),
      $this->renderObject(),
      $this->renderOldDate(),
      $this->renderNewDate());
  }

  public function validateTransactions($object, array $xactions) {
    $errors = array();
    return $errors;
  }
}
