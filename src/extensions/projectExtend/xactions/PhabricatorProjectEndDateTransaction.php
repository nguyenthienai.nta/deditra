<?php

final class PhabricatorProjectEndDateTransaction
  extends PhabricatorProjectTransactionType {

  const TRANSACTIONTYPE = 'project:endDate';

  public function generateOldValue($object)
  {
    return  $object->getEndDate() === null ? null : (int)$object->getEndDate();
  }

  public function generateNewValue($object, $value)
  {
    
     if($value->getValueDate() === null || $value->getValueDate() == "" ) {
      return null;
     } else {
      return  $value->newPhutilDateTime()
        ->newAbsoluteDateTime()
        ->getEpoch();
     }
  }

  public function applyInternalEffects($object, $value)
  {
    $object->setEndDate($value);
  }

  public function getTitle()
  {
    return pht(
      '%s updated the end date from %s to %s.',
      $this->renderAuthor(),
      $this->renderOldDate(),
      $this->renderNewDate()
    );
  }

  public function getTitleForFeed()
  {
    return pht(
      '%s updated the end date for %s from %s to %s.',
      $this->renderAuthor(),
      $this->renderObject(),
      $this->renderOldDate(),
      $this->renderNewDate()
    );
  }

  public function validateTransactions($object, array $xactions)
  {
    $errors = array();
    return $errors;
  }
}
