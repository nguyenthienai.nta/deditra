<?php

final class PhabricatorHephaestosOkrQuery extends PhabricatorCursorPagedPolicyAwareQuery {


    const KEY_ALL = 'hephaestos.all';
    const KEY_DEFAULT = 'hephaestos.default';
    const KEY_VIEWER = 'hephaestos.default.view';

    private $ids;
    private $phids;
    private $isArchived;

    public function withIDs(array $ids) {
        $this->ids = $ids;
        return $this;
    }

    public function withPHIDs(array $phids) {
        $this->phids = $phids;
        return $this;
    }

    public function withIsArchived($archived) {
        $this->isArchived = $archived;
        return $this;
    }

    public function getQueryApplicationClass() {
        return 'PhabricatorHephaestosApplication';
    }

    protected function loadPage() {
        return $this->loadStandardPage(new PhabricatorHephaestosOkr());
    }

    protected function buildWhereClauseParts(AphrontDatabaseConnection $conn)
    {
        $where = parent::buildWhereClauseParts($conn);

        if ($this->ids !== null) {
            $where[] = qsprintf(
                $conn,
                'id IN (%Ld)',
                $this->ids);
        }

        if ($this->phids !== null) {
            $where[] = qsprintf(
                $conn,
                'phid IN (%Ls)',
                $this->phids);
        }

        if ($this->isArchived !== null) {
            $where[] = qsprintf(
                $conn,
                'isArchived = %d',
                (int)$this->isArchived);
        }


        return $where;
    }


}
