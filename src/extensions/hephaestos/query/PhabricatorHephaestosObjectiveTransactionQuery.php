<?php

final class PhabricatorHephaestosObjectiveTransactionQuery
    extends PhabricatorApplicationTransactionQuery
{

    public function getTemplateApplicationTransaction()
    {
        return new PhabricatorOkrsNamespaceTransaction();
    }

}
