<?php

final class PhabricatorHephaestosCheckinTransactionQuery
    extends PhabricatorApplicationTransactionQuery {

    public function getTemplateApplicationTransaction() {
        return new PhabricatorHephaestosCheckinTransaction();
    }

}
