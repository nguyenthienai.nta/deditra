<?php

final class PhabricatorHephaestosKeyResultTransactionQuery
    extends PhabricatorApplicationTransactionQuery {

    public function getTemplateApplicationTransaction() {
        return new PhabricatorHephaestosKeyResultTransaction();
    }

}
