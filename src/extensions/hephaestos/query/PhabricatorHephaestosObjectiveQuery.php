<?php

final class PhabricatorHephaestosObjectiveQuery
    extends PhabricatorCursorPagedPolicyAwareQuery
{

    const KEY_ALL = 'hephaestos.all';
    const KEY_DEFAULT = 'hephaestos.default';
    const KEY_VIEWER = 'hephaestos.viewer';

    private $ids;
    private $phids;
    private $hephaestosPHIDs;
    private $isArchived;

    public function withIDs(array $ids)
    {
        $this->ids = $ids;
        return $this;
    }

    public function withPHIDs(array $phids)
    {
        $this->phids = $phids;
        return $this;
    }

    public function withHephaestosPHIDs(array $okr_phids) {
        $this->hephaestosPHIDs = $okr_phids;
        return $this;
    }

    public function withIsArchived($archived)
    {
        $this->isArchived = $archived;
        return $this;
    }

    public function getQueryApplicationClass()
    {
        return 'PhabricatorHephaestosApplication';
    }

    protected function loadPage()
    {
        return $this->loadStandardPage(new PhabricatorHephaestosObjective());
    }

    protected function buildWhereClauseParts(AphrontDatabaseConnection $conn)
    {
        $where = parent::buildWhereClauseParts($conn);

        if ($this->ids !== null) {
            $where[] = qsprintf(
                $conn,
                'id IN (%Ld)',
                $this->ids);
        }

        if ($this->phids !== null) {
            $where[] = qsprintf(
                $conn,
                'phid IN (%Ls)',
                $this->phids);
        }

        if ($this->isArchived !== null) {
            $where[] = qsprintf(
                $conn,
                'isArchived = %d',
                (int)$this->isArchived);
        }

        if ($this->hephaestosPHIDs !== null) {
            $where[] = qsprintf(
                $conn,
                'o.okrPHID in (%Ls)',
                $this->hephaestosPHIDs);
        }

        return $where;
    }

    public static function destroyOkrsCache()
    {
        $cache = PhabricatorCaches::getRequestCache();
        $cache->deleteKeys(
            array(
                self::KEY_ALL,
                self::KEY_DEFAULT,
            ));
    }

    public static function getOkrsExist()
    {
        return (bool)self::getAllOkrs();
    }

    public static function getViewerOkrsExist(PhabricatorUser $viewer)
    {
        if (!self::getOkrsExist()) {
            return false;
        }

        // If the viewer has access to only one space, pretend Okrs simply don't
        // exist.
        $Okrs = self::getViewerOkrs($viewer);
        return (count($Okrs) > 1);
    }

    public static function getAllOkrs()
    {
        $cache = PhabricatorCaches::getRequestCache();
        $cache_key = self::KEY_ALL;

        $Okrs = $cache->getKey($cache_key);
        if ($Okrs === null) {
            $Okrs = id(new PhabricatorHephaestosObjectiveQuery())
                ->setViewer(PhabricatorUser::getOmnipotentUser())
                ->execute();
            $Okrs = mpull($Okrs, null, 'getPHID');
            $cache->setKey($cache_key, $Okrs);
        }

        return $Okrs;
    }

    public static function getDefaultSpace()
    {
        $cache = PhabricatorCaches::getRequestCache();
        $cache_key = self::KEY_DEFAULT;

        $default_space = $cache->getKey($cache_key, false);
        if ($default_space === false) {
            $default_space = null;

            $Okrs = self::getAllOkrs();
            foreach ($Okrs as $space) {
                if ($space->getIsDefaultNamespace()) {
                    $default_space = $space;
                    break;
                }
            }

            $cache->setKey($cache_key, $default_space);
        }

        return $default_space;
    }

    public static function getViewerOkrs(PhabricatorUser $viewer)
    {
        $cache = PhabricatorCaches::getRequestCache();
        $cache_key = self::KEY_VIEWER . '(' . $viewer->getCacheFragment() . ')';

        $result = $cache->getKey($cache_key);
        if ($result === null) {
            $Okrs = self::getAllOkrs();

            $result = array();
            foreach ($Okrs as $key => $space) {
                $can_see = PhabricatorPolicyFilter::hasCapability(
                    $viewer,
                    $space,
                    PhabricatorPolicyCapability::CAN_VIEW);
                if ($can_see) {
                    $result[$key] = $space;
                }
            }

            $cache->setKey($cache_key, $result);
        }

        return $result;
    }


    public static function getViewerActiveOkrs(PhabricatorUser $viewer)
    {
        $Okrs = self::getViewerOkrs($viewer);

        foreach ($Okrs as $key => $space) {
            if ($space->getIsArchived()) {
                unset($Okrs[$key]);
            }
        }

        return $Okrs;
    }

    public static function getSpaceOptionsForViewer(
        PhabricatorUser $viewer,
        $space_phid)
    {

        $viewer_Okrs = self::getViewerOkrs($viewer);
        $viewer_Okrs = msort($viewer_Okrs, 'getTitle');

        $map = array();
        foreach ($viewer_Okrs as $space) {

            // Skip archived Okrs, unless the object is already in that space.
            if ($space->getIsArchived()) {
                if ($space->getPHID() != $space_phid) {
                    continue;
                }
            }

            $map[$space->getPHID()] = pht(
                'Objective %s: %s',
                $space->getMonogram(),
                $space->getTitle());
        }

        return $map;
    }


    /**
     * Get the Objective PHID for an object, if one exists.
     *
     * This is intended to simplify performing a bunch of redundant checks; you
     * can intentionally pass any value in (including `null`).
     *
     * @param wild
     * @return phid|null
     */
    public static function getObjectSpacePHID($object)
    {
        if (!$object) {
            return null;
        }

        if (!($object instanceof PhabricatorHephaestosInterface)) {
            return null;
        }

        $space_phid = $object->getSpacePHID();
        if ($space_phid === null) {
            $default_space = self::getDefaultSpace();
            if ($default_space) {
                $space_phid = $default_space->getPHID();
            }
        }

        return $space_phid;
    }

    protected function getPrimaryTableAlias() {
        return 'o';
    }


}
