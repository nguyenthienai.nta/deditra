<?php

final class PhabricatorHephaestosObjectiveSearchEngine
  extends PhabricatorApplicationSearchEngine
{

  public function getApplicationClassName()
  {
    return 'PhabricatorHephaestosApplication';
  }

  public function getResultTypeDescription()
  {
    return pht('obj');
  }

  public function newQuery()
  {
    return new PhabricatorHephaestosObjectiveQuery();
  }

  protected function buildCustomSearchFields()
  {
    return array(
      id(new PhabricatorSearchThreeStateField())
        ->setLabel(pht('Active'))
        ->setKey('active')
        ->setOptions(
          pht('(Show All)'),
          pht('Show Only Active Objective'),
          pht('Hide Active Objective')),
    );
  }

  protected function buildQueryFromParameters(array $map)
  {
    $query = $this->newQuery();

    if ($map['active']) {
      $query->withIsArchived(!$map['active']);
    }

    return $query;
  }

  protected function getURI($path)
  {
    $okr_id = explode('/', $_SERVER['REQUEST_URI'])[3];
    return '/hephaestos/obj/' . $okr_id . '/' . $path;
  }

  protected function getBuiltinQueryNames()
  {
    $names = array(
      'active' => pht('Active Objectives'),
      'all' => pht('All Objectives'),
    );

    return $names;
  }

  public function buildSavedQueryFromBuiltin($query_key)
  {
    $query = $this->newSavedQuery();
    $query->setQueryKey($query_key);

    switch ($query_key) {
      case 'active':
        return $query->setParameter('active', true);
      case 'all':
        return $query;
    }

    return parent::buildSavedQueryFromBuiltin($query_key);
  }

  public function setFrequenceString($frequenceTime, $frequence)
  {
    return $frequenceTime == 1 ? pht('Every ' . $frequence) : pht('Every ' . $frequenceTime . ' ' . $frequence . 's');
  }

  protected function renderResultList(
    array $Okrs,
    PhabricatorSavedQuery $query,
    array $handles)
  {
    assert_instances_of($Okrs, 'PhabricatorHephaestosObjective');
    require_celerity_resource('okrs-css');

    $request = $this->getRequest();

    $viewer = $this->requireViewer();
    Javelin::initBehavior('jquery');
    Javelin::initBehavior('edit_kr_success');

    $id = $request->getURIData('id');
    $okr = id(new PhabricatorHephaestosOkrQuery())
      ->setViewer($viewer)
      ->withIDs(array($id))
      ->requireCapabilities(
        array(
          PhabricatorPolicyCapability::CAN_VIEW,
        ))
      ->executeOne();

    $can_edit = PhabricatorPolicyFilter::hasCapability(
      $viewer,
      $okr,
      PhabricatorPolicyCapability::CAN_EDIT);

    $check_icon_can_edit = $can_edit === true ? 'kr-edit' : 'hidden-icon-edit-kr';
    $temp_check_icon = $check_icon_can_edit;
    $objectives = id(new PhabricatorHephaestosObjective())->loadAllWhere('hephaestosPHID = %s', $okr->getPHID());

    $tables = [];

    session_start();
    if (isset($_SESSION['message'])) {
      $save = id(new PHUIInfoView())
        ->setSeverity(PHUIInfoView::SEVERITY_NOTICE)
        ->appendChild($_SESSION['message']);
      $tables[] = $save;
      unset($_SESSION['message']);
    }

    foreach ($objectives as $key => $objective) {
      if ($temp_check_icon !== 'hidden-icon-edit-kr'){
        $check_objective = id(new PhabricatorHephaestosObjectiveQuery())
          ->setViewer($viewer)
          ->withIDs(array($objective->getID()))
          ->requireCapabilities(
            array(
              PhabricatorPolicyCapability::CAN_VIEW,
            ))
          ->executeOne();

        $can_edit_objective = PhabricatorPolicyFilter::hasCapability(
          $viewer,
          $check_objective,
          PhabricatorPolicyCapability::CAN_EDIT);
        $check_icon_can_edit = $can_edit_objective === true ? 'kr-edit' : 'hidden-icon-edit-kr';
      }

      ${"row" . $key} = array();
      $krs = id(new PhabricatorHephaestosKeyResult())->loadAllWhere('objectivePHID = %s', $objective->getPHID());

      if (count($krs) > 0) {

        foreach ($krs as $krID => $kr) {
          $currents = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $kr->getPHID());
          $currents = array_slice($currents, -2);
          $totalCurrents = count($currents);
          $current = empty($currents) ? null : end($currents)->getCheckinValue();
          $previousCurrent = isset($currents[$totalCurrents - 2]) ? $currents[$totalCurrents - 2]->getCheckinValue() : null;
          if ($kr->getProjectPHID() == null || $kr->getManualCheckIn() == 0 ) {
            $baseValue = $kr->getBaseValue();
            $meansure = $kr->getMeansure();
            $process = $this->getProcessFromKr($current, $kr);
            $target = $kr->getTarget();
            $lessIsBest = ($baseValue > $target) ? true : false;

            if (($current === null) && ($previousCurrent === null)){
              $increase = null;
            }elseif (($current !== null) &&($previousCurrent === null)){
              $increase = $current - $previousCurrent - $baseValue;
            }else{
              $increase = $current - $previousCurrent;
            }

            is_null($current) ? $current = $baseValue : '';
          } else {
            if ($kr->getManualCheckin() == 1){
              $valueProjectPoint = $this->getValueProjectPoint($kr);
              $baseValue = 0;
              $meansure = $valueProjectPoint['meansure'];
              $target = $valueProjectPoint['target'];
              $process = ($target != 0) ? ($current / $target) * 100 : NULL;
              $lessIsBest = false;
              $increase = (($current === null) && ($previousCurrent === null)) ? null : $current - $previousCurrent;
            }
            if ($kr->getManualCheckin() == 2){
              $baseValue = 0;
              $target = 100;
              $current = null;
              $processValue = $this->getProcessVelocity($kr);
              $process = $processValue['process'];
              $increase = $processValue['increase'];
              $meansure = 'Velocity';
              $lessIsBest = false;
            }
            if ($kr->getManualCheckin() == 3){
              $baseValue = 0;
              $target = 100;
              $current = null;
              $processValue = $this->getProcessEffective($kr);
              $process = $processValue['process'];
              $increase = $processValue['increase'];
              $meansure = 'Effective Of Develop';
              $lessIsBest = false;
            }
            if ($kr->getManualCheckin() == 4){
              $baseValue = 0;
              $target = 100;
              $current = null;
              $processValue = $this->getProcessHIIHFI($kr, 'hii');
              $process = $processValue['process'];
              $increase = $processValue['increase'];
              $meansure = 'DEBIT Bug Rate';
              $lessIsBest = true;
            }
            if ($kr->getManualCheckin() == 5){
              $baseValue = 0;
              $target = 100;
              $current = null;
              $processValue = $this->getProcessHIIHFI($kr, 'hfi');
              $process = $processValue['process'];
              $increase = $processValue['increase'];
              $meansure = 'Customer Bug Rate';
              $lessIsBest = true;
            }
          }

          ($kr->getConfidenceLevel() == 1) ? $color = 'default' : ($kr->getConfidenceLevel() == 2 ? $color = 'warning' : $color = 'danger');
          ${"row" . $key}[] = [
            phutil_tag(
              'div',
              array(), [
                phutil_tag(
                  'h3',
                  array(),
                  $kr->getTitle()),
                phutil_tag(
                  'kr-edit',
                  array(),
                  id(new PHUIButtonOKRView())
                    ->setTag('a')
                    ->setID($check_icon_can_edit)
                    ->setHref('/hephaestos/kr/edit/' . $krID)
                    ->setIcon('fa-edit')),
                phutil_tag(
                  'kr-detail',
                  array(),
                  id(new PHUIButtonOKRView())
                    ->setTag('a')
                    ->setID('kr-edit')
                    ->setHref('/hephaestos/kr/detail/' . $krID)
                    ->setIcon('fa-eye')),
              ]
            ),
            pht(id(new PhabricatorUser())->loadOneWhere('PHID = %s', $kr->getOwnerPHID())->getUserName()),
            $meansure,
            phutil_tag(
              'div',
              array(), [
                phutil_tag(
                  'kr-content',
                  array(),
                  id(new AphrontFormContentKRControl())
                    ->setValue($process < 0 ? 0 : $process)
                    ->setManualCheckin($kr->getManualCheckin())
                    ->setColor($color)
                    ->setMax(100)
                    ->setCurrent($current)
                    ->setBaseValue($baseValue)
                    ->setTarget($target)
                    ->setCaption(round($process, 1) . '%')
                    ->render()),
                phutil_tag(
                  'kr-edit',
                  array(),
                  id(new AphrontProgressBarKRView())
                    ->setValue($process < 0 ? 0 : $process)
                    ->setIncrease($increase)
                    ->setLessIsBest($lessIsBest)
                    ->setManualCheckin($kr->getManualCheckin())
                    ->setColor($color)
                    ->setMax(100)
                    ->setCaption(round($process, 1) . '%')
                    ->render())
              ]
            ),];
          ${"keys" . $key} = array_keys(id(new PhabricatorHephaestosKeyResult())->loadAllWhere('objectivePHID = %s', $objective->getPHID()));
        }
      }

      ${"table" . $key} = new AphrontTableView(${"row" . $key});
      ${"table" . $key}->setNoDataString(pht('No data available'));
      ${"table" . $key}->setHeaders(
        array(
          phutil_tag(
            'a',
            array('href' => '/hephaestos/obj/edit/' . $objective->getId(), 'style' => 'color:blue'),
            $objective->getTitle()),
          pht('Owner'),
          pht('Description'),
          pht('Status'),
        ));
      ${"table" . $key}->setColumnClasses(
        array(
          'kr',
          'owner',
          'meansure',
          'process',
        ));
      $tables[] = ${"table" . $key};
      $tables[] = id(new PHUIButtonView())
        ->setTag('a')
        ->setText(pht('Create KR'))
        ->setHref('/hephaestos/kr/edit/?objective=' . $objective->getID())
        ->setIcon('fa-plus');

    }

    $result = new PhabricatorApplicationSearchResultView();
    $result->setTable($tables);
    $result->setNoDataString(pht('No Objective found.'));

    return $result;
  }

  protected function getProcessFromKr($current, $kr)
  {
    $baseValue = $kr->getBaseValue();
    $target = $kr->getTarget();
    $process = null;
    $current = ($current === null) ? $baseValue : $current;
    if ($baseValue < $target) {
      if ($target != 0) {
      $process = (($current - $baseValue) / ($target - $baseValue)) * 100;
      }
    } else {
      if (($baseValue - $target) != 0) {
      $process = (($baseValue - $current) / ($baseValue - $target)) * 100;
      }
    }
    return $process;
  }
  protected function getProcessEffective($kr){

    $processEffective = null;
    $increase = 0;
    $totalEffective = 0;
    $currents = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $kr->getPHID());
    $currents = $this->filterArrayCurrentTableEffective($currents);
    $totalCurrents = count($currents);
    foreach($currents as $current){
      $totalEffective +=  (float)$current->getCheckinValue();
    }
    if($totalCurrents > 0){
      $processEffective = round($totalEffective / $totalCurrents, 1);
    }

    $currentsSlice = array_slice($currents, -2);
    $totalCurrentsSlice = count($currentsSlice);
    $current = empty($currentsSlice) ? null : end($currentsSlice)->getCheckinValue();

    $previousCurrent = isset($currentsSlice[$totalCurrentsSlice - 2]) ? $currentsSlice[$totalCurrentsSlice - 2]->getCheckinValue() : null;
    $increase = (float)$current - (float)$previousCurrent;

    return ['process' => $processEffective, 'increase' => $increase];
  }

  private function filterArrayCurrentTableEffective($arrayCurrentTable){
    $arrayFilter = [];
    foreach($arrayCurrentTable as $current){
      if($current->getEffective() != null){
        $arrayFilter[] = $current;
      }
    }
    return $arrayFilter;
  }

  protected function getProcessVelocity($kr){
    $process = null;
    $increase = null;
    $data = id(new PhabricatorHephaestosKrEditController())
              ->setRequest($this->getRequest())
              ->getVelocityChartData($kr);
    if(count($data['value'] >= 2)){
      if($data['value'][1] != 0) {
        $process = ($data['value'][0] / $data['value'][1]) * 100;
        $increase = $data['value'][0] - $data['value'][1];
      }
    }
    return ['process' => $process, 'increase' => $increase];
  }

  protected function getProcessHIIHFI($kr, $type){
    $process = null;
    $increase = null;

    $arrayHII = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $kr->getPHID());
    $data = array();
    if($type == 'hii') {
      foreach ($arrayHII as $key => $value) {
        if(!$value->getHII()) {
          continue;
        } else {
          $data[] = $value->getCheckinValue();
        }
      }
    } else if($type == 'hfi') {
      foreach ($arrayHII as $key => $value) {
        if(!$value->getHFI()) {
          continue;
        } else {
          $data[] = $value->getCheckinValue();
        }
      }
    }


    if (count($data) >= 2) {
      if($data[count($data) - 2] == 0) {
        $process = 0;
      } else {
        $process = $data[count($data) - 1] / $data[count($data) - 2] * 100;
      }
      $increase = $data[count($data) - 1] - $data[count($data) - 2];
    }

    return ['process' => $process, 'increase' => $increase];
  }

  protected function getValueProjectPoint($kr){
    $viewer = $this->requireViewer();
    $project = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $kr->getProjectPHID());
    $state = id(new PhabricatorOkrsViewState())
      ->setProject($project)
      ->readFromRequest($this->getRequest());
    $tasks = $state->getObjects();
    $meansure = id(new PHUIListView());
    $totalPoint = 0;
    foreach ($tasks as $taskID => $task) {
      $graph_limit = 200;
      $task_graph = id(new ManiphestTaskGraph())
        ->setViewer($viewer)
        ->setSeedPHID($taskID)
        ->setLimit($graph_limit)
        ->loadGraph();
      $parent_type = ManiphestTaskDependedOnByTaskEdgeType::EDGECONST;
      $parent_map = $task_graph->getEdges($parent_type);
      $parent_list = idx($parent_map, $task->getPHID(), array());
      $has_parents = (bool)$parent_list;

      if (!$has_parents) {
        $item = id(new PHUIListItemView())
          ->setName(" " . $task->getTitle())
          ->setType(PHUIListItemView::TYPE_LINK)
          ->setIcon(PHUIStatusItemView::ICON_MINUS);
        $meansure = $meansure->addMenuItem($item);
      }

      if ($task->getPoints() !== null) {
        $totalPoint += $task->getPoints();
      }
    }
    $target = $totalPoint;
    return ['target' => $target, 'meansure' => $meansure];
  }

  protected function getNewUserBody()
  {
    require_celerity_resource('okrs-css');
    $request = $this->getRequest();
    $id = $request->getURIData('id');
    $create_button = id(new PHUIButtonView())
      ->setTag('a')
      ->setText(pht('Create a Objective'))
      ->setHref('/hephaestos/obj/edit/?okr=' . $id)
      ->setColor(PHUIButtonView::GREEN);

    $icon = $this->getApplication()->getIcon();
    $app_name = $this->getApplication()->getName();

    session_start();
    if (isset($_SESSION['message'])) {
      $view[] = id(new PHUIInfoView())
        ->setSeverity(PHUIInfoView::SEVERITY_NOTICE)
        ->appendChild($_SESSION['message']);
      unset($_SESSION['message']);
    }
    $view[] = id(new PHUIBigInfoView())
      ->appendChild($_SESSION['message'])
      ->setIcon($icon)
      ->setDescription(
        pht('Make a Objective'))
      ->addAction($create_button);

    return $view;
  }

}
