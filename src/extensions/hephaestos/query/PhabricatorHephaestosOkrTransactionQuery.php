<?php

final class PhabricatorHephaestosOkrTransactionQuery
    extends PhabricatorApplicationTransactionQuery {

    public function getTemplateApplicationTransaction() {
        return new PhabricatorHephaestosOkrTransaction();
    }

}
