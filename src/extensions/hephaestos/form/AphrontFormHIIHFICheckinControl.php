<?php
//
//final class AphrontFormHIIHFICheckinControl extends AphrontFormControl
//{
//
//  private $disableAutocomplete;
//  private $sigil;
//  private $placeholder;
//  private $autofocus;
//  private $readonly;
//  private $totalBug;
//  private $type;
//
//  public function setDisableAutocomplete($disable)
//  {
//    $this->disableAutocomplete = $disable;
//    return $this;
//  }
//
//  private function getDisableAutocomplete()
//  {
//    return $this->disableAutocomplete;
//  }
//
//  public function getPlaceholder()
//  {
//    return $this->placeholder;
//  }
//
//  public function setPlaceholder($placeholder)
//  {
//    $this->placeholder = $placeholder;
//    return $this;
//  }
//
//  public function getTotalBug()
//  {
//    return $this->totalBug;
//  }
//
//  public function setTotalBug($totalBug)
//  {
//    $this->totalBug = $totalBug;
//    return $this;
//  }
//
//  public function setAutofocus($autofocus)
//  {
//    $this->autofocus = $autofocus;
//    return $this;
//  }
//
//  public function getAutofocus()
//  {
//    return $this->autofocus;
//  }
//
//  public function getSigil()
//  {
//    return $this->sigil;
//  }
//
//  public function setSigil($sigil)
//  {
//    $this->sigil = $sigil;
//    return $this;
//  }
//
//  public function getType()
//  {
//    return $this->type;
//  }
//
//  public function setType($type)
//  {
//    $this->type = $type;
//    return $this;
//  }
//
//  public function getReadonly()
//  {
//    return $this->readonly;
//  }
//
//  public function setReadonly($readonly)
//  {
//    $this->readonly = $readonly;
//    return $this;
//  }
//
//  protected function getCustomControlClass()
//  {
//    return 'aphront-form-control-text';
//  }
//
//  protected function renderInput()
//  {
//    return javelin_tag(
//      'div',
//      array(
//        'style' => 'display:flex; flex-wrap: wrap; margin-left: 65px; margin-top: 10px; padding-bottom: 25px'
//      ),
//      [
//        javelin_tag(
//        'p',
//        array(
//          'style' => 'padding-right: 10px; margin-top: 6px'
//        ),
//        ($this->type == 4) ? 'HII' : 'HFI'),
//        javelin_tag(
//          'p',
//          array(
//            'id' => 'totalBug',
//            'style' => 'margin-top: 6px'
//          ),
//          $this->getTotalBug()),
//        javelin_tag(
//          'p',
//          array(
//            'style' => 'padding-left: 5px; padding-right: 5px; margin-top: 6px'
//          ),
//          ' Bug(s) '.'/'),
//        javelin_tag(
//          'input',
//          array(
//            'style' => 'width:50px',
//            'type' => 'number',
//            'step' => '0.5',
//            'name' => $this->getName(),
//            'value' => $this->getValue(),
//            'disabled' => $this->getDisabled() ? 'disabled' : null,
//            'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
//            'id' => $this->getID(),
//            'sigil' => $this->getSigil(),
//            'placeholder' => $this->getPlaceholder(),
//            'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
//          )
//        ),javelin_tag(
//        'p',
//        array(
//          'style' => 'padding-left: 5px; padding-right: 5px; margin-top: 6px'
//        ),
//        'Dev(s)'),
//        javelin_tag(
//          'p',
//          array(
//            'style' => 'position: absolute; margin-top: 35px; margin-right: 50px; color: red',
//            'id' => 'error-checkin'
//          ),
//          '(Dev must be greater than 0 and valid. Example: 0.5 or 1)'),
//      ]
//    );
//  }
//
//}
