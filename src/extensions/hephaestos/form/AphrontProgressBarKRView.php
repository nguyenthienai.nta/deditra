<?php
//
//final class AphrontProgressBarKRView extends AphrontBarView {
//
//  const WIDTH = 100;
//
//  private $value;
//  private $max = 100;
//  private $alt = '';
//  private $increase = null;
//  private $lessIsBest = false;
//  private $manualCheckIn;
//
//  protected function getDefaultColor() {
//    return parent::COLOR_AUTO_BADNESS;
//  }
//
//  public function setManualCheckin($manualCheckIn){
//    $this->manualCheckIn = $manualCheckIn;
//
//    return $this;
//  }
//
//  public function setValue($value) {
//    $this->value = $value;
//    return $this;
//  }
//
//  public function setIncrease($value) {
//    $this->increase = $value;
//    return $this;
//  }
//
//  public function getValue(){
//    return $this->value;
//  }
//
//  public function getIncrease(){
//    return $this->increase;
//  }
//
//  public function setLessIsBest($value) {
//    $this->lessIsBest = $value;
//    return $this;
//  }
//
//  public function getLessIsBest(){
//    return $this->lessIsBest;
//  }
//
//  public function getManualCheckin(){
//    return $this->manualCheckIn;
//  }
//
//  public function setMax($max) {
//    $this->max = $max;
//    return $this;
//  }
//
//  public function setAlt($text) {
//    $this->alt = $text;
//    return $this;
//  }
//
//  protected function getRatio() {
//    return min($this->value, $this->max) / $this->max;
//  }
//
//  public function render() {
//    require_celerity_resource('aphront-bars');
//    $ratio = $this->getRatio();
//    $width = self::WIDTH * $ratio;
//    $color = $this->getColor();
//    $hidden = true;
//    $hiddenProcess = true;
//    $containManualCheckIns = ['1', '2', '3','4', '5'];
//
//    if (in_array($this->getManualCheckin(), $containManualCheckIns)){
//      $hiddenProcess = $this->getValue() === null ? 'hiddenProcess' : '';
//    }
//
//    $increase = $this->getIncrease();
//    $icon = 'fa-arrow-up';
//    $colorIcon = $this->getLessIsBest() ? 'red' : 'green';
//    if ($increase === null){
//      $hidden = 'none';
//    }
//    if ($increase == 0){
//      $colorIcon = 'grey';
//    }
//    if ($increase < 0){
//      $increase = abs($this->getIncrease());
//      $icon = 'fa-arrow-down';
//      $colorIcon = $this->getLessIsBest() ? 'green' : 'red';
//    }
//
//    return phutil_tag_div(
//      "aphront-bar progress color-{$color} {$hiddenProcess} ",
//      array(
//        phutil_tag(
//          'div',
//          array('title' => $this->alt),
//          phutil_tag(
//            'div',
//            array('style' => "width: {$width}px;"),
//            '')),
//        phutil_tag(
//          'span',
//          array('style' => "float: right;"),
//          $this->getCaption()),
//        phutil_tag(
//          'span',
//          array('style' => "float: left; color : $colorIcon; padding-top : 2px; display:$hidden", "class" => "visual-only phui-icon-view phui-font-fa phui-list-item-icon $icon"),
//          null),
//        phutil_tag(
//          'span',
//          array('style' => "float: left; margin-left : 1px"),
//          round($increase, 1)),
//        ));
//  }
//
//}
