<?php
//
//final class AphrontFormConfidentControl extends AphrontFormControl
//{
//
//  private $disableAutocomplete;
//  private $sigil;
//  private $placeholder;
//  private $autofocus;
//
//  public function setDisableAutocomplete($disable)
//  {
//    $this->disableAutocomplete = $disable;
//    return $this;
//  }
//
//  private function getDisableAutocomplete()
//  {
//    return $this->disableAutocomplete;
//  }
//
//  public function getPlaceholder()
//  {
//    return $this->placeholder;
//  }
//
//  public function setPlaceholder($placeholder)
//  {
//    $this->placeholder = $placeholder;
//    return $this;
//  }
//
//  public function setAutofocus($autofocus)
//  {
//    $this->autofocus = $autofocus;
//    return $this;
//  }
//
//  public function getAutofocus()
//  {
//    return $this->autofocus;
//  }
//
//  public function getSigil()
//  {
//    return $this->sigil;
//  }
//
//  public function setSigil($sigil)
//  {
//    $this->sigil = $sigil;
//    return $this;
//  }
//
//  protected function getCustomControlClass()
//  {
//    return 'aphront-form-control-text';
//  }
//
//  protected function renderInput()
//  {
//    return javelin_tag(
//      'div',
//      array(
//        'class' => 'txtSuccessRule confident-scope'
//      ),
//      [
//        javelin_tag(
//          'div',
//          array(
//            'style' => 'padding-right:15px'
//          ),
//          [
//            phutil_tag(
//              'p',
//              array(
//                'id' => 'yellow-1',
//                'style' => 'position:relative; top:50%;',
//              ),
//              'Green'
//            ),
//          ]
//        ),
//        javelin_tag(
//          'div',
//          array(
//            'style' => 'padding: 0px 2px 0px 20px'
//          ),
//          [
//            phutil_tag(
//              'p',
//              array(
//                'style' => 'color:#FFD700; text-align:center; margin-right:12px'
//              ),
//              'Yellow'
//            ),
//            javelin_tag(
//              'div',
//              array(),[
//            phutil_tag(
//              'input',
//              array(
//                'style' => '',
//                'type' => 'number',
//                'name' => $this->getName(),
//                'value' => $this->getValue()[0],
//                'disabled' => $this->getDisabled() ? 'disabled' : null,
//                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
//                'id' => $this->getID(),
//                'class' => 'point scope1',
//                'sigil' => $this->getSigil(),
//                'placeholder' => $this->getPlaceholder(),
//                'step' => 0.01,
//                'min' => 0,
//                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
//              )),
//            phutil_tag(
//              'span',
//              array(
//                'class' => 'point',
//                'style' => 'padding-right:10px'
//              ),
//              '-'
//            ),
//            phutil_tag(
//              'input',
//              array(
//                'style' => '',
//                'type' => 'number',
//                'name' => $this->getName(),
//                'value' => $this->getValue()[1],
//                'disabled' => $this->getDisabled() ? 'disabled' : null,
//                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
//                'id' => $this->getID(),
//                'class' => 'point scope2',
//                'sigil' => $this->getSigil(),
//                'placeholder' => $this->getPlaceholder(),
//                'step' => 0.01,
//                'min' => 0,
//                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
//              ))]
//        ),]),
//        javelin_tag(
//          'div',
//          array(
//            'style' => 'padding-left:15px',
//          ),
//          [
//            phutil_tag(
//              'p',
//              array(
//                'id' => 'yellow-2',
//                'style' => 'position: relative; top:50%;',
//              ),
//              'Red'
//            ),
//            phutil_tag(
//              'p',
//              array()),
//          ]
//        ),
////        javelin_tag(
////          'div',
////          array(),
////          [
////            phutil_tag(
////              'p',
////              array(
////                'style' => 'color:red',
////              ),
////              'Red'
////            ),
////            phutil_tag(
////              'input',
////              array(
////                'style' => 'border:1px solid red',
////                'type' => 'number',
////                'name' => $this->getName(),
////                'value' => $this->getValue()[2],
////                'disabled' => $this->getDisabled() ? 'disabled' : null,
////                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
////                'id' => $this->getID(),
////                'class' => 'point',
////                'sigil' => $this->getSigil(),
////                'placeholder' => $this->getPlaceholder(),
////                'step' => 0.01,
////                'min' => 0,
////                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
////              )),
////            phutil_tag(
////              'span',
////              array(
////                'class' => 'point',
////                'style' => 'color:red'
////              ),
////              '->'
////            ),
////          ]
////        ),
////        javelin_tag(
////          'div',
////          array(),
////          [
////            phutil_tag(
////              'p',
////              array(
////                'style' => 'color:red'
////              ),
////              '.'
////            ),
////            phutil_tag(
////              'input',
////              array(
////                'style' => 'border:1px solid red',
////                'type' => 'number',
////                'name' => $this->getName(),
////                'value' => $this->getValue()[3],
////                'disabled' => $this->getDisabled() ? 'disabled' : null,
////                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
////                'id' => $this->getID(),
////                'class' => 'point',
////                'sigil' => $this->getSigil(),
////                'placeholder' => $this->getPlaceholder(),
////                'step' => 0.01,
////                'min' => 0,
////                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
////              )),
////            phutil_tag(
////              'span',
////              array(
////                'class' => 'point'
////              ),
////              ''
////            ),
////          ]
////        ),
//      ]);
//  }
//
//}
