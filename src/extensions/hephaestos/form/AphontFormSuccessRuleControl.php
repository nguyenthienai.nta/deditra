<?php
//
//final class AphrontFormSuccessRuleControl extends AphrontFormControl
//{
//
//  private $disableAutocomplete;
//  private $sigil;
//  private $placeholder;
//  private $autofocus;
//
//  public function setDisableAutocomplete($disable)
//  {
//    $this->disableAutocomplete = $disable;
//    return $this;
//  }
//
//  private function getDisableAutocomplete()
//  {
//    return $this->disableAutocomplete;
//  }
//
//  public function getPlaceholder()
//  {
//    return $this->placeholder;
//  }
//
//  public function setPlaceholder($placeholder)
//  {
//    $this->placeholder = $placeholder;
//    return $this;
//  }
//
//  public function setAutofocus($autofocus)
//  {
//    $this->autofocus = $autofocus;
//    return $this;
//  }
//
//  public function getAutofocus()
//  {
//    return $this->autofocus;
//  }
//
//  public function getSigil()
//  {
//    return $this->sigil;
//  }
//
//  public function setSigil($sigil)
//  {
//    $this->sigil = $sigil;
//    return $this;
//  }
//
//  protected function getCustomControlClass()
//  {
//    return 'aphront-form-control-text';
//  }
//
//  protected function renderInput()
//  {
//    return javelin_tag(
//      'div',
//      array(
//        'class' => 'txtSuccessRule'
//      ),
//      [
//        javelin_tag(
//          'div',
//          array(),
//          [
//            phutil_tag(
//              'p',
//              array(),
//              '1 Point'
//            ),
//            phutil_tag(
//              'input',
//              array(
//                'type' => 'number',
//                'name' => $this->getName(),
//                'value' => $this->getValue()[0],
//                'disabled' => $this->getDisabled() ? 'disabled' : null,
//                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
//                'id' => $this->getID(),
//                'class' => 'point',
//                'sigil' => $this->getSigil(),
//                'placeholder' => $this->getPlaceholder(),
//                'step' => 0.01,
//                'min' => 0,
//                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
//              )),
//            phutil_tag(
//              'span',
//              array(
//                'class' => 'point'
//              ),
//              '%'
//            )
//          ]
//        ),
//        javelin_tag(
//          'div',
//          array(),
//          [
//            phutil_tag(
//              'p',
//              array(),
//              '2 Points'
//            ),
//            phutil_tag(
//              'input',
//              array(
//                'type' => 'number',
//                'name' => $this->getName(),
//                'value' => $this->getValue()[1],
//                'disabled' => $this->getDisabled() ? 'disabled' : null,
//                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
//                'id' => $this->getID(),
//                'class' => 'point',
//                'sigil' => $this->getSigil(),
//                'placeholder' => $this->getPlaceholder(),
//                'step' => 0.01,
//                'min' => 0,
//                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
//              )),
//            phutil_tag(
//              'span',
//              array(
//                'class' => 'point'
//              ),
//              '%'
//            ),
//          ]
//        ),
//        javelin_tag(
//          'div',
//          array(),
//          [
//            phutil_tag(
//              'p',
//              array(),
//              '3 Points'
//            ),
//            phutil_tag(
//              'input',
//              array(
//                'type' => 'number',
//                'name' => $this->getName(),
//                'value' => $this->getValue()[2],
//                'disabled' => $this->getDisabled() ? 'disabled' : null,
//                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
//                'id' => $this->getID(),
//                'class' => 'point',
//                'sigil' => $this->getSigil(),
//                'placeholder' => $this->getPlaceholder(),
//                'step' => 0.01,
//                'min' => 0,
//                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
//              )),
//            phutil_tag(
//              'span',
//              array(
//                'class' => 'point'
//              ),
//              '%'
//            ),
//          ]
//        ),
//        javelin_tag(
//          'div',
//          array(),
//          [
//            phutil_tag(
//              'p',
//              array(),
//              '4 Points'
//            ),
//            phutil_tag(
//              'input',
//              array(
//                'type' => 'number',
//                'name' => $this->getName(),
//                'value' => $this->getValue()[3],
//                'disabled' => $this->getDisabled() ? 'disabled' : null,
//                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
//                'id' => $this->getID(),
//                'class' => 'point',
//                'sigil' => $this->getSigil(),
//                'placeholder' => $this->getPlaceholder(),
//                'step' => 0.01,
//                'min' => 0,
//                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
//              )),
//            phutil_tag(
//              'span',
//              array(
//                'class' => 'point'
//              ),
//              '%'
//            ),
//          ]
//        ),
//        javelin_tag(
//          'div',
//          array(),
//          [
//            phutil_tag(
//              'p',
//              array(),
//              '5 Points'
//            ),
//            phutil_tag(
//              'input',
//              array(
//                'type' => 'number',
//                'name' => $this->getName(),
//                'value' => $this->getValue()[4],
//                'disabled' => $this->getDisabled() ? 'disabled' : null,
//                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
//                'id' => $this->getID(),
//                'class' => 'point',
//                'sigil' => $this->getSigil(),
//                'placeholder' => $this->getPlaceholder(),
//                'step' => 0.01,
//                'min' => 0,
//                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
//              )),
//            phutil_tag(
//              'span',
//              array(
//                'class' => 'point'
//              ),
//              '%'
//            ),
//          ]
//        ),
//        javelin_tag(
//          'div',
//          array(),
//          [
//            phutil_tag(
//              'p',
//              array(),
//              '6 Points'
//            ),
//            phutil_tag(
//              'input',
//              array(
//                'type' => 'number',
//                'name' => $this->getName(),
//                'value' => $this->getValue()[5],
//                'disabled' => $this->getDisabled() ? 'disabled' : null,
//                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
//                'id' => $this->getID(),
//                'class' => 'point',
//                'sigil' => $this->getSigil(),
//                'placeholder' => $this->getPlaceholder(),
//                'step' => 0.01,
//                'min' => 0,
//                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
//              )),
//            phutil_tag(
//              'span',
//              array(
//                'class' => 'point'
//              ),
//              '%'
//            ),
//          ]
//        ),
//        javelin_tag(
//          'div',
//          array(),
//          [
//            phutil_tag(
//              'p',
//              array(),
//              '7 Points'
//            ),
//            phutil_tag(
//              'input',
//              array(
//                'type' => 'number',
//                'name' => $this->getName(),
//                'value' => $this->getValue()[6],
//                'disabled' => $this->getDisabled() ? 'disabled' : null,
//                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
//                'id' => $this->getID(),
//                'class' => 'point',
//                'sigil' => $this->getSigil(),
//                'placeholder' => $this->getPlaceholder(),
//                'step' => 0.01,
//                'min' => 0,
//                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
//              )),
//            phutil_tag(
//              'span',
//              array(
//                'class' => 'point'
//              ),
//              '%'
//            ),
//          ]
//        ),
//        javelin_tag(
//          'div',
//          array(),
//          [
//            phutil_tag(
//              'p',
//              array(),
//              '8 Points'
//            ),
//            phutil_tag(
//              'input',
//              array(
//                'type' => 'number',
//                'name' => $this->getName(),
//                'value' => $this->getValue()[7],
//                'disabled' => $this->getDisabled() ? 'disabled' : null,
//                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
//                'id' => $this->getID(),
//                'class' => 'point',
//                'sigil' => $this->getSigil(),
//                'placeholder' => $this->getPlaceholder(),
//                'step' => 0.01,
//                'min' => 0,
//                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
//              )),
//            phutil_tag(
//              'span',
//              array(
//                'class' => 'point'
//              ),
//              '%'
//            ),
//          ]
//        ),
//        javelin_tag(
//          'div',
//          array(),
//          [
//            phutil_tag(
//              'p',
//              array(),
//              '9 Points'
//            ),
//            phutil_tag(
//              'input',
//              array(
//                'type' => 'number',
//                'name' => $this->getName(),
//                'value' => $this->getValue()[8],
//                'disabled' => $this->getDisabled() ? 'disabled' : null,
//                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
//                'id' => $this->getID(),
//                'class' => 'point',
//                'sigil' => $this->getSigil(),
//                'placeholder' => $this->getPlaceholder(),
//                'step' => 0.01,
//                'min' => 0,
//                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
//              )),
//            phutil_tag(
//              'span',
//              array(
//                'class' => 'point'
//              ),
//              '%'
//            ),
//          ]
//        ),
//        javelin_tag(
//          'div',
//          array(),
//          [
//            phutil_tag(
//              'p',
//              array(),
//              '10 Points'
//            ),
//            phutil_tag(
//              'input',
//              array(
//                'type' => 'number',
//                'name' => $this->getName(),
//                'value' => $this->getValue()[9],
//                'disabled' => $this->getDisabled() ? 'disabled' : null,
//                'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
//                'id' => $this->getID(),
//                'class' => 'point',
//                'sigil' => $this->getSigil(),
//                'placeholder' => $this->getPlaceholder(),
//                'step' => 0.01,
//                'min' => 0,
//                'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
//              )),
//            phutil_tag(
//              'span',
//              array(
//                'class' => 'point'
//              ),
//              '%'
//            ),
//          ]
//        ),
//      ]);
//  }
//
//}
