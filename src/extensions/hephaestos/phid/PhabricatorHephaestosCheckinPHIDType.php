<?php

final class PhabricatorHephaestosCheckinPHIDType extends PhabricatorPHIDType {

    const TYPECONST = 'HCIN';

    public function getTypeName() {
        return pht('Checkin');
    }

    public function newObject() {
        return new PhabricatorHephaestosCheckin();
    }

    public function getPHIDTypeApplicationClass() {
        return 'PhabricatorHephaestosApplication';
    }

    protected function buildQueryForObjects(
        PhabricatorObjectQuery $query,
        array $phids) {

        return id(new PhabricatorHephaestosCheckinQuery())
            ->withPHIDs($phids);
    }

    public function loadHandles(
        PhabricatorHandleQuery $query,
        array $handles,
        array $objects) {

        foreach ($handles as $phid => $handle) {
            $current = $objects[$phid];
            $handle->setName($current->getCheckinValue());
            $handle->setFullName(pht('Kr: ').$current->getCheckinValue());
            $handle->setURI('/kr'.$current->getID());
        }

    }

}
