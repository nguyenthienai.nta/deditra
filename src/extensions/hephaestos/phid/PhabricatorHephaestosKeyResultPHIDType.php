<?php

final class PhabricatorHephaestosKeyResultPHIDType extends PhabricatorPHIDType {

    const TYPECONST = 'HKRS';

    public function getTypeName() {
        return pht('Kr ');
    }

    public function newObject() {
        return new PhabricatorHephaestosKeyResult();
    }

    public function getPHIDTypeApplicationClass() {
        return 'PhabricatorHephaestosApplication';
    }

    protected function buildQueryForObjects(
        PhabricatorObjectQuery $query,
        array $phids) {

        return id(new PhabricatorHephaestosKeyResultQuery())
            ->withPHIDs($phids);
    }

    public function loadHandles(
        PhabricatorHandleQuery $query,
        array $handles,
        array $objects) {

        foreach ($handles as $phid => $handle) {
            $keyResult = $objects[$phid];
            $handle->setName($keyResult->getTitle());
            $handle->setFullName(pht('Kr: ').$keyResult->getTitle());
            $handle->setURI('/kr'.$keyResult->getID());
        }

    }

}
