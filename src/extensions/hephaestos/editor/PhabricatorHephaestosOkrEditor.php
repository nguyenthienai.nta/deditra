<?php

final class PhabricatorHephaestosOkrEditor
    extends PhabricatorApplicationTransactionEditor
{

    public function getEditorApplicationClass()
    {
        return pht('PhabricatorHephaestosApplication');
    }

    public function getEditorObjectsDescription()
    {
        return pht('hephaestos');
    }

    public function getTransactionTypes()
    {
        $types = parent::getTransactionTypes();

        $types[] = PhabricatorTransactions::TYPE_VIEW_POLICY;
        $types[] = PhabricatorTransactions::TYPE_EDIT_POLICY;

        return $types;
    }

    public function getCreateObjectTitle($author, $object)
    {
        return pht('%s created this hephaestos.', $author);
    }

    public function getCreateObjectTitleForFeed($author, $object)
    {
        return pht('%s created hephaestos %s.', $author, $object);
    }

}
