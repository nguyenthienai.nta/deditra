<?php

final class PhabricatorHephaestosKeyResultEditor
    extends PhabricatorApplicationTransactionEditor
{

    public function getEditorApplicationClass()
    {
        return pht('PhabricatorHephaestosApplication');
    }

    public function getEditorObjectsDescription()
    {
        return pht('hephaestos.keyresult');
    }

    public function getTransactionTypes()
    {
        $types = parent::getTransactionTypes();

        $types[] = PhabricatorTransactions::TYPE_VIEW_POLICY;
        $types[] = PhabricatorTransactions::TYPE_EDIT_POLICY;

        return $types;
    }

    public function getCreateObjectTitle($author, $object)
    {
        return pht('%s created this kr.', $author);
    }

    public function getCreateObjectTitleForFeed($author, $object)
    {
        return pht('%s created kr %s.', $author, $object);
    }

  protected function validateAllTransactions(
    PhabricatorLiskDAO $object,
    array $xactions) {

    $errors = parent::validateAllTransactions($object, $xactions);

    $start_date_xaction =
      PhabricatorHephaestosKeyResultStartDateTransaction::TRANSACTIONTYPE;
    $end_date_xaction =
      PhabricatorHephaestosKeyResultEndDateTransaction::TRANSACTIONTYPE;
    $day_offs_xaction =
      PhabricatorHephaestosKeyResultDayOffsTransaction::TRANSACTIONTYPE;
    $type_baseValue =
      PhabricatorHephaestosKeyResultBaseValueTransaction::TRANSACTIONTYPE;
    $type_target =
      PhabricatorHephaestosKeyResultTargetTransaction::TRANSACTIONTYPE;
    $type_manualCheckin =
      PhabricatorHephaestosKeyResultManualCheckinTransaction::TRANSACTIONTYPE;
    $type_project =
      PhabricatorHephaestosKeyResultProjectTransaction::TRANSACTIONTYPE;
    $type_measure =
      PhabricatorHephaestosKeyResultMeansureTransaction::TRANSACTIONTYPE;
    $type_totalDev =
      PhabricatorHephaestosKeyResultTotalDevTransaction::TRANSACTIONTYPE;
    $type_frequenceTime =
      PhabricatorHephaestosKeyResultFrequenceTimeTransaction::TRANSACTIONTYPE;
    $type_isAutoCheckin =
      PhabricatorHephaestosKeyResultIsAutoCheckinTransaction::TRANSACTIONTYPE;

    $start_date = $object->getStartDate();
    $end_date = $object->getEndDate();
    //$projectPHID = $object->getProjectPHID();
    $day_offs = $object->getDayOffs();
    $manualCheckin = $object->getManualCheckin();
    $target = $object->getTarget();
    $baseValue = $object->getBaseValue();
    $measure = $object->getMeansure();
    $totalDev = $object->getTotalDev();
    $checkHII = false;
    $checkHFI = false;
    $arrayCurrent = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $object->getPHID());
    foreach ($arrayCurrent as $key => $value) {
      if(($value->getHII() !== null) && ($value->getTotalDev() > 0)) {
        $checkHII = true;
      }
      if(($value->getHFI() !== null) && ($value->getTotalDev() > 0)) {
        $checkHFI = true;
      }
    }

    foreach ($xactions as $xaction) {
      if ($xaction->getTransactionType() == $start_date_xaction) {
        $start_date = $xaction->getNewValue();
      } else if ($xaction->getTransactionType() == $end_date_xaction) {
        $end_date = $xaction->getNewValue();
      } else if ($xaction->getTransactionType() == $type_target) {
        $target = $xaction->getNewValue();
      } else if ($xaction->getTransactionType() == $type_baseValue) {
        $baseValue = $xaction->getNewValue();
      } else if ($xaction->getTransactionType() == $type_manualCheckin) {
        $manualCheckin = $xaction->getNewValue();
      } else if ($xaction->getTransactionType() == $type_measure) {
        $measure = $xaction->getNewValue();
      } else if ($xaction->getTransactionType() == $day_offs_xaction) {
        $day_offs = $xaction->getNewValue();
      } else if ($xaction->getTransactionType() == $type_totalDev) {
        $totalDev = $xaction->getNewValue();
      } else if ($xaction->getTransactionType() == $type_isAutoCheckin) {
        $isAutoCheckin = $xaction->getNewValue();
      }

      if($xaction->getTransactionType() == $type_project) {
        $projectPHID = $xaction->getNewValue();
        $projectPHIDs = [];
        if($projectPHID != null){
          $projectPHIDs = explode(',',$projectPHID);
        }
      }
    }

    if($manualCheckin == 0){
      if($measure == null){
        $errors[] = new PhabricatorApplicationTransactionValidationError(
          $type_measure,
          pht('Required'),
          pht('KR must have a description.'),
          null);
      }
      if($target == null){
        $errors[] = new PhabricatorApplicationTransactionValidationError(
          $type_target,
          pht('Required'),
          pht('KR must have a target value.'),
          null);
      }
      if($baseValue == null){
        $errors[] = new PhabricatorApplicationTransactionValidationError(
          $type_baseValue,
          pht('Required'),
          pht('KR must have a base value.'),
          null);
      }
      if($baseValue == $target){
        $errors[] = new PhabricatorApplicationTransactionValidationError(
          $type_baseValue,
          pht('Invalid'),
          pht('Base value and target value must be different.'),
          null);
        $errors[] = new PhabricatorApplicationTransactionValidationError(
          $type_target,
          pht('Invalid'), null, null);
      }

    } else {
      if(count($projectPHIDs) == 0) {
        $errors[] = new PhabricatorApplicationTransactionValidationError(
          $type_project,
          pht('Required'),
          pht('KR must have a project.'),
          null);

        if (($manualCheckin == 4 || $manualCheckin == 5) && ($isAutoCheckin == 1)) {
          if (($manualCheckin == 4 && $checkHII === false) || ($manualCheckin == 5 && $checkHFI === false)) {
            if ($totalDev == null) {
              $errors[] = new PhabricatorApplicationTransactionValidationError(
                $type_frequenceTime,
                pht('Required'),
                pht('KR must have a dev total.'),
                null);
            } else if ($totalDev < 0.5) {
              $errors[] = new PhabricatorApplicationTransactionValidationError(
                $type_frequenceTime,
                pht('Invalid'),
                pht('Dev must be greater than 0 and valid. Example: 0.5 or 1.'),
                null);
            }
          } else if ($totalDev !== 0) {
            $value = (float)$totalDev * 2;
            if ((strval($value) !== strval(intval($value))) || ($totalDev < 0.5)) {
              $errors[] = new PhabricatorApplicationTransactionValidationError(
                $type_frequenceTime,
                pht('Invalid'),
                pht('Dev must be greater than 0 and valid. Example: 0.5 or 1.'),
                null);
            }
          }
        }

      } else {
        switch ($manualCheckin) {
          case 1:
            if (($projectPHIDs[0] !== $object->getProjectPHID()) && (!id(new PhabricatorProject())->loadOneWhere('phid = %s', $projectPHIDs[0])->getStartDate())) {
              $errors[] = new PhabricatorApplicationTransactionValidationError(
                $type_project,
                pht('Invalid'),
                pht('Project must be sprint.'),
                null);
            };
          case 2:
              if(count($projectPHIDs) > 1){
                $errors[] = new PhabricatorApplicationTransactionValidationError(
                  $type_project,
                  pht('Invalid'),
                  pht('Project cannot be greater than 1.'),
                  null);
              }
            break;
          case 3:
            break;
          case 4:
          case 5:
            if (($manualCheckin == 4 || $manualCheckin == 5) && ($isAutoCheckin == 1)) {
              if (($manualCheckin == 4 && $checkHII === false && count($projectPHIDs) === 1) || ($manualCheckin == 5 && $checkHFI === false && count($projectPHIDs) === 1)) {
                if ($totalDev == null) {
                  $errors[] = new PhabricatorApplicationTransactionValidationError(
                    $type_frequenceTime,
                    pht('Required'),
                    pht('KR must have a dev total.'),
                    null);
                } else if ($totalDev < 0.5) {
                  $errors[] = new PhabricatorApplicationTransactionValidationError(
                    $type_frequenceTime,
                    pht('Invalid'),
                    pht('Dev must be greater than 0 and valid. Example: 0.5 or 1.'),
                    null);
                }
              } else if ($totalDev !== 0) {
                $value = (float)$totalDev * 2;
                if ((strval($value) !== strval(intval($value))) || ($totalDev < 0.5)) {
                  $errors[] = new PhabricatorApplicationTransactionValidationError(
                    $type_frequenceTime,
                    pht('Invalid'),
                    pht('Dev must be greater than 0 and valid. Example: 0.5 or 1.'),
                    null);
                }
              }
            }
        }

      }
    }
    if ($start_date > $end_date) {
      $errors[] = new PhabricatorApplicationTransactionValidationError(
        $end_date_xaction,
        pht('Invalid'),
        pht('End date must be after or equal to start date.'),
        null);
    }

    $dates = [];
    $current = $start_date;
    $date2 = $end_date;
    $stepVal = '+1 day';
    while( $current <= $date2 ) {
       $dates[] = date('d/m/Y', $current);
       $current = strtotime($stepVal, $current);
    }

    if(strlen($day_offs) != 0){
      if (count(array_intersect(explode(", ", $day_offs), $dates)) == count($dates)) {
        $errors[] = new PhabricatorApplicationTransactionValidationError(
          $day_offs_xaction,
          pht('Invalid'),
          pht('Day-offs must be valid.'),
          null);
      }
    }


    return $errors;
  }


}
