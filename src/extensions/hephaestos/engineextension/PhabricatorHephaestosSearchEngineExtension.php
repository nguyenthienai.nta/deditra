<?php

final class PhabricatorHephaestosSearchEngineExtension
    extends PhabricatorSearchEngineExtension
{

    const EXTENSIONKEY = 'Hephaestos';

    public function isExtensionEnabled()
    {
        return PhabricatorApplication::isClassInstalled(
            'PhabricatorHephaestosApplication');
    }

    public function getExtensionName()
    {
        return pht('Support for Hephaestos');
    }

    public function getExtensionOrder()
    {
        return 4000;
    }

    public function supportsObject($object)
    {
        return ($object instanceof PhabricatorHephaestosInterface);
    }

    public function getSearchFields($object)
    {
        $fields = array();

        if (PhabricatorHephaestosObjectiveQuery::getOkrsExist()) {
            $fields[] = id(new PhabricatorHephaestosSearchField())
                ->setKey('spacePHIDs')
                ->setConduitKey('heos')
                ->setAliases(array('objective', 'heos'))
                ->setLabel(pht('Hephaestos'))
                ->setDescription(
                    pht('Search for objects in certain Hephaestos.'));
        }

        return $fields;
    }

    public function applyConstraintsToQuery(
        $object,
        $query,
        PhabricatorSavedQuery $saved,
        array $map)
    {

        if (!empty($map['spacePHIDs'])) {
            $query->withObjectivePHIDs($map['spacePHIDs']);
        } else {
            // If the user doesn't search for objects in specific Okrs, we
            // default to "all active Okrs you have permission to view".
            $query->withObjectiveIsArchived(false);
        }
    }

    public function getFieldSpecificationsForConduit($object)
    {
        return array(
            id(new PhabricatorConduitSearchFieldSpecification())
                ->setKey('spacePHID')
                ->setType('phid?')
                ->setDescription(
                    pht('PHID of the policy space this object is part of.')),
        );
    }

    public function getFieldValuesForConduit($object, $data)
    {
        return array(
            'spacePHID' => $object->getObjectivePHID(),
        );
    }

}
