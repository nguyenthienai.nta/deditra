<?php

final class PhabricatorHephaestosMailEngineExtension
    extends PhabricatorMailEngineExtension
{

    const EXTENSIONKEY = 'hephaestos';

    public function supportsObject($object)
    {
        return ($object instanceof PhabricatorHephaestosInterface);
    }

    public function newMailStampTemplates($object)
    {
        return array(
            id(new PhabricatorPHIDMailStamp())
                ->setKey('space')
                ->setLabel(pht('Objective')),
        );
    }

    public function newMailStamps($object, array $xactions)
    {
        $editor = $this->getEditor();
        $viewer = $this->getViewer();

        if (!PhabricatorHephaestosObjectiveQuery::getOkrsExist()) {
            return;
        }

        $space_phid = PhabricatorHephaestosObjectiveQuery::getObjectObjectivePHID(
            $object);

        $this->getMailStamp('objective')
            ->setValue($space_phid);
    }

}
