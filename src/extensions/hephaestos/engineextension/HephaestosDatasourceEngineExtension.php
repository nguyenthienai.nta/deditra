<?php

final class HephaestosDatasourceEngineExtension
  extends PhabricatorDatasourceEngineExtension {

  public function newQuickSearchDatasources() {
    return array(
      new PhabricatorHephaestosOkrDataSource(),
      new PhabricatorHephaestosKrDatasource(),
      new PhabricatorHephaestosObjDatasource()
    );
  }

  public function newJumpURI($query) {

    return null;
  }
}
