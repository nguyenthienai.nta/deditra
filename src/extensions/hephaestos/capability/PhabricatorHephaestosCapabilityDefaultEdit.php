<?php

final class PhabricatorHephaestosCapabilityDefaultEdit
    extends PhabricatorPolicyCapability
{

    const CAPABILITY = 'hephaestos.default.edit';

    public function getCapabilityName()
    {
        return pht('Default Edit Policy');
    }

}
