<?php

final class PhabricatorHephaestosCapabilityCreate
    extends PhabricatorPolicyCapability
{

    const CAPABILITY = 'hephaestos.create';

    public function getCapabilityName()
    {
        return pht('Can Create Hephaestos');
    }

    public function describeCapabilityRejection()
    {
        return pht('You do not have permission to create Hephaestos.');
    }

}
