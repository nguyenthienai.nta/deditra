<?php

final class PhabricatorHephaestosCapabilityDefaultView
    extends PhabricatorPolicyCapability
{

    const CAPABILITY = 'hephaestos.default.view';

    public function getCapabilityName()
    {
        return pht('Default View Policy');
    }

    public function shouldAllowPublicPolicySetting()
    {
        return true;
    }

}
