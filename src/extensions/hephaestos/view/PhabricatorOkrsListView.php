<?php
//
//final class PhabricatorOkrsListView extends AphrontView
//{
//
//    private $okrs;
//    private $showMember;
//    private $showWatching;
//    private $noDataString;
//
//    public function setOkr(array $okrs)
//    {
//        $this->okrs = $okrs;
//        return $this;
//    }
//
//    public function getOkr()
//    {
//        return $this->okrs;
//    }
//
//    public function setShowWatching($watching)
//    {
//        $this->showWatching = $watching;
//        return $this;
//    }
//
//    public function setShowMember($member)
//    {
//        $this->showMember = $member;
//        return $this;
//    }
//
//    public function setNoDataString($text)
//    {
//        $this->noDataString = $text;
//        return $this;
//    }
//
//    public function renderList()
//    {
//        $okrs = $this->getOkr();
//        $no_data = pht('No OKR found.');
//        if ($this->noDataString) {
//            $no_data = $this->noDataString;
//        }
//
//        $list = id(new PHUIObjectItemListView())
//            ->setNoDataString($no_data);
//
//        foreach ($okrs as $key => $okr) {
//            $id = $okr->getID();
//            $okr->excution_quality == 1 ? $color = 'green' : ($okr->excution_quality == 2 ? $color = 'yellow': $color = 'red');
//
//            $item = id(new PHUIObjectItemView())
//                ->setObject($okr)
//                ->setHeader($okr->getContent())
//                ->setHref("/obj/{$id}/")
//                ->addAttribute(
//                    array(
//                        id(new PHUIIconView())
//                            ->setIcon('fa-bullseye '),
//                        ' ',
//                        'OKR',
//                    ));
//
//            // Edit button
//
//            $href = new PhutilURI('/okrs/edit/' . $okr->getID() . '/');
//            $item->addAction(
//                id(new PHUIListItemView())
//                    ->setIcon('fa-pencil')
//                    ->setHref($href));
//
//            $list->addItem($item);
//
//            $item->addIcon(
//                'fa-circle ' . $color
//            );
//        }
//
//        return $list;
//    }
//
//    public function render()
//    {
//        return $this->renderList();
//    }
//
//}
