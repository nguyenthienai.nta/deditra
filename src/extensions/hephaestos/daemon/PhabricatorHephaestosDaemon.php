<?php

final class PhabricatorHephaestosDaemon extends PhabricatorDaemon
{

  private $engines;

  protected function run()
  {
    $this->setEngines(PhabricatorHephaestosEngine::loadAllEngines());
    while (!$this->shouldExit()) {
      PhabricatorCaches::destroyRequestCache();
      $wait = phutil_units('12 hours in seconds');
      $now_hour = date('H:i');
      $now_minute = date('i');
      $hiihfiManyProjects = array();

      if ($now_hour == "23:00") {
        $okrs = id(new PhabricatorHephaestosOkr())->loadAll();
        foreach ($okrs as $okr) {
          if ($okr->getIsArchived() == 0) {
            $objectives = id(new PhabricatorHephaestosObjective())->loadAllWhere('hephaestosPHID = %s', $okr->getPHID());
            foreach ($objectives as $objective) {
              $key_results = id(new PhabricatorHephaestosKeyResult())->loadAllWhere('objectivePHID = %s', $objective->getPHID());
              foreach ($key_results as $key_result) {
                if (
                  (count(explode(",", $key_result->getProjectPHID())) > 1) &&
                  (($key_result->getManualCheckin() == 4) || ($key_result->getManualCheckin() == 5))
                ) {
                  $hiihfiManyProjects[] = $key_result;
                  continue;
                }
                if ($key_result->getAutoCheckIn() == 1) {
                  $this->checkInThisKR($key_result);

                  $this->log(pht('Waiting for check-in next KR...'));
                  //$this->sleep(3);
                }
              }
            }
          }
        }

        foreach ($hiihfiManyProjects as $hiihfiManyProject) {
          $this->checkInThisKR($hiihfiManyProject);
        }

      }
    }
  }


  public function setEngines(array $engines)
  {
    assert_instances_of($engines, 'PhabricatorFactEngine');

    $viewer = PhabricatorUser::getOmnipotentUser();
    foreach ($engines as $engine) {
      $engine->setViewer($viewer);
    }

    $this->engines = $engines;
    return $this;
  }

  public function checkInThisKR($key_result)
  {
    if (!empty($key_result) && $key_result->getManualCheckin()) {
      switch ($key_result->getManualCheckin()) {
        case 1:
          $this->checkInRemainOfProjectPointKR($key_result);
          break;
        case 2:
          $this->checkInVelocityKR($key_result);
          break;
        case 3:
          $this->checkInEFFKR($key_result);
          break;
        case 4:
          $this->checkInHIIKR($key_result);
          break;
        case 5:
          $this->checkInHFIKR($key_result);
          break;
        default:
          $this->log(pht('KR' . $key_result->getTitle() . ' is manually check-in'));
      }
    }
  }

  public function checkInRemainOfProjectPointKR($key_result)
  {
    if (!$this->isCheckedInToDay($key_result) && $this->isInCheckInTime($key_result) && !$this->isInDayOff($key_result)) {
      $now = date('H:i:s d/m/Y');
      $current = PhabricatorHephaestosCheckin::initializeCurrent($key_result)
        ->setKrPHID($key_result->getPHID());
      $point = $this->getPointFromKrHasProject($key_result);
      $resolvedPoint = $this->getResolvedPoint($key_result);
      $current->setCheckinValue($point['current']);
      $current->setColor($this->setColorByMeasureBy($key_result));
      $current->setDateCheckin(strtotime(date('m/d/Y')));
      $current->setComment('Automatically check-in Remain of Project Point at ' . $now);
      $current->setResolvedPoint(json_encode($resolvedPoint));
      $current->attachKeyResult($key_result);
      $current->save();

      id(new PhabricatorHephaestosKeyResult())->load($key_result->getID())
        ->setConfidenceLevel($this->setColorByMeasureBy($key_result))
        ->save();
    }
  }

  public function checkInVelocityKR($key_result)
  {
    if (!$this->isCheckedInToDay($key_result) && $this->isInCheckInTime($key_result)) {
      $now = date('H:i:s d/m/Y');
      $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID());
      $this->checkCurrentByMeasureBy($checkInData, $key_result);
      $checkInData == null ? $velocityData = null : $velocityData = json_decode(end($checkInData)->getVelocityData(), TRUE);
      $velocityData ? $isFirstCheckin = false : $isFirstCheckin = true;
      $current = PhabricatorHephaestosCheckin::initializeCurrent($key_result)
        ->setKrPHID($key_result->getPHID());
      $current->setCheckinValue($this->getResolvedPointOfLastSprint($key_result));
      $current->setColor($this->setColorByMeasureBy($key_result));
      $current->setDateCheckin(strtotime(date('m/d/Y')));
      $current->setComment('Automatically check-in Velocity at ' . $now);
      $current->setVelocityData($this->getVelocityChartData($key_result, $isFirstCheckin));
      $current->attachKeyResult($key_result);
      $current->save();

      id(new PhabricatorHephaestosKeyResult())->load($key_result->getID())
        ->setConfidenceLevel($this->setColorByMeasureBy($key_result))
        ->save();
    }
  }

  public function checkInEFFKR($key_result)
  {
    if (!$this->isCheckedInToDay($key_result) && $this->isInCheckInTime($key_result) && !$this->isInDayOff($key_result)) {
      $now = date('H:i:s d/m/Y');
      $viewer = PhabricatorUser::getOmnipotentUser();
      $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID());
      $this->checkCurrentByMeasureBy($checkInData, $key_result);

      $startDate = $this->getStartTimeCurrent($checkInData, $key_result);
      $projects = id(new PhabricatorProjectQuery())
        ->setViewer($viewer)
        ->withPHIDs(explode(',', $key_result->getProjectPHID()))
        ->execute();
      $dataEffective = $this->getEffectiveOfDevelopChartDataOfMilestones($projects, $startDate);
      $current = PhabricatorHephaestosCheckin::initializeCurrent($key_result)
        ->setKrPHID($key_result->getPHID());
      $current->setCheckinValue($dataEffective['effective']);
      $current->setColor($this->setColorByMeasureBy($key_result, $dataEffective['effective']));
      $current->setDateCheckin(strtotime(date('m/d/Y')));
      $current->setComment('Automatically check-in EFF at ' . $now);
      $current->setEffective(json_encode($dataEffective));
      $current->attachKeyResult($key_result);
      $current->save();

      id(new PhabricatorHephaestosKeyResult())->load($key_result->getID())
        ->setConfidenceLevel($this->setColorByMeasureBy($key_result, $dataEffective['effective']))
        ->save();
    }
  }

  public function checkInHIIKR($key_result)
  {
    if (!$this->isCheckedInToDay($key_result) && $this->isInCheckInTime($key_result) && !$this->isInDayOff($key_result)) {
      $now = date('H:i:s d/m/Y');
      $total_dev = 0;
      $typeBug = $key_result->getManualCheckin() == 4 ? 'DEBIT Bug Rate' : 'Customer Bug Rate';
      $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID());
      $this->checkCurrentByMeasureBy($checkInData, $key_result);
      $checkInData == null ? $HIIData = null : $HIIData = json_decode(end($checkInData)->getHII(), TRUE);
      $HIIData ? $isFirstCheckin = false : $isFirstCheckin = true;

      if ($isFirstCheckin) {
        $total_dev = id(new PhabricatorHephaestosKeyResult())->loadOneWhere('PHID = %s', $key_result->getPHID())->getTotalDev() ?
          id(new PhabricatorHephaestosKeyResult())->loadOneWhere('PHID = %s', $key_result->getPHID())->getTotalDev() : 0;
      } else {
        $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID());
        foreach (array_reverse($checkInData) as $checkIn) {
          $total_dev = $checkIn->getTotalDev() ? $checkIn->getTotalDev() : 0;
          break;
        }
      }

      if ((count(explode(",", $key_result->getProjectPHID())) === 1) && ($total_dev > 0)) {
        $hii = $this->getIndexChartData($key_result->getProjectPHID(), $total_dev, 'DEBIT Bug Rate');
        $totalBug = $hii['value'][0];
      } else if (count(explode(",", $key_result->getProjectPHID())) > 1) {
        $hiiProjects = $this->getHIIHFIProjects($key_result, $typeBug);
        $totalBug = $hiiProjects['totalBug'];
        $total_dev = $hiiProjects['totalDev'];
      } else {
        $totalBug = 0;
      }

      if($total_dev > 0) {
        $hii = $totalBug / $total_dev;
        $current = PhabricatorHephaestosCheckin::initializeCurrent($key_result)
          ->setKrPHID($key_result->getPHID());
        $current->setCheckinValue(round($hii, 2));
        $current->setColor($this->setColorByMeasureBy($key_result, $totalBug / $total_dev));
        $current->setDateCheckin(strtotime(date('m/d/Y')));
        $current->setComment('Automatically check-in DEBIT Bug Rate at ' . $now);
        $current->setHII($totalBug);
        $current->setTotalDev($total_dev);
        $current->attachKeyResult($key_result);
        $current->save();

        id(new PhabricatorHephaestosKeyResult())->load($key_result->getID())
          ->setConfidenceLevel($this->setColorByMeasureBy($key_result, $totalBug / $total_dev))
          ->save();
      }

    }
  }

  public function checkInHFIKR($key_result)
  {
    if (!$this->isCheckedInToDay($key_result) && $this->isInCheckInTime($key_result) && !$this->isInDayOff($key_result)) {
      $now = date('H:i:s d/m/Y');
      $total_dev = 0;
      $typeBug = $key_result->getManualCheckin() == 4 ? 'DEBIT Bug Rate' : 'Customer Bug Rate';
      $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID());
      $this->checkCurrentByMeasureBy($checkInData, $key_result);
      $checkInData == null ? $HFIData = null : $HFIData = json_decode(end($checkInData)->getHFI(), TRUE);
      $HFIData ? $isFirstCheckin = false : $isFirstCheckin = true;

      if ($isFirstCheckin) {
        $total_dev = id(new PhabricatorHephaestosKeyResult())->loadOneWhere('PHID = %s', $key_result->getPHID())->getTotalDev() ?
          id(new PhabricatorHephaestosKeyResult())->loadOneWhere('PHID = %s', $key_result->getPHID())->getTotalDev() : 0;
      } else {
        $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID());
        foreach (array_reverse($checkInData) as $checkIn) {
          $total_dev = $checkIn->getTotalDev() ? $checkIn->getTotalDev() : 0;
          break;
        }
      }

      if ((count(explode(",", $key_result->getProjectPHID())) === 1) && ($total_dev > 0)) {
        $hfi = $this->getIndexChartData($key_result->getProjectPHID(), $total_dev, 'Customer Bug Rate');
        $totalBug = $hfi['value'][0];
      } else if (count(explode(",", $key_result->getProjectPHID())) > 1) {
        $hfiProjects = $this->getHIIHFIProjects($key_result, $typeBug);
        $totalBug = $hfiProjects['totalBug'];
        $total_dev = $hfiProjects['totalDev'];
      } else {
        $totalBug = 0;
      }

      if ($total_dev > 0) {
        $hfi = $totalBug / $total_dev;
        $current = PhabricatorHephaestosCheckin::initializeCurrent($key_result)
          ->setKrPHID($key_result->getPHID());
        $current->setCheckinValue(round($hfi , 2));
        $current->setColor($this->setColorByMeasureBy($key_result, $totalBug / $total_dev));
        $current->setDateCheckin(strtotime(date('m/d/Y')));
        $current->setComment('Automatically check-in Customer Bug Rate at ' . $now);
        $current->setHFI($totalBug);
        $current->setTotalDev($total_dev);
        $current->attachKeyResult($key_result);
        $current->save();

        id(new PhabricatorHephaestosKeyResult())->load($key_result->getID())
          ->setConfidenceLevel($this->setColorByMeasureBy($key_result, $totalBug / $total_dev))
          ->save();
      }
    }
  }

  public function isCheckedInToDay($key_result)
  {
    $check_in_data = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID());
    if (empty($check_in_data)) {
      return false;
    }
    $check_in_data == null ? '' : $last_check_in = end($check_in_data);
    $last_date_check_in = date('d/m/Y', $last_check_in->getDateCreated());
    $now = date('d/m/Y');

    return $last_date_check_in == $now;
  }

  public function isInCheckInTime($key_result)
  {
    $dates = [];
    $start_date =  $key_result->getStartDate();
    $end_date =  $key_result->getEndDate();
    $now = date('d/m/Y');

    if($key_result->getFrequence() === 'Day'){
      $step = $key_result->getFrequenceTime();
      $stepVal = '+ ' . $step .' day';
      while( $start_date <= $end_date ) {
        $dates[] = date('d/m/Y', $start_date);
        $start_date = strtotime($stepVal, $start_date);
      }
    }
    elseif($key_result->getFrequence() === 'Week'){
      $step = $key_result->getFrequenceTime() * 7;
      $stepVal = '+ ' . $step .' day';
      while( $start_date <= $end_date ) {
        $dates[] = date('d/m/Y', $start_date);
        $start_date = strtotime($stepVal, $start_date);
      }
    }
    else{
      while($start_date <= $end_date ) {
        $step = 0;
        for($i = 0; $i < $key_result->getFrequenceTime(); $i++){
          $step += cal_days_in_month(CAL_GREGORIAN, date('m', $start_date) + $i, date('Y', $start_date));
        }
        $stepVal = '+ ' . $step . ' day';
        $dates[] = date('d/m/Y', $start_date);
        $start_date = strtotime($stepVal, $start_date);
      }
    }
    return in_array($now, $dates);
  }

  public function isInDayOff($key_result)
  {
    $day_offs =  explode(", ",$key_result->getDayOffs());
    $now = date('d/m/Y');

    return in_array($now, $day_offs);
  }

  public function getResolvedPointOfLastSprint($key_result)
  {
    $projectPHID = $key_result->getProjectPHID();
    $project = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $projectPHID);
    $milestones = $this->getMilestones($project);

    if ($milestones) {
      foreach ($milestones as $milestone) {
        $lastSprint = $milestone;
        break;
      };
      $config = new AphrontApplicationConfiguration;
      $request = new AphrontRequest($config->getHost(), $config->getPath());

      $state = id(new PhabricatorOkrsDaemonViewState())->setProject($lastSprint);
      $tasks = $state->getObjects();
      $resolvedPoint = 0;

      foreach ($tasks as $taskID => $task) {
        if ($task->getPoints() !== null) {
          if ($task->getStatus() == 'resolved' || $task->getStatus() == 'closed') {
            $resolvedPoint += $task->getPoints();
          }
        }
      }
    } else {
      $state = id(new PhabricatorOkrsDaemonViewState())->setProject($project);
      $tasks = $state->getObjects();
      $resolvedPoint = 0;

      foreach ($tasks as $taskID => $task) {
        if ($task->getPoints() !== null) {
          if ($task->getStatus() == 'resolved' || $task->getStatus() == 'closed') {
            $resolvedPoint += $task->getPoints();
          }
        }
      }
    }
    return $resolvedPoint;
  }

  protected function getMilestones($project)
  {
    $viewer = PhabricatorUser::getOmnipotentUser();

    $allows_milestones = $project->supportsMilestones();

    if ($allows_milestones) {
      $milestones = id(new PhabricatorProjectQuery())
        ->setViewer($viewer)
        ->withParentProjectPHIDs(array($project->getPHID()))
        ->needImages(true)
        ->withIsMilestone(true)
        ->setOrderVector(array('milestoneNumber', 'id'))
        ->execute();
    } else {
      $milestones = array();
    }
    return $milestones;
  }

  protected function setColorByMeasureBy($kr, $currentValue = null)
  {
    if ($kr->getManualCheckin() == 1) {
      return 1;
    } elseif ($kr->getManualCheckin() == 2) {
      $confidentScope = explode(',', $kr->getConfidentScope());
      $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $kr->getPHID());
      $this->checkCurrentByMeasureBy($checkInData, $kr);
      $checkInData == null ? $velocityData = null : $velocityData = json_decode(end($checkInData)->getVelocityData(), TRUE);

      if ($confidentScope == null || $velocityData == null) {
        return 1;
      } else {
        if ($velocityData['value'][0] > (float)$confidentScope[1]) {
          return 1;
        } elseif ($velocityData['value'][0] < (float)$confidentScope[0]) {
          return 3;
        } else {
          return 2;
        }
      }
    } elseif ($kr->getManualCheckin() == 3) {
      $confidentScope = explode(',', $kr->getConfidentScope());
      $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $kr->getPHID());
      $this->checkCurrentByMeasureBy($checkInData, $kr);
      $checkInData == null ? $effData = null : $effData = json_decode(end($checkInData)->getEffective(), TRUE);

      if ($confidentScope == null || $effData == null) {
        return 1;
      } else {
        if ($currentValue > (float)$confidentScope[1]) {
          return 1;
        } elseif ($currentValue < (float)$confidentScope[0]) {
          return 3;
        } else {
          return 2;
        }
      }
    } elseif ($kr->getManualCheckin() == 4) {
      $confidentScope = explode(',', $kr->getConfidentScope());
      $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $kr->getPHID());
      $this->checkCurrentByMeasureBy($checkInData, $kr);
      $checkInData == null ? $HIIHFIData = null : $HIIHFIData = json_decode(end($checkInData)->getHII(), TRUE);

      if ($confidentScope == null || $HIIHFIData == null) {
        return 1;
      } else {
        if ($currentValue > (float)$confidentScope[1]) {
          return 3;
        } elseif ($currentValue < (float)$confidentScope[0]) {
          return 1;
        } else {
          return 2;
        }
      }
    }
    elseif ($kr->getManualCheckin() == 5) {
      $confidentScope = explode(',', $kr->getConfidentScope());
      $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $kr->getPHID());
      $this->checkCurrentByMeasureBy($checkInData, $kr);
      $checkInData == null ? $HIIHFIData = null : $HIIHFIData = json_decode(end($checkInData)->getHFI(), TRUE);

      if ($confidentScope == null || $HIIHFIData == null) {
        return 1;
      } else {
        if ($currentValue > (float)$confidentScope[1]) {
          return 3;
        } elseif ($currentValue < (float)$confidentScope[0]) {
          return 1;
        } else {
          return 2;
        }
      }
    }

  }

  protected function checkCurrentByMeasureBy($arrayCurrentTable, $key_result)
  {
    $newCurrentTable = [];
    if ($key_result->getManualCheckin() == 0 || $key_result->getManualCheckin() == 1) {
      $newCurrentTable = [];
      foreach ($arrayCurrentTable as $current) {
        if ($current->getVelocityData() == null && $current->getHII() == null && $current->getEffective() == null) {
          $newCurrentTable[] = $current;
        }
      }
    } elseif ($key_result->getManualCheckin() == 2) {
      $newCurrentTable = [];
      foreach ($arrayCurrentTable as $current) {
        if ($current->getVelocityData() != null) {
          $newCurrentTable[] = $current;
        }
      }
    } elseif ($key_result->getManualCheckin() == 3) {
      $newCurrentTable = [];
      foreach ($arrayCurrentTable as $current) {
        if ($current->getEffective() != null) {
          $newCurrentTable[] = $current;
        }
      }
    } elseif ($key_result->getManualCheckin() == 4 || $key_result->getManualCheckin() == 5) {
      $newCurrentTable = [];
      foreach ($arrayCurrentTable as $current) {
        if ($current->getHII() != null) {
          $newCurrentTable[] = $current;
        }
      }
    } else {
      $newCurrentTable = $arrayCurrentTable;
    }

    $arrayCurrentTable = $newCurrentTable;
  }

  private function getStartTimeCurrent($arrayCurrentTable, $key_result)
  {
    $startDate = 0;
    if (count($arrayCurrentTable) == 0) {
      $startDate = $key_result->getStartDate();
    } else {
      $dateCheckin = $arrayCurrentTable[$this->lastKey($arrayCurrentTable)]->getDateCheckin();
      if (date("d/m/Y", $dateCheckin) !== date("d/m/Y")) {
        $startDate = $dateCheckin;
      } else {
        if ($arrayCurrentTable[$this->lastKey($arrayCurrentTable) - 1] != null) {
          $startDate = $arrayCurrentTable[$this->lastKey($arrayCurrentTable) - 1]->getDateCheckin();
        } else {
          $startDate = $key_result->getStartDate();
        }
      }
    }
    return $startDate;
  }

  public function getNewVelocityDataValue($milestones)
  {
    $data = [];
    foreach ($milestones as $milestone) {
      $state = id(new PhabricatorOkrsDaemonViewState())->setProject($milestone);
      $tasks = $state->getObjects();
      $currentPoint = 0;

      foreach ($tasks as $taskID => $task) {
        if ($task->getPoints() !== null) {
          if ($task->getStatus() == 'resolved' || $task->getStatus() == 'closed') {
            $currentPoint += $task->getPoints();
          }
        }
      }
      $data['label'][] = $milestone->getName();
      $data['value'][] = $currentPoint;
    }
    return $data;
  }

  public function getVelocityChartData($key_result, $isFirstCheckin)
  {
    $viewer = PhabricatorUser::getOmnipotentUser();

    $project = id(new PhabricatorProjectQuery())
      ->setViewer($viewer)
      ->withPHIDs(array($key_result->getProjectPHID()))
      ->needMembers(true)
      ->needWatchers(true)
      ->needImages(true)
      ->executeOne();

    $milestones = $this->getMilestones($project);
    $data = [];
    if ($milestones) {
      $newDataValue = $this->getNewVelocityDataValue($milestones);
      if ($isFirstCheckin) {
        $data = $newDataValue;
      } else {
        $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID());
        $velocityData = json_decode(end($checkInData)->getVelocityData(), TRUE);
        $data = $velocityData;

        if (count($newDataValue['label']) == count($data['label'])) {
          $data['label'] = $newDataValue['label'];
          $data['value'][0] = $newDataValue['value'][0];
        } else {
          $offset = count($newDataValue['label']) - count($data['label']);
          $change_value = array_slice($newDataValue['value'], 0, $offset);

          if ($offset > 1) {
            for ($i = 1; $i < $offset; $i++) {
              $change_value[$i] = 0;
            }
          }

          $data['label'] = $newDataValue['label'];
          $data['value'] = array_merge($change_value, $data['value']);
        }
      }
    } else {
      $state = id(new PhabricatorOkrsDaemonViewState())->setProject($project);
      $tasks = $state->getObjects();
      $currentPoint = 0;

      foreach ($tasks as $taskID => $task) {
        if ($task->getPoints() !== null) {
          if ($task->getStatus() == 'resolved' || $task->getStatus() == 'closed') {
            $currentPoint += $task->getPoints();
          }
        }
      }
      $data['label'][] = $project->getName();
      $data['value'][] = $currentPoint;
    }
    return json_encode($data);
  }

  public function getIndexChartData($projectPHID, $totalDev, $index)
  {
    $viewer = PhabricatorUser::getOmnipotentUser();
    $project = id(new PhabricatorProjectQuery())
      ->setViewer($viewer)
      ->withPHIDs(array($projectPHID))
      ->needMembers(true)
      ->needWatchers(true)
      ->needImages(true)
      ->executeOne();


    $total_dev = $totalDev;

    $milestones = $this->getMilestones($project);
    $data = [];
    if ($milestones) {
      $milestone_names = [];
      foreach ($milestones as $milestone) {
        $milestone_names[] = $milestone->getName();
      }
      $data = $this->getIndexChartDataOfMilestones($project, $data, $total_dev, $milestone_names, $index);
    } else {
      $milestone_names = [$project->getName()];
      $data = $this->getIndexChartDataOfMilestones($project, $data, $total_dev, $milestone_names, $index);
    }

    if ($milestones) {
      foreach ($milestone_names as $milestone_name) {
        if (!in_array($milestone_name, array_keys($data))) {
          $data[$milestone_name] = 0;
        }
      }
    } else {
      if (count($data) == 0) {
        $data[$milestone_names[0]] = 0;
      }
    }
    $data = $this->sortBySprint($data, $milestone_names, null);

    return $data;
  }

  protected function getIndexChartDataOfMilestones($project, $data, $total_dev, $milestone_names, $index)
  {
    $state = id(new PhabricatorOkrsDaemonViewState())->setProject($project);
    $tasks = $state->getObjects();
    $index === 'DEBIT Bug Rate' ? $bugTag = 'Bug Report (QA)' : $bugTag = 'Bug Report (Customer)';

    $bug = id(new PhabricatorProject())->loadOneWhere('name = %s', $bugTag);
    $bugPHID = isset($bug) ? $bug->getPHID() : null;

    $totalBug = 0;
    $type = 'edge';
    foreach ($tasks as $taskID => $task) {
      $index === 'DEBIT Bug Rate' ? $isBug = $this->checkTaskIsBugQA($task, $bugPHID) : $isBug = $this->checkTaskIsBugQA($task, $bugPHID);

      if ($isBug == true) {
        $xactions = $this->getXactionsFromTask($task, $type);

        foreach ($xactions as $xaction) {
          if (isset($xaction->getNewValue()[0])) {
            $project_first_xaction = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $xaction->getNewValue()[0]);
            if ($project_first_xaction != null) {
              $project_tag = $project_first_xaction->getName();
              if (in_array($project_tag, $milestone_names)) {
                !array_key_exists($project_tag ,$data) ? $data[$project_tag] = 1 : $data[$project_tag]++;
                break;
              }
            }
          }
          if (isset(array_keys($xaction->getNewValue())[0])) {
            $project_first_xaction1 = id(new PhabricatorProject())->loadOneWhere('PHID = %s', array_keys($xaction->getNewValue())[0]);
            if ($project_first_xaction1 != null) {
              $project_tag1 = $project_first_xaction1->getName();
              if (in_array($project_tag1, $milestone_names)) {
                !array_key_exists($project_tag1 ,$data) ? $data[$project_tag1] = 1 : $data[$project_tag1]++;
                break;
              }
            }
          }
        }
      }
    }
    return $data;
  }

  protected function checkTaskIsBugQA($task, $bugQAPHID)
  {
    $tags = $task->getProjectPHIDs();
    $isBug = false;
    if (in_array($bugQAPHID, $tags)) {
      $isBug = true;
    }
    return $isBug;
  }


  protected function checkTaskIsBugCustomer($task, $bugCustomerPHID)
  {
    $tags = $task->getProjectPHIDs();
    $isBug = false;
    if (in_array($bugCustomerPHID, $tags)) {
      $isBug = true;
    }
  }

  protected function getXactionsFromTask($task, $type)
  {
    $viewer = PhabricatorUser::getOmnipotentUser();
    $pager = id(new AphrontCursorPagerView())->setURI(new PhutilURI('http://hzx/'));
    $query = id(new ManiphestTransactionQuery());

    if ($type === 'edge') {
      $xactions = $query
        ->setViewer($viewer)
        ->withObjectPHIDs(array($task->getPHID()))
        ->withTransactionTypes(array(PhabricatorTransactions::TYPE_EDGE))
        ->needComments(true)
        ->executeWithCursorPager($pager);

    } elseif ($type === 'columns') {
      $xactions = $query
        ->setViewer($viewer)
        ->withObjectPHIDs(array($task->getPHID()))
        ->withTransactionTypes(array(PhabricatorTransactions::TYPE_COLUMNS))
        ->needComments(true)
        ->executeWithCursorPager($pager);
    }
    return array_reverse((array)$xactions);
  }

  protected function sortBySprint($data, $milestone_names, $total_dev)
  {

    $chartData = [];
    if ($total_dev == null) {
      foreach ($milestone_names as $milestone_name) {
        foreach ($data as $k => $v) {
          if ($milestone_name == $k) {
            $chartData[$milestone_name] = $v;
          }
        }
      }
    } else {
      foreach ($milestone_names as $milestone_name) {
        foreach ($data as $k => $v) {
          if ($milestone_name == $k) {
            $chartData[$milestone_name] = round($v / $total_dev, 2);
          }
        }
      }
    }

    $data1 = [];
    $data1['label'] = array_keys($chartData);
    $data1['value'] = array_values($chartData);
    return $data1;
  }

  protected function getEffectiveOfDevelopChartDataOfMilestones($projects, $startTime)
  {
    $data = [];

    $totalTimeDEV = 0;
    $totalTimeBUG = 0;

    $bugQA = id(new PhabricatorProject())->loadOneWhere('name = %s', 'Bug Report (QA)');
    $bugQAPHID = isset($bugQA) ? $bugQA->getPHID() : null;
    $bugCustomer = id(new PhabricatorProject())->loadOneWhere('name = %s', 'Bug Report (Customer)');
    $bugCustomerPHID = isset($bugCustomer) ? $bugCustomer->getPHID() : null;

    foreach ($projects as $project) {
      $state = id(new PhabricatorOkrsDaemonViewState())->setProject($project);
      $tasks = $state->getObjects();

      foreach ($tasks as $taskID => $task) {

        $startDoing = 0;
        $endDoing = 0;

        $isBug = $this->checkTaskIsBug($task, $bugQAPHID, $bugCustomerPHID);
        $type = 'columns';
        $xactions = $this->getXactionsFromTask($task, $type);

        foreach ($xactions as $xaction) {
          $column = $xaction->getNewValue()[0]["columnPHID"];
          $nameColumn = id(new PhabricatorProjectColumn())->loadOneWhere('PHID = %s', $column)->getName();

          if (strtolower($nameColumn) == "doing" || strtolower($nameColumn) == "in develop") {
            $startDoing = $xaction->getDateCreated();
          } else {
            if ($startDoing != 0) {
              $endDoing = $xaction->getDateCreated();
            }
          }

          if ($startDoing != 0 && $endDoing != 0) {
            if ($startDoing > $startTime && $endDoing > $startTime) {
              $timeDoing = ($endDoing - $startDoing) / 3600;
              $totalTimeDEV += $timeDoing;

              if ($isBug == true) {
                $totalTimeBUG += $timeDoing;
              }
            } else if ($startDoing <= $startTime && $endDoing > $startTime) {
              $timeDoing = ($endDoing - $startTime) / 3600;
              $totalTimeDEV += $timeDoing;

              if ($isBug == true) {
                $totalTimeBUG += $timeDoing;
              }
            }
            $startDoing = 0;
            $endDoing = 0;
          }
        }

        if ($startDoing != 0) {
          if ($startDoing > $startTime) {
            $endDoing = id(new DateTime())->getTimestamp();
            $timeDoing = ($endDoing - $startDoing) / 3600;
            $totalTimeDEV += $timeDoing;
            if ($isBug == true) {
              $totalTimeBUG += $timeDoing;
            }
          } else {
            $endDoing = id(new DateTime())->getTimestamp();
            $timeDoing = ($endDoing - $startTime) / 3600;
            $totalTimeDEV += $timeDoing;
            if ($isBug == true) {
              $totalTimeBUG += $timeDoing;
            }
          }
        }
      }
    }
    if (round($totalTimeDEV, 1) != 0) {
      $effective = round(((round($totalTimeDEV, 1) - round($totalTimeBUG, 1)) / round($totalTimeDEV, 1)) * 100, 1);
    } else {
      $effective = 0;
    }
    $data['totalTime'] = round($totalTimeDEV, 1);
    $data['totalTimeBUG'] = round($totalTimeBUG, 1);
    $data['totalTimeDEV'] = round(round($totalTimeDEV, 1) - round($totalTimeBUG, 1), 1);
    $data['effective'] = $effective;
    return $data;
  }

  protected function checkTaskIsBug($task, $bugQAPHID, $bugCustomerPHID = null)
  {
    $tags = $task->getProjectPHIDs();
    $isBug = false;
    if (in_array($bugQAPHID, $tags) || in_array($bugCustomerPHID, $tags)) {
      $isBug = true;
    }
    return $isBug;
  }


  private function getDateLabelChart($keyResult)
  {
    $dayOffs = explode(", ", $keyResult->getDayOffs());
    $begin = new DateTime(date('Y-m-d', $keyResult->getStartDate()));
    $end = new DateTime(date('Y-m-d', $keyResult->getEndDate()));
    $end = $end->modify('+1 day');
    $period = new DatePeriod(
      $begin,
      new DateInterval('P1D'),
      $end
    );
    $arrays = [];
    foreach ($period as $key => $value) {
      $date = $value->format('d/m/Y');
      if (!in_array($date, $dayOffs)) array_push($arrays, $date);
    }

    return $arrays;
  }

  private function getResolvedPoint($kr)
  {
    $project = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $kr->getProjectPHID());
    $state = id(new PhabricatorOkrsDaemonViewState())
      ->setProject($project);
    $tasks = $state->getObjects();
    $arr_label = $this->getDateLabelChart($kr);
    $current = date('d/m/Y');

    foreach ($arr_label as $key => $dateChart) {
      $pointOfDate = null;
      $resolvedPoint = null;
      $resolvedPoint1 = null;
      $pointToResolve = null;

      foreach ($tasks as $task) {
        if ($task->getClosedEpoch() != null) {
          $taskStatus = $task->getStatus();
          if ($taskStatus == 'resolved' || $taskStatus == 'closed' ) {
            $resolvedDate = date('d/m/Y', $task->getClosedEpoch());
            if ($resolvedDate == $dateChart)
              $resolvedPoint += $task->getPoints() != 0 ? $task->getPoints() : null;
          }
        }
        if ($task->getEndDate() != null) {
          $endDate = date('d/m/Y', $task->getEndDate());
          $taskStatus = $task->getStatus();
          if ($endDate == $dateChart)
            if ($taskStatus == 'open' || $taskStatus == 'doing')
              $pointToResolve += $task->getPoints() != 0 ? $task->getPoints() : null;
        }
        $arr_label['resolvedPoint'][$dateChart] = $resolvedPoint;
        $arr_label['pointToResolve'][$dateChart] = $pointToResolve;
      }
    }

    if(!isset($arr_label['resolvedPoint']) || !isset($arr_label['resolvedPoint'])){
      $data = ['resolvedPoint' => array(null), 'pointToResolve' => array(null)];
    }
    else{
      $data = ['resolvedPoint' => $arr_label['resolvedPoint'], 'pointToResolve' => $arr_label['pointToResolve']];
    }
    return $data;
  }

  private function getPointFromKrHasProject($kr)
  {
    $project = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $kr->getProjectPHID());
    $state = id(new PhabricatorOkrsDaemonViewState())
      ->setProject($project);

    $tasks = $state->getObjects();
    $totalPoint = 0;
    $currentPoint = 0;
    foreach ($tasks as $taskID => $task) {
      if ($task->getPoints() !== null) {
        $totalPoint += $task->getPoints();
        if ($task->getStatus() == 'resolved' || $task->getStatus() == 'closed') {
          $currentPoint += $task->getPoints();
        }
      }
    }
    return ['current' => $currentPoint, 'target' => $totalPoint];
  }

  private function getHIIHFIProjects($key_result, $typeBug) {
    $totalProjects = explode(",", $key_result->getProjectPHID());
    $bugRate = 0;
    $totalDev = 0;
    foreach ($totalProjects as $project) {
      $krBugs = id(new PhabricatorHephaestosKeyResult())->loadAllWhere('projectPHID = %s', $project);
      $date = 0;
      $bugRatePerKr = $bugRate;
      $totalDevPerKr = $totalDev;

      foreach ($krBugs as $krBug) {
        if(($typeBug == 'DEBIT Bug Rate') ? ($krBug->getManualCheckin() == 4) : ($krBug->getManualCheckin() == 5)) {
          $currents = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $krBug->getPHID());
          foreach (array_reverse($currents) as $current) {
            if(($typeBug == 'DEBIT Bug Rate') ? ($current->getHII() !== null) : ($current->getHFI() !== null)) {
              if($current->getDateCheckin() > $date) {
                $bugRatePerKr = $current->getCheckinValue();
                $totalDevPerKr = $current->getTotalDev();
                $date = $current->getDateCheckin();
              }
              break;
            }
          }
        }
      }
      if ($bugRatePerKr >= $bugRate) {
        $bugRate = $bugRatePerKr;
        $totalDev = $totalDevPerKr;
      }
    }

    return array(
      'totalBug' => round($bugRate * $totalDev),
      'totalDev' => $totalDev
    );
  }

  private function lastKey($array)
  {
    end($array);
    return key($array);
  }

}
