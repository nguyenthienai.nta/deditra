<?php

final class PhabricatorHephaestosOkrDataSource
  extends PhabricatorTypeaheadDatasource
{

  public function getBrowseTitle()
  {
    return pht('Browse Hephaestos');
  }

  public function getPlaceholderText()
  {
    return pht('Type a okr name...');
  }

  public function getDatasourceApplicationClass()
  {
    return 'PhabricatorHephaestosApplication';
  }

  public function loadResults()
  {
    $query = id(new PhabricatorHephaestosOkrQuery());

    $Okrs = $this->executeQuery($query);
    $results = array();
    foreach ($Okrs as $space) {
      $full_name = pht(
        '%s %s',
        '',
        $space->getContent());

      $uri = isset($space) ? "/hephaestos/obj/" . $space->getID() : '';

      $result = id(new PhabricatorTypeaheadResult())
        ->setName($full_name)
        ->setDisplayType("Hephaestos")
        ->setURI($uri)
        ->setImageSprite(
          'phabricator-search-icon phui-font-fa phui-icon-view fa-gavel')
        ->setPHID($space->getPHID());

      if ($space->getIsArchived()) {
        $result->setClosed(pht('Archived'));
      }

      $results[] = $result;
    }

    return $this->filterResultsAgainstTokens($results);
  }

}
