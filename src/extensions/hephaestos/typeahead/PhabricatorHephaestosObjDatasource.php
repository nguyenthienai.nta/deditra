<?php

final class PhabricatorHephaestosObjDatasource
    extends PhabricatorTypeaheadDatasource
{

    public function getBrowseTitle()
    {
        return pht('Browse Hephaestos');
    }

    public function getPlaceholderText()
    {
        return pht('Type a space name...');
    }

    public function getDatasourceApplicationClass()
    {
        return 'PhabricatorOkrsApplication';
    }

  public function loadResults()
  {
    $query = id(new PhabricatorHephaestosObjectiveQuery());

    $objective = $this->executeQuery($query);
    $results = array();

    $query_okr = id(new PhabricatorHephaestosOkrQuery());

    $okrs_check = $this->executeQuery($query_okr);
    $okr_ids = [];
    $viewer = $this->getViewer();

    foreach ($okrs_check as $okr) {
      $okr_ids[] = $okr->getID();
    }
    foreach ($objective as $space) {
      $full_name = pht(
        '%s %s',
        $space->getMonogram(),
        $space->getTitle());

      $Okrs = id(new PhabricatorHephaestosOkr())->loadOneWhere('phid = %s', $space->getHephaestosPHID());
      $okr_id = $Okrs ? $Okrs->getId() : '';
      $display_name_type = $Okrs ? $Okrs->getContent() : '';
      $uri = isset($space) ? "/hephaestos/obj/" . $okr_id : '';

      if ($okr_ids !== NULL) {
        if (in_array($okr_id, $okr_ids)) {
          $check_objective = id(new PhabricatorHephaestosOkrQuery())
            ->setViewer($viewer)
            ->withIDs(array($okr_id))
            ->executeOne();
          $can_view = PhabricatorPolicyFilter::hasCapability(
            $viewer,
            $check_objective,
            PhabricatorPolicyCapability::CAN_VIEW);
          $check_can_search = $can_view === true ? 1 : 0;

          if ($check_can_search === 1) {
            $result = id(new PhabricatorTypeaheadResult())
              ->setName($full_name)
              ->setDisplayName($space->getTitle())
              ->setURI($uri)
              ->setDisplayType($display_name_type)
              ->setPHID($space->getPHID());

            if ($space->getIsArchived()) {
              $result->setClosed(pht('Archived'));
            }
            $results[] = $result;
          }
        }
      }
    }

    return $this->filterResultsAgainstTokens($results);
  }

}
