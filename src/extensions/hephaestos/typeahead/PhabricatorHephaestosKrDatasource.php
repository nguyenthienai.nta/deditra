<?php

final class PhabricatorHephaestosKrDatasource
  extends PhabricatorTypeaheadDatasource
{

  public function getBrowseTitle()
  {
    return pht('Browse Hephaestos');
  }

  public function getPlaceholderText()
  {
    return pht('Type a okr name...');
  }

  public function getDatasourceApplicationClass()
  {
    return 'PhabricatorHephaestosApplication';
  }

  public function loadResults()
  {
    $query = id(new PhabricatorHephaestosKeyResultQuery());

    $viewer = $this->getViewer();
    $krs = $this->executeQuery($query);
    $results = array();

    $query_okr = id(new PhabricatorHephaestosOkrQuery());
    $Okrs = $this->executeQuery($query_okr);
    $okr_ids = [];

    foreach ($Okrs as $okr){
      $okr_ids[] = $okr->getID();
    }

    foreach ($krs as $space) {
      $full_name = pht(
        '%s %s',
        '',
        $space->getTitle());

      $uri = isset($space) ? "/hephaestos/kr/detail/" . $space->getID() : '';

      $obj = id(new PhabricatorHephaestosObjective())->loadOneWhere('phid = %s', $space->getObjectivePHID());
      $okrPHID = $obj->getHephaestosPHID();
      $Okrs = id(new PhabricatorHephaestosOkr())->loadOneWhere('phid = %s', $okrPHID);
      $okr_id = $Okrs ? $Okrs->getID() : '';
      $nameOkr = $Okrs ? $Okrs->getContent() : '';

      if ($okr_ids !== NULL) {
        if (in_array($okr_id, $okr_ids)) {
          $check_objective = id(new PhabricatorHephaestosOkrQuery())
            ->setViewer($viewer)
            ->withIDs(array($okr_id))
            ->executeOne();
          $can_edit = PhabricatorPolicyFilter::hasCapability(
            $viewer,
            $check_objective,
            PhabricatorPolicyCapability::CAN_VIEW);
          $check_can_search = $can_edit === true ? 1 : 0;

          if ($check_can_search === 1) {
            $result = id(new PhabricatorTypeaheadResult())
              ->setName($full_name)
              ->setDisplayName($space->getTitle())
              ->setDisplayType($nameOkr . ' - ' . $obj->getTitle())
              ->setIcon('fa-gavel')
              ->setURI($uri)
              ->setImageSprite(
                'phabricator-search-icon phui-font-fa phui-icon-view fa-gavel')
              ->setPHID($space->getPHID());
            $results[] = $result;
          }
        }
      }
    }

    return $this->filterResultsAgainstTokens($results);
  }

}
