<?php

final class HeraldHephaestosField extends HeraldField
{

    const FIELDCONST = 'hepha';

    public function getHeraldFieldName()
    {
        return pht('hephaestos');
    }

    public function getFieldGroupKey()
    {
        return HeraldSupportFieldGroup::FIELDGROUPKEY;
    }

    public function supportsObject($object)
    {
        return ($object instanceof PhabricatorHephaestosInterface);
    }

    public function getHeraldFieldValue($object)
    {
        return PhabricatorHephaestosOkrQuery::getObjectOkrsPHID($object);
    }

    protected function getHeraldFieldStandardType()
    {
        return self::STANDARD_PHID;
    }

    protected function getDatasource()
    {
        return new PhabricatorHephaestosOkrDataSource();
    }

}
