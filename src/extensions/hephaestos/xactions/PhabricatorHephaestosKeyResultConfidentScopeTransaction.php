<?php

final class PhabricatorHephaestosKeyResultConfidentScopeTransaction
  extends PhabricatorHephaestosKeyResultTransactionType {

  const TRANSACTIONTYPE = 'hkr.confidentScope';

  public function generateOldValue($object)
  {
    return $object->getConfidentScope();
  }

  public function applyInternalEffects($object, $value)
  {
    $object->setConfidentScope($value);
  }

  public function getTitle()
  {
    $old = $this->getOldValue();
    if (!strlen($old)) {
      return pht(
        '%s created this objective.',
        $this->renderAuthor());
    } else {
      return pht(
        '%s renamed this objective from %s to %s.',
        $this->renderAuthor(),
        $this->renderOldValue(),
        $this->renderNewValue());
    }
  }

  public function getTitleForFeed()
  {
    return pht(
      '%s renamed objective %s from %s to %s.',
      $this->renderAuthor(),
      $this->renderObject(),
      $this->renderOldValue(),
      $this->renderNewValue());
  }

  public function validateTransactions($object, array $xactions)
  {
    $errors = array();

    foreach ($xactions as $xaction) {
      $new_value = explode(',', $xaction->getNewValue());
      if (($new_value[0] == '') || ($new_value[1] == '')) {
        if(($new_value[0] =='') && ($new_value[1] =='')) {
          return array();
        }
        $errors['required'] = $this->newRequiredError(
          pht('Confidence scope must required.'));
      }

      if ($new_value[0] > $new_value[1]) {
        $errors['invalid'] = $this->newInvalidError(
          pht('Invalid scope.'));
      }

      return array_values($errors);
    }
  }

}
