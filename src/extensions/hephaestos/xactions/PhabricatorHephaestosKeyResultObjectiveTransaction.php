<?php

final class PhabricatorHephaestosKeyResultObjectiveTransaction
    extends PhabricatorHephaestosKeyResultTransactionType {

    const TRANSACTIONTYPE = 'hephaestos.key_result.objective';

    public function generateOldValue($object) {
        return $object->getObjectivePHID();
    }

    public function applyInternalEffects($object, $value) {
        $object->setObjectivePHID($value);
    }

    public function getTitle() {
        return pht(
            '%s changed the objective for this kr.',
            $this->renderAuthor());
    }

    public function getTitleForFeed() {
        return pht(
            '%s changed the objective for kr %s.',
            $this->renderAuthor(),
            $this->renderObject());
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();

        if ($this->isEmptyTextTransaction($object->getObjectivePHID(), $xactions)) {
            $errors[] = $this->newRequiredError(
                pht('Kr must be attached to a objective.'));
        }

        foreach ($xactions as $xaction) {
            $new_phid = $xaction->getNewValue();

            $objective = id(new PhabricatorOkrsNamespaceQuery())
                ->setViewer($this->getActor())
                ->withPHIDs(array($new_phid))
                ->requireCapabilities(
                    array(
                        PhabricatorPolicyCapability::CAN_VIEW,
                        PhabricatorPolicyCapability::CAN_EDIT,
                    ))
                ->execute();

            if ($objective) {
                continue;
            }

            $errors[] = $this->newInvalidError(
                pht('The specified objective PHID ("%s") is not valid. You can only '.
                    'create a kr on (or move a kr into) a objective which you '.
                    'have permission to see and edit.',
                    $new_phid));
        }

        return $errors;
    }

}
