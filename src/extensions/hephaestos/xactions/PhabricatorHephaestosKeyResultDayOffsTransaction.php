<?php

final class PhabricatorHephaestosKeyResultDayOffsTransaction
    extends PhabricatorHephaestosKeyResultTransactionType {

    const TRANSACTIONTYPE = 'hkr.dayOffs';

    public function generateOldValue($object) {
        return $object->getDayOffs();
    }

    public function applyInternalEffects($object, $value) {
        $object->setDayOffs($value);
    }

    public function getTitle() {
        return pht('%s changed the day off for this kr.', $this->renderAuthor());
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();
      return $errors;
    }
}
