<?php

abstract class PhabricatorHephaestosKeyResultTransactionType
  extends PhabricatorModularTransactionType {}

final class PhabricatorHephaestosKeyResultBaseValueTransaction
    extends PhabricatorHephaestosKeyResultTransactionType {

    const TRANSACTIONTYPE = 'hkr.baseValue';

    public function generateOldValue($object) {
        return $object->getBaseValue();
    }

    public function applyInternalEffects($object, $value) {
        $object->setBaseValue($value == "" ? 0 : $value);
    }

    public function getTitle() {
        return pht(
            '%s updated the kr base Value.',
            $this->renderAuthor());
    }

    public function getTitleForFeed() {
        return pht(
            '%s updated the kr base Value for %s.',
            $this->renderAuthor(),
            $this->renderObject());
    }

    public function validateTransactions($object, array $xactions) {
      return array();
    }

}
