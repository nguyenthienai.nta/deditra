<?php

final class PhabricatorHephaestosKeyResultTotalDevTransaction
  extends PhabricatorHephaestosKeyResultTransactionType
{

  const TRANSACTIONTYPE = 'hkr.totalDev';

  public function generateOldValue($object)
  {
    return $object->getTotalDev();
  }

  public function applyInternalEffects($object, $value)
  {
    $object->setTotalDev($value);
  }

  public function getTitle()
  {
    $new = $this->getNewValue();
    if ($new) {
      return pht(
        '%s created the total dev for this kr.',
        $this->renderAuthor());
    } else {
      return pht(
        '%s changed the total dev for this kr.',
        $this->renderAuthor());
    }
  }

  public function getTitleForFeed()
  {
    $new = $this->getNewValue();
    if ($new) {
      return pht(
        '%s created the total dev for this kr %s.',
        $this->renderAuthor(),
        $this->renderObject());
    } else {
      return pht(
        '%s changed the total dev for this kr %s.',
        $this->renderAuthor(),
        $this->renderObject());
    }
  }

  public function getIcon()
  {
    $new = $this->getNewValue();
    if ($new) {
      return 'fa-ban';
    } else {
      return 'fa-check';
    }
  }

  public function getColor()
  {
    $new = $this->getNewValue();
    if ($new) {
      return 'indigo';
    }
  }

  public function validateTransactions($object, array $xactions)
  {
    return array();
  }
}


