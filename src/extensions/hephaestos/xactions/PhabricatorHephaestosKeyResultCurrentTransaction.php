<?php

final class PhabricatorHephaestosKeyResultCurrentTransaction
  extends PhabricatorHephaestosKeyResultTransactionType {

  const TRANSACTIONTYPE = 'hephaestos.key_result.current';

  public function generateOldValue($object) {
    return $object->getCurrent();
  }

  public function applyInternalEffects($object, $value) {
    $object->setCurrent($value);
  }

  public function getTitle() {
    return pht(
      '%s updated the kr Current.',
      $this->renderAuthor());
  }

  public function getTitleForFeed() {
    return pht(
      '%s updated the kr Current for %s.',
      $this->renderAuthor(),
      $this->renderObject());
  }

  public function validateTransactions($object, array $xactions) {
    $errors = array();

    // if ($this->isEmptyTextTransaction($object->getCurrent(), $xactions)) {
    //     $errors[] = $this->newRequiredError(
    //         pht('KR must have a Current.'));
    // }

    // foreach ($xactions as $xaction) {
    //     $new_value = $xaction->getNewValue();
    //     if (!is_numeric($new_value) || $new_value < 0 ) {
    //         $errors[] = $this->newInvalidError(
    //             pht('Current must be a number and bigger than 0.'
    //             ));
    //     }
    // }

    return $errors;
  }

}
