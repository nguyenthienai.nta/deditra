<?php

final class PhabricatorHephaestosKeyResultStartDateTransaction
    extends PhabricatorHephaestosKeyResultTransactionType {

    const TRANSACTIONTYPE = 'hkr.startDate';

    public function generateOldValue($object) {
        return $object->getStartDate();
    }

    public function applyInternalEffects($object, $value) {
        return $object->setStartDate($value);
    }

    public function getTitle() {
        return pht(
            '%s changed the start date for this kr.',
            $this->renderAuthor());
    }

    public function getTitleForFeed() {
        return pht(
            '%s changed the start date for kr %s.',
            $this->renderAuthor(),
            $this->renderObject());
    }

    public function getIcon() {
        $new = $this->getNewValue();
        if ($new == PhameConstants::VISIBILITY_PUBLISHED) {
            return 'fa-rss';
        } else if ($new == PhameConstants::VISIBILITY_ARCHIVED) {
            return 'fa-ban';
        } else {
            return 'fa-eye-slash';
        }
    }

    public function validateTransactions($object, array $xactions) {
      $errors = array();

      if ($this->isEmptyTextTransaction($object->getStartDate(), $xactions)) {
        $errors[] = $this->newRequiredError(
          pht('KR must have a start date.'));
      }

      return $errors;
    }
}
