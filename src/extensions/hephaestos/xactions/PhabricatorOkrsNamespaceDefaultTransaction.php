<?php
//
//final class PhabricatorOkrsNamespaceDefaultTransaction
//    extends PhabricatorOkrsNamespaceTransactionType
//{
//
//    const TRANSACTIONTYPE = 'okrs:default';
//
//    public function generateOldValue($object)
//    {
//        return $object->getIsDefaultNamespace();
//    }
//
//    public function applyInternalEffects($object, $value)
//    {
//        $object->setIsDefaultNamespace($value);
//    }
//
//    public function getTitle()
//    {
//        return pht(
//            '%s made this the default objective.',
//            $this->renderAuthor());
//    }
//
//    public function getTitleForFeed()
//    {
//        return pht(
//            '%s made space %s the default objective.',
//            $this->renderAuthor(),
//            $this->renderObject());
//
//    }
//
//    public function validateTransactions($object, array $xactions)
//    {
//        $errors = array();
//
//        if (!$this->isNewObject()) {
//            foreach ($xactions as $xaction) {
//                $errors[] = $this->newInvalidError(
//                    pht('Only the first space created can be the default objective, and ' .
//                        'it must remain the default objective evermore.'));
//            }
//        }
//
//        return $errors;
//    }
//
//}
