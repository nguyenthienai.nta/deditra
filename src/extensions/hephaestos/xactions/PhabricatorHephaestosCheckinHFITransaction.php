<?php

final class PhabricatorHephaestosCheckinHFITransaction
  extends PhabricatorHephaestosCheckinTransactionType {

  const TRANSACTIONTYPE = 'hcheckin.hfi';

  public function generateOldValue($object) {
    return $object->getHFI();
  }

  public function applyInternalEffects($object, $value) {
    $object->setHFI($value);
  }

  public function validateTransactions($object, array $xactions) {
    return array();
  }


}
