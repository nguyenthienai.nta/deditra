<?php

final class PhabricatorHephaestosKeyResultTargetTransaction
    extends PhabricatorHephaestosKeyResultTransactionType {

    const TRANSACTIONTYPE = 'hkr.target';

    public function generateOldValue($object) {
        return $object->getTarget();
    }

    public function applyInternalEffects($object, $value) {
        return $object->setTarget($value == "" ? 0 : $value);
    }

    public function getTitle() {
        return pht(
            '%s updated the kr Target.',
            $this->renderAuthor());
    }

    public function getTitleForFeed() {
        return pht(
            '%s updated the kr Target for %s.',
            $this->renderAuthor(),
            $this->renderObject());
    }

    public function validateTransactions($object, array $xactions) {
      return array();
    }

}
