<?php

final class PhabricatorHephaestosKeyResultMeansureTransaction
    extends PhabricatorHephaestosKeyResultTransactionType {

    const TRANSACTIONTYPE = 'hkr.meansure';

    public function generateOldValue($object) {
        return $object->getMeansure();
    }

    public function applyInternalEffects($object, $value) {
        $object->setMeansure($value);
    }

    public function getTitle() {
        return pht(
            '%s updated the kr meansure.',
            $this->renderAuthor());
    }

    public function getTitleForFeed() {
        return pht(
            '%s updated the kr meansure for %s.',
            $this->renderAuthor(),
            $this->renderObject());
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();

        $max_length = $object->getColumnMaximumByteLength('meansure');
        foreach ($xactions as $xaction) {
            $new_value = $xaction->getNewValue();
            $new_length = strlen($new_value);
            if ($new_length > $max_length) {
                $errors[] = $this->newInvalidError(
                    'The description can be no longer than 255 characters.');
            }
        }

        return $errors;
    }

}
