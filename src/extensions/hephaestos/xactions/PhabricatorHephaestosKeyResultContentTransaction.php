<?php

final class PhabricatorHephaestosKeyResultContentTransaction
    extends PhabricatorHephaestosKeyResultTransactionType {

    const TRANSACTIONTYPE = 'hkr.content';

    public function generateOldValue($object) {
        return $object->getTitle();
    }

    public function applyInternalEffects($object, $value) {
        $object->setTitle(trim($value));
    }

    public function getTitle() {
        return pht(
            '%s renamed this kr from %s to %s.',
            $this->renderAuthor(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

    public function getTitleForFeed() {
        return pht(
            '%s renamed %s kr from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();

        if ($this->isEmptyTextTransaction($object->getTitle(), $xactions)) {
            $errors[] = $this->newRequiredError(
                pht('KR must have a name.'));
        }

        $max_length = $object->getColumnMaximumByteLength('title');
        foreach ($xactions as $xaction) {
            $new_value = trim($xaction->getNewValue());
            $new_length = strlen($new_value);
            if ($new_length == 0) {
                $errors[] = $this->newRequiredError(
                    pht('KR must have a name.'));
                }
            if ($new_length > $max_length) {
                $errors[] = $this->newInvalidError(
                  'The name can be no longer than 255 characters.');
            }
        }

        return $errors;
    }

}
