<?php

final class PhabricatorHephaestosKeyResultDayOfWeekTransaction
  extends PhabricatorHephaestosKeyResultTransactionType {

  const TRANSACTIONTYPE = 'hkr.dateOfWeek';

  public function generateOldValue($object) {
    return $object->getDayOfWeeks();
  }

  public function applyInternalEffects($object, $value) {
    $object->setDayOfWeeks($value);
  }

  public function getTitle() {
    return pht(
      '%s updated day of week.',
      $this->renderAuthor());
  }

  public function getTitleForFeed() {
    return pht(
      '%s updated day of week for %s.',
      $this->renderAuthor(),
      $this->renderObject());
  }

  public function validateTransactions($object, array $xactions) {
    return array();
  }

}
