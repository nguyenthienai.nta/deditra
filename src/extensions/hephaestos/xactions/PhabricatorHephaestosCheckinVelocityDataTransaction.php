<?php

final class PhabricatorHephaestosCheckinVelocityDataTransaction
    extends PhabricatorHephaestosCheckinTransactionType {

    const TRANSACTIONTYPE = 'hcheckin.velocitydata';

    public function generateOldValue($object) {
        return $object->getVelocityData();
    }

    public function applyInternalEffects($object, $value) {
        $object->setVelocityData($value);
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();
        return $errors;
    }

}
