<?php

final class PhabricatorHephaestosKeyResultIsAutoCheckinTransaction
  extends PhabricatorHephaestosKeyResultTransactionType {

  const TRANSACTIONTYPE = 'hkr.autoCheckin';

  public function generateOldValue($object) {
    return $object->getAutoCheckin();
  }

  public function applyInternalEffects($object, $value) {
    $object->setAutoCheckin($value);
  }

  public function getTitle() {
    return pht(
      '%s updated the auto checkin.',
      $this->renderAuthor());
  }

  public function getTitleForFeed() {
    return pht(
      '%s updated the current auto checkin for %s.',
      $this->renderAuthor(),
      $this->renderObject());
  }

  public function validateTransactions($object, array $xactions) {
    return array();
  }

}
