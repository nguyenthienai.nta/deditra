<?php

final class PhabricatorHephaestosOkrProjectTransaction
    extends PhabricatorHephaestosOkrTransactionType {

    const TRANSACTIONTYPE = 'hephaestos.okr.project';

    public function generateOldValue($object) {
        return null;
    }
    public function generateNewValue($object, $value) {
        return null;
      }
    public function applyInternalEffects($object, $value) {
        return null;
    }
    public function getTitle() {
        return pht(
            '%s changed the project for this .',
            $this->renderAuthor());
    }

    public function getTitleForFeed() {
        return pht(
            '%s changed the project for  %s.',
            $this->renderAuthor(),
            $this->renderObject());
    }
    public function validateTransactions($object, array $xactions) {
        $errors = array();

        foreach ($xactions as $xaction) {
            $new_value = $xaction->getNewValue();
            if($new_value == null){
                $errors[] = $this->newRequiredError(
                    pht('Hephaestos must have a project.'));
            } else {
                $project = id(new PhabricatorProject())->loadOneWhere('PHID = %s', array_pop($new_value));
                if(isset($project) && $project->isMileStone()){
                    $errors[] = $this->newInvalidError(
                        pht("Project must not be milestone"));
                }
            }
        }
        return $errors;
    }

}
