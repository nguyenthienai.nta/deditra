<?php

final class PhabricatorHephaestosCheckinCommentTransaction
    extends PhabricatorHephaestosCheckinTransactionType {

    const TRANSACTIONTYPE = 'hcheckin.comment';

    public function generateOldValue($object) {
        return $object->getComment();
    }

    public function applyInternalEffects($object, $value) {
        $object->setComment($value);
    }

    public function getTitle() {
        $old = $this->getOldValue();
        $currentPHID = $this->getObjectPHID();
        $current = id(new PhabricatorHephaestosCheckin())->loadOneWhere('PHID = %s', $currentPHID);
        $dateCheckin = date('d/m/Y', $current->getDateCreated());
        if (!strlen($old)) {
        return pht(
            "%s add comment for check-in at %s: %s",
            $this->renderAuthor(),
            $dateCheckin,
            $this->renderNewValue()
            );
        } else {
        return pht(
            "%s update comment for check-in at %s: %s",
            $this->renderAuthor(),
            $dateCheckin,
            $this->renderNewValue());
        }
    }

    public function getTitleForFeed() {
        return pht(
            '%s updated the comment for %s.',
            $this->renderAuthor(),
            $this->renderOldValue());
    }
}
