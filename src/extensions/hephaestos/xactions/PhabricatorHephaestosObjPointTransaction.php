<?php

final class PhabricatorHephaestosObjPointTransaction
  extends PhabricatorHephaestosObjTransactionType
{

  const TRANSACTIONTYPE = 'hephaestos_obj:point';

  public function generateOldValue($object)
  {
    return $object->getPoint();
  }

  public function applyInternalEffects($object, $value)
  {
    $object->setPoint($value);
  }

  public function getTitle()
  {
    $old = $this->getOldValue();
    if (!strlen($old)) {
      return pht(
        '%s created this objective.',
        $this->renderAuthor());
    } else {
      return pht(
        '%s renamed this objective from %s to %s.',
        $this->renderAuthor(),
        $this->renderOldValue(),
        $this->renderNewValue());
    }
  }

  public function getTitleForFeed()
  {
    return pht(
      '%s renamed objective %s from %s to %s.',
      $this->renderAuthor(),
      $this->renderObject(),
      $this->renderOldValue(),
      $this->renderNewValue());
  }

  public function validateTransactions($object, array $xactions)
  {
    $errors = array();

    foreach ($xactions as $xaction) {
      $new_value = explode(',', $xaction->getNewValue());
      if ($new_value[0] == '') {
        $errors['required'] = $this->newRequiredError(
          pht('Success rule must not null.'));
      }

      if ($new_value[0] === '-0') {
        $errors['invalid'] = $this->newRequiredError(
          pht('Invalid number.'));
      }

      for ($i = 1; $i < 10; $i++) {
        if ($new_value[$i] < 0 || $new_value[$i] === '-0') {
          $errors['invalid'] = $this->newRequiredError(
            pht('Invalid number.'));
        }
        if ($new_value[$i] == '') {
          $errors['required'] = $this->newRequiredError(
            pht('Success rule must not null.'));
        }
        if ((float)$new_value[$i] < (float)$new_value[$i - 1]) {
          $errors['invalid'] = $this->newInvalidError(
            pht('Success rule: The next number must be greater or equal to the previous number.'));
        }
      }
    }

    return array_values($errors);
  }

}
