<?php

final class PhabricatorHephaestosKeyResultConfidenceLevelTransaction
    extends PhabricatorHephaestosKeyResultTransactionType {

    const TRANSACTIONTYPE = 'hkr.confidenceLevel';

    public function generateOldValue($object) {
        return $object->getConfidenceLevel();
    }

    public function applyInternalEffects($object, $value) {
        $object->setConfidenceLevel($value);
    }

    public function getTitle() {
        return pht(
            '%s updated the kr confidenceLevel.',
            $this->renderAuthor());
    }

    public function getTitleForFeed() {
        return pht(
            '%s updated the kr confidenceLevel for %s.',
            $this->renderAuthor(),
            $this->renderObject());
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();

        if ($this->isEmptyTextTransaction($object->getConfidenceLevel(), $xactions)) {
            $errors[] = $this->newRequiredError(
                pht('KR must have a confidenceLevel.'));
        }
        return $errors;
    }

}
