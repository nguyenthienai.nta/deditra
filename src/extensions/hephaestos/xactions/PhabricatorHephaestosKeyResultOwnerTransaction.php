<?php

final class PhabricatorHephaestosKeyResultOwnerTransaction
    extends PhabricatorHephaestosKeyResultTransactionType {

    const TRANSACTIONTYPE = 'hkr.owner';

    public function generateOldValue($object) {
        return $object->getOwnerPHID();
    }

    public function applyInternalEffects($object, $value) {
        $object->setOwnerPHID($value);
    }

    public function getTitle() {
        return pht(
            '%s changed the owner for this kr.',
            $this->renderAuthor());
    }

    public function getTitleForFeed() {
        return pht(
            '%s changed the owner for kr %s.',
            $this->renderAuthor(),
            $this->renderObject());
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();

        if ($this->isEmptyTextTransaction($object->getOwnerPHID(), $xactions)) {
            $errors[] = $this->newRequiredError(
                pht('KR must have an owner.'));
        }

        return $errors;
    }

}
