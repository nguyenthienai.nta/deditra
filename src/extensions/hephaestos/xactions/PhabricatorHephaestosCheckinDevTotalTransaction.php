<?php

final class PhabricatorHephaestosCheckinDevTotalTransaction
  extends PhabricatorHephaestosCheckinTransactionType {

  const TRANSACTIONTYPE = 'hcheckin.dev';

  public function generateOldValue($object) {
    return $object->getTotalDev();
  }

  public function applyInternalEffects($object, $value) {
    $object->setTotalDev($value);
  }

  public function validateTransactions($object, array $xactions) {
    return array();
  }


}
