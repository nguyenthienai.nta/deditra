<?php

final class PhabricatorHephaestosApplication extends PhabricatorApplication
{

  public function getBaseURI()
  {
    return '/hephaestos/';
  }

  public function getName()
  {
    return pht('Hephaestos');
  }

  public function getShortDescription()
  {
    return pht('Hephaestos');
  }

  public function getIcon()
  {
    return 'fa-gavel';
  }

  public function getTitleGlyph()
  {
    return "\xE2\x97\x8E";
  }

  public function getFlavorText()
  {
    return pht('Control access to groups of objects.');
  }

  public function getApplicationGroup()
  {
    return self::GROUP_UTILITIES;
  }

  public function canUninstall()
  {
    return false;
  }

  public function getRemarkupRules()
  {
    return array(
      new PhabricatorHephaestosRemarkupRule(),
    );
  }

  public function getRoutes()
  {
    return array(
      '/hephaestos/' => array(
        '(?:query/(?P<queryKey>[^/]+)/)?' => 'PhabricatorHephaestosListOkrController',
//        'create/' => 'PhabricatorHephaestosEditOkrController',
        'edit/(?:(?P<id>\d+)/)?' => 'PhabricatorHephaestosEditOkrController',
        'delete-obj/(?P<id>[1-9]\d*)/'
        => 'PhabricatorHephaestosDestroyObjController',
        'delete/key&result/(?:(?P<id>\d+)/)' => 'PhabricatorHephaestosDestroyKRController',
        '(?P<action>activate|archive)/(?P<id>\d+)/'
        => 'PhabricatorHephaestosArchiveController',
        'create-tracker/' => 'PhabricatorHephaestosEditTrackerController',
      ),

      '/hephaestos/obj/' => array(
        '(?:query/(?P<queryKey>[^/]+)/)?' => 'PhabricatorHephaestosObjListController',
        '(?P<id>[1-9]\d*)/(?:query/(?P<queryKey>[^/]+)/)?' => 'PhabricatorHephaestosObjListController',

        'create/' => 'PhabricatorHephaestosEditObjController',
        'edit/(?:(?P<id>\d+)/)?' => 'PhabricatorHephaestosEditObjController',

        'edit/key&result/(?:(?P<id>\d+)/)' => 'PhabricatorHephaestosKrEditController',
        'kr/' => array(
          $this->getEditRoutePattern('edit/') => 'PhabricatorHephaestosKrEditController',
        ),
      ),

      '/hephaestos/kr/' => array(
        $this->getEditRoutePattern('edit/') => 'PhabricatorHephaestosKrEditController',
        'checkin/(?P<id>[1-9]\d*)/'
        => 'PhabricatorHephaestosCheckinController',
        'detail/(?P<id>[1-9]\d*)/' => 'PhabricatorHephaestosKrDetailController',
      ),
    );
  }

  public function isPinnedByDefault(PhabricatorUser $viewer)
  {
    return true;
  }

  protected function getCustomCapabilities()
  {
    return array(
      PhabricatorHephaestosCapabilityCreate::CAPABILITY => array(
        'default' => PhabricatorPolicies::POLICY_USER,
      ),
      PhabricatorHephaestosCapabilityDefaultView::CAPABILITY => array(
        'caption' => pht('Default view policy for newly created Okrs.'),
        'default' => PhabricatorPolicies::POLICY_USER,
        'template' => PhabricatorHephaestosObjectivePHIDType::TYPECONST,
        'capability' => PhabricatorPolicyCapability::CAN_VIEW,
      ),
      PhabricatorHephaestosCapabilityDefaultEdit::CAPABILITY => array(
        'caption' => pht('Default edit policy for newly created Okrs.'),
        'default' => PhabricatorPolicies::POLICY_USER,
        'template' => PhabricatorHephaestosObjectivePHIDType::TYPECONST,
        'capability' => PhabricatorPolicyCapability::CAN_EDIT,
      ),
    );
  }

}
