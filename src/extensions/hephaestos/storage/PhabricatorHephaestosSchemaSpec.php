<?php

final class PhabricatorHephaestosSchemaSpec
    extends PhabricatorConfigSchemaSpec
{

    public function buildSchemata()
    {
        $this->buildEdgeSchemata(new PhabricatorHephaestosObjective());
    }

}
