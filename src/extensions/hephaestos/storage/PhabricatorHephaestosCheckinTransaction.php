<?php

final class PhabricatorHephaestosCheckinTransaction
    extends PhabricatorModularTransaction {


    public function getApplicationName() {
        return 'hephaestos';
    }

    public function getApplicationTransactionType() {
        return PhabricatorHephaestosCheckinPHIDType::TYPECONST;
    }

    public function getBaseTransactionClass() {
        return 'PhabricatorHephaestosCheckinTransactionType';
    }

}
