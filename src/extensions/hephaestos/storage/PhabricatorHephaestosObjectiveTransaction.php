<?php

final class PhabricatorHephaestosObjectiveTransaction
    extends PhabricatorModularTransaction
{

    public function getApplicationName()
    {
        return 'hephaestos';
    }

    public function getApplicationTransactionType()
    {
        return PhabricatorHephaestosObjectivePHIDType::TYPECONST;
    }

    public function getBaseTransactionClass()
    {
        return 'PhabricatorHephaestosObjTransactionType';
    }

}
