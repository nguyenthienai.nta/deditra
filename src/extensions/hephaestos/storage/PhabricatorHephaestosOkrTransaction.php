<?php

final class PhabricatorHephaestosOkrTransaction
    extends PhabricatorModularTransaction {


    public function getApplicationName() {
        return 'hephaestos';
    }

    public function getApplicationTransactionType() {
        return PhabricatorHephaestosOkrPHIDType::TYPECONST;
    }

    public function getBaseTransactionClass() {
        return 'PhabricatorHephaestosOkrTransactionType';
    }

}
