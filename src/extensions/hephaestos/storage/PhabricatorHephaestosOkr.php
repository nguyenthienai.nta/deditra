<?php

final class PhabricatorHephaestosOkr
  extends PhabricatorHephaestosDAO
  implements
  PhabricatorPolicyInterface,
  PhabricatorApplicationTransactionInterface,
  PhabricatorDestructibleInterface,
  PhabricatorFlaggableInterface,
  PhabricatorProjectInterface
{

  protected $content;
  protected $viewPolicy;
  protected $editPolicy;
  protected $phid;
  protected $isArchived;
  private $objective = self::ATTACHABLE;


  public static function initializeNewOkrs(PhabricatorUser $actor)
  {
    $app = id(new PhabricatorApplicationQuery())
      ->setViewer($actor)
      ->withClasses(array('PhabricatorHephaestosApplication'))
      ->executeOne();



    return id(new PhabricatorHephaestosOkr())

      ->setIsArchived(0);
  }

  protected function getConfiguration()
  {
    return array(
        self::CONFIG_AUX_PHID => true,
        self::CONFIG_COLUMN_SCHEMA => array(
          'content' => 'text255',
          'isArchived' => 'bool',
        ),
        self::CONFIG_KEY_SCHEMA => array(
          'phid' => array(
            'columns' => array('phid'),
            'unique' => true,
          ),
        ),
      ) + parent::getConfiguration();
  }

  public function generatePHID()
  {
    return PhabricatorPHID::generateNewPHID(
      PhabricatorHephaestosOkrPHIDType::TYPECONST);
  }

  public function getMonogram()
  {
    return $this->getContent();
  }


  /* -(  PhabricatorPolicyInterface  )----------------------------------------- */


  public function getCapabilities()
  {
    return array(
      PhabricatorPolicyCapability::CAN_VIEW,
      PhabricatorPolicyCapability::CAN_EDIT,
    );
  }

  public function getPolicy($capability)
  {
    switch ($capability) {
      case PhabricatorPolicyCapability::CAN_VIEW:
        return $this->getViewPolicy();
      case PhabricatorPolicyCapability::CAN_EDIT:
        return $this->getEditPolicy();
    }
  }

  public function hasAutomaticCapability($capability, PhabricatorUser $viewer)
  {
    return false;
  }

  /* -(  PhabricatorApplicationTransactionInterface  )------------------------- */


  public function getApplicationTransactionEditor()
  {
    return new PhabricatorHephaestosOkrEditor();
  }

  public function getApplicationTransactionTemplate()
  {
    return new PhabricatorHephaestosOkrTransaction();
  }


  /* -(  PhabricatorDestructibleInterface  )----------------------------------- */


  public function destroyObjectPermanently(
    PhabricatorDestructionEngine $engine)
  {
    $this->delete();
  }

}
