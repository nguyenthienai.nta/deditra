<?php

final class PhabricatorHephaestosObjective
  extends PhabricatorHephaestosDAO
  implements
  PhabricatorPolicyInterface,
  PhabricatorApplicationTransactionInterface,
  PhabricatorDestructibleInterface
{

  protected $title;
  protected $point;
  protected $viewPolicy;
  protected $editPolicy;
  protected $description;
  protected $isArchived;
  protected $hephaestosPHID;

  private $okr = self::ATTACHABLE;

  public static function initializeNewNamespace(PhabricatorUser $actor, PhabricatorHephaestosOkr $okr)
  {
    $app = id(new PhabricatorApplicationQuery())
      ->setViewer($actor)
      ->withClasses(array('PhabricatorHephaestosApplication'))
      ->executeOne();

    $view_policy = $app->getPolicy(
      PhabricatorHephaestosCapabilityDefaultView::CAPABILITY);
    $edit_policy = $app->getPolicy(
      PhabricatorHephaestosCapabilityDefaultEdit::CAPABILITY);

    return id(new PhabricatorHephaestosObjective())
      ->setViewPolicy($view_policy)
      ->setEditPolicy($edit_policy)
      ->setDescription('')
      ->setIsArchived(0)
      ->setHephaestosPHID($okr->getPHID())
      ->attachOkr($okr);
  }

  public function attachOkr(PhabricatorHephaestosOkr $okr)
  {
    $this->okr = $okr;
    return $this;
  }

  public function getOkr()
  {
    return $this->assertAttached($this->okr);
  }

  protected function getConfiguration()
  {
    return array(
        self::CONFIG_AUX_PHID => true,
        self::CONFIG_COLUMN_SCHEMA => array(
          'title' => 'text255',
          'point' => 'text',
          'description' => 'text',
          'isArchived' => 'bool',
          'hephaestosPHID' => 'phid?',
        ),
        self::CONFIG_KEY_SCHEMA => array(
          'key_default' => array(
            'columns' => array('isDefaultNamespace'),
            'unique' => true,
          ),
        ),
      ) + parent::getConfiguration();
  }

  public function generatePHID()
  {
    return PhabricatorPHID::generateNewPHID(
      PhabricatorHephaestosOkrPHIDType::TYPECONST);
  }

  public function getMonogram()
  {
    return $this->getTitle();
  }


  /* -(  PhabricatorPolicyInterface  )----------------------------------------- */


  public function getCapabilities()
  {
    return array(
      PhabricatorPolicyCapability::CAN_VIEW,
      PhabricatorPolicyCapability::CAN_EDIT,
    );
  }

  public function getPolicy($capability)
  {
    switch ($capability) {
      case PhabricatorPolicyCapability::CAN_VIEW:
        return $this->getViewPolicy();
      case PhabricatorPolicyCapability::CAN_EDIT:
        return $this->getEditPolicy();
    }
  }

  public function hasAutomaticCapability($capability, PhabricatorUser $viewer)
  {
    return false;
  }

  /* -(  PhabricatorApplicationTransactionInterface  )------------------------- */


  public function getApplicationTransactionEditor()
  {
    return new PhabricatorHephaestosObjectiveEditor();
  }

  public function getApplicationTransactionTemplate()
  {
    return new PhabricatorHephaestosObjectiveTransaction();
  }


  /* -(  PhabricatorDestructibleInterface  )----------------------------------- */


  public function destroyObjectPermanently(
    PhabricatorDestructionEngine $engine)
  {
    $this->openTransaction();

    $okrs = id(new PhabricatorHephaestosOkrQuery())
      ->setViewer($engine->getViewer())
      ->withSpacePHIDs(array($this->getPHID()))
      ->execute();
    foreach ($okrs as $okrs) {
      $engine->destroyObject($okrs);
    }
    $this->delete();

    $this->saveTransaction();
  }

}
