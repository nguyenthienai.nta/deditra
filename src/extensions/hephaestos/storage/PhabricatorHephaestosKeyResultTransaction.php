<?php

final class PhabricatorHephaestosKeyResultTransaction
    extends PhabricatorModularTransaction {


    public function getApplicationName() {
        return 'hephaestos';
    }

    public function getApplicationTransactionType() {
        return PhabricatorHephaestosKeyResultPHIDType::TYPECONST;
    }

    public function getBaseTransactionClass() {
        return 'PhabricatorHephaestosKeyResultTransactionType';
    }

}
