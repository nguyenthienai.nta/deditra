<?php
abstract class PhabricatorHephaestosBaseEditTrackerController extends PhabricatorController
{
}

final class PhabricatorHephaestosEditTrackerController
  extends PhabricatorHephaestosBaseEditTrackerController
{
  public function handleRequest(AphrontRequest $request)
  {
    $viewer = $request->getUser();

    $this->requireApplicationCapability(
      PhabricatorHephaestosCapabilityCreate::CAPABILITY
    );
    $okr = PhabricatorHephaestosOkr::initializeNewOkrs($viewer);
    $is_new = true;
    $cancel_uri = $this->getApplicationURI();
    $title = pht('Create Hephaestos');

    $validation_exception = null;
    $e_name = true;
    $e_project = true;
    $v_name = $okr->getContent();
    $v_view = $okr->getViewPolicy();
    $v_edit = $okr->getEditPolicy();
    $v_project = array();
    if ($request->isFormPost()) {
      $xactions = array();
      $e_name = null;
      $e_project = null;
      $v_name = $request->getStr('name');
      $v_view = $request->getStr('viewPolicy');
      $v_edit = $request->getStr('editPolicy');
      $v_project = $request->getArr('projectPHID');

      $type_name =
        PhabricatorHephaestosOkrContentTransaction::TRANSACTIONTYPE;
      $type_view = PhabricatorTransactions::TYPE_VIEW_POLICY;
      $type_edit = PhabricatorTransactions::TYPE_EDIT_POLICY;
      $type_project = PhabricatorHephaestosOkrProjectTransaction::TRANSACTIONTYPE;

      $xactions[] = id(new PhabricatorHephaestosOkrTransaction())
        ->setTransactionType($type_name)
        ->setNewValue($v_name);
      $xactions[] = id(new PhabricatorHephaestosOkrTransaction())
        ->setTransactionType($type_view)
        ->setNewValue($v_view);
      $xactions[] = id(new PhabricatorHephaestosOkrTransaction())
        ->setTransactionType($type_edit)
        ->setNewValue($v_edit);
      $xactions[] = id(new PhabricatorHephaestosOkrTransaction())
        ->setTransactionType($type_project)
        ->setNewValue($v_project);

      $editor = id(new PhabricatorHephaestosOkrEditor())
        ->setActor($viewer)
        ->setContinueOnNoEffect(true)
        ->setContentSourceFromRequest($request);
      try {
        $editor->applyTransactions($okr, $xactions);
        $projectPHID = array_pop($v_project);
        $this->createObjectiveAndKr($okr, $projectPHID);
        session_start();
        $_SESSION['message'] = 'Hephaestos added successfully';
        if( $request->getBool('check')){
          return id(new AphrontRedirectResponse())
            ->setURI('/hephaestos/create-tracker/');
        }else
          return id(new AphrontRedirectResponse())
            ->setURI('/hephaestos/');
      } catch (PhabricatorApplicationTransactionValidationException $ex) {
        $validation_exception = $ex;

        $e_name = $ex->getShortMessage($type_name);
        $e_project = $ex->getShortMessage($type_project);
      }
    }
    $policies = id(new PhabricatorPolicyQuery())
      ->setViewer($viewer)
      ->setObject($okr)
      ->execute();
    Javelin::initBehavior(
      'submit-objective'
    );
    $okrs_police = PhabricatorHephaestosOkr::initializeNewOkrs($viewer);
    $form = id(new AphrontFormView())
      ->setUser($viewer);
    $form
      ->appendChild(
        id(new AphrontFormTextControl())
          ->setLabel(pht('Name'))
          ->setName('name')
          ->setValue($v_name)
          ->setError($e_name)
      )
      ->appendChild(
        id(new AphrontFormPolicyControl())
          ->setUser($viewer)
          ->setCapability(PhabricatorPolicyCapability::CAN_VIEW)
          ->setPolicyObject($okrs_police)
          ->setPolicies($policies)
          ->setValue($v_view)
          ->setName('viewPolicy')
      )
      ->appendChild(
        id(new AphrontFormPolicyControl())
          ->setUser($viewer)
          ->setCapability(PhabricatorPolicyCapability::CAN_EDIT)
          ->setPolicyObject($okrs_police)
          ->setPolicies($policies)
          ->setValue($v_edit)
          ->setName('editPolicy')
      )
      ->appendControl(
        id(new AphrontFormTokenizerKRControl())
          ->setLabel(pht('Project'))
          ->setName('projectPHID')
          ->setLimit(1)
          ->setDatasource(new PhabricatorProjectDatasource())
          ->setID('projectPHID')
          ->setValue($v_project)
          ->setError($e_project)
      )
      ->addHiddenInput('check', 'false');

    $form->appendChild(
      id(new AphrontFormSubmitControl())
        ->addButton(id(new PHUIButtonView())
          ->setText('Save And New')
          ->addSigil('submitObjetiveButton')
          ->setColor(PHUIButtonView::GREEN))
        ->addButton(id(new PHUIButtonView())
          ->setText('Save'))
        ->addCancelButton($cancel_uri)
    );

    $box = id(new PHUIObjectBoxView())
      ->setHeaderText($title)
      ->setBackground(PHUIObjectBoxView::WHITE_CONFIG)
      ->setValidationException($validation_exception)
      ->appendChild($form);

    session_start();
    if (isset($_SESSION['message'])) {
      $box->setFormSaved(true, $_SESSION['message']);
      unset($_SESSION['message']);
    }

    $crumbs = $this->buildApplicationCrumbs();
    if (!$is_new) {
      $crumbs->addTextCrumb(
        "Edit Hephaestos"
      );
    } else {
      $crumbs->addTextCrumb($title);
    }

    $crumbs->setBorder(true);

    $view = id(new PHUITwoColumnView())
      ->setFooter(array(
        $box,
      ));

    return $this->newPage()
      ->setTitle($title)
      ->setCrumbs($crumbs)
      ->appendChild($view);
  }
  protected function createObjectiveAndKr($okr, $projectPHID)
  {
    $viewer = $this->getViewer();
    $exactProgress = PhabricatorHephaestosObjective::initializeNewNamespace($viewer, $okr);
    $exactProgress->setTitle('Tiến độ chuẩn xác')
      ->setPoint('10,20,30,40,50,60,70,80,90,100')
      ->save();
    $project = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $projectPHID);
    $milestones = $this->getMilestones($project);
    $milestones = array_reverse($milestones);
    foreach ($milestones as $milestone) {
      if ($milestone->getIsSprint() == true) {
        $keyResultProject = PhabricatorHephaestosKeyResult::initializeKeyResult($exactProgress);
        $keyResultProject->setTitle($milestone->getName() . ' - Tiến độ')
          ->setStartDate($milestone->getStartDate())
          ->setEndDate($milestone->getEndDate())
          ->setOwnerPHID($viewer->getPHID())
          ->setFrequenceTime(1)
          ->setFrequence('Day')
          ->setTotalDev(-1)
          ->setProjectPHID($milestone->getPHID())
          ->setTarget(0)
          ->setBaseValue(0)
          ->setMeansure("")
          ->setConfidenceLevel(1)
          ->setManualCheckin(1)
          ->setTypeChart(0)
          ->save();
      } else if (strpos(strtolower($milestone->getName()), "sprint") !== false) {
        $keyResultProject = PhabricatorHephaestosKeyResult::initializeKeyResult($exactProgress);
        $keyResultProject->setTitle($milestone->getName() . ' - Tiến độ')
          ->setStartDate(strtotime(date('m/d/Y')))
          ->setEndDate(strtotime(date('m/d/Y') . '+ 30day'))
          ->setOwnerPHID($viewer->getPHID())
          ->setFrequenceTime(1)
          ->setFrequence('Day')
          ->setTotalDev(-1)
          ->setProjectPHID($milestone->getPHID())
          ->setTarget(0)
          ->setBaseValue(0)
          ->setMeansure("")
          ->setConfidenceLevel(1)
          ->setManualCheckin(1)
          ->setTypeChart(0)
          ->save();
      }
    }
    $excellentQuality = PhabricatorHephaestosObjective::initializeNewNamespace($viewer, $okr);
    $excellentQuality->setTitle('Chất lượng tuyệt hảo')
      ->setPoint('10,20,30,40,50,60,70,80,90,100')
      ->save();
    $keyResultHII = PhabricatorHephaestosKeyResult::initializeKeyResult($excellentQuality);
    $keyResultHII->setTitle('HII')
      ->setStartDate(strtotime(date('m/d/Y')))
      ->setEndDate(strtotime(date('m/d/Y') . '+ 30day'))
      ->setOwnerPHID($viewer->getPHID())
      ->setFrequenceTime(1)
      ->setFrequence('Day')
      ->setTotalDev(1)
      ->setProjectPHID($projectPHID)
      ->setTarget(0)
      ->setBaseValue(0)
      ->setMeansure("")
      ->setConfidenceLevel(1)
      ->setManualCheckin(4)
      ->setTypeChart(0)
      ->save();
    $keyResultHFI = PhabricatorHephaestosKeyResult::initializeKeyResult($excellentQuality);
    $keyResultHFI->setTitle('HFI')
      ->setStartDate(strtotime(date('m/d/Y')))
      ->setEndDate(strtotime(date('m/d/Y') . '+ 30day'))
      ->setOwnerPHID($viewer->getPHID())
      ->setFrequenceTime(1)
      ->setFrequence('Day')
      ->setTotalDev(1)
      ->setProjectPHID($projectPHID)
      ->setTarget(0)
      ->setBaseValue(0)
      ->setMeansure("")
      ->setConfidenceLevel(1)
      ->setManualCheckin(5)
      ->setTypeChart(0)
      ->save();
    $superiorProductivity = PhabricatorHephaestosObjective::initializeNewNamespace($viewer, $okr);
    $superiorProductivity->setTitle('Năng suất vượt trội')
      ->setPoint('10,20,30,40,50,60,70,80,90,100')
      ->save();
    $keyResultVelocity = PhabricatorHephaestosKeyResult::initializeKeyResult($superiorProductivity);
    $keyResultVelocity->setTitle('Velocity')
      ->setStartDate(strtotime(date('m/d/Y')))
      ->setEndDate(strtotime(date('m/d/Y') . '+ 30day'))
      ->setOwnerPHID($viewer->getPHID())
      ->setFrequenceTime(1)
      ->setFrequence('Day')
      ->setTotalDev(-1)
      ->setProjectPHID($projectPHID)
      ->setTarget(0)
      ->setBaseValue(0)
      ->setMeansure("")
      ->setConfidenceLevel(1)
      ->setManualCheckin(2)
      ->setTypeChart(0)
      ->save();
    $keyResultEff = PhabricatorHephaestosKeyResult::initializeKeyResult($superiorProductivity);
    $keyResultEff->setTitle('Effective of Develop')
      ->setStartDate(strtotime(date('m/d/Y')))
      ->setEndDate(strtotime(date('m/d/Y') . '+ 30day'))
      ->setOwnerPHID($viewer->getPHID())
      ->setFrequenceTime(1)
      ->setFrequence('Day')
      ->setTotalDev(-1)
      ->setProjectPHID($projectPHID)
      ->setTarget(0)
      ->setBaseValue(0)
      ->setMeansure("")
      ->setConfidenceLevel(1)
      ->setManualCheckin(3)
      ->setTypeChart(0)
      ->save();
  }
  protected function getMilestones($project)
  {
    $viewer = $this->getViewer();
    $allows_milestones = $project->supportsMilestones();

    if ($allows_milestones) {
      $milestones = id(new PhabricatorProjectQuery())
        ->setViewer($viewer)
        ->withParentProjectPHIDs(array($project->getPHID()))
        ->needImages(true)
        ->withIsMilestone(true)
        ->setOrderVector(array('milestoneNumber', 'id'))
        ->execute();
    } else {
      $milestones = array();
    }
    return $milestones;
  }
}
