<?php
abstract class PhabricatorHephaestosObjListBaseListController extends PhabricatorController
{
}

final class PhabricatorHephaestosObjListController
    extends PhabricatorHephaestosObjListBaseListController
{

    public function shouldAllowPublic()
    {
        return true;
    }

    public function handleRequest(AphrontRequest $request)
    {
        $request = $this->getRequest();
        $id = $request->getURIData('id');
        $okr = id(new PhabricatorHephaestosOkr())->loadAllWhere('id = %s', $id);
        if (!$okr) {
            return new Aphront404Response();
        }

        $controller = id(new PhabricatorApplicationSearchController())
            ->setQueryKey($request->getURIData('queryKey'))
            ->setSearchEngine(new PhabricatorHephaestosObjectiveSearchEngine())
            ->setNavigation($this->buildSideNavView());

        return $this->delegateToController($controller);
    }

    public function buildSideNavView($for_app = false)
    {
        $user = $this->getRequest()->getUser();
        $request = $this->getRequest();
        $id = $request->getURIData('id');
        $okr = id(new PhabricatorHephaestosOkr())->loadAllWhere('id = %s', $id);
        $okr = array_pop(array_reverse($okr));

        $nav = new AphrontSideNavFilterView();
        $nav->setBaseURI(new PhutilURI($this->getApplicationURI()));
        $nav->addLabel(mb_strimwidth('Hephaestos: ' . $okr->getContent(), 0, 26, '...'));
        id(new PhabricatorHephaestosObjectiveSearchEngine())
            ->setViewer($user)
            ->addNavigationItems($nav->getMenu());

        $nav->selectFilter(null);

        return $nav;
    }

  protected function buildApplicationCrumbs()
  {
    $crumbs = parent::buildApplicationCrumbs();
    $request = $this->getRequest();
    $id = $request->getURIData('id');
    $objectives = id(new PhabricatorHephaestosOkr())->loadAllWhere('id = %s', $id);
    $objectives = array_pop(array_reverse($objectives));
    $can_create = $this->hasApplicationCapability(
      PhabricatorHephaestosCapabilityCreate::CAPABILITY);
    $crumbs->addTextCrumb(
      $objectives->getContent());

    $crumbs->addAction(
      id(new PHUIListItemView())
        ->setName(pht('New Objective'))
        ->setHref('/hephaestos/obj/edit/?okr=' . $id)
        ->setIcon('fa-plus-square')
        ->setDisabled(!$can_create)
        ->setWorkflow(!$can_create));

    return $crumbs;
  }

}
