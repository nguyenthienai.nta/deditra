<?php

abstract class PhabricatorHephaestosKrBaseEditController extends PhabricatorController
{
}

final class PhabricatorHephaestosKrEditController extends PhabricatorHephaestosKrBaseEditController
{
  public function handleRequest(AphrontRequest $request)
  {
    require_celerity_resource('krForm-css');
    require_celerity_resource('jquery-ui');
    $viewer = $request->getUser();

    $make_default = false;
    $id = $request->getURIData('id');

    require_celerity_resource('chart-css');
    require_celerity_resource('chartMin-css');

    Javelin::initBehavior(
      'chart-bundle'
    );
    Javelin::initBehavior(
      'chart'
    );
    Javelin::initBehavior(
      'chartjs-plugin-datalabels'
    );
    Javelin::initBehavior(
      'jquery'
    );
    Javelin::initBehavior(
      'jquery-ui'
    );
    Javelin::initBehavior(
      'multidatespicker'
    );
    Javelin::initBehavior(
      'kr'
    );
    Javelin::initBehavior(
      'chart-render'
    );


    if ($id) {
      $keyResult = id(new PhabricatorHephaestosKeyResultQuery())
        ->setViewer($viewer)
        ->withIDs(array($id))
        ->requireCapabilities(
          array(
            PhabricatorPolicyCapability::CAN_VIEW,
            PhabricatorPolicyCapability::CAN_EDIT,
          )
        )
        ->executeOne();

      $obj = id(new PhabricatorHephaestosObjective())->loadOneWhere('phid = %s',$keyResult->getObjectivePHID());
      $okrPHID = $obj->getHephaestosPHID();
      $okr = id(new PhabricatorHephaestosOkr())->loadOneWhere('phid = %s', $okrPHID);

      $query = id(new PhabricatorHephaestosOkrQuery())
        ->setViewer($viewer)
        ->withIDs(array($okr->getID()))
        ->requireCapabilities(
          array(
            PhabricatorPolicyCapability::CAN_VIEW,
            PhabricatorPolicyCapability::CAN_EDIT,
          ))
        ->executeOne();

      if (!$keyResult) {
        return new Aphront404Response();
      }
      $objective = $keyResult->getObjective();

      $okrPHID = $objective->getHephaestosPHID();
      $okr = id(new PhabricatorHephaestosOkrQuery())
        ->setViewer($this->getViewer())
        ->withPHIDs(array($okrPHID))
        ->executeOne();

      $is_new = false;
      $cancel_uri = '/hephaestos/obj/' . $okr->getId();

      $header_text = pht('Edit KR');
      $title = pht('Edit %s', $keyResult->getTitle());

    } else {

      $objective_id = head($request->getArr('objective'));
      if (!$objective_id) {
        $objective_id = $request->getStr('objective');
      }

      $okrs = id(new PhabricatorHephaestosObjectiveQuery())
        ->setViewer($viewer)
        ->withIDs(array($objective_id))
        ->requireCapabilities(
          array(
            PhabricatorPolicyCapability::CAN_VIEW,
            PhabricatorPolicyCapability::CAN_EDIT,
          ))
        ->executeOne();
      $Okrs = id(new PhabricatorHephaestosOkr())->loadOneWhere('phid = %s', $okrs->getHephaestosPHID());

      $okr = id(new PhabricatorHephaestosOkrQuery())
        ->setViewer($viewer)
        ->withIDs(array($Okrs->getID()))
        ->requireCapabilities(
          array(
            PhabricatorPolicyCapability::CAN_VIEW,
            PhabricatorPolicyCapability::CAN_EDIT,
          ))
        ->executeOne();

      $query = id(new PhabricatorHephaestosObjectiveQuery())
        ->setViewer($viewer)
        ->requireCapabilities(
          array(
            PhabricatorPolicyCapability::CAN_VIEW,
            PhabricatorPolicyCapability::CAN_EDIT,
          )
        );

      if (ctype_digit($objective_id)) {
        $query->withIDs(array($objective_id));
      } else {
        $query->withPHIDs(array($objective_id));
      }

      $objective = $query->executeOne();
      if (!$objective) {
        return new Aphront404Response();
      }
      $this->requireApplicationCapability(
        PhabricatorHephaestosCapabilityCreate::CAPABILITY
      );

      $keyResult = PhabricatorHephaestosKeyResult::initializeKeyResult($objective);
      $is_new = true;

      $okrPHID = $objective->getHephaestosPHID();
      $okr = id(new PhabricatorHephaestosOkr())->loadAllWhere('phid = %s', $okrPHID);

      $okr = array_pop(array_reverse($okr));
      $cancel_uri = '/hephaestos/obj/' . $okr->getId();

      $header_text = pht('Create KR');
      $title = pht('Create KR');
      $button_text = pht('Create KR');
    }

    $validation_exception = null;

    $e_content = true;
    $e_startDate = true;
    $e_endDate = true;
    $e_daysOffs = true;
    $e_meansure = true;
    $e_owner = true;
    $e_base = true;
    $e_target = true;
    $e_frequenceTime = true;
    $e_typeChart = true;
    $e_totalDev = true;
    $e_project = true;
    $e_confidentScope = false;

    $v_content = $keyResult->getTitle();
    $v_meansure = $keyResult->getMeansure();
    $v_baseValue = $keyResult->getBaseValue();
    $v_target = $keyResult->getTarget();
    $v_manualCheckin = $keyResult->getManualCheckin();
    $v_totalDev = $keyResult->getTotalDev();
    $v_typeChart = $keyResult->gettypeChart();
    $v_startDate = $keyResult->getStartDate() ?
      date('d/m/Y', $keyResult->getStartDate()) : $keyResult->getStartDate();
    $v_endDate = $keyResult->getEndDate() ?
      date('d/m/Y', $keyResult->getEndDate()) : $keyResult->getEndDate();
    $v_frequence = $keyResult->getFrequence();
    $v_frequenceTime = round($keyResult->getFrequenceTime());
    $v_confidenceLevel = $keyResult->getConfidenceLevel();
    $v_owner = array();
    $v_project = array();
    $v_date = array();
    $v_dayOffs = $keyResult->getDayOffs();
    $v_confidentScope = explode(',', $keyResult->getConfidentScope());
    $v_isAutoCheckIn = $keyResult->getAutoCheckin();
    $v_dayOfWeeks = $keyResult->getDayOfWeeks();

    if (!$is_new) {
      $v_owner = [$keyResult->getOwnerPHID()];
      if ($keyResult->getProjectPHID() != null) {
        $v_project = explode(',', $keyResult->getProjectPHID());
      }
      $projectPHID = $keyResult->getProjectPHID();
      $totalDev = $keyResult->getTotalDev();

      $arrayChart = ['label'=>[]];
      if ($keyResult->getManualCheckin() == 2 && $projectPHID !== null) {
        $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $keyResult->getPHID());
        $checkInData ? $lastFeedback = end($checkInData)->getFeedback() : '';
        if ($checkInData) {
          end($checkInData)->getVelocityData() != null ? $velocityChartData = $this->getVelocityChartData($keyResult) : $velocityChartData = array('label' => [''], 'value' => [0]);
        } else {
          $velocityChartData = array('label' => [''], 'value' => [null]);
        }
      } else if ($keyResult->getManualCheckin() == 3 && $projectPHID !== null) {
        $effectiveOfDevelopChartData = $this->getEffectiveOfDevelopChartData($keyResult);
      } else if (($keyResult->getManualCheckin() == 4) && $projectPHID !== null) {
        $arrayChart = $this->dataChart($keyResult, 'hii');
        $chartData_HII = array('label' => [''], 'value' => [null]);
      } else if (($keyResult->getManualCheckin() == 5) && $projectPHID !== null) {
        $arrayChart = $this->dataChart($keyResult, 'hfi');
        $chartData_HII = array('label' => [''], 'value' => [null]);
      } else {
        $arrayChart = $this->dataChart($keyResult, null);
      }
    }
    $v_date = [$v_startDate, $v_endDate];

    if ($request->isFormPost()) {

      $xactions = array();

      $e_content = null;
      $e_startDate = null;
      $e_endDate = null;
      $e_dayOffs = null;
      $e_meansure = null;
      $e_owner = null;
      $e_base = null;
      $e_target = null;
      $e_frequenceTime = null;
      $e_typeChart = null;
      $e_totalDev = null;
      $e_project = null;
      $e_confidentScope = null;

      $v_content = $request->getStr('content');
      $v_meansure = $request->getStr('meansure');
      $v_baseValue = $request->getStr('baseValue');
      $v_target = $request->getStr('target');
      $v_frequence = $request->getStr('frequence');
      $v_frequenceTime = $request->getStr('frequenceTime');
      $v_owner = $request->getArr('ownerPHID');
      $v_confidenceLevel = $request->getStr('confidenceLevel');
      $v_project = $request->getStr('manualCheckin') == 0 ? [] : $request->getArr('projectPHID');
      $v_manualCheckin = $request->getStr('manualCheckin');
      $v_totalDev = $request->getStr('totalDev');
      $v_typeChart = $request->getStr('typeChart');
      $v_date = $request->getArr('date');
      $v_dayOffs = $request->getStr('manualCheckin') == 2 ? '' :$request->getStr('dayOffs');
      $arr_v_startDate = explode('/', $v_date[0]);
      $v_startDate = $arr_v_startDate[1].'/'.$arr_v_startDate[0].'/'.$arr_v_startDate[2];
      $arr_v_endDate = explode('/', $v_date[1]);
      $v_endDate = $arr_v_endDate[1].'/'.$arr_v_endDate[0].'/'.$arr_v_endDate[2];
      $v_confidentScope = $request->getArr('confidentScope');
      $v_isAutoCheckIn = $request->getInt('isAutoCheckin') ? $request->getInt('isAutoCheckin') : 0;
      $v_dayOfWeeks = $request->getStr('dayOfWeeksValue');

      if($v_manualCheckin != 4 && $v_manualCheckin != 5) {
        $v_totalDev = 0;
      }

      if($v_frequence === 'Day' && ($request->getStr('every') === 'every-weekday')) {
        $v_dayOfWeeks = 'every-weekday';
        $v_frequenceTime = 1;
      } elseif ($v_frequence === 'Day' && ($request->getStr('every') === 'every-day')) {
        $v_dayOfWeeks = '';
      } elseif ($v_frequence === 'Month') {
        if ($request->getStr('month-on') === 'on_day') {
          $v_dayOfWeeks = $request->getStr('dayOfMonth');
        } else if ($request->getStr('month-on') === 'on_the') {
          $v_dayOfWeeks = $request->getStr('pre_dayOfWeek') . ',' . $request->getStr('dayOfWeek');
        }
      }

      $type_content =
        PhabricatorHephaestosKeyResultContentTransaction::TRANSACTIONTYPE;
      $type_startDate =
        PhabricatorHephaestosKeyResultStartDateTransaction::TRANSACTIONTYPE;
      $type_endDate =
        PhabricatorHephaestosKeyResultEndDateTransaction::TRANSACTIONTYPE;
      $type_dayOffs =
        PhabricatorHephaestosKeyResultDayOffsTransaction::TRANSACTIONTYPE;
      $type_owner =
        PhabricatorHephaestosKeyResultOwnerTransaction::TRANSACTIONTYPE;
      $type_meansure =
        PhabricatorHephaestosKeyResultMeansureTransaction::TRANSACTIONTYPE;
      $type_manualCheckin =
        PhabricatorHephaestosKeyResultManualCheckinTransaction::TRANSACTIONTYPE;
      $type_totalDev =
        PhabricatorHephaestosKeyResultTotalDevTransaction::TRANSACTIONTYPE;
      $type_baseValue =
        PhabricatorHephaestosKeyResultBaseValueTransaction::TRANSACTIONTYPE;
      $type_target =
        PhabricatorHephaestosKeyResultTargetTransaction::TRANSACTIONTYPE;
      $type_confidenceLevel =
        PhabricatorHephaestosKeyResultConfidenceLevelTransaction::TRANSACTIONTYPE;
      $type_frequence =
        PhabricatorHephaestosKeyResultFrequenceTransaction::TRANSACTIONTYPE;
      $type_frequenceTime =
        PhabricatorHephaestosKeyResultFrequenceTimeTransaction::TRANSACTIONTYPE;
      $type_project =
        PhabricatorHephaestosKeyResultProjectTransaction::TRANSACTIONTYPE;
      $type_typeChart =
        PhabricatorHephaestosKeyResultTypeChartTransaction::TRANSACTIONTYPE;
      $type_confidentScope =
        PhabricatorHephaestosKeyResultConfidentScopeTransaction::TRANSACTIONTYPE;
      $type_isAutoCheckin =
        PhabricatorHephaestosKeyResultIsAutoCheckinTransaction::TRANSACTIONTYPE;
      $type_dayOfWeeks =
        PhabricatorHephaestosKeyResultDayOfWeekTransaction::TRANSACTIONTYPE;

      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_content)
        ->setNewValue($v_content);
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_startDate)
        ->setNewValue(strtotime($v_startDate));
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_endDate)
        ->setNewValue(strtotime($v_endDate));
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_dayOffs)
        ->setNewValue($v_dayOffs);
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_owner)
        ->setNewValue(array_pop(array_reverse($v_owner)));
      if ($v_meansure !== '') {
        $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
          ->setTransactionType($type_meansure)
          ->setNewValue($v_meansure);
      }
      if (trim($v_meansure, ' ') == '' && count($v_project) == 0) {
        $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
          ->setTransactionType($type_meansure)
          ->setNewValue(trim($v_meansure, ' '));
      }
      if (trim($v_meansure, ' ') == '' && count($v_project) !== 0) {
        $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
          ->setTransactionType($type_meansure)
          ->setNewValue(' ');
      }
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_manualCheckin)
        ->setNewValue($v_manualCheckin);
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_totalDev)
        ->setNewValue($v_totalDev ? $v_totalDev : 0);
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_baseValue)
        ->setNewValue($v_baseValue);
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_target)
        ->setNewValue($v_target);
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_confidenceLevel)
        ->setNewValue($v_confidenceLevel);
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_frequence)
        ->setNewValue($v_frequence);
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_frequenceTime)
        ->setNewValue($v_frequenceTime);
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_project)
        ->setNewValue(implode(",",$v_project));
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_typeChart)
        ->setNewValue($v_typeChart);
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_confidentScope)
        ->setNewValue(implode(",", $v_confidentScope));
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_isAutoCheckin)
        ->setNewValue($v_isAutoCheckIn);
      $xactions[] = id(new PhabricatorHephaestosKeyResultTransaction())
        ->setTransactionType($type_dayOfWeeks)
        ->setNewValue($v_dayOfWeeks);

      $editor = id(new PhabricatorHephaestosKeyResultEditor())
        ->setActor($viewer)
        ->setContinueOnNoEffect(true)
        ->setContentSourceFromRequest($request);

      try {
        $editor->applyTransactions($keyResult, $xactions);

        session_start();
        if (!$is_new) {
          $_SESSION['message'] = 'KR edited successfully';
        } else {
          $_SESSION['message'] = 'KR added successfully';
        }
        if ($request->getBool('check')) {
          return id(new AphrontRedirectResponse())
            ->setURI("/hephaestos/kr/edit/?objective=$objective_id");
        } else
          return id(new AphrontRedirectResponse())
            ->setURI("/hephaestos/kr/detail/".$keyResult->getID());
//            ->setURI('/obj/' . $okr->getId());
      } catch (PhabricatorApplicationTransactionValidationException $ex) {

        $validation_exception = $ex;
        $e_content = $ex->getShortMessage($type_content);
        $e_startDate = $ex->getShortMessage($type_startDate);
        $e_endDate = $ex->getShortMessage($type_endDate);
        $e_dayOffs = $ex->getShortMessage($type_dayOffs);
        $e_meansure = $ex->getShortMessage($type_meansure);
        $e_owner = $ex->getShortMessage($type_owner);
        $e_base = $ex->getShortMessage($type_baseValue);
        $e_target = $ex->getShortMessage($type_target);
        $e_frequenceTime = $ex->getShortMessage($type_frequenceTime);
        $e_typeChart = $ex->getShortMessage($type_typeChart);
        $e_totalDev = $ex->getShortMessage($type_totalDev);
        $e_project = $ex->getShortMessage($type_project);
        $e_confidentScope = $ex->getShortMessage($type_confidentScope);

      }
    }

    if (($e_endDate === "Required" || $e_startDate === "Required")) {
      $checkDate = "Required";
    } elseif (($e_endDate === NULL && $e_startDate === NULL)) {
      $checkDate = NULL;
    } elseif ($e_endDate === "Invalid") {
      $checkDate = "Invalid";
    } else {
      $checkDate = true;
    }

    Javelin::initBehavior(
      'submit-objective'
    );
    $form = id(new AphrontFormView())
      ->setUser($viewer)
      ->setID('form_edit');

    if ($make_default) {
      $form->appendRemarkupInstructions(
        pht(
          'NOTE: You are creating the **default objective**. All existing ' .
          'objects will be put into this objective. You must create a default ' .
          'objective before you can create other Okrs.'
        )
      );
    }
    $form
      ->appendControl(
        id(new AphrontFormStaticKRControl())
          ->setLabel(pht("Objective"))
          ->setID('NameObjectiveKR')
          ->setValue([$objective->getTitle()])
      )
      ->appendChild(
        id(new AphrontFormTextControl())
          ->setLabel(pht('KR'))
          ->setName('content')
          ->setValue($v_content)
          ->setError($e_content)
      )
      ->appendChild(
        id(new AphrontFormSelectControl())
          ->setLabel(pht('Measure by'))
          ->setName('manualCheckin')
          ->setOptions([5 => 'Customer Bug Rate', 4 => 'DEBIT Bug Rate', 3 => 'Effective of Develop', 0 => 'Manually', 1 => 'Remain of Project\'s point', 2 => 'Velocity'])
          ->setValue($v_manualCheckin)
          ->setID('manual-checkin')
      )
      ->appendChild(
        id(new AphrontFormStartDateKRControl())
          ->setLabel(pht('Repeat'))
          ->setName('date[]')
          ->setValue($v_date)
          ->setError($checkDate)
      )
      ->appendChild(
        id(new AphrontFormSelectControl())
          ->setName('frequence')
          ->setOptions(['Day' => 'Daily', 'Week' => 'Weekly', 'Month' => 'Monthly'])
          ->setValue($v_frequence)
      )
      ->appendChild(
        id(new AphontFormFrequenceKRControl())
          ->setID('frequence_time')
          ->setName('frequenceTime')
          ->setValue($v_frequenceTime)
          ->setIsNew($is_new)
          ->setIsAutoCheckIn($v_isAutoCheckIn)
          ->setTotalDev($v_totalDev)
          ->setError($e_frequenceTime)
      )
      ->appendChild(
        id(new AphontFormFrequenceKRMonthlyControl())
      )
      ->appendChild(
        id(new AphrontFormDateOfWeekControl())
          ->setName('dateOfWeek[]')
          ->setValue($v_dayOfWeeks)
      )
      ->appendChild(
        id(new AphrontFormEndDateKRControl())
          ->setName('date[]')
          ->setValue($v_date)
          ->setError($checkDate)
      )
      ->appendChild(
        id(new AphrontFormDayOffDateControl())
          ->setLabel(pht('Day-Offs'))
          ->setName('dayOffs')
      )
      ->appendControl(
        id(new AphrontFormStaticKRControl())
          ->setID('btn-checkin')
      )
      ->appendChild(
        id(new AphrontFormNumberControl())
          ->setLabel(pht('Base'))
          ->setID('baseValue')
          ->setSigil('base')
          ->setName('baseValue')
          ->setValue($v_baseValue)
          ->setError($e_base)
      )
      ->appendChild(
        id(new AphrontFormNumberControl())
          ->setLabel(pht('Target'))
          ->setID('target')
          ->setSigil('target')
          ->setName('target')
          ->setValue($v_target)
          ->setError($e_target)
      )
      ->appendControl(
        id(new AphrontFormTokenizerKRControl())
          ->setLabel(pht('Project'))
          ->setName('projectPHID')
          ->setLimit(10)
          ->setDatasource(new PhabricatorProjectDatasource())
          ->setValue($v_project)
          ->setID('projectPHID')
          ->setError($e_project)
      )
      ->appendChild(
        id(new AphrontFormSelectControl())
          ->setLabel(pht('Chart Type'))
          ->setName('typeChart')
          ->setID('type_chart')
          ->setOptions(["" => "", 0 => 'Burnup Chart', 1 => 'Burndown Chart'])
          ->setValue($v_typeChart)
          ->setError($e_typeChart)
      )
      ->appendChild(
        id(new AphrontFormTextControl())
          ->setLabel(pht('Description'))
          ->setName('meansure')
          ->setValue($v_meansure)
          ->setError($e_meansure)
      )
      ->appendControl(
        id(new AphrontFormTokenizerControl())
          ->setLabel(pht('Owner'))
          ->setName('ownerPHID')
          ->setLimit(1)
          ->setDatasource(new PhabricatorPeopleDatasource())
          ->setValue($v_owner)
          ->setError($e_owner)
      )
      ->appendChild(
        id(new AphrontFormSelectControl())
          ->setLabel(pht('Confidence level'))
          ->setName('confidenceLevel')
          ->setOptions([1 => 'Green', 2 => 'Yellow', 3 => 'Red'])
          ->setValue($v_confidenceLevel))
      ->appendChild(
        id(new AphrontFormConfidentControl())
          ->setLabel(pht('Confidence Scope'))
          ->setName('confidentScope[]')
          ->setValue($v_confidentScope)
          ->setError($e_confidentScope))
      ->addHiddenInput('check', 'false')
      ->addHiddenInput
      (
        'data',
        json_encode(array('id' => $id, 'target' => $v_target))
      )
      ->addHiddenInput
      (
        'labelChart',
        isset($arrayChart) ? json_encode($arrayChart['label']) : ''
      )
      ->addHiddenInput
      (
        'labelDateChart',
        isset($arrayChart) ? json_encode($arrayChart['labelDate']): ''
      )
      ->addHiddenInput
      (
        'dataChart',
        isset($arrayChart) ? json_encode($arrayChart['data']) : ''
      )
      ->addHiddenInput
      (
        'colorChart',
        isset($arrayChart) ? json_encode($arrayChart['color']) : ''
      )
      ->addHiddenInput
      (
        'dataTarget',
        isset($arrayChart) ? json_encode($arrayChart['data2']) : ''
      )
      ->addHiddenInput
      (
        'emotions',
        isset($arrayChart) ? json_encode($arrayChart['emotions']) : ''
      )
      ->addHiddenInput
      (
        'nameCurrent',
        isset($arrayChart) ? json_encode($arrayChart['nameCurrent']) : ''
      )
      ->addHiddenInput
      (
        'scopeLine',
        isset($arrayChart) ? json_encode($arrayChart['data3']) : ''
      )
      ->addHiddenInput
      (
        'velocityChartData',
        isset($velocityChartData) ? (count($velocityChartData['label']) > 0 ? json_encode($velocityChartData) : json_encode(array('label' => [], 'value' => []))) : json_encode(array('null'))
      )
      ->addHiddenInput
      (
        'effectiveOfDevelopChartData',
        isset($effectiveOfDevelopChartData) ? (count($effectiveOfDevelopChartData['label']) > 0 ? json_encode($effectiveOfDevelopChartData) : json_encode(array('label' => [], 'value' => []))) : json_encode(array('null'))
      )
      ->addHiddenInput('chartData_HII',
        isset($chartData_HII) ? (count($chartData_HII['label']) > 0 ? json_encode($chartData_HII) : json_encode(array('label' => [], 'value' => []))) : json_encode(array('null'))
      )
      ->addHiddenInput
      (
        'chartData_HFI',
        isset($chartData_HFI) ? (count($chartData_HFI['label']) > 0 ? json_encode($chartData_HFI) : json_encode(array('label' => [], 'value' => []))) : json_encode(array('null'))
      )
      ->addHiddenInput
      (
        'taskID',
        isset($arrayChart) ? json_encode($arrayChart['taskID']) : ''
      )
      ->addHiddenInput
      (
        'estimation',
        isset($arrayChart) ? json_encode($arrayChart['estimation']) : ''
      )
      ->addHiddenInput
      (
        'dayOffs',
        json_encode(explode(", ", $keyResult->getDayOffs()))
      )
      ->addHiddenInput
      (
        'confidentScope',
        json_encode($v_confidentScope)
      )
      ->addHiddenInput('lastFeedback', isset($lastFeedback) ? $lastFeedback : 6)
      ->addHiddenInput
      (
        'resolvedPoint',
        isset($arrayChart) ? json_encode($arrayChart['resolvedPoint']) : ''
      )
      ->addHiddenInput
      (
        'pointToResolve',
        isset($arrayChart) ? json_encode($arrayChart['pointToResolve']) : ''
      )
      ->addHiddenInput('oldMeasure', $keyResult->getManualCheckin());

    if (isset($arrayChart)) {
      foreach ($arrayChart['task'] as $key => $arrayTask) {
        $form->addHiddenInput
        (
          'task' . $key,
          isset($arrayChart) ? json_encode($arrayTask) : ''
        );
      }
    }

    if (!$is_new) {
      $form->appendChild(
        id(new AphrontFormSubmitControl())
          ->addButton(id(new PHUIButtonView())
            ->setTag('a')
            ->setHref($this->getApplicationURI('delete/key&result/' . $id . '/'))
            ->setText('Delete')
            ->setWorkflow(true)
            ->setColor(PHUIButtonView::RED))
          ->addButton(id(new PHUIButtonView())
            ->setText('Save')
            ->setID('save'))
          ->addCancelButton($cancel_uri)
      );
    } else {
      $form->appendChild(
        id(new AphrontFormSubmitControl())
          ->addButton(id(new PHUIButtonView())
            ->setText('Save And New')
            ->setID('save-new')
            ->addSigil('submitObjetiveButton')
            ->setColor(PHUIButtonView::GREEN))
          ->addButton(id(new PHUIButtonView())
            ->setText('Save')
            ->setID('save'))
          ->addCancelButton($cancel_uri)
      );
    }

    $crumbs = $this->buildApplicationCrumbs();
    $crumbs->addTextCrumb(pht('Create KRs'));
    $crumbs->setBorder(true);

    $box = id(new PHUIOkrsBoxView())
      ->setHeaderText($header_text)
      ->setBackground(PHUIObjectBoxView::WHITE_CONFIG)
      ->setValidationException($validation_exception)
      ->appendChild($form);

    session_start();
    if (isset($_SESSION['message'])) {
      $box->setFormSaved(true, $_SESSION['message']);
      unset($_SESSION['message']);
    }

    $crumbs = $this->buildApplicationCrumbs();
    $crumbs->addTextCrumb(
      $okr->getContent(),
      $cancel_uri);

    $crumbs->addTextCrumb($objective->getTitle());
    $viewFooter = [$box];
    if (!$is_new) {
      $crumbs->addTextCrumb(
        "Edit KR"
      );
      $currents = id(new PhabricatorHephaestosCheckin())
        ->loadAllWhere('krPHID = %s', $keyResult->getPHID());

      $timeline = $this->buildTransactionTimeline(
        $currents,
        new PhabricatorHephaestosCheckinTransactionQuery());
      $timeline->setShouldTerminate(true);
      array_push($viewFooter, $timeline);
    }

    if ($is_new) {
      $crumbs->addTextCrumb($header_text);
    }

    $crumbs->setBorder(true);

    $view = id(new PHUITwoColumnView())
      ->setFooter($viewFooter);

    return $this->newPage()
      ->setTitle($title)
      ->setCrumbs($crumbs)
      ->appendChild($view);
  }

  private function dataChart($keyResult, $type = null)
  {
    $arrays = id(new PhabricatorHephaestosCheckin())
      ->loadAllWhere('krPHID = %s', $keyResult->getPHID());
    $dayOffs =  explode(", ",$keyResult->getDayOffs());
    $begin = new DateTime(date('Y-m-d', $keyResult->getStartDate()));
    $end = new DateTime(date('Y-m-d', $keyResult->getEndDate()));
    $v_dayOfWeeks = $keyResult->getDayOfWeeks();
    if($keyResult->getFrequence() == 'Day') {
      $end = $end->modify('+1 day');
    }

    ($keyResult->getManualCheckIn() != 4 && $keyResult->getManualCheckIn() != 5) ?
      $tasks = $this->getTasksFromKr($keyResult) : $tasks = array();

    $interval = new DateInterval(
      'P' .
      (
      ($keyResult->getFrequenceTime() > 1) ? $keyResult->getFrequenceTime() : 1
      ) .
      (
      $keyResult->getFrequence() == 'Day' ? 'D' :
        ($keyResult->getFrequence() == 'Week' ? 'W' : 'M')
      )
    );
    $daterange = new DatePeriod($begin, $interval, $end);
    $arrayDate = [];

    if (($keyResult->getFrequence()) == 'Week' && ($keyResult->getDayOfWeeks() != "")) {
      $start_day = $begin->format('d-m-Y');
      $start_strtotime = strtotime($begin->format('d-m-Y'));
      $end_day = $end->format('d-m-Y');
      $end_strtotime = strtotime($end_day);
      foreach ($daterange as $key => $date) {
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '1', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '2', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '3', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '4', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '5', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '6', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '7', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
      }
      if ($end_strtotime > strtotime($arrayDate['label'][count($arrayDate['label']) - 1])) {
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '1', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '2', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '3', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '4', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '5', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '6', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '7', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
      }
      if(!in_array($end_day, $arrayDate['label'])) {
        $arrayDate['label'][] = $end_day;
        $arrayDate['data'][] = null;
        $arrayDate['data2'][] = null;
        $arrayDate['emotions'][] = null;
        $arrayDate['color'][] = null;
        $arrayDate['data3'][] = null;
      }
      if(!in_array($start_day, $arrayDate['label'])) {
        array_unshift($arrayDate['label'],$start_day);
        array_unshift($arrayDate['data'], null);
        array_unshift($arrayDate['data2'], null);
        array_unshift($arrayDate['emotions'], null);
        array_unshift($arrayDate['color'], null);
        array_unshift($arrayDate['data3'], null);
      }
    } else if(($keyResult->getFrequence() == 'Day') && ($v_dayOfWeeks == 'every-weekday')){
      $myDays = array('1', '2', '3', '4', '5');
      $start_day = $begin->format('d-m-Y');
      $end = $end->modify('-1 day');
      $end_day = $end->format('d-m-Y');
      foreach ($daterange as $date) {
        if (in_array($date->format('N'), $myDays) && !in_array($date->format('d/m/Y'), $dayOffs)) {
          $arrayDate['label'][] = $date->format('d-m-Y');
          $arrayDate['data'][] = null;
          $arrayDate['data2'][] = null;
          $arrayDate['emotions'][] = null;
          $arrayDate['color'][] = null;
          $arrayDate['data3'][] = null;
        }
      }
      if(!in_array($end_day, $arrayDate['label'])) {
        $arrayDate['label'][] = $end_day;
        $arrayDate['data'][] = null;
        $arrayDate['data2'][] = null;
        $arrayDate['emotions'][] = null;
        $arrayDate['color'][] = null;
        $arrayDate['data3'][] = null;
      }
      if(!in_array($start_day, $arrayDate['label'])) {
        array_unshift($arrayDate['label'],$start_day);
        array_unshift($arrayDate['data'], null);
        array_unshift($arrayDate['data2'], null);
        array_unshift($arrayDate['emotions'], null);
        array_unshift($arrayDate['color'], null);
        array_unshift($arrayDate['data3'], null);
      }
    } else if(($keyResult->getFrequence()) == 'Month' && $keyResult->getDayOfWeeks()) {
      $start_day = $begin->format('d-m-Y');
      $start_strtotime = strtotime($begin->format('d-m-Y'));
      $end_day = $end->format('d-m-Y');
      $end_strtotime = strtotime($end_day);
      if(strpos($keyResult->getDayOfWeeks(), ',') !== false) {
        $dayOfWeek = $this->getNameDay(explode(',', $keyResult->getDayOfWeeks())[1]);
        $position = explode(',', $keyResult->getDayOfWeeks())[0];
        foreach ($daterange as $key => $date) {
          if (strtotime($date->modify("$position $dayOfWeek of this month")->format('d-m-Y')) >= $start_strtotime &&
            strtotime($date->modify("$position $dayOfWeek of this month")->format('d-m-Y')) <= $end_strtotime &&
            (!in_array($date->modify("$position $dayOfWeek of this month")->format('d-m-Y'), $arrayDate['label']))
          ) {
            if (!in_array($date->modify("$position $dayOfWeek of this month")->format('d/m/Y'), $dayOffs)) {
              $arrayDate['label'][] = $date->modify("$position $dayOfWeek of this month")->format('d-m-Y');
              $arrayDate['data'][] = null;
              $arrayDate['data2'][] = null;
              $arrayDate['emotions'][] = null;
              $arrayDate['color'][] = null;
              $arrayDate['data3'][] = null;
            }
          }
        }
      } else {
        foreach ($daterange as $key => $date) {
          $dayOfMonth = $keyResult->getDayOfWeeks();
          $currentMonth = $date->format('m');
          $currentYear = $date->format('Y');
          $checkDate = checkdate($dayOfMonth , $currentMonth, $currentYear);

          if($checkDate) {
            if (strtotime("$dayOfMonth-$currentMonth-$currentYear") >= $start_strtotime &&
              strtotime(strtotime("$dayOfMonth-$currentMonth-$currentYear")) <= $end_strtotime &&
              (!in_array("$dayOfMonth-$currentMonth-$currentYear", $arrayDate['label']))
            ) {
              if (!in_array("$dayOfMonth-$currentMonth-$currentYear", $dayOffs)) {
                $arrayDate['label'][] = "$dayOfMonth-$currentMonth-$currentYear";
                $arrayDate['data'][] = null;
                $arrayDate['data2'][] = null;
                $arrayDate['emotions'][] = null;
                $arrayDate['color'][] = null;
                $arrayDate['data3'][] = null;
              }
            }
          } else {
            if (strtotime($date->modify('last day of this month')->format('d-m-Y')) >= $start_strtotime &&
              strtotime($date->modify('last day of this month')->format('d-m-Y')) <= $end_strtotime &&
              (!in_array($date->modify('last day of this month')->format('d-m-Y'), $arrayDate['label']))
            ) {
              if (!in_array($date->modify('last day of this month')->format('d/m/Y'), $dayOffs)) {
                $arrayDate['label'][] = $date->modify('last day of this month')->format('d-m-Y');
                $arrayDate['data'][] = null;
                $arrayDate['data2'][] = null;
                $arrayDate['emotions'][] = null;
                $arrayDate['color'][] = null;
                $arrayDate['data3'][] = null;
              }
            }
          }
        }
      }
      if(!in_array($end_day, $arrayDate['label'])) {
        $arrayDate['label'][] = $end_day;
        $arrayDate['data'][] = null;
        $arrayDate['data2'][] = null;
        $arrayDate['emotions'][] = null;
        $arrayDate['color'][] = null;
        $arrayDate['data3'][] = null;
      }
      if(!in_array($start_day, $arrayDate['label'])) {
        array_unshift($arrayDate['label'],$start_day);
        array_unshift($arrayDate['data'], null);
        array_unshift($arrayDate['data2'], null);
        array_unshift($arrayDate['emotions'], null);
        array_unshift($arrayDate['color'], null);
        array_unshift($arrayDate['data3'], null);
      }
    }
    else {
      foreach ($daterange as $date) {
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, null, null, $dayOffs, null, null, 1);
      }
      if (count($arrayDate['label']) === 0) {
        $arrayDate['label'][] = $begin->format('d-m-Y');;
        $arrayDate['data'][] = null;
        $arrayDate['data2'][] = null;
        $arrayDate['emotions'][] = null;
        $arrayDate['color'][] = null;
        $arrayDate['data3'][] = null;
      }
    }

    $arrayChart = $arrayDate;
    $count = 1;
    $arrayChart['color'][0] = 'green';
    foreach ($arrayDate['label'] as $key => $dateChart) {
      foreach ($arrays as $value) {
        if($type == 'hii' && $value->getHII() === null) {
          continue;
        } else if ($type == 'hfi' && $value->getHFI() === null) {
          continue;
        }
        $dateCheckin = strtotime(date('d-m-Y', $value->getDateCreated()));
        $color = $value->getColor() == 1 ? 'green' :
          ($value->getColor() == 2 ? 'yellow' : 'red');
        if ($dateCheckin == (int)strtotime($dateChart)) {
          $arrayChart['data'][$key + $count - 1] = $this->getCurrentWithTypeChart($keyResult, $value);
          $arrayChart['color'][$key + $count - 1] = $color;
          $arrayChart['emotions'][$key + $count - 1] = $value->getFeedBack();
          $arrayChart['data2'][$key + $count - 1] = null;
          $arrayChart['data3'][$key + $count - 1] = null;
        }
        if(!in_array(date('d/m/Y', $value->getDateCreated()), $dayOffs)){
          if (
            $dateCheckin > (int)strtotime($dateChart) &&
            $dateCheckin < (int)strtotime($arrayDate['label'][$key + 1])
          ) {
            array_splice
            (
              $arrayChart['label'],
              $key + $count,
              0,
              array(date('d-m-Y', $value->getDateCreated()))
            );
            array_splice
            (
              $arrayChart['data'],
              $key + $count,
              0,
              array($this->getCurrentWithTypeChart($keyResult, $value))
            );
            array_splice
            (
              $arrayChart['emotions'],
              $key + $count,
              0,
              array($value->getFeedBack())
            );
            array_splice
            (
              $arrayChart['data2'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['data3'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['color'],
              $key + $count,
              0,
              array($color)
            );
            $count++;
          }
        }
      }
    }

    $arrayChart['task'] = [];
    $arrayChart['taskID'] = [];

    if($keyResult->getManualCheckIn() != 4 && $keyResult->getManualCheckIn() != 5) {
      $arrayChart = $this->getTaskEndChartData($tasks, $arrayChart, $dayOffs);
      $target = $this->getTargetFromKrHasProject($keyResult);
    } else {
      $target = 100;
    };

    $arrayChart['estimation'] = [];
    if ($keyResult->getProjectPHID() !== null && ($keyResult->getManualCheckIn() == 1)) {
      $arrayChart = $this->getEstimationChartData($tasks, $arrayChart, $keyResult, $dayOffs);
    }

    if ($arrayChart['data'][0] == null) {
      $arrayChart['data'][0] = ($keyResult->getTypeChart() != 1) ? $keyResult->getBaseValue() : $target;
    }
    $arrayChart['data2'][0] = ($keyResult->getTypeChart() != 1) ? 0 : $target;
    $arrayChart['data2'][count($arrayChart['label']) - 1] = ($keyResult->getTypeChart() != 1) ? $target : 0;
    $arrayChart['data3'][0] = $target;
    $arrayChart['data3'][count($arrayChart['label']) - 1] = $target;

    $arrayChart['nameCurrent'] = "Check-in Value";

    foreach ($arrayChart['label'] as $k => $chart) {
      $arrayChart['labelDate'][$k] = str_replace('-', '/', substr($chart, 0, 5));
    }
    return $arrayChart;
  }

  protected function getTaskEndChartData($tasks, $arrayChart, $dayOffs)
  {
    foreach ($tasks as $task) {
      if ($task->getEndDate() != null) {
        $arrayChart['taskID'][] = $task->getID();
        $endDate = strtotime(date('d-m-Y', $task->getEndDate()));
        foreach ($arrayChart['label'] as $key => $dateChart) {
          $arrayChart['task'][$task->getID()][] = null;
          if ($endDate == (int)strtotime($dateChart)) {
            $arrayChart['task'][$task->getID()][$key] = '0';
          }

          if(!in_array(date('d/m/Y', $task->getEndDate()) , $dayOffs)){
            if (
              $endDate > (int)strtotime($dateChart) &&
              $endDate < (int)strtotime($arrayChart['label'][$key + 1])
            ) {
              array_splice
              (
                $arrayChart['label'],
                $key + 1,
                0,
                array(date('d-m-Y', $task->getEndDate()))
              );

              array_splice
              (
                $arrayChart['data'],
                $key + 1,
                0,
                array(null)
              );
              array_splice
              (
                $arrayChart['emotions'],
                $key + 1,
                0,
                array(null)
              );
              array_splice
              (
                $arrayChart['data2'],
                $key + 1,
                0,
                array(null)
              );
              array_splice
              (
                $arrayChart['data3'],
                $key + 1,
                0,
                array(null)
              );
              array_splice
              (
                $arrayChart['color'],
                $key + 1,
                0,
                array(null)
              );
              foreach ($arrayChart['task'] as $name => $array) {
                if ($name == $task->getID()) {

                  $arrayChart['task'][$task->getID()][] = 0;
                } else {
                  array_splice
                  (
                    $arrayChart['task'][$name],
                    $key + 1,
                    0,
                    array(null)
                  );
                }
              }
            }
          }
        }
      }
    }
    return $arrayChart;
  }

  private function getPointOfDateCheckin($keyResult)
  {
    $arrays = id(new PhabricatorHephaestosCheckin())
      ->loadAllWhere('krPHID = %s', $keyResult->getPHID());
    $points = $arrays ? json_decode(end($arrays)->getResolvedPoint()) : null;
    return $points;
  }

  protected function getEstimationChartData($tasks, $arrayChart, $keyResult, $dayOffs)
  {
    $target = $this->getTargetFromKrHasProject($keyResult);
    $point = ($keyResult->getTypeChart() != 1) ? 0 : $target;
    $arr_Point = (array)$this->getPointOfDateCheckin($keyResult);
    $array_resolvedPoint = (array)$arr_Point["resolvedPoint"];
    $array_pointToResolve = (array)$arr_Point["pointToResolve"];
    foreach ($arrayChart['label'] as $key => $dateChart) {
      $arrayChart['estimation'][] = null;
      $arrayChart['resolvedPoint'][] = null;
      $arrayChart['pointToResolve'][] = null;
      $resolvedPoint = 0;
      $pointToResolve = 0;

      foreach ($tasks as $task) {
        if ($task->getEndDate() != null) {
          $endDate = strtotime(date('d-m-Y', $task->getEndDate()));
          $taskStatus = $task->getStatus();
          if ($endDate == (int)strtotime($dateChart)) {
            if (($keyResult->getTypeChart() != 1))
              $point += $task->getPoints() != null ? $task->getPoints() : 0;
            else
              $point -= $task->getPoints() != null ? $task->getPoints() : 0;

            if (!$arr_Point) {
              if ($taskStatus == 'resolved' || $taskStatus == 'closed')
                $resolvedPoint += $task->getPoints() != null ? $task->getPoints() : null;
              elseif ($taskStatus == 'open' || $taskStatus == 'doing')
                $pointToResolve += $task->getPoints() != null ? $task->getPoints() : null;

              $arrayChart['resolvedPoint'][$key] = null;
              $arrayChart['pointToResolve'][$key] = $pointToResolve;
            }

            $arrayChart['estimation'][$key] = $point;
          }
        }
      }

      $date = date_format(date_create($dateChart), 'd/m/Y');
      if(isset($array_pointToResolve[$date]))
        $arrayChart['pointToResolve'][$key] = $array_pointToResolve[$date];
    }
    if ($arrayChart['estimation'][0] == null)
      $arrayChart['estimation'][0] = ($keyResult->getTypeChart() != 1) ? 0 : $target;
    if ($arrayChart['estimation'][$key] == null)
      $arrayChart['estimation'][$key] = $point;

    $count = 1;
    $arrayDate = $arrayChart;
    foreach ($arrayDate['label'] as $key => $dateChart) {
      foreach ($array_resolvedPoint as $dateResolved => $resolvedPoint){
        if(is_null($resolvedPoint)){
          continue;
        }
        $date = str_replace('/', '-', $dateResolved);
        $timeResolved = (int)strtotime($date);
        if ($timeResolved == (int)strtotime($dateChart)) {
          $arrayChart['resolvedPoint'][$key +  $count - 1] = $resolvedPoint;
          continue;
        }
        else if(!in_array(date('d/m/Y', $timeResolved) , $dayOffs)){
          if (
            (int)$timeResolved > (int)strtotime($dateChart) &&
            (int)$timeResolved < (int)strtotime($arrayDate['label'][$key + 1])
          ) {
            array_splice
            (
              $arrayChart['label'],
              $key + $count,
              0,
              array($date)
            );

            array_splice
            (
              $arrayChart['data'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['emotions'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['data2'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['data3'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['color'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['estimation'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['pointToResolve'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['resolvedPoint'],
              $key + $count,
              0,
              array($resolvedPoint)
            );
            foreach ($arrayChart['task'] as $name => $array) {
              array_splice
              (
                $arrayChart['task'][$name],
                $key + $count,
                0,
                array(null)
              );
            }
            $count++;
          }
        }
      }
    }
    return $arrayChart;
  }

  public function getVelocityChartData($keyResult)
  {
    $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $keyResult->getPHID());
    return $checkInData == null ? null : json_decode(end($checkInData)->getVelocityData(), TRUE);
  }

  protected function buildTransactionTimeline(
    $objects,
    PhabricatorApplicationTransactionQuery $query = null,
    PhabricatorMarkupEngine $engine = null,
    $view_data = array())
  {

    $request = $this->getRequest();
    $viewer = $this->getViewer();

    $pager = id(new AphrontCursorPagerView())
      ->readFromRequest($request);

    if (empty($objects)) {
      $handle = id(new PhabricatorObjectHandle());
      $event = id(new PHUITimelineEventView())
        ->setUserHandle($handle)
        ->setIcon('fa-exclamation')
        ->setTitle(pht('This KR not have any check-in'))
        ->setUser($viewer);
      $timeline = id(new PHUITimelineView());
      $timeline->setUser($viewer);
      $timeline->addEvent($event);
      return $timeline;
    }
    $object = end($objects);

    $objectPHIDs = mpull($objects, 'getPHID');

    $xactions = $query
      ->setViewer($viewer)
      ->withObjectPHIDs($objectPHIDs)
      ->needComments(true)
      ->executeWithCursorPager($pager);
    $xactions = array_reverse($xactions);

    $timeline_engine = PhabricatorTimelineEngine::newForObject($object)
      ->setViewer($viewer)
      ->setTransactions($xactions)
      ->setViewData($view_data);

    $view = $timeline_engine->buildTimelineView();

    $timeline = $view
      ->setPager($pager)
      ->setQuoteTargetID($this->getRequest()->getStr('quoteTargetID'))
      ->setQuoteRef($this->getRequest()->getStr('quoteRef'));

    return $timeline;
  }

  private function getTargetFromKrHasProject($kr)
  {
    if ($kr->getProjectPHID() == null || ($kr->getManualCheckIn() == 0)) {
      return $kr->getTarget();
    } else {
      $project = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $kr->getProjectPHID());
      $state = id(new PhabricatorOkrsViewState())
        ->setProject($project)
        ->readFromRequest($this->getRequest());
      $tasks = $state->getObjects();
      $totalPoint = 0;
      foreach ($tasks as $taskID => $task) {
        if ($task->getPoints() !== null) {
          $totalPoint += $task->getPoints();
        }
      }
      return $totalPoint;
    }
  }

  private function getTasksFromKr($kr)
  {
    if ($kr->getProjectPHID() == null || ($kr->getManualCheckIn() == 0)) {
      return null;
    } else {
      $project = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $kr->getProjectPHID());
      $state = id(new PhabricatorOkrsViewState())
        ->setProject($project)
        ->readFromRequest($this->getRequest());
      $tasks = $state->getObjects();
      return $tasks;
    }
  }


  protected function getCurrentWithTypeChart($keyResult, $current)
  {
    if ($keyResult->getTypeChart() != 1) {
      return $current->getCheckinValue();
    } else {
      return $this->getTargetFromKrHasProject($keyResult) - $current->getCheckinValue();
    }
  }

  protected function getMilestones($project)
  {
    $viewer = $this->getViewer();
    $allows_milestones = $project->supportsMilestones();

    if ($allows_milestones) {
      $milestones = id(new PhabricatorProjectQuery())
        ->setViewer($viewer)
        ->withParentProjectPHIDs(array($project->getPHID()))
        ->needImages(true)
        ->withIsMilestone(true)
        ->setOrderVector(array('milestoneNumber', 'id'))
        ->execute();
    } else {
      $milestones = array();
    }
    return $milestones;
  }

  public function getEffectiveOfDevelopChartData($keyResult)
  {
    $data = [];
    $data['totalTimeDEV'] = [];
    $data['totalTimeBUG'] = [];
    $data['totalTime'] = [];
    $data['label'] = [];
    $data['value'] = [];
    $data['emotion'] = [];

    $currents = id(new PhabricatorHephaestosCheckin())
      ->loadAllWhere('krPHID = %s', $keyResult->getPHID());
    $currents = $this->filterArrayCurrentTableEffective($currents);
    $dayOffs =  explode(", ",$keyResult->getDayOffs());
    $startDate = $keyResult->getStartDate();
    $endDate = strtotime(date('d-m-Y 24:59:59', $keyResult->getEndDate()));
    foreach ($currents as $current){
      $dateCreate = $current->getDateCreated();
      if(!in_array(date('d/m/Y', $dateCreate), $dayOffs) && $dateCreate >= $startDate && $dateCreate <= $endDate){
        $effective = json_decode($current->getEffective());
        $confidenceLevel = $current->getColor();
        $emotion = $current->getFeedback();
        $dateCheckin = date("d/m", $current->getDateCreated());
        $currentValue = $current->getCheckinValue();
        $data['totalTimeDEV'][] = $effective->totalTimeDEV;
        $data['totalTimeBUG'][] = $effective->totalTimeBUG;
        $data['totalTime'][] = $effective->totalTime;
        $data['label'][] = $dateCheckin;
        $data['value'][] = $currentValue;
        $data['emotion'][] = $emotion;
        $data['confidenceLevel'][] = $this->getColorConfidenceLevel($confidenceLevel);
      }
    }
    if(empty($data['label'])){
      $data['label'][] = 'No data checkin';
    }
    return $data;
  }

  private function getColorConfidenceLevel($confidenceLevel){
    $color = null;
    switch ($confidenceLevel) {
      case 1:
        $color = 'green';
        break;
      case 2:
        $color = 'yellow';
        break;
      case 3:
        $color = 'red';
        break;
      default:
        $color = 'green';
    }
    return $color;
  }

  private function filterArrayCurrentTableEffective($arrayCurrentTable){
    $arrayFilter = [];
    foreach($arrayCurrentTable as $current){
      if($current->getEffective() != null){
        $arrayFilter[] = $current;
      }
    }
    return $arrayFilter;
  }

  protected function getEffectiveOfDevelopChartDataOfMilestones($milestone, $data)
  {

    $state = id(new PhabricatorOkrsViewState())->setProject($milestone)->readFromRequest($this->getRequest());
    $tasks = $state->getObjects();

    $bugQA = id(new PhabricatorProject())->loadOneWhere('name = %s', 'Bug Report (QA)');
    $bugQAPHID =isset($bugQA) ? $bugQA->getPHID() : null;
    $bugCustomer = id(new PhabricatorProject())->loadOneWhere('name = %s', 'Bug Report (Customer)');
    $bugCustomerPHID = isset($bugCustomer) ? $bugCustomer->getPHID() : null;

    $totalTimeDEV = 0;
    $totalTimeBUG = 0;

    foreach ($tasks as $taskID => $task) {

      $startDoing = 0;
      $endDoing = 0;

      $isBug = $this->checkTaskIsBug($task, $bugQAPHID, $bugCustomerPHID);
      $type = 'columns';
      $xactions = $this->getXactionsFromTask($task, $type);

      foreach($xactions as $xaction){
        $column = $xaction->getNewValue()[0]["columnPHID"];
        $nameColumn = id(new PhabricatorProjectColumn())->loadOneWhere('PHID = %s', $column)->getName();

        if (strtolower($nameColumn) == "doing" || strtolower($nameColumn) == "in develop") {
          $startDoing = $xaction->getDateCreated();
        } else {
          if ($startDoing != 0) {
            $endDoing = $xaction->getDateCreated();
          }
        }

        if($startDoing != 0 && $endDoing != 0){
          $timeDoing = ($endDoing - $startDoing) / 3600;
          $totalTimeDEV += $timeDoing;

          if($isBug == true) {
            $totalTimeBUG += $timeDoing;
          }
          $startDoing = 0;
          $endDoing = 0;
        }
      }

      if($startDoing != 0){
        $endDoing = id(new DateTime())->getTimestamp();
        $timeDoing = ($endDoing - $startDoing) / 3600;
        $totalTimeDEV += $timeDoing;
        if($isBug == true) {
          $totalTimeBUG += $timeDoing;
        }
      }
    }

    if($totalTimeDEV !== 0){
      $effective = round((($totalTimeDEV - $totalTimeBUG)/$totalTimeDEV) * 100, 1);
    } else {
      $effective = 0;
    }
    $data['totalTimeDEV'][] = round($totalTimeDEV, 1);
    $data['totalTimeBUG'][] = round($totalTimeBUG, 1);
    $data['totalTimeDoing'][] = round(round($totalTimeDEV, 1) -  round($totalTimeBUG, 1), 1);
    $data['label'][] = $milestone->getName();
    $data['value'][] = $effective;
    return $data;
  }

  protected function getIndexChartDataOfMilestones($project , $data, $total_dev, $milestone_names, $index)
  {
    $state = id(new PhabricatorOkrsViewState())->setProject($project)->readFromRequest($this->getRequest());
    $tasks = $state->getObjects();
    $index === 'DEBIT Bug Rate' ? $bugTag = 'Bug Report (QA)' : $bugTag = 'Bug Report (Customer)';

    $bug = id(new PhabricatorProject())->loadOneWhere('name = %s', $bugTag);
    $bugPHID = isset($bug) ? $bug->getPHID() : null;

    $totalBug = 0;
    $type = 'edge';
    foreach ($tasks as $taskID => $task) {
      $index === 'DEBIT Bug Rate' ?  $isBug = $this->checkTaskIsBugQA($task, $bugPHID) :  $isBug = $this->checkTaskIsBugQA($task, $bugPHID);

      if ($isBug == true) {
        $xactions = $this->getXactionsFromTask($task, $type);

        foreach($xactions as $xaction){
          if($xaction->getNewValue()[0]){
            $project_first_xaction = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $xaction->getNewValue()[0]);
            if($project_first_xaction != null){
              $project_tag = $project_first_xaction->getName();
              if (in_array($project_tag, $milestone_names)){
                $data[$project_tag]++;
                break;
              }
            }
          }
          if(array_keys($xaction->getNewValue())[0]){
            $project_first_xaction1 = id(new PhabricatorProject())->loadOneWhere('PHID = %s', array_keys($xaction->getNewValue())[0]);
            if($project_first_xaction1 != null){
              $project_tag1 = $project_first_xaction1->getName();
              if(in_array($project_tag1 ,$milestone_names)){
                $data[$project_tag1]++;
                break;
              }
            }
          }
        }
      }
    }
    return $data;
  }

  protected function getXactionsFromTask($task, $type)
  {
    $request = $this->getRequest();
    $viewer = $this->getViewer();
    $pager = id(new AphrontCursorPagerView())
      ->readFromRequest($request);
    $query = id(new ManiphestTransactionQuery());

    if($type === 'edge'){
      $xactions = $query
        ->setViewer($viewer)
        ->withObjectPHIDs(array($task->getPHID()))
        ->withTransactionTypes(array(PhabricatorTransactions::TYPE_EDGE))
        ->needComments(true)
        ->executeWithCursorPager($pager);
    }
    elseif($type === 'columns'){
      $xactions = $query
        ->setViewer($viewer)
        ->withObjectPHIDs(array($task->getPHID()))
        ->withTransactionTypes(array(PhabricatorTransactions::TYPE_COLUMNS))
        ->needComments(true)
        ->executeWithCursorPager($pager);
    }
    return array_reverse($xactions);
  }

  protected function checkTaskIsBug($task, $bugQAPHID, $bugCustomerPHID = null)
  {
    $tags = $task->getProjectPHIDs();
    $isBug = false;
    if(in_array($bugQAPHID, $tags) || in_array($bugCustomerPHID, $tags)){
      $isBug = true;
    }
    return $isBug;
  }

  protected function checkTaskIsBugQA($task, $bugQAPHID)
  {
    $tags = $task->getProjectPHIDs();
    $isBug = false;
    if(in_array($bugQAPHID, $tags)){
      $isBug = true;
    }
    return $isBug;
  }


  protected function checkTaskIsBugCustomer($task, $bugCustomerPHID)
  {
    $tags = $task->getProjectPHIDs();
    $isBug = false;
    if(in_array($bugCustomerPHID, $tags)){
      $isBug = true;
    }
    return $isBug;
  }

//  protected function getTotalMembership($project){
//    return count($project->getMemberPHIDs());
//  }

  protected function getTotalBug($milestone){
    $state = id(new PhabricatorOkrsViewState())->setProject($milestone)->readFromRequest($this->getRequest());
    $tasks = $state->getObjects();
    $total_bug = 0;

    foreach ($tasks as $taskID => $task) {
      $tags = $this->getProjectTags($task);
      $bug = count(array_filter($tags,function($v, $k){
        return ($v == 'Bug Report (QA)');
      }, ARRAY_FILTER_USE_BOTH));
      $total_bug += $bug;
    }

    return $total_bug;
  }

  public function getIndexChartData($projectPHID, $totalDev, $index){
    $project = id(new PhabricatorProjectQuery())
      ->setViewer($this->getViewer())
      ->withPHIDs(array($projectPHID))
      ->needMembers(true)
      ->needWatchers(true)
      ->needImages(true)
      ->executeOne();
    $total_dev = $totalDev;
    //$totalMember = $this->getTotalMembership($project);

    $milestones = $this->getMilestones($project);
    $data = [];
    if($milestones){
      $milestone_names = [];
      foreach($milestones as $milestone){
        $milestone_names[] = $milestone->getName();
      }
      $data = $this->getIndexChartDataOfMilestones($project, $data, $total_dev, $milestone_names, $index);
    }
    else{
      $milestone_names = [$project->getName()];
      $data = $this->getIndexChartDataOfMilestones($project, $data, $total_dev, $milestone_names, $index);
    }

    if($milestones){
      foreach($milestone_names as $milestone_name){
        if(!in_array($milestone_name, array_keys($data))){
          $data[$milestone_name] = 0;
        }
      }
    }
    else{
      if(count($data) ==  0){
        $data[$milestone_names[0]] = 0;
      }
    }
    $data = $this->sortBySprint($data, $milestone_names, $total_dev);

    return $data;
  }

  protected function getProjectTags($task)
  {
    if (json_encode($task->edgeProjectPHIDs)) {
      $text = json_encode($task->edgeProjectPHIDs);
      $replacements = [
        '"' => "",
        "[" => "",
        "]" => ""
      ];
      $arrTags = explode(',', strtr($text, $replacements));
      $tags = [];
      for ($i = 0; $i < count($arrTags); $i++) {
        $tag = id(new PhabricatorProject())->loadOneWhere('phid = %s', $arrTags[$i])->getName();
        $tags[] = $tag;
      }
      return $tags;
    } else {
      return null;
    }
  }

  protected function sortBySprint($data, $milestone_names, $total_dev){

    $chartData = [];
    if($total_dev == null) {
      foreach($milestone_names as $milestone_name){
        foreach($data as $k => $v){
          if($milestone_name == $k){
            $chartData[$milestone_name] = $v;
          }
        }
      }
    } else {
      foreach($milestone_names as $milestone_name){
        foreach($data as $k => $v){
          if($milestone_name == $k){
            $chartData[$milestone_name] = round($v / $total_dev, 2);
          }
        }
      }
    }

    $data1 = [];
    $data1['label'] = array_keys($chartData);
    $data1['value'] = array_values($chartData);
    return $data1;
  }

  protected function getNameDay($day) {
    switch ($day) {
      case 1 : return 'monday';
      case 2 : return 'tuesday';
      case 3 : return 'wednesday';
      case 4 : return 'thursday';
      case 5 : return 'friday';
      case 6 : return 'saturday';
      case 7 : return 'sunday';
        break;
    }
  }

  protected function dayOfWeekDataChart($arrayDate, $date, $numOfDay, $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key)
  {
    if ($numOfDay === null) {
      if (!in_array($date->format('d/m/Y'), $dayOffs)) {
        $arrayDate['label'][] = $date->format('d-m-Y');
        $arrayDate['data'][] = null;
        $arrayDate['data2'][] = null;
        $arrayDate['emotions'][] = null;
        $arrayDate['color'][] = null;
        $arrayDate['data3'][] = null;
      }
    } else {
      if ((strpos($v_dayOfWeeks, $numOfDay) !== false)) {
        if ($end_strtotime === null) {
          if ($key == 0) {
            if (strtotime($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y')) >= $start_strtotime) {
              if (!in_array($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d/m/Y'), $dayOffs)) {
                $arrayDate['label'][] = $date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y');
                $arrayDate['data'][] = null;
                $arrayDate['data2'][] = null;
                $arrayDate['emotions'][] = null;
                $arrayDate['color'][] = null;
                $arrayDate['data3'][] = null;
              }
            }
          } else {
            if (!in_array($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d/m/Y'), $dayOffs)) {
              $arrayDate['label'][] = $date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y');
              $arrayDate['data'][] = null;
              $arrayDate['data2'][] = null;
              $arrayDate['emotions'][] = null;
              $arrayDate['color'][] = null;
              $arrayDate['data3'][] = null;
            }
          }
        }

        if ($key === 0) {
          if (strtotime($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y')) >= $start_strtotime &&
            (strtotime($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y')) <= $end_strtotime)) {
            if (!in_array($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d/m/Y'), $dayOffs)) {
              $arrayDate['label'][] = $date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y');
              $arrayDate['data'][] = null;
              $arrayDate['data2'][] = null;
              $arrayDate['emotions'][] = null;
              $arrayDate['color'][] = null;
              $arrayDate['data3'][] = null;
            }
          }
        } else if (strtotime($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y')) <= $end_strtotime
        && (!in_array($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y'), $arrayDate['label']))
        ) {
          if (!in_array($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d/m/Y'), $dayOffs)) {
            $arrayDate['label'][] = $date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y');
            $arrayDate['data'][] = null;
            $arrayDate['data2'][] = null;
            $arrayDate['emotions'][] = null;
            $arrayDate['color'][] = null;
            $arrayDate['data3'][] = null;
          }
        }
      }
    }
    return $arrayDate;
  }

}
