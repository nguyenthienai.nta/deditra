<?php

abstract class PhabricatorHephaestosBaseArchiveController extends PhabricatorController
{
}


final class PhabricatorHephaestosArchiveController
    extends PhabricatorHephaestosBaseArchiveController
{

    public function handleRequest(AphrontRequest $request)
    {
        $viewer = $request->getUser();

        $space = id(new PhabricatorHephaestosOkrQuery())
            ->setViewer($viewer)
            ->withIDs(array($request->getURIData('id')))
            ->requireCapabilities(
                array(
                    PhabricatorPolicyCapability::CAN_VIEW,
                    PhabricatorPolicyCapability::CAN_EDIT,
                ))
            ->executeOne();
        if (!$space) {
            return new Aphront404Response();
        }

        $is_archive = ($request->getURIData('action') == 'archive');
        $cancel_uri = '/hephaestos';

        if ($request->isFormPost()) {
            $type_archive = PhabricatorHephaestosOkrArchiveTransaction::TRANSACTIONTYPE;

            $xactions = array();
            $xactions[] = id(new PhabricatorHephaestosOkrTransaction())
                ->setTransactionType($type_archive)
                ->setNewValue($is_archive ? 1 : 0);

            $editor = id(new PhabricatorHephaestosOkrEditor())
                ->setActor($viewer)
                ->setContinueOnNoEffect(true)
                ->setContinueOnMissingFields(true)
                ->setContentSourceFromRequest($request);

            $editor->applyTransactions($space, $xactions);

            return id(new AphrontRedirectResponse())->setURI($cancel_uri);
        }

        $body = array();
        if ($is_archive) {
            $title = pht('Archive Hephaestos: %s', $space->getContent());
            $body[] = pht(
                'If you archive this Hephaestos, you will no longer be able to create ' .
                'new objects inside it.');
            $body[] = pht(
                'Existing objects in this Hephaestos will be hidden from query results ' .
                'by default.');
            $button = pht('Archive Hephaestos');
        } else {
            $title = pht('Activate Hephaestos: %s', $space->getContent());
            $body[] = pht(
                'If you activate this Hephaestos, you will be able to create objects ' .
                'inside it again.');
            $body[] = pht(
                'Existing objects will no longer be hidden from query results.');
            $button = pht('Activate Hephaestos');
        }


        $dialog = $this->newDialog()
            ->setTitle($title)
            ->addCancelButton($cancel_uri)
            ->addSubmitButton($button);

        foreach ($body as $paragraph) {
            $dialog->appendParagraph($paragraph);
        }

        return $dialog;
    }
}
