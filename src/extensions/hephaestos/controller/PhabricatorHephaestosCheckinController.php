<?php

final class PhabricatorHephaestosCheckinController
  extends PhabricatorController
{

  public function handleRequest(AphrontRequest $request)
  {
    require_celerity_resource('checkinForm-css');
    Javelin::initBehavior('jquery');
    Javelin::initBehavior('checkin');

    $viewer = $request->getViewer();
    $id = $request->getURIData('id');
    $currentID = $request->getStr('current-id');
    $is_new = false;
    $key_result = id(new PhabricatorHephaestosKeyResultQuery())
      ->setViewer($viewer)
      ->withIDs(array($id))
      ->requireCapabilities(
        array(
          PhabricatorPolicyCapability::CAN_VIEW,
          PhabricatorPolicyCapability::CAN_EDIT,
        ))
      ->executeOne();

    if (!$key_result) {
      return new Aphront404Response();
    }

    $arrayCurrentTable = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID());
    $lastCurrentTable = $arrayCurrentTable[$this->lastKey($arrayCurrentTable)];
    $this->checkCurrentByMeasureBy($arrayCurrentTable, $key_result);
    // $checkHIIHFI = true;
    // if(($key_result->getManualCheckin() == 4) && ($lastCurrentTable->getHII() == null)) {
    //   $checkHIIHFI = false;
    // } else if(($key_result->getManualCheckin() == 5) && ($lastCurrentTable->getHFI() == null)) {
    //   $checkHIIHFI = false;
    // }

    if ($currentID == 0) {
      if (
        isset($lastCurrentTable) &&
        (date("Y/m/d", $lastCurrentTable->getDateCreated()) == date("Y/m/d"))
      ) {
        $currentID = $this->lastKey($arrayCurrentTable);
        $current = $this->currentWhenIssetCurrentID($viewer, $arrayCurrentTable[$this->lastKey($arrayCurrentTable)]->getID());
      } else {
        $current = $this->currentWhenNotIssetCurrentID($key_result);
        $is_new = true;
      }
    } else {
      $current = $this->currentWhenIssetCurrentID($viewer, $currentID);
    }

    $v_dateCheckin = date("d/m/Y", $current->getDateCheckin());

    if ($key_result->getProjectPHID() && $key_result->getManualCheckin()) {
      if($key_result->getManualCheckin() == 3){
        $arrayCurrentTable = $this->filterArrayCurrentTableEffective($arrayCurrentTable);
        $startDate  = $this->getStartTimeCurrent($arrayCurrentTable, $key_result);
        $projects = id(new PhabricatorProjectQuery())
          ->setViewer($viewer)
          ->withPHIDs(explode(',', $key_result->getProjectPHID()))
          ->execute();
        $dataEffective = $this->getEffectiveOfDevelopChartDataOfMilestones($projects , $startDate);
        $v_current = $dataEffective['effective'];
        $process = '(%)';
      } else if(($key_result->getManualCheckin() != 4) && ($key_result->getManualCheckin() != 5)) {
        $point = $this->getPointFromKrHasProject($key_result);
        $v_current = $point['current'];
        $process = $point['target'];
      }
    } else {
      $v_current = $current->getCheckinValue();
      $process = $key_result->getTarget();
    }
    $v_feedback = $current->getFeedback();
    $v_comment = $current->getComment();
    $v_velocity_data = $current->getVelocityData();
    $v_totalDev = $current->getTotalDev();
    $e_current = null;
    $cancel_uri = ('/hephaestos/kr/edit/' . $key_result->getID() . '/');
    if($key_result->getManualCheckin() == 4 || $key_result->getManualCheckin() == 5) {
      $typeBug = $key_result->getManualCheckin() == 4 ? 'DEBIT Bug Rate' : 'Customer Bug Rate';
      if(count(explode(",", $key_result->getProjectPHID())) === 1) {
        $v_hii = $this->getSprintHII($key_result, $typeBug);
        $v_totalBug = $v_hii['value'][0];
      } else if (count(explode(",", $key_result->getProjectPHID())) > 1) {
        $hiihfiProjects = $this->getHIIHFIProjects($key_result, $typeBug);
        $v_totalBug = $hiihfiProjects['totalBug'];
        $v_totalDev = $hiihfiProjects['totalDev'];
      }
      else {
        $v_totalBug = 0;
      }
    }

    $arrayCurrents = $this->arrayCurrents($arrayCurrentTable, $key_result , isset($v_totalBug) ? $v_totalBug : 0, $v_totalDev);

    if ($request->isFormPost()) {
      $xactions = array();
      $v_resolvedPoint = $request->getStr('resolvedPoint') ? $request->getStr('resolvedPoint') : null;
      $v_totalDev = $request->getStr('dev') ? $request->getStr('dev') : null;
      $v_current = $request->getInt('totalBug')  ?
        round($request->getInt('totalBug')/$v_totalDev,2) : $request->getStr('current');
      if($v_current == null) {
        $v_current = 0;
      }
      $currentID = $request->getStr('current_id');
      $v_feedback = $request->getStr('feedback') == NULL ? 0 : $request->getStr('feedback');
      $v_effective = $request->getStr('effective');
      $v_comment = $request->getStr('comment') == "" ? null : $request->getStr('comment');
      $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID());
      $checkInData == null ? $velocityData = null : $velocityData = json_decode(end($checkInData)->getVelocityData(), TRUE);
      $velocityData ? $isFirstCheckin = false : $isFirstCheckin = true;
      if($key_result->getManualCheckin() == 2){
        $v_velocity_data = $this->getVelocityChartData($key_result, $isFirstCheckin);
      }
      else{
        $v_velocity_data = null;
      }
      $v_hii = $request->getInt('totalBug') !== null ? $request->getInt('totalBug') : null;

      if ($currentID == 0) {
        $current = $this->currentWhenNotIssetCurrentID($key_result);
        $is_new = true;
      } else {
        $current = $this->currentWhenIssetCurrentID($viewer, $currentID);
      }

      $type_current =
        PhabricatorHephaestosCheckinCheckinValueTransaction::TRANSACTIONTYPE;

      $type_comment =
        PhabricatorHephaestosCheckinCommentTransaction::TRANSACTIONTYPE;

      $type_velocity_data =
        PhabricatorHephaestosCheckinVelocityDataTransaction::TRANSACTIONTYPE;

      $type_totalDev =
        PhabricatorHephaestosCheckinDevTotalTransaction::TRANSACTIONTYPE;

      $type_hii =
        PhabricatorHephaestosCheckinHIITransaction::TRANSACTIONTYPE;

      $type_hfi =
        PhabricatorHephaestosCheckinHFITransaction::TRANSACTIONTYPE;

      $xactions[] = id(new PhabricatorHephaestosCheckinTransaction())
        ->setTransactionType($type_current)
        ->setNewValue((string)$v_current);
      $xactions[] = id(new PhabricatorHephaestosCheckinTransaction())
        ->setTransactionType($type_comment)
        ->setNewValue($v_comment);
      $xactions[] = id(new PhabricatorHephaestosCheckinTransaction())
        ->setTransactionType($type_velocity_data)
        ->setNewValue($v_velocity_data);
      $xactions[] = id(new PhabricatorHephaestosCheckinTransaction())
        ->setTransactionType($type_hii)
        ->setNewValue(($key_result->getManualCheckin() == 4) ? $v_hii : null);
      $xactions[] = id(new PhabricatorHephaestosCheckinTransaction())
        ->setTransactionType($type_hfi)
        ->setNewValue(($key_result->getManualCheckin() == 5) ? $v_hii : null);
      $xactions[] = id(new PhabricatorHephaestosCheckinTransaction())
        ->setTransactionType($type_totalDev)
        ->setNewValue($v_totalDev);

      $editor = id(new PhabricatorHephaestosCheckinEditor())
        ->setActor($viewer)
        ->setContinueOnNoEffect(true)
        ->setContentSourceFromRequest($request);

      try {
          $editor->applyTransactions($current, $xactions);
        if(($key_result->getManualCheckin() != 4) && ($key_result->getManualCheckin() != 5)) {
          if ($currentID == 0) {
            id(new PhabricatorHephaestosCheckin())
              ->load(end(id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID()))->getID())
              ->setColor($this->setColorByMeasureBy($request->getStr('confidenceLevel'), $key_result, $v_current))
              ->setFeedback($v_feedback)
              ->setEffective($v_effective)
              ->setResolvedPoint($v_resolvedPoint)
              ->save();
            id(new PhabricatorHephaestosKeyResult())->load($key_result->getID())
              ->setConfidenceLevel($this->setColorByMeasureBy($request->getStr('confidenceLevel'), $key_result, $v_current))
              ->save();
          } else {
            if (end(id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID()))->getID() == $currentID) {
              id(new PhabricatorHephaestosKeyResult())->load($key_result->getID())
                ->setConfidenceLevel($this->setColorByMeasureBy($request->getStr('confidenceLevel'), $key_result, $v_current))
                ->save();
            }
            id(new PhabricatorHephaestosCheckin())
              ->load($currentID)
              ->setColor($this->setColorByMeasureBy($request->getStr('confidenceLevel'), $key_result, $v_current))
              ->setFeedback($v_feedback)
              ->setEffective($v_effective)
              ->setResolvedPoint($v_resolvedPoint)
              ->save();
            id(new PhabricatorHephaestosKeyResult())->load($key_result->getID())
              ->setConfidenceLevel($this->setColorByMeasureBy($request->getStr('confidenceLevel'), $key_result, $v_current))
              ->save();
          }
        }
        else if(($key_result->getManualCheckin() == 4) || ($key_result->getManualCheckin() == 5)) {
          $confidentScope = explode(',', $key_result->getConfidentScope());
          $confidentLevel = $key_result->getConfidenceLevel();

          if ($confidentScope[0] !== '') {
            if ($v_current < $confidentScope[0]) {
              $confidentLevel = 1;
            } else if ($v_current > $confidentScope[1]) {
              $confidentLevel = 3;
            } else {
              $confidentLevel = 2;
            }
          }

          if ($currentID == 0) {
            $arrCheckin = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID());

              id(new PhabricatorHephaestosCheckin())
                ->load(end($arrCheckin)->getID())
                ->setColor($confidentLevel)
                ->setFeedback($v_feedback)
                ->save();
              id(new PhabricatorHephaestosKeyResult())->load($key_result->getID())
                ->setConfidenceLevel($confidentLevel)
                ->save();
          } else {

            if (end(id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID()))->getID() == $currentID) {
              id(new PhabricatorHephaestosKeyResult())->load($key_result->getID())
                ->setConfidenceLevel($confidentLevel)
                ->save();
            }
            id(new PhabricatorHephaestosCheckin())
              ->load($currentID)
              ->setColor($confidentLevel)
              ->setFeedback($v_feedback)
              ->save();
          }
        }
          return id(new AphrontRedirectResponse())
            ->setURI('/hephaestos/kr/edit/' . $key_result->getID() . '/');
      } catch (PhabricatorApplicationTransactionValidationException $ex) {
      }
    }

    $title = pht("Check-in KR  $v_dateCheckin");
    $button = pht('Save');
    $dialog = id(new AphrontDialogView())
      ->setUser($viewer)
      ->setTitle($title);

    if($key_result->getManualCheckin() == 0 || $key_result->getManualCheckin() == 1){
      $dialog->appendChild(
        id(new AphrontFormSelectControl())
          ->setLabel(pht('Confidence Level'))
          ->setID('confidenceLevel')
          ->setName('confidenceLevel')
          ->setOptions([1 => 'Green', 2 => 'Yellow', 3 => 'Red'])
          ->setValue($current->getColor()));
    }
    if($key_result->getManualCheckin() == 1){
      $dialog->addHiddenInput('resolvedPoint', json_encode($this->getResolvedPoint($key_result)));
    }
    if($key_result->getManualCheckin() == 4 || $key_result->getManualCheckin() == 5) {
      $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID());
      $this->checkCurrentByMeasureBy($checkInData, $key_result);
      $checkInData == null ? $lastCheckIn = null : $lastCheckIn = end($checkInData);
      (isset($lastCheckIn)) ?
        $isFirstCheckin = false : $isFirstCheckin = true;
      $dialog->appendChild(
        id(new AphrontFormHIIHFICheckinControl())
          ->setType($key_result->getManualCheckin())
          ->setName('dev')
          ->setID('dev')
          ->setTotalBug($v_totalBug)
          ->setValue(
            (count(explode(",", $key_result->getProjectPHID())) == 1) ?
              ($isFirstCheckin ? (($key_result->getAutoCheckin() == 1 && $key_result->getTotalDev() > 0) ?
                $key_result->getTotalDev() : $key_result) : $lastCheckIn->getTotalDev()) :
              $v_totalDev
          )
      )
        ->addHiddenInput('totalBug', $v_totalBug);
    } else if($key_result->getManualCheckin() == 3){
      $dialog->appendChild(
        id(new AphrontFormNumberControl())
          ->setID('timeDevelop')
          ->setName('timeDevelop')
          ->setLabel('Time to develop')
          ->setValue($dataEffective['totalTimeDEV'])
          ->setReadonly(true))
        ->appendChild(
        id(new AphrontFormNumberControl())
          ->setID('timeFixBug')
          ->setName('timeFixBug')
          ->setLabel('Time to fix bug')
          ->setValue($dataEffective['totalTimeBUG'])
          ->setReadonly(true))
        ->appendChild(
        id(new AphrontFormNumberControl())
          ->setID('effective')
          ->setName('current')
          ->setLabel('Effective of develop')
          ->setValue($v_current)
          ->setReadonly(true))
        ->appendParagraph('(%)')
        ->addHiddenInput('confidenceLevel', 1)
        ->addHiddenInput('effective', json_encode($dataEffective));
    } else {
      $dialog->appendChild(
        id(new AphrontFormNumberControl())
          ->setID('curent')
          ->setName('current')
          ->setLabel($this->getLabelByMeasureBy($key_result))
          ->setValue($this->getValueByMeasureBy($key_result, $current))
          ->setReadonly(($key_result->getProjectPHID() && $key_result->getManualCheckin()) == null ? NULL : 'true'));
      }
      $dialog->addHiddenInput('current_id', $currentID)
      ->addHiddenInput('arrayCurrents', json_encode($arrayCurrents));

    if($key_result->getManualCheckin() == 0 || $key_result->getManualCheckin() == 1){
      $dialog->appendParagraph('/' . $this->getProcessByMeasureBy($key_result));
    }
    if($key_result->getManualCheckin() == 2){
      $dialog->appendParagraph('Points');
    }

    if($key_result->getManualCheckin() != 2) {
      $dialog->appendChild(
        id(new AphrontFormSelectControl())
          ->setLabel(pht('Feedback'))
          ->setID('feedback')
          ->setName('feedback')
          ->setOptions([0 =>"",5 => '😍', 4 => '😘', 3 => '🙂', 2 => '🙁', 1 => '😧'])
          ->setValue($v_feedback))
        ->appendChild(
          id(new AphrontFormTextAreaControl())
            ->setLabel(pht('Comment'))
            ->setID('comment')
            ->setName('comment')
            ->setValue($v_comment));
    }else{
      $dialog->appendChild(
          id(new AphrontFormTextAreaControl())
            ->setLabel(pht('Comment'))
            ->setID('comment')
            ->setName('comment')
            ->setValue($v_comment));
    }

    $dialog ->appendChild(
      id(new PHUIButtonView())
        ->setTag('a')
        ->setIcon('fa-angle-double-left')
        ->setId('preview')
        ->setColor(PHUIButtonView::GREEN)
    )
      ->appendChild(
        id(new PHUIButtonView())
          ->setTag('a')
          ->setIcon('fa-angle-double-right')
          ->setId('next')
          ->setColor(PHUIButtonView::GREEN)
      )

      ->addHiddenInput('current_id', $currentID)
      ->addHiddenInput('arrayCurrents', json_encode($arrayCurrents))
      ->addHiddenInput('is_new', $is_new ? 1 : 0)
      ->addCancelButton($cancel_uri)
      ->addSubmitButton($button);


    return id(new AphrontDialogResponse())->setDialog($dialog);
  }

  private function lastKey($array)
  {
    end($array);
    return key($array);
  }

  private function arrayCurrents($arrayCurrentTable, $key_result, $v_totalBug , $v_totalDev)
  {
    $arrayCurrents = array();
    $position = 0;
    if (count($arrayCurrentTable) > 0) {
      foreach ($arrayCurrentTable as $v_currentTable) {
        $arrayCurrents[$position][] = $v_currentTable->getID();
        $arrayCurrents[$position][] = date("d/m/Y", $v_currentTable->getDateCreated());
        $arrayCurrents[$position][] = $v_currentTable->getCheckinValue();
        $arrayCurrents[$position][] = $v_currentTable->getColor();
        $arrayCurrents[$position][] = $v_currentTable->getFeedback();
        $arrayCurrents[$position][] = $v_currentTable->getComment();
        $arrayCurrents[$position][] = $v_currentTable->getTotalDev();
        $arrayCurrents[$position][] = round($v_currentTable->getTotalDev() * $v_currentTable->getCheckinValue());
        $arrayCurrents[$position]['effective'] = json_decode($v_currentTable->getEffective());
        $position++;
      }
    }

    if ($arrayCurrents[$this->lastKey($arrayCurrents)][1] !== date("d/m/Y")) {
      $key_last = $position++;
      $arrayCurrents[$key_last][] = 0;
      $arrayCurrents[$key_last][] = date("d/m/Y");
      if ($key_result->getProjectPHID() && $key_result->getManualCheckin() == 1) {
        $arrayCurrents[$key_last][] = $this->getPointFromKrHasProject($key_result)['current'];
      } elseif ($key_result->getProjectPHID() && $key_result->getManualCheckin() == 2) {
        $arrayCurrents[$key_last][] = $this->getResolvedPointOfLastSprint($key_result);
      } else {
        $arrayCurrents[$key_last][] = null;
      }

      $arrayCurrents[$key_last][] = 1;
      $arrayCurrents[$key_last][6] = !isset($v_currentTable) ? '' : $v_currentTable->getTotalDev();
      $arrayCurrents[$key_last][7] = $v_totalBug;
      $arrayCurrents[$key_last]['effective'] = null;
    }

    if($key_result->getManualCheckin() == 4 || $key_result->getManualCheckin() == 5) {
      $arrayCurrents[$position - 1][7] = $v_totalBug;
      if(count(explode(",", $key_result->getProjectPHID())) > 1) {
        $arrayCurrents[$position - 1][6] = $v_totalDev;
      }
    }

    return $arrayCurrents;
  }


  private function getStartTimeCurrent($arrayCurrentTable, $key_result)
  {
    $startDate = 0;
    if(count($arrayCurrentTable) == 0){
      $startDate = $key_result->getStartDate();
    } else {
      $dateCheckin = $arrayCurrentTable[$this->lastKey($arrayCurrentTable)]->getDateCheckin();
      if(date("d/m/Y", $dateCheckin) !== date("d/m/Y")){
        $startDate = $dateCheckin;
      } else {
        if($arrayCurrentTable[$this->lastKey($arrayCurrentTable) - 1] != null){
          $startDate = $arrayCurrentTable[$this->lastKey($arrayCurrentTable) - 1]->getDateCheckin();
        } else {
          $startDate = $key_result->getStartDate();
        }
      }
    }
    return $startDate;
  }
  private function filterArrayCurrentTableEffective($arrayCurrentTable){
    $arrayFilter = [];
    foreach($arrayCurrentTable as $current){
      if($current->getEffective() != null){
        $arrayFilter[] = $current;
      }
    }
    return $arrayFilter;
  }

  private function getPointFromKrHasProject($kr)
  {
    $project = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $kr->getProjectPHID());
    $state = id(new PhabricatorOkrsViewState())
      ->setProject($project)
      ->readFromRequest($this->getRequest());
    $tasks = $state->getObjects();
    $totalPoint = 0;
    $currentPoint = 0;
    foreach ($tasks as $taskID => $task) {
      if ($task->getPoints() !== null) {
        $totalPoint += $task->getPoints();
        if ($task->getStatus() == 'resolved' || $task->getStatus() == 'closed') {
          $currentPoint += $task->getPoints();
        }
      }
    }
    return ['current' => $currentPoint, 'target' => $totalPoint];
  }

  private function currentWhenIssetCurrentID($viewer, $currentID)
  {
    return id(new PhabricatorHephaestosCheckinQuery())
      ->setViewer($viewer)
      ->withIDs(array($currentID))
      ->requireCapabilities(
        array(
          PhabricatorPolicyCapability::CAN_VIEW,
          PhabricatorPolicyCapability::CAN_EDIT,
        ))
      ->executeOne();
  }

  private function currentWhenNotIssetCurrentID($key_result)
  {
    return PhabricatorHephaestosCheckin::initializeCurrent($key_result);
  }

  private function getLabelByMeasureBy($key_result)
  {
    if($key_result->getManualCheckin() == 0 || $key_result->getManualCheckin() == 1){
      return 'Current';
    }
    elseif($key_result->getManualCheckin() == 2){
      require_celerity_resource('checkinVelocityForm-css');
      Javelin::initBehavior('checkinVelocityForm');
      return 'Resolved';
    }
    elseif($key_result->getManualCheckin() == 3){
      return 'Develop Productivity';
    }
    elseif($key_result->getManualCheckin() == 4){
      return 'Debit Bug rate';
    }
    elseif($key_result->getManualCheckin() == 5){
      return 'Customer Bug rate';
    }
  }

  protected function getSprintHII($kr , $index){
    $process = null;
    $increase = null;
    return id(new PhabricatorHephaestosKrEditController())
      ->setRequest($this->getRequest())
      ->getIndexChartData($kr->getProjectPHID(), null, $index);
  }

  private function getValueByMeasureBy($key_result, $current)
  {
    if($key_result->getManualCheckin() == 0){
      return $current->getCheckinValue();
    }
    elseif($key_result->getManualCheckin() == 1){
      $point = $this->getPointFromKrHasProject($key_result);
      return $point['current'];
    }
    elseif($key_result->getManualCheckin() == 2){
      return $this->getResolvedPointOfLastSprint($key_result);
    }
    elseif($key_result->getManualCheckin() == 3){
      return '';
    }
    elseif($key_result->getManualCheckin() == 4){
      return '';
    }
    elseif($key_result->getManualCheckin() == 5){
      return '';
    }
  }

  private function getProcessByMeasureBy($key_result)
  {
    if($key_result->getManualCheckin() == 0){
      return $key_result->getTarget();
    }
    elseif($key_result->getManualCheckin() == 1){
      $point = $this->getPointFromKrHasProject($key_result);
      return  $point['target'];
    }
  }

  private function getResolvedPointOfLastSprint($key_result)
  {
    $projectPHID = $key_result->getProjectPHID();
    $project = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $projectPHID);
    $milestones = $this->getMilestones($project);

    if($milestones){
      $lastSprint = array_pop(array_reverse(($milestones)));

      $state = id(new PhabricatorOkrsViewState())->setProject($lastSprint)->readFromRequest($this->getRequest());
      $tasks = $state->getObjects();
      $resolvedPoint = 0;

      foreach ($tasks as $taskID => $task) {
        if ($task->getPoints() !== null) {
          if ($task->getStatus() == 'resolved' || $task->getStatus() == 'closed') {
            $resolvedPoint += $task->getPoints();
          }
        }
      }
    }
    else{
      $state = id(new PhabricatorOkrsViewState())->setProject($project)->readFromRequest($this->getRequest());
      $tasks = $state->getObjects();
      $resolvedPoint = 0;

      foreach ($tasks as $taskID => $task) {
        if ($task->getPoints() !== null) {
          if ($task->getStatus() == 'resolved' || $task->getStatus() == 'closed') {
            $resolvedPoint += $task->getPoints();
          }
        }
      }
    }
    return $resolvedPoint;
  }

  protected function getMilestones($project)
  {
    $viewer = $this->getViewer();
    $allows_milestones = $project->supportsMilestones();

    if ($allows_milestones) {
      $milestones = id(new PhabricatorProjectQuery())
        ->setViewer($viewer)
        ->withParentProjectPHIDs(array($project->getPHID()))
        ->needImages(true)
        ->withIsMilestone(true)
        ->setOrderVector(array('milestoneNumber', 'id'))
        ->execute();
    } else {
      $milestones = array();
    }
    return $milestones;
  }

  public function getNewVelocityDataValue($milestones){
    $data = [];
    foreach ($milestones as $milestone) {
      $state = id(new PhabricatorOkrsViewState())->setProject($milestone)->readFromRequest($this->getRequest());
      $tasks = $state->getObjects();
      $currentPoint = 0;

      foreach ($tasks as $taskID => $task) {
        if ($task->getPoints() !== null) {
          if ($task->getStatus() == 'resolved' || $task->getStatus() == 'closed') {
            $currentPoint += $task->getPoints();
          }
        }
      }
      $data['label'][] = $milestone->getName();
      $data['value'][] = $currentPoint;
    }
    return $data;
  }


  public function getVelocityChartData($key_result, $isFirstCheckin)
  {
    $project = id(new PhabricatorProjectQuery())
      ->setViewer($this->getViewer())
      ->withPHIDs(array($key_result->getProjectPHID()))
      ->needMembers(true)
      ->needWatchers(true)
      ->needImages(true)
      ->executeOne();

    $milestones = $this->getMilestones($project);
    $data = [];
    if ($milestones) {
      $newDataValue = $this->getNewVelocityDataValue($milestones);
      if($isFirstCheckin){
       $data = $newDataValue;
      }
      else{
        $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $key_result->getPHID());
        $velocityData = json_decode(end($checkInData)->getVelocityData(), TRUE);
        $data = $velocityData;

        if(count($newDataValue['label']) == count($data['label'])){
          $data['label'] = $newDataValue['label'];
          $data['value'][0] = $newDataValue['value'][0];
        }
        else{
          $offset = count($newDataValue['label']) - count($data['label']);
          $change_value = array_slice($newDataValue['value'], 0, $offset);

          if($offset > 1) {
            for($i = 1; $i < $offset; $i++) {
              $change_value[$i] = 0;
            }
          }

          $data['label'] = $newDataValue['label'];
          $data['value'] = array_merge($change_value, $data['value']);
        }
      }
    } else {
      $state = id(new PhabricatorOkrsViewState())->setProject($project)->readFromRequest($this->getRequest());
      $tasks = $state->getObjects();
      $currentPoint = 0;

      foreach ($tasks as $taskID => $task) {
        if ($task->getPoints() !== null) {
          if ($task->getStatus() == 'resolved' || $task->getStatus() == 'closed') {
            $currentPoint += $task->getPoints();
          }
        }
      }
      $data['label'][] = $project->getName();
      $data['value'][] = $currentPoint;
    }
    return json_encode($data);
  }

  protected function setColorByMeasureBy($newValue, $kr, $currentValue)
  {
    if($kr->getManualCheckin() == 0 || $kr->getManualCheckin() == 1){
      return $newValue;
    }
    elseif($kr->getManualCheckin() == 2){
      $confidentScope = explode(',', $kr->getConfidentScope());
      $checkInData = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $kr->getPHID());
      $velocityData =  json_decode(end($checkInData)->getVelocityData(), TRUE);

      if($confidentScope == null){
        return 1;
      }
      else{
        if ($velocityData['value'][0] > (float)$confidentScope[1]){
          return 1;
        } elseif ($velocityData['value'][0] < (float)$confidentScope[0]) {
          return 3;
        } else {
          return 2;
        }
      }
    }
    elseif($kr->getManualCheckin() == 3){
      $confidentScope = explode(',', $kr->getConfidentScope());
      if($confidentScope == null){
        return 1;
      }
      else{
        if ($currentValue > (float)$confidentScope[1]){
          return 1;
        } elseif ($currentValue < (float)$confidentScope[0]) {
          return 3;
        } else {
          return 2;
        }
      }
    }
    elseif($kr->getManualCheckin() == 4){
      return 1;
    }
    elseif($kr->getManualCheckin() == 5){
      return 1;
    }
  }


  protected function getEffectiveOfDevelopChartDataOfMilestones($projects, $startTime)
  {
    $data = [];

    $totalTimeDEV = 0;
    $totalTimeBUG = 0;

    $bugQA = id(new PhabricatorProject())->loadOneWhere('name = %s', 'Bug Report (QA)');
    $bugQAPHID =isset($bugQA) ? $bugQA->getPHID() : null;
    $bugCustomer = id(new PhabricatorProject())->loadOneWhere('name = %s', 'Bug Report (Customer)');
    $bugCustomerPHID = isset($bugCustomer) ? $bugCustomer->getPHID() : null;

    foreach($projects as $project){
      $state = id(new PhabricatorOkrsViewState())->setProject($project)->readFromRequest($this->getRequest());
      $tasks = $state->getObjects();

      foreach ($tasks as $taskID => $task) {

        $startDoing = 0;
        $endDoing = 0;

        $isBug = $this->checkTaskIsBug($task, $bugQAPHID, $bugCustomerPHID);
        $type = 'columns';
        $xactions = $this->getXactionsFromTask($task, $type);

        foreach($xactions as $xaction){
          $column = $xaction->getNewValue()[0]["columnPHID"];
          $nameColumn = id(new PhabricatorProjectColumn())->loadOneWhere('PHID = %s', $column)->getName();

          if (strtolower($nameColumn) == "doing" || strtolower($nameColumn) == "in develop") {
            $startDoing = $xaction->getDateCreated();
          } else {
            if ($startDoing != 0) {
              $endDoing = $xaction->getDateCreated();
            }
          }

          if($startDoing != 0 && $endDoing != 0){
            if($startDoing > $startTime && $endDoing > $startTime){
              $timeDoing = ($endDoing - $startDoing) / 3600;
              $totalTimeDEV += $timeDoing;

              if($isBug == true) {
                $totalTimeBUG += $timeDoing;
              }
            } else if ($startDoing <= $startTime && $endDoing > $startTime){
              $timeDoing = ($endDoing - $startTime) / 3600;
              $totalTimeDEV += $timeDoing;

              if($isBug == true) {
                $totalTimeBUG += $timeDoing;
              }
            }
            $startDoing = 0;
            $endDoing = 0;
          }
        }

        if($startDoing != 0 ){
          if($startDoing > $startTime){
            $endDoing = id(new DateTime())->getTimestamp();
            $timeDoing = ($endDoing - $startDoing) / 3600;
            $totalTimeDEV += $timeDoing;
            if($isBug == true) {
              $totalTimeBUG += $timeDoing;
            }
          } else {
            $endDoing = id(new DateTime())->getTimestamp();
            $timeDoing = ($endDoing - $startTime) / 3600;
            $totalTimeDEV += $timeDoing;
            if($isBug == true) {
              $totalTimeBUG += $timeDoing;
            }
          }
        }
      }
    }
    if(round($totalTimeDEV, 1) != 0){
      $effective = round(((round($totalTimeDEV, 1) - round($totalTimeBUG, 1))/round($totalTimeDEV, 1)) * 100, 1);
    } else {
      $effective = 0;
    }
    $data['totalTime'] = round($totalTimeDEV, 1);
    $data['totalTimeBUG'] = round($totalTimeBUG, 1);
    $data['totalTimeDEV'] = round(round($totalTimeDEV, 1) -  round($totalTimeBUG, 1), 1);
    $data['effective'] = $effective;
    return $data;
  }
  protected function getXactionsFromTask($task, $type)
  {
    $request = $this->getRequest();
    $viewer = $this->getViewer();
    $pager = id(new AphrontCursorPagerView())
      ->readFromRequest($request);
    $query = id(new ManiphestTransactionQuery());

    if($type === 'edge'){
      $xactions = $query
              ->setViewer($viewer)
              ->withObjectPHIDs(array($task->getPHID()))
              ->withTransactionTypes(array(PhabricatorTransactions::TYPE_EDGE))
              ->needComments(true)
              ->executeWithCursorPager($pager);
    }
    elseif($type === 'columns'){
      $xactions = $query
              ->setViewer($viewer)
              ->withObjectPHIDs(array($task->getPHID()))
              ->withTransactionTypes(array(PhabricatorTransactions::TYPE_COLUMNS))
              ->needComments(true)
              ->executeWithCursorPager($pager);
      }
    return array_reverse($xactions);
  }

  protected function checkTaskIsBug($task, $bugQAPHID, $bugCustomerPHID = null)
  {
    $tags = $task->getProjectPHIDs();
    $isBug = false;
    if(in_array($bugQAPHID, $tags) || in_array($bugCustomerPHID, $tags)){
      $isBug = true;
    }
    return $isBug;
  }

  protected function checkCurrentByMeasureBy($arrayCurrentTable, $key_result){
    $newCurrentTable = [];
    if($key_result->getManualCheckin() == 0 || $key_result->getManualCheckin() == 1){
      $newCurrentTable = [];
      foreach ($arrayCurrentTable as $current){
        if($current->getVelocityData() == null && $current->getHII() == null && $current->getEffective() == null){
          $newCurrentTable[] = $current;
        }
      }
    }
    elseif($key_result->getManualCheckin() == 2){
      $newCurrentTable = [];
      foreach ($arrayCurrentTable as $current){
        if($current->getVelocityData() != null){
          $newCurrentTable[] = $current;
        }
      }
    }
    elseif($key_result->getManualCheckin() == 3){
      $newCurrentTable = [];
      foreach ($arrayCurrentTable as $current){
        if($current->getEffective() != null){
          $newCurrentTable[] = $current;
        }
      }
    }
    elseif($key_result->getManualCheckin() == 4 || $key_result->getManualCheckin() == 5){
      $newCurrentTable = [];
      foreach ($arrayCurrentTable as $current){
        if($current->getHII() != null){
          $newCurrentTable[] = $current;
        }
      }
    } else {
      $newCurrentTable = $arrayCurrentTable;
    }

    $arrayCurrentTable = $newCurrentTable;
  }

  private function getDateLabelChart($keyResult)
  {
    $dayOffs =  explode(", ",$keyResult->getDayOffs());
    $begin = new DateTime(date('Y-m-d', $keyResult->getStartDate()));
    $end = new DateTime(date('Y-m-d', $keyResult->getEndDate()));
    $end = $end->modify('+1 day');
    $period = new DatePeriod(
      $begin,
      new DateInterval('P1D'),
      $end
    );
    $arrays = [];
    foreach ($period as $key => $value) {
      $date = $value->format('d/m/Y');
      if (!in_array($date, $dayOffs)) array_push($arrays, $date);
    }

    return $arrays;
  }

  private function getResolvedPoint($kr)
  {
    $project = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $kr->getProjectPHID());
    $state = id(new PhabricatorOkrsViewState())
      ->setProject($project)
      ->readFromRequest($this->getRequest());
    $tasks = $state->getObjects();
    $arr_label = $this->getDateLabelChart($kr);
    foreach ($arr_label as $key => $dateChart) {
      $pointOfDate = null;
      $resolvedPoint = null;
      $resolvedPoint1 = null;
      $pointToResolve = null;

      foreach ($tasks as $task) {
        if ($task->getClosedEpoch() != null) {
          $taskStatus = $task->getStatus();
          if ($taskStatus == 'resolved' || $taskStatus == 'closed' ) {
            $resolvedDate = date('d/m/Y', $task->getClosedEpoch());
            if ($resolvedDate == $dateChart)
              $resolvedPoint += $task->getPoints() != 0 ? $task->getPoints() : null;
          }
        }
        if ($task->getEndDate() != null) {
          $endDate = date('d/m/Y', $task->getEndDate());
          $taskStatus = $task->getStatus();
          if ($endDate == $dateChart)
            if ($taskStatus == 'open' || $taskStatus == 'doing')
              $pointToResolve += $task->getPoints() != 0 ? $task->getPoints() : null;
        }
        $arr_label['resolvedPoint'][$dateChart] = $resolvedPoint;
        $arr_label['pointToResolve'][$dateChart] = $pointToResolve;
      }
    }
    return ['resolvedPoint' => $arr_label['resolvedPoint'], 'pointToResolve' => $arr_label['pointToResolve']];
  }

  private function getHIIHFIProjects($key_result, $typeBug) {
    $totalProjects = explode(",", $key_result->getProjectPHID());
    $bugRate = 0;
    $totalDev = 0;
    foreach ($totalProjects as $project) {
      $krBugs = id(new PhabricatorHephaestosKeyResult())->loadAllWhere('projectPHID = %s', $project);
      $date = 0;
      $bugRatePerKr = $bugRate;
      $totalDevPerKr = $totalDev;

      foreach ($krBugs as $krBug) {
        if(($typeBug == 'DEBIT Bug Rate') ? ($krBug->getManualCheckin() == 4) : ($krBug->getManualCheckin() == 5)) {
          $currents = id(new PhabricatorHephaestosCheckin())->loadAllWhere('krPHID = %s', $krBug->getPHID());
          foreach (array_reverse($currents) as $current) {
            if(($typeBug == 'DEBIT Bug Rate') ? ($current->getHII() !== null) : ($current->getHFI() !== null)) {
              if($current->getDateCheckin() > $date) {
                $bugRatePerKr = $current->getCheckinValue();
                $totalDevPerKr = $current->getTotalDev();
                $date = $current->getDateCheckin();
              }
              break;
            }
          }
        }
      }
      if ($bugRatePerKr >= $bugRate) {
        $bugRate = $bugRatePerKr;
        $totalDev = $totalDevPerKr;
      }
    }

    return array(
      'totalBug' => round($bugRate * $totalDev),
      'totalDev' => $totalDev
    );
  }

}
