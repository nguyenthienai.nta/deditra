<?php
abstract class PhabricatorHephaestosBaseViewController extends PhabricatorController
{
}

final class PhabricatorHephaestosListOkrController
    extends PhabricatorHephaestosBaseViewController
{

    public function shouldAllowPublic()
    {
        return true;
    }

    public function handleRequest(AphrontRequest $request)
    {
        $request = $this->getRequest();
        $controller = id(new PhabricatorApplicationSearchController())
            ->setQueryKey($request->getURIData('queryKey'))
            ->setSearchEngine(new PhabricatorHephaestosOkrsearchEngine())
            ->setNavigation($this->buildSideNavView());

        return $this->delegateToController($controller);
    }

    public function buildSideNavView($for_app = false)
    {
        $user = $this->getRequest()->getUser();

        $nav = new AphrontSideNavFilterView();
        $nav->setBaseURI(new PhutilURI($this->getApplicationURI()));

        id(new PhabricatorHephaestosOkrSearchEngine())
            ->setViewer($user)
            ->addNavigationItems($nav->getMenu());

        $nav->selectFilter(null);

        return $nav;
    }

    protected function buildApplicationCrumbs()
    {
        $crumbs = parent::buildApplicationCrumbs();

        $can_create = $this->hasApplicationCapability(
            PhabricatorHephaestosCapabilityCreate::CAPABILITY);

        $crumbs->addAction(
          id(new PHUIListItemView())
            ->setName(pht('Tracker for Project'))
            ->setHref('/hephaestos/create-tracker/')
            ->setIcon('fa-plus-square')
            ->setDisabled(!$can_create)
            ->setWorkflow(!$can_create));

        return $crumbs;
    }


}
