<?php

final class PhabricatorHephaestosDestroyObjController
  extends PhabricatorController
{

  public function handleRequest(AphrontRequest $request)
  {
    $viewer = $request->getViewer();
    $id = $request->getURIData('id');

    $objective = id(new PhabricatorHephaestosObjectiveQuery())
      ->setViewer($viewer)
      ->withIDs(array($id))
      ->requireCapabilities(
        array(
          PhabricatorPolicyCapability::CAN_VIEW,
          PhabricatorPolicyCapability::CAN_EDIT,
        ))
      ->executeOne();
    if (!$objective) {
      return new Aphront404Response();
    }
    $okr_id = id(new PhabricatorHephaestosOkr())->loadOneWhere('phid = %s', $objective->getHephaestosPHID())->getID();
    $cancel_uri = $this->getApplicationURI('/hephaestos/edit/' . $objective->getID() . '/');

    if ($request->isFormPost()) {

      id(new PhabricatorHephaestosObjective())->load($objective->getID())->delete();

      if (count(id(new PhabricatorHephaestosKeyResult())->loadAllWhere('objectivePHID = %s', $objective->getPHID())) > 0) {
        foreach (id(new PhabricatorHephaestosKeyResult())->loadAllWhere('objectivePHID = %s', $objective->getPHID()) as $kr) {
          $kr->delete();
        }
      }
      session_start();
      $_SESSION['message'] = 'Objective deleted successfully';

      return id(new AphrontRedirectResponse())->setURI('/hephaestos/obj/' . $okr_id . '/');
    }

    $title = pht('Really Delete Objective?');
    $body = pht('This Objective will delete.');
    $button = pht('Delete Objective');

    $dialog = id(new AphrontDialogView())
      ->setUser($viewer)
      ->setTitle($title)
      ->appendChild($body)
      ->addCancelButton($cancel_uri)
      ->addSubmitButton($button);

    return id(new AphrontDialogResponse())->setDialog($dialog);
  }


}
