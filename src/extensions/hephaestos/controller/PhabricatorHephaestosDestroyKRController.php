<?php
final class PhabricatorHephaestosDestroyKRController
    extends PhabricatorController {

    public function handleRequest(AphrontRequest $request) {
        $viewer = $request->getViewer();
        $id = $request->getURIData('id');

        $kr = id(new PhabricatorHephaestosKeyResultQuery())
            ->setViewer($viewer)
            ->withIDs(array($id))
            ->requireCapabilities(
                array(
                    PhabricatorPolicyCapability::CAN_VIEW,
                    PhabricatorPolicyCapability::CAN_EDIT,
                ))
            ->executeOne();
        if (!$kr) {
            return new Aphront404Response();
        }
        $obj_id = id(new PhabricatorHephaestosObjective())->loadOneWhere('phid = %s',$kr->getObjectivePHID())->getHephaestosPHID();
        $okr_id = id(new PhabricatorHephaestosOkr())->loadOneWhere('phid = %s',$obj_id)->getID();

        $cancel_uri = ('/hephaestos/kr/edit/'.$kr->getID().'/');

        if ($request->isFormPost()) {

            id(new PhabricatorHephaestosKeyResult())->load($kr->getID())->delete();
            session_start();
            $_SESSION['message'] = 'KR deleted successfully';

            return id(new AphrontRedirectResponse())->setURI('/hephaestos/obj/'.$okr_id.'/');
        }

        $title = pht('Really Delete KR?');
        $body = pht('This KR will delete.');
        $button = pht('Delete KR');

        $dialog = id(new AphrontDialogView())
            ->setUser($viewer)
            ->setTitle($title)
            ->appendChild($body)
            ->addCancelButton($cancel_uri)
            ->addSubmitButton($button);

        return id(new AphrontDialogResponse())->setDialog($dialog);
    }


}
