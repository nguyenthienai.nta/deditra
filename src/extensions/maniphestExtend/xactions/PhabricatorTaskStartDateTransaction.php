<?php

final class PhabricatorTaskStartDateTransaction
  extends ManiphestTaskTransactionType {

  const TRANSACTIONTYPE = 'startDate';

  public function generateOldValue($object) {
    return $object->getStartDate() == null ? null : (int)$object->getStartDate();
  }

  public function generateNewValue($object, $value) {

    if($value->getValueDate() == null || $value->getValueDate() == "" ) {
      return null;
    } else {
      return  $value->newPhutilDateTime()
        ->newAbsoluteDateTime()
        ->getEpoch();
    }
  }

  public function applyInternalEffects($object, $value) {
    $object->setStartDate($value);
  }

  public function getTitle() {
    return pht(
      '%s updated the start work from %s to %s.',
      $this->renderAuthor(),
      $this->renderOldDate(),
      $this->renderNewDate());
  }

  public function getTitleForFeed() {
    return pht(
      '%s updated the start work for %s from %s to %s.',
      $this->renderAuthor(),
      $this->renderObject(),
      $this->renderOldDate(),
      $this->renderNewDate());
  }

  public function validateTransactions($object, array $xactions) {
    $errors = array();

    foreach ($xactions as $xaction) {
      $value = $xaction->getNewValue();
      $startDate = $value->getValueDate(); 
      if ( $startDate != null && !$value->isValid()) {
        $errors[] = $this->newInvalidError(
          pht('Start work is invalid.'));
      }
    }

    return $errors;
  }
}
