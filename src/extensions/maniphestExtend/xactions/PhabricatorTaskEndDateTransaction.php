<?php

final class PhabricatorTaskEndDateTransaction
extends ManiphestTaskTransactionType
{

  const TRANSACTIONTYPE = 'endDate';

  public function generateOldValue($object)
  {
    return  $object->getEndDate() == null ? null : (int)$object->getEndDate();
  }

  public function generateNewValue($object, $value)
  {
     if($value->getValueDate() == null || $value->getValueDate() == "" ) {
      return null;
     } else {
      return  $value->newPhutilDateTime()
        ->newAbsoluteDateTime()
        ->getEpoch();
     }
  }

  public function applyInternalEffects($object, $value)
  {
    $object->setEndDate($value);
  }

  public function getTitle()
  {
    return pht(
      '%s updated the build plan from %s to %s.',
      $this->renderAuthor(),
      $this->renderOldDate(),
      $this->renderNewDate()
    );
  }

  public function getTitleForFeed()
  {
    return pht(
      '%s updated the build plan for %s from %s to %s.',
      $this->renderAuthor(),
      $this->renderObject(),
      $this->renderOldDate(),
      $this->renderNewDate()
    );
  }

  public function validateTransactions($object, array $xactions)
  {
    $errors = array();
    foreach ($xactions as $xaction) {
      $value = $xaction->getNewValue();
      $endDate = $value->getValueDate(); 
      if ( $endDate!= null && !$value->isValid()) {
        $errors[] = $this->newInvalidError(
          pht('Build plan is invalid.')
        );
      }
    }

    return $errors;
  }
}
