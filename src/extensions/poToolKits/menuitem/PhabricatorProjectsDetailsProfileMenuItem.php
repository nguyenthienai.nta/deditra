<?php

final class PhabricatorProjectsDetailsProfileMenuItem
    extends PhabricatorProfileMenuItem
{

    const MENUITEMKEY = 'projects.details';

    public function getMenuItemTypeName()
    {
        return pht('Projects Details');
    }

    private function getDefaultName()
    {
        return pht('Projects Details');
    }

    public function getMenuItemTypeIcon()
    {
        return 'fa fa-eye';
    }

    public function canHideMenuItem(
        PhabricatorProfileMenuItemConfiguration $config)
    {
        return false;
    }

    public function canMakeDefault(
        PhabricatorProfileMenuItemConfiguration $config)
    {
        return true;
    }

    public function getDisplayName(
        PhabricatorProfileMenuItemConfiguration $config)
    {
        $name = $config->getMenuItemProperty('name');

        if (strlen($name)) {
            return $name;
        }

        return $this->getDefaultName();
    }

    public function buildEditEngineFields(
        PhabricatorProfileMenuItemConfiguration $config)
    {
        return array(
            id(new PhabricatorTextEditField())
                ->setKey('name')
                ->setLabel(pht('Name'))
                ->setPlaceholder($this->getDefaultName())
                ->setValue($config->getMenuItemProperty('name')),
        );
    }

    protected function newMenuItemViewList(
        PhabricatorProfileMenuItemConfiguration $config)
    {

        $project = $config->getProfileObject();
        $id = $project->getID();
        $name = $project->getName();
        $icon = 'fa fa-eye';
        $uri = "/projects/profile/{$id}/";

        $item = $this->newItemView()
            ->setURI($uri)
            ->setName($name)
            ->setIcon($icon);

        return array(
            $item,
        );
    }

}
