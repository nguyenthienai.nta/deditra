<?php

final class PhabricatorProjectsNSSReleaseSurveyMenuItem
    extends PhabricatorProfileMenuItem
{

    const MENUITEMKEY = 'projects.survey';

    public function getMenuItemTypeName()
    {
        return pht('Projects NSS Release Survey');
    }

    private function getDefaultName()
    {
        return pht('NSS - Release Survey');
    }

    public function getMenuItemTypeIcon()
    {
        return 'fa fa-pencil-square-o';
    }

    public function canMakeDefault(
        PhabricatorProfileMenuItemConfiguration $config)
    {
        return true;
    }

    public function shouldEnableForObject($object)
    {
        $viewer = $this->getViewer();
        $class = 'PhabricatorManiphestApplication';
        if (!PhabricatorApplication::isClassInstalledForViewer($class, $viewer)) {
            return false;
        }

        return true;
    }

    public function getDisplayName(
        PhabricatorProfileMenuItemConfiguration $config)
    {
        $name = $config->getMenuItemProperty('name');

        if (strlen($name)) {
            return $name;
        }

        return $this->getDefaultName();
    }

    public function buildEditEngineFields(
        PhabricatorProfileMenuItemConfiguration $config)
    {
        return array(
            id(new PhabricatorTextEditField())
                ->setKey('name')
                ->setLabel(pht('Name'))
                ->setPlaceholder($this->getDefaultName())
                ->setValue($config->getMenuItemProperty('name')),
        );
    }

    protected function newMenuItemViewList(
        PhabricatorProfileMenuItemConfiguration $config)
    {
        $project = $config->getProfileObject();

        $id = $project->getID();
        $name = $this->getDisplayName($config);

        $item = $this->newItemView()
            ->setURI("/po-tools/release-survey/{$id}")
            ->setName($name)
            ->setIcon('fa fa-pencil-square-o');

        return array(
            $item,
        );
    }

}
