<?php

final class PhabricatorProjectsMembersProfileMenuItem
    extends PhabricatorProfileMenuItem
{

    const MENUITEMKEY = 'projects.customer';

    public function getMenuItemTypeName()
    {
        return pht('Projects Members');
    }

    private function getDefaultName()
    {
        return pht('Scope Management');
    }

    public function getMenuItemTypeIcon()
    {
        return 'fa-users';
    }

    public function getDisplayName(
        PhabricatorProfileMenuItemConfiguration $config)
    {
        $name = $config->getMenuItemProperty('name');

        if (strlen($name)) {
            return $name;
        }

        return $this->getDefaultName();
    }

    public function buildEditEngineFields(
        PhabricatorProfileMenuItemConfiguration $config)
    {
        return array(
            id(new PhabricatorTextEditField())
                ->setKey('name')
                ->setLabel(pht('Name'))
                ->setPlaceholder($this->getDefaultName())
                ->setValue($config->getMenuItemProperty('name')),
        );
    }

    protected function newMenuItemViewList(
        PhabricatorProfileMenuItemConfiguration $config)
    {

        $project = $config->getProfileObject();

        $id = $project->getID();

        $name = $this->getDisplayName($config);
        $icon = 'fa fa-vcard';
        $uri = "#";

        $item = $this->newItemView()
            ->setURI($uri)
            ->setName($name)
            ->setIcon($icon);

        return array(
            $item,
        );
    }

}
