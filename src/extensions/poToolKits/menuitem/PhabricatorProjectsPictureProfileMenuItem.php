<?php

final class PhabricatorProjectsPictureProfileMenuItem
    extends PhabricatorProfileMenuItem
{

    const MENUITEMKEY = 'projects.picture';

    public function getMenuItemTypeName()
    {
        return pht('Projects Picture');
    }

    private function getDefaultName()
    {
        return pht('Projects Picture');
    }

    public function getMenuItemTypeIcon()
    {
        return 'fa-image';
    }

    public function canHideMenuItem(
        PhabricatorProfileMenuItemConfiguration $config)
    {
        return false;
    }

    public function getDisplayName(
        PhabricatorProfileMenuItemConfiguration $config)
    {
        return $this->getDefaultName();
    }

    public function buildEditEngineFields(
        PhabricatorProfileMenuItemConfiguration $config)
    {
        return array();
    }

    protected function newMenuItemViewList(
        PhabricatorProfileMenuItemConfiguration $config)
    {

        $project = $config->getProfileObject();
        $viewer = $config->getMenuItem()->getViewer();
        $image = id(new PhabricatorFile())->loadOneWhere('phid = %s', $project->getIcon());

        if (!empty($image)) {
            $project->attachProfileImageFile($image);
        } else {
            $builtin_name = PhabricatorProjectIconSet::getIconImage('project');
            $builtin_name = 'projects/' . $builtin_name;
            $builtin_file = PhabricatorFile::loadBuiltin($viewer, $builtin_name);
            $project->attachProfileImageFile($builtin_file);
        }

        $picture = $project->getProfileImageURI();
        $item = $this->newItemView();
        $item->newProfileImage($picture);

        return array(
            $item,
        );
    }

}
