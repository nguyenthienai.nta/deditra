<?php

class PhabricatorPoToolEditor
    extends PhabricatorApplicationTransactionEditor
{

    public function getEditorApplicationClass()
    {
        return pht('PhabricatorPoToolKitsApplication');
    }

    public function getEditorObjectsDescription()
    {
        return pht('po_toolkit');
    }

    public function getTransactionTypes()
    {
        $types = parent::getTransactionTypes();

        $types[] = PhabricatorTransactions::TYPE_VIEW_POLICY;
        $types[] = PhabricatorTransactions::TYPE_EDIT_POLICY;

        return $types;
    }

    public function getCreateObjectTitle($author, $object)
    {
        return pht('%s created this project.', $author);
    }

    public function getCreateObjectTitleForFeed($author, $object)
    {
        return pht('%s created project %s.', $author, $object);
    }

}
