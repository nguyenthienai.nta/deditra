<?php

final class PhabricatorProjectsEditor extends PhabricatorPoToolEditor
{
    protected function validateAllTransactions(
        PhabricatorLiskDAO $object,
        array $xactions)
    {

        $errors = parent::validateAllTransactions($object, $xactions);
        $parent_xaction = null;
        $type_openDate = PhabricatorProjectsOpenDateTransaction::TRANSACTIONTYPE;
        $type_closeDate = phabricatorProjectsCloseDateTransaction::TRANSACTIONTYPE;
        $open_date = $object->getOpenDate();
        $close_date = $object->getCloseDate();


        foreach ($xactions as $xaction) {
            switch ($xaction->getTransactionType()) {
                case $type_openDate:
                    $open_date = $xaction->getNewValue();
                    break;
                case $type_closeDate:
                    $close_date = $xaction->getNewValue();
                    break;
            }
        }

        if ($open_date !== null && $close_date !== null) {
            if ($open_date > $close_date) {
                $errors[] = new PhabricatorApplicationTransactionValidationError(
                    $type_closeDate,
                    pht('Invalid'),
                    pht('Close date must be after or equal to Open date.'),
                    null);
            }
        }

        $checkOpenDateCloseDate = $open_date === null && $close_date === null;

        if (!isset($object->checkRedirectTaskOrProject)) {
            $data = $this->getData($object->getPHID());
            if (!$checkOpenDateCloseDate) {
                if ($data['minDate']) {
                    if ($open_date > (int)$data['minDate']) {
                        $errors[] = new PhabricatorApplicationTransactionValidationError(
                            $type_openDate,
                            pht('Invalid'),
                            pht('The project opening date must less than the task end date.'),
                            null);
                    }

                    if (!isset($data['maxDate'])) {
                        if ($close_date < (int)($data['minDate']) && isset($close_date)) {
                            $errors[] = new PhabricatorApplicationTransactionValidationError(
                                $type_closeDate,
                                pht('Invalid'),
                                pht('The project closing date must be greater than the task creation date.'),
                                null);
                        }
                    }
                }
                if (isset($data['maxDate'])) {
                    if ($close_date < (int)($data['maxDate']) && isset($close_date)) {
                        $errors[] = new PhabricatorApplicationTransactionValidationError(
                            $type_closeDate,
                            pht('Invalid'),
                            pht('The project closing date must be greater than the task creation date.'),
                            null);
                    }

                    $checkMinDateMaxDate = $data['minDate'] === false && $data['maxDate'] === false;

                    if (!$data['minDate'] && !$checkMinDateMaxDate) {
                        if ($open_date > (int)($data['maxDate'])) {
                            $errors[] = new PhabricatorApplicationTransactionValidationError(
                                $type_closeDate,
                                pht('Invalid'),
                                pht('The project open date must be less than the task end date.'),
                                null);
                        }
                    }
                }
            }
        }

        return $errors;
    }

    private function getData($project_id)
    {
        $allStartDate = array();
        $allEndDate = array();
        $i = 0;

        foreach (id(new PhabricatorTask())->loadAllWhere('projectPHID = %s', $project_id) as $task) {

            $allEndDate[$i++] = $task->getEndDate();

            if (!is_null($task->getStartDate())) {
                $allStartDate[$i++] = $task->getStartDate();
            }
        }

        return ['minDate' => min($allStartDate), 'maxDate' => max($allEndDate)];
    }

    /**
     * @param $open_date
     * @param $min_date
     * @return bool
     */
    protected function isHasOpenDateError($open_date, $min_date)
    {
        return $open_date > (int)$min_date && $min_date !== false;
    }

    /**
     * @param $close_date
     * @param $max_date
     * @return bool
     */
    protected function isHasCloseDateError($close_date, $max_date)
    {
        return $close_date < (int)($max_date) && isset($max_date);
    }
}
