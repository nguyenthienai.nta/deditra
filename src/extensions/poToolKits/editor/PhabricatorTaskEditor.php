<?php

final class PhabricatorTaskEditor
    extends PhabricatorApplicationTransactionEditor
{

    protected function validateAllTransactions(
        PhabricatorLiskDAO $object,
        array $xactions)
    {

        $errors = parent::validateAllTransactions($object, $xactions);
        $parent_xaction = null;
        $type_starDate = PhabricatorProjectsStartDateGrantChartTransaction::TRANSACTIONTYPE;
        $type_endDate = PhabricatorProjectsEndDateGrantChartTransaction::TRANSACTIONTYPE;
        $type_process = PhabricatorProjectsProgressGrantChartTransaction::TRANSACTIONTYPE;
        $start_date = $object->getStartDate();
        $end_date = $object->getEndDate();
        $progress = $object->getProgress();
        $project_phid = $object->project_phid;
        $parent_task = $object->parent_task;

        $project = id(new PhabricatorProjects())->loadOneWhere('phid = %s', $project_phid);


        foreach ($xactions as $xaction) {
            switch ($xaction->getTransactionType()) {
                case $type_starDate:
                    $start_date = $xaction->getNewValue();
                    break;
                case $type_endDate:
                    $end_date = $xaction->getNewValue();
                    break;
                case $type_process:
                    $progress = $xaction->getNewValue();
                    break;

            }
        }


        if ($parent_task !== PhabricatorPoToolsEditTaskController::PARENT_TASK) {
            if ($start_date !== null && $end_date !== null) {
                if ($start_date > $end_date) {
                    $errors[] = new PhabricatorApplicationTransactionValidationError(
                        $type_endDate,
                        pht('Invalid'),
                        pht('End date must be greater or equal to start date.'),
                        null);
                }
            }

            if (!is_null($project->getOpenDate()) && !is_null($project->getCloseDate())) {
                if ($end_date !== null) {
                    if ($end_date > $project->getCloseDate() || $end_date < $project->getOpenDate()) {
                        $errors[] = new PhabricatorApplicationTransactionValidationError(
                            $type_endDate,
                            pht('Invalid'),
                            pht('End date must be less than closed date and open date'),
                            null);
                    }
                }

                if ($start_date !== null) {
                    if ($start_date < $project->getOpenDate() || $start_date > $project->getCloseDate()) {
                        $errors[] = new PhabricatorApplicationTransactionValidationError(
                            $type_endDate,
                            pht('Invalid'),
                            pht('Start date must be less than open date and close date'),
                            null);
                    }
                }
            }

            if (!is_null($project->getOpenDate()) && is_null($project->getCloseDate())) {
                if ($start_date < $project->getOpenDate() && $start_date !== null) {
                    $errors[] = new PhabricatorApplicationTransactionValidationError(
                        $type_endDate,
                        pht('Invalid'),
                        pht('Start date must be less than open date and close date'),
                        null);
                }
            }

            if (is_null($project->getOpenDate()) && !is_null($project->getCloseDate())) {
                if ($end_date > $project->getCloseDate() && $end_date !== null) {
                    $errors[] = new PhabricatorApplicationTransactionValidationError(
                        $type_endDate,
                        pht('Invalid'),
                        pht('End date must be less than closed date and open date'),
                        null);
                }
            }
        }

        if ($progress < 0 || $progress > 100) {
            $errors[] = new PhabricatorApplicationTransactionValidationError(
                $type_endDate,
                pht('Invalid'),
                pht('Progress not greater than 100 and not less than 0.'),
                null);
        }

        return $errors;

    }

    public function getEditorApplicationClass()
    {
        // TODO: Implement getEditorApplicationClass() method.
    }

    public function getEditorObjectsDescription()
    {
        // TODO: Implement getEditorObjectsDescription() method.
    }
}

