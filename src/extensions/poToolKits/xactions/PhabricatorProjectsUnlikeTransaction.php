<?php

final class PhabricatorProjectsUnlikeTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects:unlike';

    public function generateOldValue($object)
    {
        return $object->getUnlike();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setUnlike($value);
    }

    public function getTitle()
    {
        return pht(
            " %s updated this project's Unlike field",
            $this->renderAuthor(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s Unlike project %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

}
