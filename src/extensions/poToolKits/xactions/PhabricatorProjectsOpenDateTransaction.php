<?php

final class PhabricatorProjectsOpenDateTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects:openDate';

    public function generateOldValue($object)
    {
        return $object->getOpenDate();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setOpenDate($value);
    }

    public function getTitle()
    {
        $old = $this->getOldValue();
        if (!strlen($old)) {
            return pht(
                "%s created this project's open date field",
                $this->renderAuthor());
        } else {
            return pht(
                " %s updated this project's open date field",
                $this->renderAuthor(),
                $this->renderOldValue(),
                $this->renderNewValue());
        }
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s Open date project %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }
}
