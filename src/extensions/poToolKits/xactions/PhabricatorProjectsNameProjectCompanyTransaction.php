<?php

final class PhabricatorProjectsNameProjectCompanyTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects:nameProjectCompany';

    public function generateOldValue($object)
    {
        return $object->getNameProjectCompany();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setNameProjectCompany($value);
    }

    public function getTitle()
    {
        $old = $this->getOldValue();
        if (!strlen($old)) {
            return pht(
                '%s created this name project company.',
                $this->renderAuthor());
        } else {
            return pht(
                '%s renamed this name project company from %s to %s.',
                $this->renderAuthor(),
                $this->renderOldValue(),
                $this->renderNewValue());
        }
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s renamed name project company %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

}
