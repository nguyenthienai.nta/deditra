<?php
final class PhabricatorProjectsDefaultTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'project:default';

    public function generateOldValue($object)
    {
        return $object->getIsDefaultProjects();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setIsDefaultProjects($value);
    }

    public function getTitle()
    {
        return pht(
            '%s made this the default space.',
            $this->renderAuthor());
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s made space %s the default space.',
            $this->renderAuthor(),
            $this->renderObject());

    }

    public function validateTransactions($object, array $xactions)
    {
        $errors = array();

        if (!$this->isNewObject()) {
            foreach ($xactions as $xaction) {
                $errors[] = $this->newInvalidError(
                    pht('Only the first project created can be the default project, and ' .
                        'it must remain the default project evermore.'));
            }
        }

        return $errors;
    }

}
