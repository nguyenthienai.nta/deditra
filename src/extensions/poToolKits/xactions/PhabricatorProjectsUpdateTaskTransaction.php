<?php

final class PhabricatorProjectsUpdateTaskTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects:updateTask';

    public function generateOldValue($object)
    {

        return $object->getUpdateTask();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setUpdateTask($value);
    }

    public function getTitle()
    {
        $old = $this->getOldValue();
        if (!strlen($old)) {
            return pht(
                "%s created this project's update task",
                $this->renderAuthor());
        } else {
            return pht(
                " %s updated this project's update task",
                $this->renderAuthor(),
                $this->renderOldValue(),
                $this->renderNewValue());
        }
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s Update task project %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

}
