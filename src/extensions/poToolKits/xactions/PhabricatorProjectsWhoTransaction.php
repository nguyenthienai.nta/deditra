<?php

final class PhabricatorProjectsWhoTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects:who';

    public function generateOldValue($object)
    {
        return $object->getWho();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setWho($value);
    }

    public function getTitle()
    {
        return pht(
            " %s updated this project's Who field",
            $this->renderAuthor(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s who project %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

}
