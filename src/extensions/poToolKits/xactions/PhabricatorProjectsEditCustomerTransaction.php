<?php

final class PhabricatorProjectsEditCustomerTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects:customer';

    public function generateOldValue($object)
    {
        return $object->getCustomer();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setCustomer($value);
    }

    public function getTitle()
    {
        return pht(
            " %s updated this project's For field",
            $this->renderAuthor(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s Customer project %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

}
