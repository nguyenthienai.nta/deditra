<?php

final class PhabricatorProjectsIconTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects:icon';

    public function generateOldValue($object)
    {
        return $object->getIcon();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setIcon($value);
    }

    public function applyExternalEffects($object, $value)
    {
        $old = $this->getOldValue();
        $new = $value;
        $all = array();

        if ($old) {
            $all[] = $old;
        }

        if ($new) {
            $all[] = $new;
        }

        $files = id(new PhabricatorFileQuery())
            ->setViewer($this->getActor())
            ->withPHIDs($all)
            ->execute();
        $files = mpull($files, null, 'getPHID');
        $old_file = idx($files, $old);

        if ($old_file) {
            $old_file->detachFromObject($object->getPHID());
        }

        $new_file = idx($files, $new);
        if ($new_file) {
            $new_file->attachToObject($object->getPHID());
        }
    }

    public function getTitle()
    {
        $old = $this->getOldValue();
        if (!strlen($old)) {
            return pht(
                '%s updated this icon.',
                $this->renderAuthor());
        } else {
            return pht(
                '%s updated this icon. ',
                $this->renderAuthor(),
                $this->renderOldValue(),
                $this->renderNewValue());
        }
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s renamed icon %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

    public function getIcon()
    {
        return 'fa-photo';
    }

    public function extractFilePHIDs($object, $value)
    {
        if ($value) {
            return array($value);
        }
        return array();
    }

    public function validateTransactions($object, array $xactions)
    {
        $errors = array();
        $type = ['jpg', 'png', 'gif', 'jpeg', 'x-ico', 'x-icon', 'vnd.microsoft.icon'];
        $file = $_FILES['icon']['name'];
        $fileType = explode('.', $file);

        if ($file && !in_array(end($fileType), $type)) {
            $errors[] = $this->newInvalidError(
                pht('File mime type of "%s" is not a valid viewable image.', end($fileType)));
        }

        return $errors;
    }

}
