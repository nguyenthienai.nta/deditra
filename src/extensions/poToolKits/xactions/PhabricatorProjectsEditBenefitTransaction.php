<?php

final class PhabricatorProjectsEditBenefitTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects:benefit';

    public function generateOldValue($object)
    {
        return $object->getBenefit();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setBenefit($value);
    }

    public function getTitle()
    {
        return pht(
            " %s updated this project's That field",
            $this->renderAuthor(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s benefit project %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

}
