<?php

final class PhabricatorProjectsIsArchivedTransaction extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects::isArchived';

    public function generateOldValue($object)
    {
        return $object->getIsArchived();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setIsArchived($value);
    }

    public function getTitle()
    {
        $old = $this->getOldValue();

        if ($old == 0) {
            return pht(
                '%s archived this po tool.',
                $this->renderAuthor());
        } else {
            return pht(
                '%s activated this po tool.',
                $this->renderAuthor());
        }
    }

    public function getTitleForFeed()
    {
        $old = $this->getOldValue();

        if ($old == 0) {
            return pht(
                '%s archived %s.',
                $this->renderAuthor(),
                $this->renderObject());
        } else {
            return pht(
                '%s activated %s.',
                $this->renderAuthor(),
                $this->renderObject());
        }
    }

    public function getColor()
    {
        $old = $this->getOldValue();

        if ($old == 0) {
            return 'red';
        } else {
            return 'green';
        }
    }

    public function getIcon()
    {
        $old = $this->getOldValue();

        if ($old == 0) {
            return 'fa-ban';
        } else {
            return 'fa-check';
        }
    }

}
