<?php

final class PhabricatorProjectsIsDeletedGrantChartTransaction
    extends PhabricatorTaskTransactionType
{

    const TRANSACTIONTYPE = 'projects:isDeleted';

    public function generateOldValue($object)
    {
        return $object->getIsDeleted();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setIsDeleted($value);
    }
}