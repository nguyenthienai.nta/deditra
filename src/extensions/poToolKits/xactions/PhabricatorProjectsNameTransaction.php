<?php

final class PhabricatorProjectsNameTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects:name';

    public function generateOldValue($object)
    {
        return $object->getName();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setName($value);
    }

    public function getTitle()
    {
        $old = $this->getOldValue();
        if (!strlen($old)) {
            return pht(
                '%s created this project.',
                $this->renderAuthor());
        } else {
            return pht(
                '%s renamed this project from %s to %s.',
                $this->renderAuthor(),
                $this->renderOldValue(),
                $this->renderNewValue());
        }
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s renamed project %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

    public function validateTransactions($object, array $xactions)
    {
        $errors = array();

        if ($this->isEmptyTextTransaction($object->getName(), $xactions)) {
            $errors[] = $this->newRequiredError(
                pht('Projects must have a name.'));
        }

        $max_length = $object->getColumnMaximumByteLength('name');
        $projects = id(new PhabricatorProjects())->loadAll();
        $projects_names = [];

        foreach ($projects as $project) {
            $projects_names[] = $project->getName();
            $projects_names[] = str_replace(' ', '', $project->getName());
        }

        foreach ($xactions as $xaction) {
            $new_value = $xaction->getNewValue();
            $new_length = strlen($new_value);
            if ($new_length > $max_length) {
                $errors[] = $this->newInvalidError(
                    pht('The name can be no longer than %s characters.',
                        new PhutilNumber($max_length)));
            }

            if ($object->getName() == null) {
                if (in_array($new_value, $projects_names) || in_array(str_replace(' ', '', $new_value), $projects_names)) {
                    $errors[] = $this->newInvalidError(
                        pht('This name is already taken.'));
                }
            } else {
                if ($new_value != $object->getName() && str_replace(' ', '', $new_value) != str_replace(' ', '', $object->getName())) {
                    if (in_array($new_value, $projects_names) || in_array(str_replace(' ', '', $new_value), $projects_names)) {
                        $errors[] = $this->newInvalidError(
                            pht('This name is already taken.'));
                    }
                }
            }
        }

        return $errors;
    }

}
