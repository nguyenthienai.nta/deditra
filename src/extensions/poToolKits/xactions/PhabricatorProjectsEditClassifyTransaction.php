<?php

final class PhabricatorProjectsEditClassifyTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects:classify';

    public function generateOldValue($object)
    {
        return $object->getClassify();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setClassify($value);
    }

    public function getTitle()
    {
        return pht(
            " %s updated this project's Is a field",
            $this->renderAuthor(),
            $this->renderOldValue(),
            $this->renderNewValue());

    }

    public function getTitleForFeed()
    {
        return pht(
            '%s Classify project %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

}
