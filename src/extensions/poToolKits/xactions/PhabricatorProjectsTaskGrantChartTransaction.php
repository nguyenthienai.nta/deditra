<?php

final class PhabricatorProjectsTaskGrantChartTransaction
    extends PhabricatorTaskTransactionType
{

    const TRANSACTIONTYPE = 'projects:nameTask';

    public function generateOldValue($object)
    {
        return $object->getNameTask();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setNameTask($value);
    }

    public function validateTransactions($object, array $xactions)
    {
        $errors = array();

        $check_show_error = 0;
        if ($this->isEmptyTextTransaction($object->getNameTask(), $xactions)) {
            $check_show_error = 1;
            $errors[] = $this->newRequiredError(
                pht('Task must have a name.'));
        }

        if ($check_show_error === 0){
            foreach ($xactions as $xaction) {
                $new_value = trim($xaction->getNewValue());
                $new_length = strlen($new_value);
                if ($new_length == 0) {
                    $errors[] = $this->newRequiredError(
                        pht('Task must have a name.'));
                }
            }
        }

        return $errors;

    }
}