<?php

final class PhabricatorProjectsProductNameTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects:productName';

    public function generateOldValue($object)
    {
        return $object->getProductName();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setProductName($value);
    }

    public function getTitle()
    {
        return pht(
            " %s updated this project's The field",
            $this->renderAuthor(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s ProductNamed project %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

}
