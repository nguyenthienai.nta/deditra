<?php

final class PhabricatorProjectsParentTaskGrantChartTransaction
    extends PhabricatorTaskTransactionType
{

    const TRANSACTIONTYPE = 'projects:parentTaskPHID';

    public function generateOldValue($object)
    {
        return $object->getParentTaskPHID();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setParentTaskPHID($value);
    }
}