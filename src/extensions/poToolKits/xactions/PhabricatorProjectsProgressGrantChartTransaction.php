<?php

final class PhabricatorProjectsProgressGrantChartTransaction
    extends PhabricatorTaskTransactionType
{

    const TRANSACTIONTYPE = 'projects:progress';

    public function generateOldValue($object)
    {
        return $object->getProgress();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setProgress($value);
    }
}
