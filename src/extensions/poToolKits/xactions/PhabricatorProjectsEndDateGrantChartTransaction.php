<?php

final class PhabricatorProjectsEndDateGrantChartTransaction
    extends PhabricatorTaskTransactionType
{

    const TRANSACTIONTYPE = 'projects:endDate';

    public function generateOldValue($object)
    {
        return $object->getEndDate();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setEndDate($value);
    }
}