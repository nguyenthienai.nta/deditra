<?php
abstract class PhabricatorProjectsTransactionType
    extends PhabricatorModularTransactionType {}
    
final class phabricatorProjectsCloseDateTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects:closeDate';

    public function generateOldValue($object)
    {
        return $object->getCloseDate();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setCloseDate($value);
    }

    public function getTitle()
    {
        $old = $this->getOldValue();
        if (!strlen($old)) {
            return pht(
                '%s created this project\'s close date field',
                $this->renderAuthor());
        } else {
            return pht(
                " %s updated this project's close date field",
                $this->renderAuthor(),
                $this->renderOldValue(),
                $this->renderNewValue());
        }
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s Close date project %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

}
