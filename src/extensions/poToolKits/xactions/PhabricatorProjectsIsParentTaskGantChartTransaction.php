<?php

final class PhabricatorProjectsIsParentTaskGantChartTransaction
    extends PhabricatorTaskTransactionType
{

    const TRANSACTIONTYPE = 'projects:isParentTask';

    public function generateOldValue($object)
    {
        return $object->getIsParentTask();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setIsParentTask($value);
    }
}