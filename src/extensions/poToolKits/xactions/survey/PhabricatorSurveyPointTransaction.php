<?php

final class PhabricatorSurveyPointTransaction
    extends PhabricatorSurveyTransactionType
{

    const TRANSACTIONTYPE = 'survey:point';

    public function generateOldValue($object)
    {
        return $object->getPoint();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setPoint($value);
    }

    public function getTitle()
    {
        $old = $this->getOldValue();
        if (!strlen($old)) {
            return pht(
                '%s created this survey.',
                $this->renderAuthor());
        } else {
            return pht(
                '%s renamed this survey from %s to %s.',
                $this->renderAuthor(),
                $this->renderOldValue(),
                $this->renderNewValue());
        }
    }


    public function validateTransactions($object, array $xactions)
    {
        $errors = array();
        return $errors;
    }

}
