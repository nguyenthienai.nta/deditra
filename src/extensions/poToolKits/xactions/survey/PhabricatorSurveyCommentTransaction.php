<?php

final class PhabricatorSurveyCommentTransaction
    extends PhabricatorSurveyTransactionType
{

    const TRANSACTIONTYPE = 'survey:comment';

    public function generateOldValue($object)
    {
        return $object->getcomment();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setComment($value);
    }

    public function getTitle()
    {
        $old = $this->getOldValue();
        if (!strlen($old)) {
            return pht(
                '%s created this survey.',
                $this->renderAuthor());
        } else {
            return pht(
                '%s renamed this survey from %s to %s.',
                $this->renderAuthor(),
                $this->renderOldValue(),
                $this->renderNewValue());
        }
    }


    public function validateTransactions($object, array $xactions)
    {
        $errors = array();
        return $errors;
    }

}
