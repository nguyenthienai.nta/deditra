<?php

final class PhabricatorSurveyUrlTransaction
    extends PhabricatorSurveyTransactionType
{

    const TRANSACTIONTYPE = 'survey:url';

    public function generateOldValue($object)
    {
        return $object->getUrl();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setUrl($value);
    }

    public function getTitle()
    {
        $old = $this->getOldValue();
        if (!strlen($old)) {
            return pht(
                '%s created this survey.',
                $this->renderAuthor());
        } else {
            return pht(
                '%s renamed this survey from %s to %s.',
                $this->renderAuthor(),
                $this->renderOldValue(),
                $this->renderNewValue());
        }
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s renamed project %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

    public function validateTransactions($object, array $xactions)
    {
        $errors = array();

        if ($this->isEmptyTextTransaction($object->getUrl(), $xactions)) {
            $errors[] = $this->newRequiredError(
                pht('Survey must have a url.'));
        }
        return $errors;
    }

}
