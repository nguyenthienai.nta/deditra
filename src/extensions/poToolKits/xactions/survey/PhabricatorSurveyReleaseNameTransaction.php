<?php

final class PhabricatorSurveyReleaseNameTransaction
    extends PhabricatorSurveyTransactionType
{

    const TRANSACTIONTYPE = 'survey:releaseName';

    public function generateOldValue($object)
    {
        return $object->getReleaseName();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setReleaseName($value);
    }

    public function getTitle()
    {
        $old = $this->getOldValue();
        if (!strlen($old)) {
            return pht(
                '%s created this survey.',
                $this->renderAuthor()
            );
        } else {
            return pht(
                '%s renamed this survey from %s to %s.',
                $this->renderAuthor(),
                $this->renderOldValue(),
                $this->renderNewValue()
            );
        }
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s renamed project %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue()
        );
    }

    public function validateTransactions($object, array $xactions)
    {
        $errors = array();

        if ($this->isEmptyTextTransaction($object->getReleaseName(), $xactions)) {
            $errors[] = $this->newRequiredError(
                pht('Survey must have a release name.')
            );
        }
        $max_length = 255;
        foreach ($xactions as $xaction) {
            $new_value = $xaction->getNewValue();
            $new_length = strlen($new_value);
            if ($new_length > $max_length) {
                $errors[] = $this->newInvalidError(
                    pht(
                        'The release name can be no longer than %s characters.',
                        $max_length
                    )
                );
            }
        }
        $surveys = id(new PhabricatorSurvey())->loadAllWhere('projectPHID = %s', $object->getProjectPHID());
        $surveysUrl = [];
        foreach ($surveys as $surveySub) {
            $surveysUrl[] = $surveySub->getReleaseName();
            $surveysUrl[] = str_replace(' ', '', $surveySub->getReleaseName());
        }

        foreach ($xactions as $xaction) {
            if ($object->getReleaseName() == null) {
                if (in_array($new_value, $surveysUrl) || in_array(str_replace(' ', '', $new_value), $surveysUrl)) {
                    $errors[] = $this->newInvalidError(
                        pht('This release name is already taken.')
                    );
                }
            } else {
                if ($new_value != $object->getReleaseName() && str_replace(' ', '', $new_value) != str_replace(' ', '', $object->getReleaseName())) {
                    if (in_array($new_value, $surveysUrl) || in_array(str_replace(' ', '', $new_value), $surveysUrl)) {
                        $errors[] = $this->newInvalidError(
                            pht('This release name is already taken.')
                        );
                    }
                }
            }
        }

        return $errors;
    }
}
