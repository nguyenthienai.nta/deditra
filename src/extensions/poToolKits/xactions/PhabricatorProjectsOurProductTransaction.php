<?php

final class PhabricatorProjectsOurProductTransaction
    extends PhabricatorProjectsTransactionType
{

    const TRANSACTIONTYPE = 'projects:ourProduct';

    public function generateOldValue($object)
    {
        return $object->getOurProduct();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setOurProduct($value);
    }

    public function getTitle()
    {
        return pht(
            " %s updated this project's Our Product field",
            $this->renderAuthor(),
            $this->renderOldValue(),
            $this->renderNewValue());

    }

    public function getTitleForFeed()
    {
        return pht(
            '%s OurProduct project %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

}
