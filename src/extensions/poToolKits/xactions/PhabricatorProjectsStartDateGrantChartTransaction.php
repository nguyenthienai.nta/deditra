<?php

final class PhabricatorProjectsStartDateGrantChartTransaction
    extends PhabricatorTaskTransactionType
{

    const TRANSACTIONTYPE = 'projects:startDate';

    public function generateOldValue($object)
    {
        return $object->getStartDate();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setStartDate($value);
    }
}