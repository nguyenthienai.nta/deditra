<?php

final class PhabricatorProjectsProjectPhidGrantChartTransaction
    extends PhabricatorTaskTransactionType
{

    const TRANSACTIONTYPE = 'projects:projectPHID';

    public function generateOldValue($object)
    {
        return $object->getProjectPHID();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setProjectPHID($value);
    }
}