<?php

class PhabricatorPoToolKitsApplication extends PhabricatorApplication
{
    public function getBaseURI()
    {
        return '/po-tools/';
    }

    public function getName()
    {
        return pht('PO Toolkits');
    }

    public function getShortDescription()
    {
        return pht('PO Tools');
    }

    public function getIcon()
    {
        return 'fa-briefcase';
    }

    public function getTitleGlyph()
    {
        return "\xE2\x97\x8B";
    }

    public function getFlavorText()
    {
        return pht('Control access to groups of objects.');
    }

    public function getApplicationGroup()
    {
        return self::GROUP_UTILITIES;
    }

    public function canUninstall()
    {
        return false;
    }

    public function getRemarkupRules()
    {
        return array(
            new PhabricatorOkrsRemarkupRule(),
        );
    }

    public function getRoutes()
    {
        return array(
            '/po-tools/' => array(
                '(?:query/(?P<queryKey>[^/]+)/)?' => 'PhabricatorPoToolsListProjectController',
                'create/' => 'PhabricatorPoToolsEditProjectController',
                'edit/(?:(?P<id>\d+)/)?' => 'PhabricatorPoToolsEditProjectController',
                'archive/(?P<id>[1-9]\d*)/' => 'PhabricatorPoToolsArchiveprojectController',
                'manage/(?P<id>[1-9]\d*)/' => 'PhabricatorPoToolsManageController',
                'product-vision/(?:(?P<id>\d+)/)' => 'PhabricatorPoToolsViewProductVisionController',
                'product-vision/edit/(?:(?P<id>\d+)/)' => 'PhabricatorPoToolsEditProductVisionController',
                'release-survey/(?:(?P<id>\d+)/)' => 'PhabricatorPoToolsViewReleaseSurveyController',
                'release-survey/(?P<id>[1-9]\d*)/add' => 'PhabricatorPoToolsEditReleaseSurveyController',
                'release-survey/(?P<id>[1-9]\d*)/edit/(?:(?P<surveyId>\d+)/)?' => 'PhabricatorPoToolsEditReleaseSurveyController',
                'release-survey/delete-survey/(?:(?P<surveyId>\d+)/)?' => 'PhabriactorPoToolsDestroySurveyController',
                'gant-chart/(?:(?P<id>\d+)/)' => 'PhabricatorPoToolsTaskController',
                'gant-chart/(?P<poid>[1-9]\d*)/add-task/(?P<task>[1-9]\d*)/' => 'PhabricatorPoToolsEditTaskController',
                'gant-chart/(?P<poid>[1-9]\d*)/edit-task/(?P<id>[1-9]\d*)/' => 'PhabricatorPoToolsEditTaskController',
                'gant-chart/delete-task/(?P<id>[1-9]\d*)/' => 'PhabriactorPoToolsDestroyTaskController',
            )
        );
    }

    public function isPinnedByDefault(PhabricatorUser $viewer)
    {
        return true;
    }

    protected function getCustomCapabilities()
    {
        return array(
            PhabricatorPoToolkitCapabilityCreateSpaces::CAPABILITY => array(
                'default' => PhabricatorPolicies::POLICY_USER,
            ),
            PhabricatorPoToolkitCapabilityDefaultView::CAPABILITY => array(
                'caption' => pht('Default view policy for newly created po toolkit.'),
                'default' => PhabricatorPolicies::POLICY_USER,
                'template' => PhabricatorPoToolkitProjectsPHIDType::TYPECONST,
                'capability' => PhabricatorPolicyCapability::CAN_VIEW,
            ),
            PhabricatorPoToolkitCapabilityDefaultEdit::CAPABILITY => array(
                'caption' => pht('Default edit policy for newly created po toolkit.'),
                'default' => PhabricatorPolicies::POLICY_USER,
                'template' => PhabricatorPoToolkitProjectsPHIDType::TYPECONST,
                'capability' => PhabricatorPolicyCapability::CAN_EDIT,
            ),
        );
    }
}
