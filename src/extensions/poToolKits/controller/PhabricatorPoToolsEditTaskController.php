<?php

final class PhabricatorPoToolsEditTaskController
    extends PhabricatorController
{
    const PARENT_TASK = "2";
    const CHILD_TASK = "1";
    const IS_CHILD_TASK = 0;
    const IS_PARENT_TASK = 1;

    public function shouldAllowPublic()
    {
        return true;
    }

    private $error;

    public function handleRequest(AphrontRequest $request)
    {
        require_celerity_resource('gantChart-css');
        require_celerity_resource('jquery-ui');
        Javelin::initBehavior(
            'jquery'
        );
        Javelin::initBehavior(
            'jquery-ui'
        );
        Javelin::initBehavior(
            'po'
        );

        $viewer = $request->getUser();
        $id = $request->getURIData('id');
        $po_id = $request->getURIData('poid');
        $project = id(new PhabricatorProjectsQuery())
            ->setViewer($viewer)
            ->withIDs(array($po_id))
            ->executeOne();

        $project->checkRedirectTaskOrProject = 1;

        if (!$project) {
            return new Aphront404Response();
        }
        $project_phid = $project->getPHID();
        $cancel_uri = ("/po-tools/gant-chart/{$po_id}/");
        $check_show_delete = !$id ? 'delete-task' : '';

        if ($id) {
            $task = id(new PhabricatorTaskQuery())
                ->setViewer($viewer)
                ->withIDs(array($id))
                ->executeOne();
            if (!$task) {
                return new Aphront404Response();
            }

            $check_parent_task = $task->getParentTaskPHID() === null ? self::PARENT_TASK : self::CHILD_TASK;
            $is_new = false;
            $title = pht("Edit Task");
        } else {
            $title = pht("Create Task");
            $task_id = head($request->getArr('task'));
            $check_parent_task = $request->getURIData('task');

            if (!$task_id) {
                $task_id = $request->getStr('task');
            }

            $query = id(new PhabricatorTaskQuery())
                ->setViewer($viewer);

            $task = $query->execute();


            $task = PhabricatorTask::initializeNewGantChart($viewer);
            $is_new = true;
        }
        $task->project_phid = $project_phid;
        $task->parent_task = $check_parent_task;
        $e_nameTask = true;
        $validation_exception = null;
        $is_parent_task = $check_parent_task === self::PARENT_TASK ? self::IS_PARENT_TASK : self::IS_CHILD_TASK;
        $nameTask = $task->getNameTask();
        $startDate = $task->getStartDate();
        $endDate = $task->getEndDate();
        $progress = $task->getProgress();
        $parent_task_phid = $task->getParentTaskPHID();

        if ($request->isFormPost()) {
            $xactions = array();
            $xactions_project = array();
            $parent_task_phid = $request->getStr('parentTask');
            $nameTask = $request->getStr('nameTask');
            $startDate = $this->formatDate($request->getStr('startDate'));
            $endDate = $this->formatDate($request->getStr('endDate'));
            $progress = $this->getProgress($request);

            $e_nameTask = null;
            $type_task = PhabricatorProjectsTaskGrantChartTransaction::TRANSACTIONTYPE;
            $type_project_phid = PhabricatorProjectsProjectPhidGrantChartTransaction::TRANSACTIONTYPE;
            $type_starDate = PhabricatorProjectsStartDateGrantChartTransaction::TRANSACTIONTYPE;
            $type_endDate = PhabricatorProjectsEndDateGrantChartTransaction::TRANSACTIONTYPE;
            $type_process = PhabricatorProjectsProgressGrantChartTransaction::TRANSACTIONTYPE;
            $type_is_deleted = PhabricatorProjectsIsDeletedGrantChartTransaction::TRANSACTIONTYPE;
            $type_parent_task = PhabricatorProjectsParentTaskGrantChartTransaction::TRANSACTIONTYPE;
            $type_is_parent_task = PhabricatorProjectsIsParentTaskGantChartTransaction::TRANSACTIONTYPE;
            $type_update_task = PhabricatorProjectsUpdateTaskTransaction::TRANSACTIONTYPE;


            $xactions[] = id(new PhabricatorTaskTransaction())
                ->setTransactionType($type_task)
                ->setNewValue($nameTask);

            $xactions[] = id(new PhabricatorTaskTransaction())
                ->setTransactionType($type_project_phid)
                ->setNewValue($project->getPHID());

            $xactions[] = id(new PhabricatorTaskTransaction())
                ->setTransactionType($type_starDate)
                ->setNewValue($startDate);

            $xactions[] = id(new PhabricatorTaskTransaction())
                ->setTransactionType($type_endDate)
                ->setNewValue($endDate);

            $xactions[] = id(new PhabricatorTaskTransaction())
                ->setTransactionType($type_process)
                ->setNewValue($progress);

            $xactions[] = id(new PhabricatorTaskTransaction())
                ->setTransactionType($type_is_parent_task)
                ->setNewValue($is_parent_task);

            $xactions[] = id(new PhabricatorTaskTransaction())
                ->setTransactionType($type_parent_task)
                ->setNewValue($request->getStr('parentTask'));

            $xactions[] = id(new PhabricatorTaskTransaction())
                ->setTransactionType($type_is_deleted)
                ->setNewValue(0);

            $xactions_project[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_update_task)
                ->setNewValue($this->formatDate(date("Y/m/d")));

            $editor = id(new PhabricatorTaskEditor())
                ->setActor($viewer)
                ->setContinueOnNoEffect(true)
                ->setContentSourceFromRequest($request);

            $editor_project = id(new PhabricatorProjectsEditor())
                ->setActor($viewer)
                ->setContentSourceFromRequest($request)
                ->setContinueOnNoEffect(true);


            try {
                session_start();
                $editor->applyTransactions($task, $xactions);
                $editor_project->applyTransactions($project, $xactions_project);

                $is_new ? $_SESSION['message'] = 'Task added successfully'
                    : $_SESSION['message'] = 'Task edit successfully';

                return id(new AphrontRedirectResponse())->setURI("/po-tools/gant-chart/{$po_id}/");
            } catch (PhabricatorApplicationTransactionValidationException $ex) {
                $validation_exception = $ex;
                $e_nameTask = $ex->getShortMessage($type_task);
            }

        }

        $button = pht('Save');
        $form = id(new AphrontFormView())
            ->setUser($viewer)
            ->setEncType('multipart/form-data');

        if ($check_parent_task === self::CHILD_TASK) {
            $form->appendChild(
                id(new AphrontFormSelectControl())
                    ->setLabel(pht('Parent Task'))
                    ->setID('parentTask')
                    ->setOptions($this->getParentTask($project_phid))
                    ->setName('parentTask')
                    ->setValue($parent_task_phid)
            );

        }

        $form->appendChild(
            id(new AphrontFormTextControl())
                ->setLabel(pht('Task'))
                ->setName('nameTask')
                ->setError($this->error)
                ->setID('nameTask')
                ->setError($e_nameTask)
                ->setValue($nameTask)
        );

        if ($check_parent_task === self::CHILD_TASK) {
            $form->appendChild(
                id(new AphrontFormDatePOControl())
                    ->setLabel(pht('Start Date'))
                    ->setName('startDate')
                    ->setID('startDate')
                    ->setValue($startDate)

            );

            $form->appendChild(
                id(new AphrontFormDatePOControl())
                    ->setLabel(pht('End Date'))
                    ->setName('endDate')
                    ->setID('endDate')
                    ->setValue($endDate)
            );

            $form->appendChild(
                id(new AphrontFormNumberControl())
                    ->setLabel(pht('Progress'))
                    ->setID('progress-task')
                    ->setName('progress')
                    ->setValue($progress)
            );
        }

        $form->appendChild(
            id(new AphrontFormSubmitControl())
                ->addButton(id(new PHUIButtonView())
                    ->setTag('a')
                    ->setHref($this->getApplicationURI('/gant-chart/delete-task/' . $id . '/'))
                    ->setText('Delete')
                    ->setWorkflow(true)
                    ->setID($check_show_delete)
                    ->setColor(PHUIButtonView::RED))
                ->addButton(id(new PHUIButtonView())
                    ->setText('Save')
                    ->setID('save'))
                ->addCancelButton($cancel_uri)
        );

        $box = id(new PHUIObjectBoxView())
            ->setHeaderText($title)
            ->setBackground(PHUIObjectBoxView::WHITE_CONFIG)
            ->setValidationException($validation_exception)
            ->appendChild($form);

        session_start();
        if (isset($_SESSION['message'])) {
            $box->setFormSaved(true, $_SESSION['message']);
            unset($_SESSION['message']);
        }

        $crumbs = $this->buildApplicationCrumbs();
        if (!$is_new) {
            $crumbs->addTextCrumb(
                "Edit OKR");
        } else {
            $crumbs->addTextCrumb($title);
        }

        $crumbs->setBorder(true);

        $view = id(new PHUITwoColumnView())
            ->setFooter(array(
                $box,
            ));

        return $this->newPage()
            ->setTitle($title)
            ->setCrumbs($crumbs)
            ->appendChild($view);
    }

    public static function formatDate($date)
    {
        $date = strtotime(str_replace('/', '-', $date));
        if($date == false) {
            $date = null;
        }
        return $date;
    }

    public function getParentTask($project_phid)
    {
        $tasks = id(new PhabricatorTask())->loadAllWhere('projectPHID = %s', $project_phid);

        $arrTasks = array();
        $arrTasks[null] = null;

        foreach ($tasks as $task) {
            if ($task->getIsParentTask() === self::CHILD_TASK){
                if ($task->getParentTaskPHID() === NULL) {
                    $arrTasks[$task->getPHID()] = $task->getNameTask();
                }
            }
        }

        return $arrTasks;

    }

    public function getProgress($request) {
        $progress = $request->getStr('progress');
        if ($progress == "") {
            $progress = null;
        }

        return $progress;
    }


}
