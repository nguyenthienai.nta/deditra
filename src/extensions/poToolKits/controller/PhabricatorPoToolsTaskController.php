<?php

final class PhabricatorPoToolsTaskController
    extends PhabricatorProjectsBoardController
{

    const IS_PARENT_TASK = "0";
    const IS_NULL_PROGRESS_PARENT_TASK = 0;
    const NULL_PROGRESS_PARENT_TASK = 0;

    public function shouldAllowPublic()
    {
        return true;
    }

    public function handleRequest(AphrontRequest $request)
    {
        $viewer = $request->getUser();
        $response = $this->loadProject();

        if ($response) {
            return $response;
        }

        $project = $this->getProject();
        $phid_project = $project->getPHID();

        $tasks = $this->getGantChart($phid_project) ;

        $state = $this->getViewState();

        $layout_engine = $state->getLayoutEngine();
        $board_phid = $project->getPHID();
        $columns = $layout_engine->getColumns($board_phid);

        if (!$columns || !$project->getHasWorkboard()) {
            $has_normal_columns = false;

            foreach ($columns as $column) {
                if (!$column->getProxyPHID()) {
                    $has_normal_columns = true;
                    break;
                }
            }

            $can_edit = PhabricatorPolicyFilter::hasCapability(
                $viewer,
                $project,
                PhabricatorPolicyCapability::CAN_EDIT);

            if (!$has_normal_columns) {
                if (!$can_edit) {
                    $content = $this->buildNoAccessContent($tasks);
                } else {
                    $content = $this->buildInitializeContent($tasks, $project);
                }
            } else {
                if (!$can_edit) {
                    $content = $this->buildDisabledContent($tasks);
                } else {
                    $content = $this->buildEnableContent($tasks);
                }
            }

            if ($content instanceof AphrontResponse) {
                return $content;
            }

            $nav = $this->newNavigation($project, PhabricatorProjects::ITEM_CHART);
            $crumbs = $this->buildApplicationCrumbs();
            $crumbs->addTextCrumb(pht('Gant Chart'));

            return $this->newPage()
                ->setTitle(
                    array(
                        $project->getName(),
                        pht('Gant Chart'),
                    ))
                ->setNavigation($nav)
                ->setCrumbs($crumbs)
                ->appendChild($content);
        }
    }

    private function buildInitializeContent(array $tasks, PhabricatorProjects $project)
    {
        Javelin::initBehavior('jquery');
        Javelin::initBehavior('gant-chart');
        Javelin::initBehavior('plugin-gant-chart');
        Javelin::initBehavior('phabricator-tooltips');
        require_celerity_resource('gantChart-css');
        require_celerity_resource('viewGantChart-css');

        $project = $this->getProject();
        $id_project = $project->getID();
        session_start();
        $list = array();
        $text = "";

        if (isset($_SESSION['message'])) {
            $text = $_SESSION['message'];
            $list[] = id(new PHUIInfoView())
                ->setSeverity(PHUIInfoView::SEVERITY_NOTICE)
                ->appendChild($_SESSION['message']);
            unset($_SESSION['message']);
        }

        $lastUpdate = $this->getDateUpdateMaxInGantChart($tasks);
        $dateCreated = date('d/m/Y', $project->getDateCreated());
        $dateUpdated = $project->getUpdateTask() ? date('d/m/Y', $project->getUpdateTask()) : '';
        $checkLastUpdateNotNull = !empty($dateUpdated) ? $dateUpdated : '';
        $openDate = !empty($project->getOpenDate()) ? date('d/m/Y', $project->getOpenDate()) : '';
        $closeDate = !empty($project->getCloseDate()) ? date('d/m/Y', $project->getCloseDate()) : '';

        $feed_header = id(new PHUIHeaderView())
            ->setHeader(pht('Gant Chart'))
            ->setSubheader(pht("Created at : {$dateCreated}"))
        ;

        $key = 1;

        ${"row" . $key} = array();

        $tasks = $this->sortTasks($tasks);




        $custom_data_gant_chart = [];

        $addTask = array(
            phutil_tag(
                'div',
                array(
                    'class' => 'div-parent-add-task',
                ),
                [
                    phutil_tag(
                        'div',
                        array(
                            'class' => 'icon-add-task',
                        ),
                        id(new PHUIButtonOKRView())
                            ->setTag('a')
                            ->setID('add-task')
                            ->setIcon('fa-plus-circle')
                    ),
                    phutil_tag(
                        'div',
                        array(
                            'class' => 'icon-add-task-child',
                        ),
                        id(new PHUIButtonOKRView())
                            ->setTag('a')
                            ->setID('add-task-child')
                            ->setName("task_child")
                            ->setText("Task")
                            ->setHref("/po-tools/gant-chart/{$id_project}/add-task/1/")
                    ),
                    phutil_tag(
                        'span',
                        array(
                            'class' => 'span-center',
                        ),
                        "|"
                    ),
                    phutil_tag(
                        'div',
                        array(
                            'class' => 'icon-add-task-parent',
                        ),
                        id(new PHUIButtonOKRView())
                            ->setTag('a')
                            ->setID('add-task-parent')
                            ->setName("parent_task")
                            ->setText("Parent Task")
                            ->setHref("/po-tools/gant-chart/{$id_project}/add-task/2/")
                    ),
                ]
            )
        );

        if (count($tasks) > 0) {

            foreach ($tasks as $taskID => $task) {

                $custom_data = array();

                $nameTask = $task->getNameTask();
                $startDate = !empty($task->getStartDate()) ? date('d/m/Y', $task->getStartDate()) : '';
                $endDate = !empty($task->getEndDate()) ? date('d/m/Y', $task->getEndDate()) : '';
                $progress = $task->getProgress();
                $parentTaskPHID = $task->getparentTaskPHID();
                $custom_data['name'] = $nameTask;
                $custom_data['start'] =  !empty($task->getStartDate()) ? date('Y-m-d', $task->getStartDate()) : '';
                $custom_data['end'] = !empty($task->getEndDate()) ? date('Y-m-d', $task->getEndDate()) : '';
                $custom_data['progress'] = $progress;
                $custom_data['custom_class'] = 'task-children';


                if (empty($parentTaskPHID)) {
                    if ($task->getIsParentTask() === "1") {
                        $data = $this->getData($task);
                        $progress = $data['progress'];
                        $startDate = !empty($data['minDate']) ? date('d/m/Y', $data['minDate']) : '';
                        $endDate = !empty($data['maxDate']) ? date('d/m/Y', $data['maxDate']) : '';
                        $custom_data['start'] = !empty($data['minDate']) ? date('Y-m-d', $data['minDate']) : '';
                        $custom_data['end'] = !empty($data['maxDate']) ? date('Y-m-d', $data['maxDate']) : '';
                        $custom_data['progress'] = $progress;
                        $custom_data['custom_class'] = 'task-parent';

                    }

                    $hiddenIconChildTask = $task->getIsParentTask() === self::IS_PARENT_TASK ? '' : 'fa-caret-down';


                    $task =
                        phutil_tag(
                        'div',
                        array(
                            'class' => 'task-parent',
                        ), [
                            phutil_tag(
                                'task-parent',
                                array(
                                    'class' => 'icon-task-parent',
                                ),
                                id(new PHUIButtonOKRView())
                                    ->setTag('a')
                                    ->setID('task-parent')
                                    ->setIcon($hiddenIconChildTask)),
                            phutil_tag(
                                'h3',
                                array(),
                                id(new PHUIButtonOKRView())
                                    ->setTag('a')
                                    ->setID('add-task-parent')
                                    ->setName($nameTask)
                                    ->setText($nameTask)
                                    ->addSigil('has-tooltip')
                                    ->setMetadata(
                                      array(
                                        'tip' => pht($nameTask),
                                        'size' => 500
                                      ))
                                    ->setHref("/po-tools/gant-chart/{$id_project}/edit-task/{$task->getID()}/")
                            )
                        ]
                    );
                } else {
                    $task =
                        phutil_tag(
                            'div',
                            array(
                                'class' => 'task-child',
                            ), [
                                phutil_tag(
                                    'span',
                                    array(),
                                    id(new PHUIButtonOKRView())
                                        ->setTag('a')
                                        ->setID('add-task-parent')
                                        ->setName($nameTask)
                                        ->setText($nameTask)
                                        ->addSigil('has-tooltip')
                                        ->setMetadata(
                                          array(
                                            'tip' => pht($nameTask),
                                            'size' => 500
                                          ))
                                        ->setHref("/po-tools/gant-chart/{$id_project}/edit-task/{$task->getID()}")
                                )
                            ]
                        );
                }

                ${"row" . $key}[] = [
                    $task,
                    $startDate,
                    $endDate,
                    !is_null($progress) ? $progress."%" : '0%'
                ];

                array_push($custom_data_gant_chart, $custom_data);

            }
        }

        $table = id(new AphrontTableView(${"row" . $key}))
            ->setHeaders(
                array(
                    pht('Task'),
                    pht('Start Date'),
                    pht('End Date'),
                    pht('Progress'),
                ))
        ;

        $showCloseDateEndDate  = array(
            phutil_tag(
                'div',
                array(
                    'class' => 'div-parsdasdent-add-task',
                ),
                [
                    phutil_tag(
                        'div',
                        array(
                            'class' => 'open-date',
                        ),
                        id(new PHUIButtonOKRView())
                            ->setTag('a')
                            ->setID('open-date')
                            ->setName("open_date")
                            ->setText("Open Date : {$openDate}")
                    ),
                    phutil_tag(
                        'div',
                        array(
                            'class' => 'close-date',
                        ),
                        id(new PHUIButtonOKRView())
                            ->setTag('a')
                            ->setID('close-date')
                            ->setName("close-date")
                            ->setText("Close Date : {$closeDate}")
                    ),
                ]
            )
        );

        $content_div = array(
            phutil_tag(
                'input',
                array(
                    'id' => 'value-day-update',
                    'type' => 'hidden',
                    'value' => $checkLastUpdateNotNull,
                )),
            phutil_tag(
                'input',
                array(
                    'id' => 'object-tasks',
                    'type' => 'hidden',
                    'value' => json_encode($custom_data_gant_chart),
                )),
            phutil_tag(
                'input',
                array(
                    'id' => 'openDate',
                    'type' => 'hidden',
                    'value' => json_encode(!empty($project->getOpenDate()) ? date('Y-m-d', $project->getOpenDate()) : ''),
                )),
            phutil_tag(
                'input',
                array(
                    'id' => 'closeDate',
                    'type' => 'hidden',
                    'value' => json_encode(!empty($project->getCloseDate()) ? date('Y-m-d', $project->getCloseDate()) : ''),
                )),   
            $showCloseDateEndDate,
            $table,
            $addTask
        );

        $view = id(new PHUIObjectBoxView())
            ->setHeader($feed_header)
            ->setFormSaved($list, $text)
            ->setObjectList($content_div)
        ;

        return $view;
    }

    /**
     * Get Data Gant Chart
     *
     * @param $phid_project
     * @return mixed
     */
    public function getGantChart($phid_project)
    {
        return id(new PhabricatorTask())->loadAllWhere(
            'projectPHID IN (%Ls)',
            array($phid_project));
        ;
    }

    /**
     * Sort Task In Gant Chart
     *
     * @param $tasks
     * @return array
     */
    public function sortTasks($tasks)
    {
        $sortedTasks = array();
        foreach ($tasks as $task) {
            if ($this->isParentTask($task)) {
                array_push($sortedTasks, $task);
                $parentTaskPHID = $task->getPHID();
                foreach ($tasks as $childTask) {
                    if ($this->isBelongToParentTask($childTask, $parentTaskPHID)) {
                        array_push($sortedTasks, $childTask);
                    }
                }
            }
        }
        return $sortedTasks;

    }

    /**
     * Check Parent Task
     *
     * @param $task
     * @return bool
     */
    public function isParentTask($task)
    {
        return empty($task->getParentTaskPHID());
    }

    /**
     * Check Child Task With Parent Task
     *
     * @param $childTask
     * @param $parentTaskPHID
     * @return bool
     */
    public function isBelongToParentTask($childTask, $parentTaskPHID)
    {
        return $childTask->getParentTaskPHID() === $parentTaskPHID;
    }

    /**
     * Get Max Date Update In Gant Chart
     *
     * @param $tasks
     * @return false|string
     */
    public function getDateUpdateMaxInGantChart($tasks)
    {
        $taskPHIDs = array();
        foreach ($tasks as $task) {
            array_push($taskPHIDs, $task->getPHID());
        }

        if (empty($taskPHIDs)) {
            return '';
        }
        $taskTransactions = id(new PhabricatorTaskTransaction())
            ->loadAllWhere('objectPHID IN (%Ls)', $taskPHIDs);

        $maxDate = $this->getMaxDateModified($taskTransactions);

        return $maxDate;
    }

    private function getMaxDateModified($taskTransactions)
    {
        $maxDateModified = 0;
        if (empty($taskTransactions)) {
            return $maxDateModified;
        }
        foreach ($taskTransactions as $taskTransaction) {
            $dateModified = $taskTransaction->getDateModified();
            $maxDateModified = $dateModified > $maxDateModified ? $dateModified : $maxDateModified;
        }
        $maxDateModified = date('d/m/Y', $maxDateModified);

        return $maxDateModified;
    }
    /**
     * @param $task
     * @return int
     */
    private function getData($task)
    {
        $parentProgress = 0;
        $allStartDate = array();
        $allEndDate = array();
        $i = 0;
        $countChildTask = count(id(new PhabricatorTask())->loadAllWhere('parentTaskPHID = %s', $task->getPHID()));
        foreach (id(new PhabricatorTask())->loadAllWhere('parentTaskPHID = %s', $task->getPHID()) as $task) {
            $parentProgress = $task->getProgress() + $parentProgress;

            $allEndDate[$i++] = $task->getEndDate();

            if (!is_null($task->getStartDate())) {
                $allStartDate[$i++] = $task->getStartDate();
            }
        }

        $progress =  $parentProgress === self::IS_NULL_PROGRESS_PARENT_TASK ? self::NULL_PROGRESS_PARENT_TASK : $parentProgress / $countChildTask;

        return ['minDate' => min($allStartDate), 'maxDate' => max($allEndDate), 'progress' => round($progress, 1, PHP_ROUND_HALF_UP)];
    }

}