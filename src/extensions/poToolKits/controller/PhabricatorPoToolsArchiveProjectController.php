<?php

final class PhabricatorPoToolsArchiveProjectController
    extends PhabricatorProjectsController
{

    public function handleRequest(AphrontRequest $request)
    {
        $viewer = $request->getViewer();
        $id = $request->getURIData('id');

        $project = id(new PhabricatorProjectsQuery())
            ->setViewer($viewer)
            ->withIDs(array($id))
            ->requireCapabilities(
                array(
                    PhabricatorPolicyCapability::CAN_VIEW,
                    PhabricatorPolicyCapability::CAN_EDIT,
                ))
            ->executeOne();
        if (!$project) {
            return new Aphront404Response();
        }

        $edit_uri = $this->getApplicationURI('manage/' . $project->getID() . '/');
        if ($request->isFormPost()) {
            if ($project->isArchived()) {
                $new_status = PhabricatorPoProjectStatus::STATUS_ACTIVE;
            } else {
                $new_status = PhabricatorPoProjectStatus::STATUS_ARCHIVED;;
            }

            $xactions = array();

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType(
                    PhabricatorProjectsIsArchivedTransaction::TRANSACTIONTYPE)
                ->setNewValue($new_status);

            id(new PhabricatorProjectsEditor())
                ->setActor($viewer)
                ->setContentSourceFromRequest($request)
                ->setContinueOnNoEffect(true)
                ->applyTransactions($project, $xactions);

            return id(new AphrontRedirectResponse())->setURI($edit_uri);
        }

        if ($project->isArchived()) {
            $title = pht('Really activate Project?');
            $body = pht('This Project will become active again.');
            $button = pht('Activate Project');
        } else {
            $title = pht('Really archive Project?');
            $body = pht('This Project will be moved to the archive.');
            $button = pht('Archive Project');
        }

        $dialog = id(new AphrontDialogView())
            ->setUser($viewer)
            ->setTitle($title)
            ->appendChild($body)
            ->addCancelButton($edit_uri)
            ->addSubmitButton($button);

        return id(new AphrontDialogResponse())->setDialog($dialog);
    }

}
