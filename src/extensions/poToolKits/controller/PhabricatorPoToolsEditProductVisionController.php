<?php

final class PhabricatorPoToolsEditProductVisionController
    extends PhabricatorProjectsBoardController
{

    public function shouldAllowPublic()
    {
        return true;
    }

    public function handleRequest(AphrontRequest $request)
    {
        $viewer = $request->getUser();
        $response = $this->loadProject();

        if ($response) {
            return $response;
        }

        $project = $this->getProject();
        $state = $this->getViewState();
        $saved = $state->getSavedQuery();

        if (!$saved) {
            return new Aphront404Response();
        }

        $layout_engine = $state->getLayoutEngine();
        $board_phid = $project->getPHID();
        $columns = $layout_engine->getColumns($board_phid);

        if (!$columns || !$project->getHasWorkboard()) {
            $has_normal_columns = false;

            foreach ($columns as $column) {
                if (!$column->getProxyPHID()) {
                    $has_normal_columns = true;
                    break;
                }
            }

            $can_edit = PhabricatorPolicyFilter::hasCapability(
                $viewer,
                $project,
                PhabricatorPolicyCapability::CAN_EDIT);

            if (!$has_normal_columns) {
                if (!$can_edit) {
                    $content = $this->buildNoAccessContent($project);
                } else {
                    $content = $this->buildInitializeContent($project);
                }
            } else {
                if (!$can_edit) {
                    $content = $this->buildDisabledContent($project);
                } else {
                    $content = $this->buildEnableContent($project);
                }
            }

            if ($content instanceof AphrontResponse) {
                return $content;
            }

            $nav = $this->newNavigation($project, PhabricatorProjects::ITEM_PRODUCT_VISION);
            $crumbs = $this->buildApplicationCrumbs();
            $crumbs->addTextCrumb(pht('Product Vision'));

            return $this->newPage()
                ->setTitle(
                    array(
                        $project->getName(),
                        pht('Product Vision'),
                    ))
                ->setNavigation($nav)
                ->setCrumbs($crumbs)
                ->appendChild($content);
        }
    }

    private function buildInitializeContent(PhabricatorProjects $project)
    {
        require_celerity_resource('productVisionForm-css');
        $id = $project->getID();
        $request = $this->getRequest();
        $viewer = $this->getViewer();
        $v_customer = $project->getCustomer();
        $v_who = $project->getWho();
        $v_productName = $project->getProductName();
        $v_classify = $project->getClassify();
        $v_benefit = $project->getBenefit();
        $v_unlike = $project->getUnlike();
        $v_ourProduct = $project->getOurProduct();

        if ($request->isFormPost()) {
            $xactions = array();
            $v_customer = trim($request->getStr('customer'));
            $v_who = trim($request->getStr('who'));
            $v_productName = trim($request->getStr('productName'));
            $v_classify = trim($request->getStr('classify'));
            $v_benefit = trim($request->getStr('benefit'));
            $v_unlike = trim($request->getStr('unlike'));
            $v_ourProduct = trim($request->getStr('ourProject'));

            $type_customer = PhabricatorProjectsEditCustomerTransaction::TRANSACTIONTYPE;
            $type_who = PhabricatorProjectsWhoTransaction::TRANSACTIONTYPE;
            $type_productName = PhabricatorProjectsProductNameTransaction::TRANSACTIONTYPE;
            $type_classify = PhabricatorProjectsEditClassifyTransaction::TRANSACTIONTYPE;
            $type_benefit = PhabricatorProjectsEditBenefitTransaction::TRANSACTIONTYPE;
            $type_unlike = PhabricatorProjectsUnlikeTransaction::TRANSACTIONTYPE;
            $type_ourProject = PhabricatorProjectsOurProductTransaction::TRANSACTIONTYPE;

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_customer)
                ->setNewValue($v_customer);

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_who)
                ->setNewValue($v_who);

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_productName)
                ->setNewValue($v_productName);

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_classify)
                ->setNewValue($v_classify);

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_benefit)
                ->setNewValue($v_benefit);

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_unlike)
                ->setNewValue($v_unlike);

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_ourProject)
                ->setNewValue($v_ourProduct);

            $editor = id(new PhabricatorProjectsEditor())
                ->setActor($viewer)
                ->setContinueOnNoEffect(true)
                ->setContentSourceFromRequest($request);

            session_start();
            $editor->applyTransactions($project, $xactions);
            $_SESSION['message'] = 'Edit Product Vision successfully';
            return id(new AphrontRedirectResponse())->setURI("/po-tools/product-vision/{$id}");
        }

        Javelin::initBehavior('jquery');
        Javelin::initBehavior('product-form');

        $form = id(new AphrontFormView())
            ->setUser($viewer)
            ->setEncType('multipart/form-data');

        $form
            ->appendChild(
                id(new AphrontFormTextControl())
                    ->setLabel(pht('For'))
                    ->setName('customer')
                    ->setID('customer')
                    ->setValue($v_customer)
            )
            ->appendChild(
                id(new AphrontFormTextAreaControl())
                    ->setLabel(pht('Who'))
                    ->setName('who')
                    ->setID('who')
                    ->setValue($v_who)
            )
            ->appendChild(
                id(new AphrontFormTextControl())
                    ->setLabel(pht('The'))
                    ->setName('productName')
                    ->setID('productName')
                    ->setValue($v_productName)
            )
            ->appendChild(
                id(new AphrontFormTextControl())
                    ->setLabel(pht('Is a'))
                    ->setName('classify')
                    ->setID('classify')
                    ->setValue($v_classify)
            )->appendChild(
                id(new AphrontFormTextAreaControl())
                    ->setLabel(pht('That'))
                    ->setName('benefit')
                    ->setID('benefit')
                    ->setValue($v_benefit)
            )->appendChild(
                id(new AphrontFormTextAreaControl())
                    ->setLabel(pht('Unlike'))
                    ->setName('unlike')
                    ->setID('unlike')
                    ->setValue($v_unlike)
            )->appendChild(
                id(new AphrontFormTextAreaControl())
                    ->setLabel(pht('Our  Project'))
                    ->setName('ourProject')
                    ->setID('ourProject')
                    ->setValue($v_ourProduct)
            )
            ->appendChild(
                id(new AphrontFormSubmitControl())
                    ->setValue("Save")
                    ->addCancelButton("/po-tools/product-vision/{$id}")
            );


        $box = id(new PHUIObjectBoxView())
            ->setHeaderText(pht('Vision Statement for Product'))
            ->setForm($form);

        return $box;
    }

    private function buildNoAccessContent(PhabricatorProjects $project)
    {
        $id = $project->getID();
        $profile_uri = $this->getApplicationURI("profile/{$id}/");

        return $this->newDialog()
            ->setTitle(pht('Unable to Create Workboard'))
            ->appendParagraph(
                pht(
                    'The workboard for this project has not been created yet, ' .
                    'but you do not have permission to create it. Only users ' .
                    'who can edit this project can create a workboard for it.'))
            ->addCancelButton($profile_uri);
    }

    private function buildEnableContent(PhabricatorProjects $project)
    {
        $request = $this->getRequest();
        $viewer = $this->getViewer();
        $id = $project->getID();
        $profile_uri = $this->getApplicationURI("profile/{$id}/");
        $board_uri = $this->getApplicationURI("board/{$id}/");

        if ($request->isFormPost()) {
            $xactions = array();

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType(
                    PhabricatorProjectWorkboardTransaction::TRANSACTIONTYPE)
                ->setNewValue(1);

            id(new PhabricatorProjectTransactionEditor())
                ->setActor($viewer)
                ->setContentSourceFromRequest($request)
                ->setContinueOnNoEffect(true)
                ->setContinueOnMissingFields(true)
                ->applyTransactions($project, $xactions);

            return id(new AphrontRedirectResponse())
                ->setURI($board_uri);
        }

        return $this->newDialog()
            ->setTitle(pht('Workboard Disabled'))
            ->addHiddenInput('initialize', 1)
            ->appendParagraph(
                pht(
                    'This workboard has been disabled, but can be restored to its ' .
                    'former glory.'))
            ->addCancelButton($profile_uri)
            ->addSubmitButton(pht('Enable Workboard'));
    }

    private function buildDisabledContent(PhabricatorProjects $project)
    {
        $viewer = $this->getViewer();

        $id = $project->getID();

        $profile_uri = $this->getApplicationURI("profile/{$id}/");

        return $this->newDialog()
            ->setTitle(pht('Workboard Disabled'))
            ->appendParagraph(
                pht(
                    'This workboard has been disabled, and you do not have permission ' .
                    'to enable it. Only users who can edit this project can restore ' .
                    'the workboard.'))
            ->addCancelButton($profile_uri);
    }


}
