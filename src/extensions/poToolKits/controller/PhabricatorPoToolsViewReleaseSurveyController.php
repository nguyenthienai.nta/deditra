<?php

final class PhabricatorPoToolsViewReleaseSurveyController
  extends PhabricatorProjectsBoardController
{

  public function shouldAllowPublic()
  {
    return true;
  }

  public function handleRequest(AphrontRequest $request)
  {

    $viewer = $request->getUser();
    $response = $this->loadProject();

    if ($response) {
      return $response;
    }

    $project = $this->getProject();
    $state = $this->getViewState();
    $layout_engine = $state->getLayoutEngine();
    $board_phid = $project->getPHID();
    $columns = $layout_engine->getColumns($board_phid);

    if (!$columns || !$project->getHasWorkboard()) {
      $has_normal_columns = false;

      foreach ($columns as $column) {
        if (!$column->getProxyPHID()) {
          $has_normal_columns = true;
          break;
        }
      }

      $can_edit = PhabricatorPolicyFilter::hasCapability(
        $viewer,
        $project,
        PhabricatorPolicyCapability::CAN_EDIT);

      if (!$has_normal_columns) {
        if (!$can_edit) {
          $content = $this->buildNoAccessContent($project);
        } else {
          $content = $this->buildInitializeContent($project);
        }
      } else {
        if (!$can_edit) {
          $content = $this->buildDisabledContent($project);
        } else {
          $content = $this->buildEnableContent($project);
        }
      }

      if ($content instanceof AphrontResponse) {
        return $content;
      }

      $nav = $this->newNavigation($project, PhabricatorProjects::ITEM_PRODUCT_VISION);
      $crumbs = $this->buildApplicationCrumbs();
      $crumbs->addTextCrumb(pht('NSS - Release Survey'));

      return $this->newPage()
        ->setTitle(
          array(
            $project->getName(),
            pht('NSS - Release Survey'),
          ))
        ->setNavigation($nav)
        ->setCrumbs($crumbs)
        ->appendChild($content);
    }
  }

  private function buildInitializeContent(PhabricatorProjects $project)
  {
    Javelin::initBehavior('jquery');
    Javelin::initBehavior('list-survey');
    Javelin::initBehavior('chart-bundle');
    Javelin::initBehavior('chart');
    Javelin::initBehavior('po-chart-render');
    require_celerity_resource('listSurvey-css');

    $id = $project->getID();
    $viewer = $this->getViewer();
    $request = $this->getRequest();
    $list = "";
    $text = "";

    session_start();
		if ($request->isFormPost()) {
      $_SESSION['message'] = 'All survey status update successfully';

      $surveyStatus = $request->getStr('surveyStatus');
      $arr = explode(',', $surveyStatus);
      $surveyId = array_slice($arr, 0, count($arr)/2);
      $surveyStatus = array_slice($arr, count($arr)/2, count($arr)/2);
      $surveys = id(new PhabricatorSurvey())->loadAll();
      foreach($surveys as $survey){
        if(in_array($survey->getId(), $surveyId)){
          $index = array_search($survey->getId(), $surveyId);
          id(new PhabricatorSurvey())->load($survey->getId())
          ->setStatus($surveyStatus[$index])
          ->save();
        }
      }
		}

    if (isset($_SESSION['message'])) {
      $text = $_SESSION['message'];
      $list = id(new PHUIInfoView())
        ->setSeverity(PHUIInfoView::SEVERITY_NOTICE)
        ->appendChild($_SESSION['message']);
      unset($_SESSION['message']);
    }


    $viewer = $this->getViewer();
    $edit = id(new PHUIButtonView())
      ->setTag('a')
      ->setText(pht('Create Survey'))
      ->setColor(PHUIButtonView::BLUE)
      ->setHref("/po-tools/release-survey/{$id}/add");

    $save_status = id(new PHUIButtonView())
      ->setTag('a')
      ->setText(pht('Save Status'))
      ->setColor(PHUIButtonView::BLUE)
      ->setHref("/po-tools/save-status/{$id}");

    $project_id =  javelin_tag(
      'input',
      array(
      'id' => 'projectId',
      'type' => 'hidden',
      'value' => $id,
      )
    );

    $result_btn =  javelin_tag(
      'button',
      array(
      'class' => 'result-btn button button-yellow has-text icon-last phui-button-default msl phui-header-action-link',
      ), 'Result'
    );


    function renderEditBtn ($project_id, $survey_id){
      $edit_btn =  javelin_tag(
        'a',
        array(
        'class' => 'button button-blue has-text icon-last phui-button-default msl phui-header-action-link',
        'href' => "/po-tools/release-survey/{$project_id}/edit/{$survey_id}"
        ), 'Edit'
      );
      return $edit_btn;
    }

    function getStatusSurvey ($survey, $is_answered_survey){
      $option = id(new AphrontFormSelectControl())
      ->setName('status')
      ->setID('status_' . $survey->getId())
      ->setOptions([1 => 'Sent', 0 => 'Not Sent'])
      ->setValue($survey->getStatus());
      $answered = pht('  Answered');
      return $is_answered_survey ? $answered : $option;
    }

    
    $feed_header = id(new PHUIHeaderView())
      ->setHeader(pht('NSS - Release Survey'));
  
    $surveys = id(new PhabricatorSurvey)->loadAllWhere('projectPHID = %s',$project->getPHID());
    $rows = [];
    $all_survey_id = [];
    $index = 0;
    
    foreach($surveys as $key => $survey){
        $customer_satisfaction = $survey->getPoint();
        $customer_comment = $survey->getComment();
        $is_answered_survey = (empty($customer_satisfaction) && empty($customer_comment)) ? 0 : 1;
        $all_survey_id[] = $key;

        $delete_btn = id(new PHUIButtonSurveyView())
        ->setTag('a')
        ->setID('add-task-parent')
        ->setName('delete')
        ->setText('delete')
        ->setColor('red')
        ->setHref("/po-tools/release-survey/delete-survey/{$survey->getID()}/")
        ->setWorkflow(true);
        
        $rows[] = array(
        ++$index,
        javelin_tag(
          'div',
          array(
          'class' => 'name-project-block',
          ),
          [
              javelin_tag(
                  'p',
                  array(
                  'class' => 'name-project',
                  ), $survey->getReleaseName()
              ),
          ]),
        javelin_tag(
            'div',
            array(
            'class' => 'url-block',
            ),
            [
                javelin_tag(
                    'p',
                    array(
                    'class' => "url url-$key",
                    ), $survey->getUrl()
                ),
                javelin_tag(
                    'span',
                    array(
                    'class' => "visual-only phui-icon-view phui-font-fa fa-copy btn-copy-$key",
                    'style' => 'margin-left: 5px; font-size: 18px',
                    )
                ),
            ]),
        date('d/m/Y', $survey->getDateCreated()),
        getStatusSurvey($survey, $is_answered_survey),
        !empty($survey->getPoint()) ? $survey->getPoint() : pht('-'),
        javelin_tag(
          'div',
          array(
          'class' => 'button-group',
          ),
          [
              $is_answered_survey ? $result_btn : '',
              $is_answered_survey ? '' : $delete_btn,
              renderEditBtn($id, $survey->getId())
          ]),
        );
    }

    $all_survey_input =  javelin_tag(
      'input',
      array(
      'id' => 'allSurveyId',
      'type' => 'hidden',
      'value' => isset($all_survey_id) ? json_encode($all_survey_id) : array(),
      )
    );
    $table = id(new AphrontTableView($rows))
      ->setNoDataString(pht('No surveys have been created yet'))
      ->setHeaders(
        array(
            pht('No'),
            pht('Release Name'),
            pht('URL'),
            pht('Created Date'),
            pht('Status'),
            pht('Point'),
            pht('')
        ))
      ->setColumnClasses(
        array(
          'thead-green',
          'thead-green',
          'thead-green',
          'thead-green',
          'thead-green status-col',
          'thead-green',
          'thead-green',
        ));

      $form = id(new AphrontFormPoToolkitView())
        ->setUser($viewer)
        ->setEncType('multipart/form-data');
  
      $form
        ->appendChild(
          id(new AphrontFormTextControl())
            ->setName('surveyStatus')
            ->setID('surveyStatus')
        )
        ->appendChild(
          id(new AphrontFormSubmitControl())
            ->setValue("Save Status")
        );        


    $view = id(new PHUIObjectBoxView())
      ->setHeader($feed_header)
      ->setFormSaved($list, $text)
      ->setForm([$project_id, $all_survey_input, $form])
      ->setObjectList($table);

    return $view;
  }


}
