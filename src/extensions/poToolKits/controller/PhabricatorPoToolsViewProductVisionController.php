<?php
final class PhabricatorPoToolsViewProductVisionController
    extends PhabricatorProjectsBoardController
{

    public function shouldAllowPublic()
    {
        return true;
    }

    public function handleRequest(AphrontRequest $request)
    {

        $viewer = $request->getUser();
        $response = $this->loadProject();

        if ($response) {
            return $response;
        }

        $project = $this->getProject();
        $state = $this->getViewState();

        $layout_engine = $state->getLayoutEngine();
        $board_phid = $project->getPHID();
        $columns = $layout_engine->getColumns($board_phid);

        if (!$columns || !$project->getHasWorkboard()) {
            $has_normal_columns = false;

            foreach ($columns as $column) {
                if (!$column->getProxyPHID()) {
                    $has_normal_columns = true;
                    break;
                }
            }

            $can_edit = PhabricatorPolicyFilter::hasCapability(
                $viewer,
                $project,
                PhabricatorPolicyCapability::CAN_EDIT);

            if (!$has_normal_columns) {
                if (!$can_edit) {
                    $content = $this->buildNoAccessContent($project);
                } else {
                    $content = $this->buildInitializeContent($project);
                }
            } else {
                if (!$can_edit) {
                    $content = $this->buildDisabledContent($project);
                } else {
                    $content = $this->buildEnableContent($project);
                }
            }

            if ($content instanceof AphrontResponse) {
                return $content;
            }

            $nav = $this->newNavigation($project, PhabricatorProjects::ITEM_PRODUCT_VISION);
            $crumbs = $this->buildApplicationCrumbs();
            $crumbs->addTextCrumb(pht('Product Vision'));

            return $this->newPage()
                ->setTitle(
                    array(
                        $project->getName(),
                        pht('Product Vision'),
                    ))
                ->setNavigation($nav)
                ->setCrumbs($crumbs)
                ->appendChild($content);
        }
    }

    private function buildInitializeContent(PhabricatorProjects $project)
    {
        Javelin::initBehavior('jquery');
        Javelin::initBehavior('product-form');

        $viewer = $this->getViewer();
        $date_createdat = phabricator_date($project->getDateCreated(), $viewer);
        $format_date_created = new DateTime($date_createdat);

        $date_updatedat = phabricator_date($project->getDateModified(), $viewer);
        $format_date_updated = new DateTime($date_updatedat);

        $date_create = $format_date_created->format('d/m/Y');
        $date_update = $format_date_updated->format('d/m/Y');

        $list = "";
        $text = "";
        session_start();
        if (isset($_SESSION['message'])) {
            $text = $_SESSION['message'];
            $list = id(new PHUIInfoView())
                ->setSeverity(PHUIInfoView::SEVERITY_NOTICE)
                ->appendChild($_SESSION['message']);
            unset($_SESSION['message']);
        }


        require_celerity_resource('productVisionForm-css');
        $viewer = $this->getViewer();
        $edit = id(new PHUIButtonView())
            ->setTag('a')
            ->setText(pht('Edit'))
            ->setColor(PHUIButtonView::BLUE)
            ->setHref($project->getWorkboardURI());

        $feed_header = id(new PHUIHeaderView())
            ->setHeader(pht('Vision Statement For Product'))
            ->addActionLink($edit)
            ->setSubheader(pht("Created at : {$date_create}"))
        ;


        $view = id(new PHUIPropertyListView())
            ->setUser($viewer);

        $view->addProperty(
            pht("For : "), $project->getCustomer());

        $view->addProperty(
            pht('Who : '), $project->getWho());

        $view->addProperty(
            pht('The : '), $project->getProductName());

        $view->addProperty(
            pht('Is a : '), $project->getClassify());

        $view->addProperty(
            pht('That : '), $project->getBenefit());

        $view->addProperty(
            pht('Unlike : '), $project->getUnlike());

        $view->addProperty(
            pht('Our Product : '), $project->getOurProduct());

        $content_div =
            phutil_tag(
                'div',
                array(
                    'class' => 'border-content-list-provision',
                ),
                phutil_tag(
                    'div',
                    array(
                        'class' => 'div-child-content-list-provision',
                    ), $view
                )
            );

        $content_div = array(
            phutil_tag(
                'input',
                array(
                    'id' => 'value-day-update',
                    'type' => 'hidden',
                    'value' => $date_update,
                )),
            $content_div,
        );


        $view = id(new PHUIObjectBoxView())
            ->setHeader($feed_header)
            ->setFormSaved($list, $text)
            ->setObjectList($content_div)
        ;

        return $view;
    }


}