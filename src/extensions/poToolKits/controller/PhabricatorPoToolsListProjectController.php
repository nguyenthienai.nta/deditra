<?php
abstract class PhabricatorPoToolsBaseViewController extends PhabricatorController
{

}

class PhabricatorPoToolsListProjectController extends PhabricatorPoToolsBaseViewController
{
    public function handleRequest(AphrontRequest $request)
    {
        $request = $this->getRequest();
        $controller = id(new PhabricatorApplicationSearchController())
            ->setQueryKey($request->getURIData('queryKey'))
            ->setSearchEngine(new PhabricatorPoToolsProjectSearchEngine())
            ->setNavigation($this->buildSideNavView());

        return $this->delegateToController($controller);
    }

    public function buildSideNavView($for_app = false)
    {
        $user = $this->getRequest()->getUser();

        $nav = new AphrontSideNavFilterView();
        $nav->setBaseURI(new PhutilURI($this->getApplicationURI()));

        id(new PhabricatorPoToolsProjectSearchEngine())
            ->setViewer($user)
            ->addNavigationItems($nav->getMenu());

        $nav->selectFilter(null);

        return $nav;
    }

    protected function buildApplicationCrumbs()
    {
        $crumbs = parent::buildApplicationCrumbs();

        $can_create = $this->hasApplicationCapability(
          PhabricatorPoToolkitCapabilityCreateSpaces::CAPABILITY);

        $crumbs->addAction(
            id(new PHUIListItemView())
                ->setName(pht('New Project'))
                ->setHref('/po-tools/create/')
                ->setIcon('fa-plus-square')
                ->setDisabled(!$can_create)
                ->setWorkflow(!$can_create));

        return $crumbs;
    }
}
