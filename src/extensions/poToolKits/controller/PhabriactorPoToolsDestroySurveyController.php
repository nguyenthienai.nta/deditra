<?php

final class PhabriactorPoToolsDestroySurveyController
extends PhabricatorController
{

    public function handleRequest(AphrontRequest $request)
    {
        $viewer = $request->getViewer();
        $id = $request->getURIData('surveyId');

        $survey = id(new PhabricatorSurveyQuery())
            ->setViewer($viewer)
            ->withIDs(array($id))
            ->executeOne();
        if (!$survey) {
            return new Aphront404Response();
        }

        $project = id(new PhabricatorProjects())->loadOneWhere('phid = %s', $survey->getProjectPHID());

        if (!$project) {
            return new Aphront404Response();
        }

        $canDelete = $survey->getPoint() === null ? true : false;
        $cancel_uri = $this->getApplicationURI('/release-survey/' . $project->getID() . '/');

        if ($request->isFormPost()) {

            id(new PhabricatorSurvey())->load($survey->getID())->delete();

            session_start();
            $_SESSION['message'] = 'Survey deleted successfully';
            return id(new AphrontRedirectResponse())->setURI('/po-tools/release-survey/' . $project->getID() . '/');
        }

        if ($canDelete) {
            $body = pht('This survey will delete.');
        } else {
            $body = pht("This survey can't delete.");
        }

        $title = pht('Really Delete Survey?');
        $button = pht('Delete survey');
        $dialog = id(new AphrontDialogView())
            ->setUser($viewer)
            ->setTitle($title)
            ->appendChild($body)
            ->addCancelButton($cancel_uri);
        if ($canDelete) {
            $dialog->addSubmitButton($button);
        }

        return id(new AphrontDialogResponse())->setDialog($dialog);
    }
}
