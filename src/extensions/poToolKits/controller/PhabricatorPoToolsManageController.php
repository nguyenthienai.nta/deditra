<?php

final class PhabricatorPoToolsManageController extends PhabricatorProjectsController
{
    const TRANSACTION_OLD_VALUE = 'old_value';
    const TRANSACTION_NEW_VALUE = 'new_value';

    public function shouldAllowPublic()
    {
        return true;
    }

    public function handleRequest(AphrontRequest $request)
    {
        $response = $this->loadProject();
        if ($response) {
            return $response;
        }

        $viewer = $request->getUser();
        $project = $this->getProject();
        $id = $project->getID();

        $header = id(new PHUIHeaderView())
            ->setHeader(pht('Project History'))
            ->setUser($viewer)
            ->setPolicyObject($project);

        if ($project->getStatus() == 0) {
            $header->setStatus('fa-check', 'bluegrey', pht('Active'));
        } else {
            $header->setStatus('fa-ban', 'red', pht('Archived'));
        }

        $curtain = $this->buildCurtain($project);
        $properties = $this->buildPropertyListView($project);

        $timeline = $this->buildTransactionTimeline(
            $project,
            new PhabricatorPoToolsProjectsTransactionQuery());
        $timeline->setShouldTerminate(true);

        $nav = $this->newNavigation($project, PhabricatorProjects::ITEM_PRODUCT_VISION);

        $crumbs = $this->buildApplicationCrumbs();
        $crumbs->addTextCrumb(pht('Manage'));
        $crumbs->setBorder(true);

        require_celerity_resource('project-view-css');

        $manage = id(new PHUITwoColumnView())
            ->setHeader($header)
            ->setCurtain($curtain)
            ->addPropertySection(pht('Details'), $properties)
            ->addClass('project-view-home')
            ->addClass('project-view-people-home')
            ->setMainColumn(
                array(
                    $timeline,
                ));

        return $this->newPage()
            ->setNavigation($nav)
            ->setCrumbs($crumbs)
            ->setTitle(
                array(
                    $project->getName(),
                    pht('Manage'),
                ))
            ->appendChild(
                array(
                    $manage,
                ));
    }

    private function buildCurtain(PhabricatorProjects $project)
    {
        $viewer = $this->getViewer();

        $id = $project->getID();
        $can_edit = PhabricatorPolicyFilter::hasCapability(
            $viewer,
            $project,
            PhabricatorPolicyCapability::CAN_EDIT);

        $curtain = $this->newCurtainView($project);

        $curtain->addAction(
            id(new PhabricatorActionView())
                ->setName(pht('Edit Details'))
                ->setIcon('fa-pencil')
                ->setHref($this->getApplicationURI("edit/{$id}/"))
                ->setDisabled(!$can_edit)
                ->setWorkflow(!$can_edit));

        if ($project->isArchived() == 1) {
            $curtain->addAction(
                id(new PhabricatorActionView())
                    ->setName(pht('Activate Project'))
                    ->setIcon('fa-check')
                    ->setHref($this->getApplicationURI("archive/{$id}/"))
                    ->setDisabled(!$can_edit)
                    ->setWorkflow(true));
        } else {
            $curtain->addAction(
                id(new PhabricatorActionView())
                    ->setName(pht('Archive Project'))
                    ->setIcon('fa-ban')
                    ->setHref($this->getApplicationURI("archive/{$id}/"))
                    ->setDisabled(!$can_edit)
                    ->setWorkflow(true));
        }

        return $curtain;
    }

    private function buildPropertyListView(
        PhabricatorProjects $project)
    {
        $viewer = $this->getViewer();

        $view = id(new PHUIPropertyListView())
            ->setUser($viewer);

        $view->addProperty(
            pht('Looks Like'),
            $viewer->renderHandle($project->getPHID())->setAsTag(true));

        $field_list = PhabricatorCustomField::getObjectFields(
            $project,
            PhabricatorCustomField::ROLE_VIEW);
        $field_list->appendFieldsToPropertyList($project, $viewer, $view);

        return $view;
    }

    protected function buildTransactionTimeline(
        PhabricatorApplicationTransactionInterface $object,
        PhabricatorApplicationTransactionQuery $query = null,
        PhabricatorMarkupEngine $engine = null,
        $view_data = array())
    {

        $request = $this->getRequest();
        $viewer = $this->getViewer();
        $xaction = $object->getApplicationTransactionTemplate();

        if (!$query) {
            $query = PhabricatorApplicationTransactionQuery::newQueryForObject(
                $object);
            if (!$query) {
                throw new Exception(
                    pht(
                        'Unable to find transaction query for object of class "%s".',
                        get_class($object)));
            }
        }

        $pager = id(new AphrontCursorPagerView())
            ->readFromRequest($request)
            ->setURI(new PhutilURI(
                '/transactions/showolder/' . $object->getPHID() . '/'));

        $xactions = $query
            ->setViewer($viewer)
            ->withObjectPHIDs(array($object->getPHID()))
            ->needComments(true)
            ->executeWithCursorPager($pager);
        foreach ($xactions as $key => $xaction) {
            $transactionType = $xaction->getTransactionType();
            switch ($transactionType) {
                case PhabricatorProjectsNameProjectCompanyTransaction::TRANSACTIONTYPE:
                    $this->getProjectSlug($xaction, self::TRANSACTION_OLD_VALUE);
                    $this->getProjectSlug($xaction, self::TRANSACTION_NEW_VALUE);
                    break;
                case PhabricatorProjectsUnlikeTransaction::TRANSACTIONTYPE:
                case PhabricatorProjectsOurProductTransaction::TRANSACTIONTYPE:
                case PhabricatorProjectsEditBenefitTransaction::TRANSACTIONTYPE:
                case PhabricatorProjectsEditClassifyTransaction::TRANSACTIONTYPE:
                case PhabricatorProjectsProductNameTransaction::TRANSACTIONTYPE:
                case PhabricatorProjectsWhoTransaction::TRANSACTIONTYPE:
                case PhabricatorProjectsEditCustomerTransaction::TRANSACTIONTYPE:
                case PhabricatorProjectsUpdateTaskTransaction::TRANSACTIONTYPE:
                case PhabricatorProjectsOpenDateTransaction::TRANSACTIONTYPE:
                case phabricatorProjectsCloseDateTransaction::TRANSACTIONTYPE:
                $old = $xaction->getOldValue();
                $new = $xaction->getNewValue();
                if ($this->isDifferent($old, $new)) {
                    unset($xactions[$key]);
                }
                break;
            }
        }

        $xactions = array_reverse($xactions);

        $timeline_engine = PhabricatorTimelineEngine::newForObject($object)
            ->setViewer($viewer)
            ->setTransactions($xactions)
            ->setViewData($view_data);

        $view = $timeline_engine->buildTimelineView();

        if ($engine) {
            foreach ($xactions as $xaction) {
                if ($xaction->getComment()) {
                    $engine->addObject(
                        $xaction->getComment(),
                        PhabricatorApplicationTransactionComment::MARKUP_FIELD_COMMENT);
                }
            }
            $engine->process();
            $view->setMarkupEngine($engine);
        }

        $timeline = $view
            ->setPager($pager)
            ->setQuoteTargetID($this->getRequest()->getStr('quoteTargetID'))
            ->setQuoteRef($this->getRequest()->getStr('quoteRef'));

        return $timeline;
    }


    private function getProjectSlug($xaction, $type)
    {
        $projectPhid = $type == self::TRANSACTION_OLD_VALUE
            ? array($xaction->getOldValue())
            : array($xaction->getNewValue());
        $projectSlugs = id(new PhabricatorProjectSlug())->loadAllWhere(
            'projectPHID IN (%Ls)',
            $projectPhid);
        $projectSlug = array_shift($projectSlugs);
        if (!empty($projectSlug)) {
            if ($type == self::TRANSACTION_OLD_VALUE) {
                $xaction->setOldValue($projectSlug->getSlug());
            } else {
                $xaction->setNewValue($projectSlug->getSlug());
            }
        }
    }

    private function isDifferent($old, $new)
    {
        return $old == null && $new == "" || $old == "" && $new == "";
    }

}
