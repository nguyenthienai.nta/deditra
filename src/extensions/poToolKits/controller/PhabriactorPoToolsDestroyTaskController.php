<?php

final class PhabriactorPoToolsDestroyTaskController
    extends PhabricatorController
{

    public function handleRequest(AphrontRequest $request)
    {
        $viewer = $request->getViewer();
        $id = $request->getURIData('id');

        $task = id(new PhabricatorTaskQuery())
            ->setViewer($viewer)
            ->withIDs(array($id))
            ->executeOne();

        $project_id = id(new PhabricatorProjects())->loadOneWhere('phid = %s', $task->getProjectPHID())->getID();
        $project = id(new PhabricatorProjectsQuery())
            ->setViewer($viewer)
            ->withIDs(array($project_id))
            ->executeOne();

        if (!$project) {
            return new Aphront404Response();
        }


        if (!$task) {
            return new Aphront404Response();
        }

        $cancel_uri = $this->getApplicationURI('/gant-chart/' . $project_id . '/');

        if ($request->isFormPost()) {
            $xactions_project = array();
            $type_update_task = PhabricatorProjectsUpdateTaskTransaction::TRANSACTIONTYPE;

            $xactions_project[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_update_task)
                ->setNewValue(PhabricatorPoToolsEditTaskController::formatDate(date("Y/m/d")));
            $editor_project = id(new PhabricatorProjectsEditor())
                ->setActor($viewer)
                ->setContentSourceFromRequest($request)
                ->setContinueOnNoEffect(true);

            try {
                $editor_project->applyTransactions($project, $xactions_project);

            } catch (PhabricatorApplicationTransactionValidationException $ex) {
                $validation_exception = $ex;
            }

            id(new PhabricatorTask())->load($task->getID())->delete();
            if (count(id(new PhabricatorTask())->loadAllWhere('parentTaskPHID = %s', $task->getPHID())) > 0) {
                foreach (id(new PhabricatorTask())->loadAllWhere('parentTaskPHID = %s', $task->getPHID()) as $task) {
                    $task->delete();
                }
            }

            session_start();
            $_SESSION['message'] = 'Task deleted successfully';
            return id(new AphrontRedirectResponse())->setURI('/po-tools/gant-chart/' . $project_id . '/');
        }

        $title = pht('Really Delete Task?');
        $body = pht('This Task will delete.');
        $button = pht('Delete Task');

        $dialog = id(new AphrontDialogView())
            ->setUser($viewer)
            ->setTitle($title)
            ->appendChild($body)
            ->addCancelButton($cancel_uri)
            ->addSubmitButton($button);

        return id(new AphrontDialogResponse())->setDialog($dialog);
    }


}