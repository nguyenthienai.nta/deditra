<?php

abstract class PhabricatorProjectBaseEditController extends PhabricatorController
{
}

final class PhabricatorPoToolsEditProjectController
    extends PhabricatorProjectBaseEditController
{

    public function handleRequest(AphrontRequest $request)
    {
        require_celerity_resource('gantChart-css');
        require_celerity_resource('jquery-ui');
        Javelin::initBehavior(
            'jquery'
        );
        Javelin::initBehavior(
            'jquery-ui'
        );
        Javelin::initBehavior(
            'po'
        );

        $viewer = $request->getUser();
        $id = $request->getURIData('id');

        if ($id) {
            $project = id(new PhabricatorProjectsQuery())
                ->setViewer($viewer)
                ->withIDs(array($id))
                ->requireCapabilities(
                    array(
                        PhabricatorPolicyCapability::CAN_VIEW,
                        PhabricatorPolicyCapability::CAN_EDIT,
                    ))
                ->executeOne();
            if (!$project) {
                return new Aphront404Response();
            }

            $is_new = false;
            $v_icon = $project->getIcon();
            $cancel_uri = "/po-tools/manage/{$id}";
            $title = pht('Edit Project');
            $button_text = pht('Save Changes');
        } else {
            $this->requireApplicationCapability(
                PhabricatorPOToolkitCapabilityCreateSpaces::CAPABILITY);

            $project = PhabricatorProjects::initializeNewProjects($viewer);
            $is_new = true;
            $v_icon = null;
            $cancel_uri = $this->getApplicationURI();
            $title = pht('Create Project');
            $button_text = pht('Save');

            id(new PhabricatorProjectsQuery())
                ->setViewer(PhabricatorUser::getOmnipotentUser())
                ->withIsDefaultNamespace(true)
                ->execute();
        }

        $validation_exception = null;
        $e_name = true;
        $supported_formats = PhabricatorFile::getTransformableImageFormats();
        $v_name = $project->getName();
        $nameProject = $project->getNameProjectCompany();
        $v_nameProjectName = $nameProject ? array($nameProject) : $nameProject;
        $v_viewPolicy = $project->getViewPolicy();
        $v_editPolicy = $project->getEditPolicy();
        $v_openDate = $project->getOpendate();
        $v_closeDate = $project->getCloseDate();

        if ($request->isFormPost()) {
            $e_name = null;
            $e_file = null;
            $file = null;
            $xactions = array();
            $v_name = trim($request->getStr('name'));
            $v_nameProjectName = $request->getArr('nameProjectCompany');
            $v_viewPolicy = $request->getStr('viewPolicy');
            $v_editPolicy = $request->getStr('editPolicy');
            $v_openDate = PhabricatorPoToolsEditTaskController::formatDate($request->getStr('openDate'));
            $v_closeDate = PhabricatorPoToolsEditTaskController::formatDate($request->getStr('closeDate'));

            $phid = $request->getStr('phid');

            if ($phid) {
                $file = id(new PhabricatorFileQuery())
                    ->setViewer($viewer)
                    ->withPHIDs(array($phid))
                    ->executeOne();
            } else {
                if ($request->getFileExists('icon')) {
                    $file = PhabricatorFile::newFromPHPUpload(
                        $_FILES['icon'],
                        array(
                            'authorPHID' => $viewer->getPHID(),
                            'canCDN' => true,
                        ));
                }
            }

            $xform = PhabricatorFileTransform::getTransformByKey(
                PhabricatorFileThumbnailTransform::TRANSFORM_PROFILE);

            if ($file) {
                $xformed = $xform->executeTransform($file);
                $v_icon = $xformed->getPHID();
            }

            $type_name = PhabricatorProjectsNameTransaction::TRANSACTIONTYPE;
            $type_nameProjectCompany = PhabricatorProjectsNameProjectCompanyTransaction::TRANSACTIONTYPE;
            $type_icon = PhabricatorProjectsIconTransaction::TRANSACTIONTYPE;
            $type_open_date = PhabricatorProjectsOpenDateTransaction::TRANSACTIONTYPE;
            $type_close_date = phabricatorProjectsCloseDateTransaction::TRANSACTIONTYPE;
            $type_view = PhabricatorTransactions::TYPE_VIEW_POLICY;
            $type_edit = PhabricatorTransactions::TYPE_EDIT_POLICY;

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_name)
                ->setNewValue($v_name);

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_close_date)
                ->setNewValue(($v_closeDate));

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_open_date)
                ->setNewValue(($v_openDate));

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_nameProjectCompany)
                ->setNewValue(array_shift($v_nameProjectName));

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_view)
                ->setNewValue($v_viewPolicy);

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_icon)
                ->setNewValue($v_icon);

            $xactions[] = id(new PhabricatorProjectsTransaction())
                ->setTransactionType($type_edit)
                ->setNewValue($v_editPolicy);

            $editor = id(new PhabricatorProjectsEditor())
                ->setActor($viewer)
                ->setContinueOnNoEffect(true)
                ->setContentSourceFromRequest($request);

            try {
                session_start();
                $editor->applyTransactions($project, $xactions);
                $is_new ? $_SESSION['message'] = 'Project added successfully'
                    : $_SESSION['message'] = 'Project edit successfully';

                return id(new AphrontRedirectResponse())->setURI('/po-tools/query/joined/');
            } catch (PhabricatorApplicationTransactionValidationException $ex) {
                $validation_exception = $ex;
                $e_name = $ex->getShortMessage($type_name);
            }
        }

        $policies = id(new PhabricatorPolicyQuery())
            ->setViewer($viewer)
            ->setObject($project)
            ->execute();
        $projects_police = PhabricatorProjects::initializeNewProjects($viewer);
        $form = id(new AphrontFormView())
            ->setUser($viewer)
            ->setEncType('multipart/form-data');

        if ($v_icon) {

            $image = id(new PhabricatorFile())
                ->loadOneWhere('phid = %s', $v_icon);
            $project->attachProfileImageFile($image);

            $button = javelin_tag(
                'button',
                array(
                    'class' => 'button-grey profile-image-button',
                    'type' => 'button',
                    'style' => 'padding: 4px !important'

                ),
                phutil_tag(
                    'img',
                    array(
                        'height' => 50,
                        'width' => 50,
                        'src' => $project->getProfileImageURI(),
                    )));

            $button = array(
                phutil_tag(
                    'input',
                    array(
                        'type' => 'hidden',
                        'value' => $v_icon,
                    )),
                $button,
            );

            if ($id) {
                $form->appendChild(
                    id(new AphrontFormMarkupControl())
                        ->setLabel(pht('Current Icon'))
                        ->setValue($button));
            }

        }

        $form
            ->appendChild(
                id(new AphrontFormTextControl())
                    ->setLabel(pht('Project Name'))
                    ->setName('name')
                    ->setValue($v_name)
                    ->setError($e_name)
            )
            ->appendControl(
                id(new AphrontFormTokenizerKRControl())
                    ->setLabel(pht('Link to Project'))
                    ->setName('nameProjectCompany')
                    ->setLimit(1)
                    ->setDatasource(new PhabricatorProjectDatasource())
                    ->setValue($v_nameProjectName)
                    ->setID('projectPHID'))
            ->appendChild(
                id(new AphrontFormFileControl())
                    ->setName('icon')
                    ->setLabel('Icon')
                    ->setValue($v_icon)
                    ->setCaption(pht('Supported formats: %s', implode(', ', $supported_formats)))
            )
            ->appendChild(
                id(new AphrontFormDatePOControl())
                    ->setLabel(pht('Open Date'))
                    ->setName('openDate')
                    ->setID('OpenDateProject')
                    ->setValue($v_openDate)
            )
            ->appendChild(
                id(new AphrontFormDatePOControl())
                    ->setLabel(pht('Close Date'))
                    ->setName('closeDate')
                    ->setID('closeDateProject')
                    ->setValue($v_closeDate)
            )
            ->appendChild(
                id(new AphrontFormPolicyControl())
                    ->setUser($viewer)
                    ->setCapability(PhabricatorPolicyCapability::CAN_VIEW)
                    ->setPolicyObject($projects_police)
                    ->setPolicies($policies)
                    ->setValue($v_viewPolicy)
                    ->setName('viewPolicy'))
            ->appendChild(
                id(new AphrontFormPolicyControl())
                    ->setUser($viewer)
                    ->setCapability(PhabricatorPolicyCapability::CAN_EDIT)
                    ->setPolicyObject($projects_police)
                    ->setPolicies($policies)
                    ->setValue($v_editPolicy)
                    ->setName('editPolicy'))
            ->appendChild(
                id(new AphrontFormSubmitControl())
                    ->setValue($button_text)
                    ->addCancelButton($cancel_uri));

        $box = id(new PHUIObjectBoxView())
            ->setHeaderText($title)
            ->setBackground(PHUIObjectBoxView::WHITE_CONFIG)
            ->setValidationException($validation_exception)
            ->appendChild($form);

        $crumbs = $this->buildApplicationCrumbs();
        $is_new ? $crumbs->addTextCrumb($title)
            : $crumbs->addTextCrumb("Edit Project");

        $crumbs->setBorder(true);
        $view = id(new PHUITwoColumnView())
            ->setFooter(array(
                $box,
            ));

        return $this->newPage()
            ->setTitle($title)
            ->setCrumbs($crumbs)
            ->appendChild($view);
    }

}
