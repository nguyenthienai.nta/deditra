<?php

final class PhabricatorPoToolsEditReleaseSurveyController
extends PhabricatorProjectsBoardController
{

	public function shouldAllowPublic()
	{
		return true;
	}

	public function handleRequest(AphrontRequest $request)
	{
		$viewer = $request->getUser();
		$response = $this->loadProject();

		if ($response) {
			return $response;
		}

		$project = $this->getProject();
		$state = $this->getViewState();
		$saved = $state->getSavedQuery();

		if (!$saved) {
			return new Aphront404Response();
		}

		$layout_engine = $state->getLayoutEngine();
		$board_phid = $project->getPHID();
		$columns = $layout_engine->getColumns($board_phid);

		if (!$columns || !$project->getHasWorkboard()) {
			$has_normal_columns = false;

			foreach ($columns as $column) {
				if (!$column->getProxyPHID()) {
					$has_normal_columns = true;
					break;
				}
			}

			$can_edit = PhabricatorPolicyFilter::hasCapability(
				$viewer,
				$project,
				PhabricatorPolicyCapability::CAN_EDIT
			);

			if (!$has_normal_columns) {
				if (!$can_edit) {
					$content = $this->buildNoAccessContent($project);
				} else {
					$content = $this->buildInitializeContent($project);
				}
			} else {
				if (!$can_edit) {
					$content = $this->buildDisabledContent($project);
				} else {
					$content = $this->buildEnableContent($project);
				}
			}

			if ($content instanceof AphrontResponse) {
				return $content;
			}

			$nav = $this->newNavigation($project, PhabricatorProjects::ITEM_PRODUCT_VISION);
			$crumbs = $this->buildApplicationCrumbs();
			$crumbs->addTextCrumb(pht('NSS - Release Survey'));

			return $this->newPage()
				->setTitle(
					array(
						$project->getName(),
						pht('NSS - Release Survey'),
					)
				)
				->setNavigation($nav)
				->setCrumbs($crumbs)
				->appendChild($content);
		}
	}

	private function buildInitializeContent(PhabricatorProjects $project)
	{
		require_celerity_resource('releaseSurveyForm-css');
		$project_phid = $project->getPHID();


		$request = $this->getRequest();
		$viewer = $this->getViewer();
		
		$id = $request->getURIData('surveyId');


		if ($id) {
			$survey = id(new PhabricatorSurveyQuery())
				->setViewer($viewer)
				->withIDs(array($id))
				->withIDs(array($id))
				->executeOne();
			if (!$survey) {
				return new Aphront404Response();
			}else if ($survey->getProjectPHID() != $project_phid){
				return new Aphront404Response();
			}

			$is_new = false;
			$title = pht("Edit Survey");
		} else {
			$title = pht("Create Survey");
			$survey = PhabricatorSurvey::initializeNewSurvey($viewer);
			$survey->setprojectPHID($project_phid);
			$surveys = id(new PhabricatorSurvey())->loadAllWhere('projectPHID = %s', $project->getPHID());
			$surveysUrl = [];
			foreach ($surveys as $surveySub) {
				$surveysUrl[] = $surveySub->getUrl();
				$surveysUrl[] = str_replace(' ', '', $surveySub->getUrl());
			}
			do {
				$code = substr(md5(uniqid(rand())), -8);
			} while (in_array($code, $surveysUrl));
			$survey->setUrl($code);
			$survey->setStatus(0);
			$is_new = true;
		}

		$e_releaseName = true;
		$validation_exception = null;
		$v_releaseName = $survey->getReleaseName();


		if ($request->isFormPost()) {
			$xactions = array();
			$v_releaseName = trim($request->getStr('releaseName'));

			$type_releaseName = PhabricatorSurveyReleaseNameTransaction::TRANSACTIONTYPE;

			$xactions[] = id(new PhabricatorProjectsTransaction())
				->setTransactionType($type_releaseName)
				->setNewValue($v_releaseName);

			$editor = id(new PhabricatorSurveyEditor())
				->setActor($viewer)
				->setContinueOnNoEffect(true)
				->setContentSourceFromRequest($request);

			try {
				session_start();
				$editor->applyTransactions($survey, $xactions);

				$is_new ? $_SESSION['message'] = 'Survey added successfully'
					: $_SESSION['message'] = 'Survey edit successfully';

				return id(new AphrontRedirectResponse())->setURI("/po-tools/release-survey/{$project->getID()}/");
			} catch (PhabricatorApplicationTransactionValidationException $ex) {
				$validation_exception = $ex;
				$e_releaseName = $ex->getShortMessage($type_releaseName);
			}
		}

		Javelin::initBehavior('jquery');
		Javelin::initBehavior('survey-form');

		$form = id(new AphrontFormView())
			->setUser($viewer)
			->setEncType('multipart/form-data');

		$form
			->appendChild(
				id(new AphrontFormTextControl())
					->setLabel(pht('Release Name'))
					->setName('releaseName')
					->setID('release_name')
					->setValue($v_releaseName)
					->setError($e_releaseName)
			)
			->appendChild(
				id(new AphrontFormSurveyControl())
					->setLabel(pht('Survey'))
					->setName('survey')
					->setID('survey')
			)
			->appendChild(
				id(new AphrontFormSubmitControl())
					->setValue("Save")
					->addCancelButton("/po-tools/release-survey/{$project->getID()}")
			);


		$box = id(new PHUIObjectBoxView())
			->setHeaderText(pht('NSS - Release Survey'))
			->setValidationException($validation_exception)
			->setForm($form);

		return $box;
	}

	private function buildNoAccessContent(PhabricatorProjects $project)
	{
		$id = $project->getID();
		$profile_uri = $this->getApplicationURI("profile/{$id}/");

		return $this->newDialog()
			->setTitle(pht('Unable to Create Workboard'))
			->appendParagraph(
				pht(
					'The workboard for this project has not been created yet, ' .
						'but you do not have permission to create it. Only users ' .
						'who can edit this project can create a workboard for it.'
				)
			)
			->addCancelButton($profile_uri);
	}

	private function buildEnableContent(PhabricatorProjects $project)
	{
		$request = $this->getRequest();
		$viewer = $this->getViewer();
		$id = $project->getID();
		$profile_uri = $this->getApplicationURI("profile/{$id}/");
		$board_uri = $this->getApplicationURI("board/{$id}/");

		if ($request->isFormPost()) {
			$xactions = array();

			$xactions[] = id(new PhabricatorProjectsTransaction())
				->setTransactionType(
					PhabricatorProjectWorkboardTransaction::TRANSACTIONTYPE
				)
				->setNewValue(1);

			id(new PhabricatorProjectTransactionEditor())
				->setActor($viewer)
				->setContentSourceFromRequest($request)
				->setContinueOnNoEffect(true)
				->setContinueOnMissingFields(true)
				->applyTransactions($project, $xactions);

			return id(new AphrontRedirectResponse())
				->setURI($board_uri);
		}

		return $this->newDialog()
			->setTitle(pht('Workboard Disabled'))
			->addHiddenInput('initialize', 1)
			->appendParagraph(
				pht(
					'This workboard has been disabled, but can be restored to its ' .
						'former glory.'
				)
			)
			->addCancelButton($profile_uri)
			->addSubmitButton(pht('Enable Workboard'));
	}

	private function buildDisabledContent(PhabricatorProjects $project)
	{
		$viewer = $this->getViewer();

		$id = $project->getID();

		$profile_uri = $this->getApplicationURI("profile/{$id}/");

		return $this->newDialog()
			->setTitle(pht('Workboard Disabled'))
			->appendParagraph(
				pht(
					'This workboard has been disabled, and you do not have permission ' .
						'to enable it. Only users who can edit this project can restore ' .
						'the workboard.'
				)
			)
			->addCancelButton($profile_uri);
	}
}
