<?php

abstract class PhabricatorProjectsController extends PhabricatorController
{

    private $project;
    private $profileMenu;
    private $profileMenuEngine;

    protected function setProject(PhabricatorProjects $project)
    {
        $this->project = $project;
        return $this;
    }

    protected function getProject()
    {
        return $this->project;
    }

    protected function loadProject()
    {
        $viewer = $this->getViewer();
        $request = $this->getRequest();
        $capabilities = array(
            PhabricatorPolicyCapability::CAN_VIEW,
        );
        $query = id(new PhabricatorProjectsQuery())
            ->setViewer($viewer)
            ->requireCapabilities($capabilities);
        $id = nonempty(
            $request->getURIData('projectID'),
            $request->getURIData('id'));
        try {
            $query->withIDs(array($id));
            $project = $query->executeOne();

        } catch (PhabricatorPolicyException $ex) {
            return new Aphront404Response();
        }

        if(!$project){
            return new Aphront404Response();
        }

        $this->setProject($project);
        return $this->loadProjectWithCapabilities(
            array(
                PhabricatorPolicyCapability::CAN_VIEW,
            ));
    }

    protected function loadProjectForEdit()
    {
        return $this->loadProjectWithCapabilities(
            array(
                PhabricatorPolicyCapability::CAN_VIEW,
                PhabricatorPolicyCapability::CAN_EDIT,
            ));
    }

    private function loadProjectWithCapabilities(array $capabilities)
    {
        $viewer = $this->getViewer();
        $request = $this->getRequest();
        $id = nonempty(
            $request->getURIData('projectID'),
            $request->getURIData('id'));

        $query = id(new PhabricatorProjectsQuery())
            ->setViewer($viewer)
            ->requireCapabilities($capabilities);

        $query->withIDs(array($id));

        $policy_exception = null;
        try {
            $project = $query->executeOne();
        } catch (PhabricatorPolicyException $ex) {
            $policy_exception = $ex;
            $project = null;
        }

        if (!$project) {
            if (!$policy_exception) {
                return new Aphront404Response();
            }

            throw $policy_exception;
        }

        $this->setProject($project);

        return null;
    }

    protected function buildApplicationCrumbs()
    {
        return $this->newApplicationCrumbs('profile');
    }

    protected function newWorkboardCrumbs()
    {
        return $this->newApplicationCrumbs('workboard');
    }

    private function newApplicationCrumbs($mode)
    {
        $crumbs = parent::buildApplicationCrumbs();
        $project = $this->getProject();

        if ($project) {
            $ancestors[] = $project;

            foreach ($ancestors as $ancestor) {
                if ($ancestor->getPHID() === $project->getPHID()) {
                    $crumb_uri = "#";
                } else {
                    switch ($mode) {
                        case 'workboard':
                            if ($ancestor->getHasWorkboard()) {
                                $crumb_uri = "#";
                            } else {
                                $crumb_uri = "#";
                            }
                            break;
                        case 'profile':
                        default:
                            $crumb_uri = "#";
                            break;
                    }
                }

                $crumbs->addTextCrumb($ancestor->getName(), $crumb_uri);
            }
        }

        return $crumbs;
    }

    protected function getProfileMenuEngine()
    {
        if (!$this->profileMenuEngine) {
            $viewer = $this->getViewer();
            $project = $this->getProject();

            if ($project) {
                $engine = id(new PhabricatorProjectsProfileMenuEngine())
                    ->setViewer($viewer)
                    ->setController($this)
                    ->setProfileObject($project);

                $this->profileMenuEngine = $engine;
            }
        }

        return $this->profileMenuEngine;
    }

    protected function setProfileMenuEngine(PhabricatorProjectsProfileMenuEngine $engine)
    {
        $this->profileMenuEngine = $engine;
        return $this;
    }

    final protected function newNavigation(PhabricatorProjects $project, $item_identifier)
    {
        $engine = $this->getProfileMenuEngine();
        $view_list = $engine->newProfileMenuItemViewList();

        if ($view_list->getViewsWithItemIdentifier($item_identifier)) {
            $view_list->setSelectedViewWithItemIdentifier($item_identifier);
        }

        $navigation = $view_list->newNavigationView();

        if ($item_identifier === PhabricatorProjects::ITEM_CHART) {
            $navigation->addClass('project-board-nav');
        }

        return $navigation;
    }

}
