<?php

final class PhabricatorPoToolTaskPHIDType extends PhabricatorPHIDType {

    const TYPECONST = 'GACH';

    public function getTypeName() {
        return pht('GantChart');
    }

    public function newObject() {
        return new PhabricatorOkrsCurrent();
    }

    public function getPHIDTypeApplicationClass() {
        return 'PhabricatorPoToolKitsApplication';
    }

    protected function buildQueryForObjects(
        PhabricatorObjectQuery $query,
        array $phids) {

        return id(new PhabricatorTaskQuery())
            ->withPHIDs($phids);
    }

    public function loadHandles(
        PhabricatorHandleQuery $query,
        array $handles,
        array $objects) {

    }

}