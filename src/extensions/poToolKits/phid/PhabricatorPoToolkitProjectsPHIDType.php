<?php

final class PhabricatorPoToolkitProjectsPHIDType extends PhabricatorPHIDType {

    const TYPECONST = 'POTK';

    public function getTypeName() {
        return pht('Projects');
    }

    public function newObject() {
        return new PhabricatorOkrsCurrent();
    }

    public function getPHIDTypeApplicationClass() {
        return 'PhabricatorPoToolKitsApplication';
    }

    protected function buildQueryForObjects(
        PhabricatorObjectQuery $query,
        array $phids) {

        return id(new PhabricatorProjectsQuery())
            ->withPHIDs($phids);
    }

    public function loadHandles(
        PhabricatorHandleQuery $query,
        array $handles,
        array $objects) {

    }

}
