<?php

final class PhabricatorPoToolSurveyPHIDType extends PhabricatorPHIDType {

    const TYPECONST = 'SURV';

    public function getTypeName() {
        return pht('Survey');
    }

    public function newObject() {
        return new PhabricatorSurvey();
    }

    public function getPHIDTypeApplicationClass() {
        return 'PhabricatorPoToolKitsApplication';
    }

    protected function buildQueryForObjects(
        PhabricatorObjectQuery $query,
        array $phids) {

        return id(new PhabricatorSurveyQuery())
            ->withPHIDs($phids);
    }

    public function loadHandles(
        PhabricatorHandleQuery $query,
        array $handles,
        array $objects) {

    }

}