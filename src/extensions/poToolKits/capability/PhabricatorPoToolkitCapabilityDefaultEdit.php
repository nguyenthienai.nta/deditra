<?php

final class PhabricatorPoToolkitCapabilityDefaultEdit
    extends PhabricatorPolicyCapability
{

    const CAPABILITY = 'po_toolkit.default.edit';

    public function getCapabilityName()
    {
        return pht('Default Edit Policy');
    }

}
