<?php

final class PhabricatorPoToolkitCapabilityDefaultView
    extends PhabricatorPolicyCapability
{

    const CAPABILITY = 'po_toolkit.default.view';

    public function getCapabilityName()
    {
        return pht('Default View Policy');
    }

    public function shouldAllowPublicPolicySetting()
    {
        return true;
    }

}
