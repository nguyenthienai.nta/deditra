<?php

final class PhabricatorPoToolkitCapabilityCreateSpaces
    extends PhabricatorPolicyCapability
{

    const CAPABILITY = 'po_toolkit.create';

    public function getCapabilityName()
    {
        return pht('Can Create Po toolkit');
    }

    public function describeCapabilityRejection()
    {
        return pht('You do not have permission to create Po toolkit.');
    }

}
