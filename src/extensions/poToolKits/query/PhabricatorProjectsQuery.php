<?php

final class PhabricatorProjectsQuery
    extends PhabricatorCursorPagedPolicyAwareQuery
{

    const KEY_ALL = 'projects.all';
    const KEY_DEFAULT = 'projects.default';
    const KEY_VIEWER = 'projects.viewer';

    private $ids;
    private $phids;
    private $isDefaultNamespace;
    private $isArchived;
    private $authorPHID;

    public function withIDs(array $ids)
    {
        $this->ids = $ids;
        return $this;
    }

    public function withAuthorPHID(array $watcher_phids)
    {
        $this->authorPHID = $watcher_phids;
        return $this;
    }

    public function withPHIDs(array $phids)
    {
        $this->phids = $phids;
        return $this;
    }

    public function withIsDefaultNamespace($default)
    {
        $this->isDefaultNamespace = $default;
        return $this;
    }

    public function withIsArchived($archived)
    {
        $this->isArchived = $archived;
        return $this;
    }

    public function getQueryApplicationClass()
    {
        return 'PhabricatorPoToolKitsApplication';
    }

    protected function loadPage()
    {
        return $this->loadStandardPage(new PhabricatorProjects());
    }

    protected function buildWhereClauseParts(AphrontDatabaseConnection $conn)
    {
        $where = parent::buildWhereClauseParts($conn);

        if ($this->ids !== null) {
            $where[] = qsprintf(
                $conn,
                'id IN (%Ld)',
                $this->ids);
        }

        if ($this->phids !== null) {
            $where[] = qsprintf(
                $conn,
                'phid IN (%Ls)',
                $this->phids);
        }

        if ($this->isArchived !== null) {
            $where[] = qsprintf(
                $conn,
                'isArchived = %d',
                (int)$this->isArchived);
        }

        if ($this->authorPHID !== null) {
          $where[] = qsprintf(
            $conn,
            'authorPHID IN (%Ls)',
            $this->authorPHID);
        }


      return $where;
    }

}
