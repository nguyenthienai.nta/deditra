<?php

final class PhabricatorSurveyQuery
    extends PhabricatorCursorPagedPolicyAwareQuery
{

    const KEY_ALL = 'survey.all';
    const KEY_DEFAULT = 'survey.default';
    const KEY_VIEWER = 'survey.viewer';

    private $ids;
    private $phids;

    public function withIDs(array $ids)
    {
        $this->ids = $ids;
        return $this;
    }

    public function withPHIDs(array $phids)
    {
        $this->phids = $phids;
        return $this;
    }

    public function getQueryApplicationClass()
    {
        return 'PhabricatorPoToolKitsApplication';
    }

    protected function loadPage()
    {
        return $this->loadStandardPage(new PhabricatorSurvey());
    }

    protected function buildWhereClauseParts(AphrontDatabaseConnection $conn)
    {
        $where = parent::buildWhereClauseParts($conn);

        if ($this->ids !== null) {
            $where[] = qsprintf(
                $conn,
                'id IN (%Ld)',
                $this->ids);
        }

        if ($this->phids !== null) {
            $where[] = qsprintf(
                $conn,
                'phid IN (%Ls)',
                $this->phids);
        }

        return $where;
    }

}
