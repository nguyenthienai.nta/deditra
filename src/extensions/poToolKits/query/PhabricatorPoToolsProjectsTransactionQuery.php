<?php

final class PhabricatorPoToolsProjectsTransactionQuery
    extends PhabricatorApplicationTransactionQuery
{

    public function getTemplateApplicationTransaction()
    {
        return new PhabricatorProjectsTransaction();
    }

}
