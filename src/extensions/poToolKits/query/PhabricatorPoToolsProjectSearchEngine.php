<?php

final class PhabricatorPoToolsProjectSearchEngine extends PhabricatorApplicationSearchEngine
{
    public function getApplicationClassName()
    {
        return 'PhabricatorPoToolKitsApplication';
    }

    public function getResultTypeDescription()
    {
        return pht('Project');
    }

    public function newQuery()
    {
        return id(new PhabricatorProjectsQuery());

    }

    protected function buildCustomSearchFields()
    {
        return array(
            id(new PhabricatorSearchThreeStateField())
                ->setLabel(pht('Active'))
                ->setKey('active')
                ->setOptions(
                    pht('(Show All)'),
                    pht('Show Only Active Project'),
                    pht('Hide Active Project')),
            id(new PhabricatorSearchThreeStateField())
                ->setLabel(pht('Joined'))
                ->setKey('joined')
                ->setOptions(
                    pht('(Show All)'),
                    pht('Show Only Active Project'),
                    pht('Hide Active Project')),
        );
    }

    protected function buildQueryFromParameters(array $map)
    {
        $query = $this->newQuery();

        if ($map['active']) {
            $query->withIsArchived(!$map['active']);
        }

        if ($map['joined']) {
            $query->withAuthorPHID([$this->requireViewer()->getPHID()]);
        }

        return $query;
    }

    protected function getURI($path)
    {
        return '/po-tools/' . $path;
    }

    protected function getBuiltinQueryNames()
    {
        $names = array(
            'joined' => pht('Joined'),
            'active' => pht('Active Project'),
            'all' => pht('All Project'),
        );

        return $names;
    }

    public function buildSavedQueryFromBuiltin($query_key)
    {
        $query = $this->newSavedQuery();
        $query->setQueryKey($query_key);

        switch ($query_key) {
            case 'active':
                return $query->setParameter('active', true);
            case 'all':
                return $query;
            case 'joined':
                return $query->setParameter('joined', true);

        }

        return parent::buildSavedQueryFromBuiltin($query_key);
    }

    protected function renderResultList(array $projects, PhabricatorSavedQuery $query, array $handles)
    {
        $viewer = $this->requireViewer();
        assert_instances_of($projects, 'PhabricatorProjects');
        $list = [];
        session_start();
        $check_display_project = $query->getQueryKey() === 'joined';

        if (isset($_SESSION['message'])) {
            $list[] = id(new PHUIInfoView())
                ->setSeverity(PHUIInfoView::SEVERITY_NOTICE)
                ->appendChild($_SESSION['message']);
            unset($_SESSION['message']);
        }
        $list[] = id(new PhabricatorPoToolKitProjectListView())
            ->setDisplayProject($check_display_project)
            ->setUser($viewer)
            ->setProjects($projects)
            ->setShowWatching(true)
            ->setShowMember(true)
            ->renderList();

        $result = new PhabricatorApplicationSearchResultView();
        $result->setContent($list);
        $result->setNoDataString(pht('No project found.'));

        $viewer = $this->requireViewer();

        return $result;
    }

    protected function getNewUserBody()
    {
        $no_data = pht('No projects found.');
        $list = id(new PHUIObjectItemListView())
            ->setNoDataString($no_data);
        return $list;
    }
}
