<?php

final class PhabricatorPoToolKitProjectListView extends AphrontView
{

    private $projects;
    private $showMember;
    private $showWatching;
    private $noDataString;
    private $displayProject;

    public function setProjects(array $projects)
    {
        $this->projects = $projects;
        return $this;
    }

    public function getProjects()
    {
        return $this->projects;
    }

    public function setShowWatching($watching)
    {
        $this->showWatching = $watching;
        return $this;
    }

    public function setShowMember($member)
    {
        $this->showMember = $member;
        return $this;
    }

    public function setNoDataString($text)
    {
        $this->noDataString = $text;
        return $this;
    }

    public function getDisplayProject()
    {
      return $this->displayProject;
    }

    public function setDisplayProject($displayProject)
    {
        $this->displayProject = $displayProject;

        return $this;
    }

    public function renderList()
    {
        $viewer = $this->getUser();
        $projects = $this->getProjects();
        $viewer_phid = $viewer->getPHID();

        $handles = $viewer->loadHandles(mpull($projects, 'getPHID'));

        $no_data = pht('No projects found.');
        if ($this->noDataString) {
            $no_data = $this->noDataString;
        }

        $list = id(new PHUIObjectItemListView())
            ->setUser($viewer)
            ->setNoDataString($no_data);

        foreach ($projects as $key => $project) {
            if ($this->getDisplayProject() === true) {
                if ($project->getIsArchived() === '0') {
                    $this->getItemProject($project, $viewer, $list);
                }
            } else {
                $this->getItemProject($project, $viewer, $list);
            }
        }

        return $list;
    }

    public function render()
    {
        return $this->renderList();
    }

  /**
   * @param $project
   * @param PhabricatorUser $viewer
   * @param $list
   */
    public function getItemProject($project, PhabricatorUser $viewer, $list)
    {
        $id = $project->getID();
        $iconPhid = $project->getIcon();
        $hidden_project = '';

        $item = id(new PHUIObjectItemView())
            ->setObject($project)
            ->setHeader($project->getName())
            ->setId($hidden_project)
            ->setHref("/po-tools/product-vision/{$id}");

        $image = id(new PhabricatorFile())
            ->loadOneWhere('phid = %s', $iconPhid);
        if (!empty($image)) {
            $project->attachProfileImageFile($image);
        } else {
            $builtin_name = PhabricatorProjectIconSet::getIconImage('project');
            $builtin_name = 'projects/' . $builtin_name;
            $builtin_file = PhabricatorFile::loadBuiltin(
                $viewer,
                $builtin_name);
            $project->attachProfileImageFile($builtin_file);
        }
        if ($project->getStatus() == 1) {
            $item->addIcon('fa-ban', pht('Archived'));
            $item->setDisabled(true);
        }
        $item = $item->setImageURI($project->getProfileImageURI());


        $list->addItem($item);
    }

}
