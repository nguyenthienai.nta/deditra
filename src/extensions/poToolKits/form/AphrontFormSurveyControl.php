<?php

final class AphrontFormSurveyControl extends AphrontFormControl
{

  private $disableAutocomplete;
  private $sigil;
  private $placeholder;
  private $autofocus;
  private $id;
  private $value;

  public function setDisableAutocomplete($disable)
  {
    $this->disableAutocomplete = $disable;
    return $this;
  }

  private function getDisableAutocomplete()
  {
    return $this->disableAutocomplete;
  }

  public function getPlaceholder()
  {
    return $this->placeholder;
  }

  public function setPlaceholder($placeholder)
  {
    $this->placeholder = $placeholder;
    return $this;
  }

  public function setAutofocus($autofocus)
  {
    $this->autofocus = $autofocus;
    return $this;
  }

  public function getAutofocus()
  {
    return $this->autofocus;
  }

  public function setID($id)
  {
    $this->id = $id;
    return $this;
  }

  public function getID()
  {
    return $this->id;
  }

  public function getSigil()
  {
    return $this->sigil;
  }

  public function setSigil($sigil)
  {
    $this->sigil = $sigil;
    return $this;
  }

  protected function getCustomControlClass()
  {
    return 'aphront-form-control-text';
  }

  public function getValue()
  {
    $value = str_replace('/', '-', $this->value);
    $value = $value ? date('d/m/Y', $value) : null;
    return $value;
  }

  public function setValue($value)
  {
    $this->value = $value;
    return $this;
  }

  protected function renderInput()
  {
    $happy = javelin_tag(
      'div',
      array(
        'class' => 'feedback',
      ),
      [javelin_tag(
        'div',
        array(
          'class' => 'emotion_img',
        ),
        [
          javelin_tag(
            'img',
            array(
              'class' => 'happy_emo',
              'src' => '/res/phabricator/0d17c6c4/rsrc/image/normal.png'
            ))]),
        javelin_tag(
          'p',
          array(
            'class' => 'emotion_decription'
          ,),
          '満足')]
    );

    $normal = javelin_tag(
      'div',
      array(
        'class' => 'feedback',
      ), [javelin_tag(
        'div',
        array(
          'class' => 'emotion_img',
        ),
        [
          javelin_tag(
            'img',
            array(
              'class' => 'happy_emo',
              'src' => '/res/phabricator/0d17c6c4/rsrc/image/suspicious.png'
            ))]),
        javelin_tag(
          'p',
          array(
            'class' => 'emotion_decription'
          ),
          'どちらともいえない')]
    );

    $unhappy = javelin_tag(
      'div',
      array(
        'class' => 'feedback',
      ), [javelin_tag(
        'div',
        array(
          'class' => 'emotion_img',
        ),
        [
          javelin_tag(
            'img',
            array(
              'class' => 'happy_emo',
              'src' => '/res/phabricator/0d17c6c4/rsrc/image/sad.png'
            ))]),
        javelin_tag(
          'p',
          array(
            'class' => 'emotion_decription'
          ),
          '不満')]
    );

    $title_and_decription = javelin_tag(
      'div',
      array(
        'class' => '',
      ),
      [
        javelin_tag(
          'h2',
          array(
            'class' => 'title'
          ),
          'お客様満足度調査のアンケート'),
        javelin_tag(
          'p',
          array(
            'class' => 'decription',
          ),
          'DEHAでは、エンジニアの技術力や、対応レベルの向上に取り組み、高品質の商品・サービスを提供するだけでなく、コスパの最適な提案をしていくことで、お客様の満足度の向上につなげたいと考えております。この度、本アンケートを実地しておりますので、是非ともご協力ください。
           お答えいただいた内容は、弊社のお客様対応の向上と業務改善に向けた活動の大切な資料としてのみご利用させていただきます。'),
      ]);

    $feedback_input = javelin_tag(
      'div',
      array(
        'class' => '',
      ),
      [javelin_tag(
        'div',
        array(
          'class' => 'title-block',
        ),
        [javelin_tag(
          'p',
          array(
            'class' => 'title'
          ),
          '1. 今回のリリース版についてご評価ください'),
          javelin_tag(
            'span',
            array(
              'style' => 'color: red; margin-left: 5px; font-size: 18px'
            ),
            '*')
        ]
      )
        ,

        javelin_tag(
          'div',
          array(
            'class' => 'feedback-block',
          ),
          [
            javelin_tag(
              'div',
              array(
                'class' => 'emotion-block',
              ),
              [
                $happy
              ]
            ),
            javelin_tag(
              'div',
              array(
                'class' => 'emotion-block',
              ),
              [
                $normal
              ]
            ),
            javelin_tag(
              'div',
              array(
                'class' => 'emotion-block',
              ),
              [
                $unhappy
              ]
            ),
          ]
        ),

        javelin_tag(
          'p',
          array(
            'class' => 'title'
          ),
          '2. 他のコメントがございましたらお教えください'),

        javelin_tag(
          'textarea',
          array(
            'name' => $this->getName(),
            'value' => $this->getValue(),
            'disabled' => $this->getDisabled() ? 'disabled' : null,
            'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
            'id' => $this->getID(),
            'sigil' => $this->getSigil(),
            'placeholder' => $this->getPlaceholder(),
            'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
            'readonly' => true
          ))
      ]);

    return
      javelin_tag(
        'div',
        array(
          'class' => 'container',
        ),
        [phutil_tag(
          'kr-edit',
          array(
            'class' => 'edit-survey',
          ),
          id(new PHUIButtonOKRView())
            ->setTag('a')
            ->setHref('#')
            ->setIcon('fa-pencil')),
          javelin_tag(
            'div',
            array(
              'class' => 'survey-container',
            ),
            [
              $title_and_decription,
              $feedback_input,
            ]),
        ]);
  }

}
