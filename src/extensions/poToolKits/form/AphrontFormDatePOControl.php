<?php

final class AphrontFormDatePOControl extends AphrontFormControl {

    private $disableAutocomplete;
    private $sigil;
    private $placeholder;
    private $autofocus;
    private $id;
    private $value;

    public function setDisableAutocomplete($disable)
    {
        $this->disableAutocomplete = $disable;
        return $this;
    }

    private function getDisableAutocomplete()
    {
        return $this->disableAutocomplete;
    }

    public function getPlaceholder()
    {
        return $this->placeholder;
    }

    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    public function setAutofocus($autofocus)
    {
        $this->autofocus = $autofocus;
        return $this;
    }

    public function getAutofocus()
    {
        return $this->autofocus;
    }

    public function setID($id)
    {
       $this->id = $id;
       return $this;
    }

    public function getID()
    {
       return $this->id;
    }

    public function getSigil()
    {
        return $this->sigil;
    }

    public function setSigil($sigil)
    {
        $this->sigil = $sigil;
        return $this;
    }

    protected function getCustomControlClass()
    {
        return 'aphront-form-control-text';
    }

    public function getValue()
    {
        $value = str_replace('/', '-', $this->value);
        $value = $value ? date('d/m/Y', $value) : null;
        return $value;
    }

    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    protected function renderInput()
    {
        return javelin_tag(
            'div',
            array(),
            [
                phutil_tag(
                    'input',
                    array(
                        'type' => 'text',
                        'id' => $this->getID(),
                        'class' => 'startDate',
                        'name' => $this->getName(),
                        'value' => $this->getValue(),
                        'disabled' => $this->getDisabled() ? 'disabled' : null,
                        'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
                        'sigil' => $this->getSigil(),
                        'placeholder' => $this->getPlaceholder(),
                        'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
                        'readonly' => 'true'
                    ))
            ]
        );
    }

}
