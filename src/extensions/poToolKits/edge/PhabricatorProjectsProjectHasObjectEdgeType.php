<?php

final class PhabricatorProjectsProjectHasObjectEdgeType
    extends PhabricatorEdgeType
{

    const EDGECONST = 4212;

    public function getInverseEdgeConstant()
    {
        return PhabricatorProjectsObjectHasProjectEdgeType::EDGECONST;
    }

}
