<?php

final class PhabricatorProjectsProfileMenuEngine
    extends PhabricatorProfileMenuEngine
{

    protected function isMenuEngineConfigurable()
    {
        return true;
    }

    protected function isMenuEnginePersonalizable()
    {
        return false;
    }

    public function getItemURI($path)
    {
        $project = $this->getProfileObject();

        $id = $project->getID();
        return "/po-tools/{$id}/edit/{$path}";
    }

    protected function getBuiltinProfileItems($object)
    {
        $items = array();

        $items[] = $this->newItem()
            ->setBuiltinKey(PhabricatorProjects::ITEM_PICTURE)
            ->setMenuItemKey(PhabricatorProjectsPictureProfileMenuItem::MENUITEMKEY)
            ->setIsHeadItem(true);

        $items[] = $this->newItem()
            ->setBuiltinKey(PhabricatorProjects::ITEM_PRODUCT_VISION)
            ->setMenuItemKey(PhabricatorProjectsProductVisionProfileMenuItem::MENUITEMKEY);

        $items[] = $this->newItem()
            ->setBuiltinKey(PhabricatorProjects::ITEM_CHART)
            ->setMenuItemKey(PhabricatorProjectsWorkboardProfileMenuItem::MENUITEMKEY);

        $items[] = $this->newItem()
            ->setBuiltinKey(PhabricatorProjects::ITEM_PROJECTS)
            ->setMenuItemKey(PhabricatorProjectsSubprojectProfileMenuItem::MENUITEMKEY);

        $items[] = $this->newItem()
            ->setBuiltinKey(PhabricatorProjects::ITEM_CUSTOMER)
            ->setMenuItemKey(PhabricatorProjectsMembersProfileMenuItem::MENUITEMKEY);

        $items[] = $this->newItem()
            ->setBuiltinKey(PhabricatorProjects::ITEM_SURVEY)
            ->setMenuItemKey(PhabricatorProjectsNSSReleaseSurveyMenuItem::MENUITEMKEY);

        $items[] = $this->newItem()
            ->setBuiltinKey(PhabricatorProjects::ITEM_MANAGE)
            ->setMenuItemKey(PhabricatorProjectsManageProfileMenuItem::MENUITEMKEY)
            ->setIsTailItem(true);

        return $items;
    }


}
