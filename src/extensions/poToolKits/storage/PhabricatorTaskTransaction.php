<?php

final class PhabricatorTaskTransaction
    extends PhabricatorModularTransaction
{

    public function getApplicationName()
    {
        return 'po_toolkit';
    }

    public function getApplicationTransactionType()
    {
        return PhabricatorPoToolTaskPHIDType::TYPECONST;
    }

    public function getBaseTransactionClass()
    {
        return 'PhabricatorTaskTransactionType';
    }

}
