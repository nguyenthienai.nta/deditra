<?php


class PhabricatorSurvey extends PhabricatorLiskDAO implements
    PhabricatorPolicyInterface,
    PhabricatorApplicationTransactionInterface
{

    protected $id;
    protected $phid;
    protected $projectPHID;
    protected $releaseName;
    protected $url;
    protected $status;
    protected $point;
    protected $comment;
    protected $dateCreated;
    protected $dateModified;

    public function getApplicationName()
    {
        return 'po_toolkit';
    }

    protected function getConfiguration()
    {
        return array(
                self::CONFIG_AUX_PHID => true,
            ) + parent::getConfiguration();
    }

    public static function initializeNewSurvey(PhabricatorUser $actor)
    {
        return id(new PhabricatorSurvey());
    }

    public function generatePHID()
    {
        return PhabricatorPHID::generateNewPHID(
            PhabricatorPoToolSurveyPHIDType::TYPECONST);
    }

    public function getCapabilities()
    {
        // TODO: Implement getCapabilities() method.
        return array(
            PhabricatorPolicyCapability::CAN_VIEW,
            PhabricatorPolicyCapability::CAN_EDIT,
        );
    }

    public function getPolicy($capability)
    {
        // TODO: Implement getPolicy() method.
    }

    public function hasAutomaticCapability($capability, PhabricatorUser $viewer)
    {
        // TODO: Implement hasAutomaticCapability() method.
        return false;
    }

    /**
     * Return a @{class:PhabricatorApplicationTransactionEditor} which can be
     * used to apply transactions to this object.
     *
     * @return PhabricatorApplicationTransactionEditor Editor for this object.
     */
    public function getApplicationTransactionEditor()
    {
        return new PhabricatorSurveyEditor();
    }

    /**
     * Return a template transaction for this object.
     *
     * @return PhabricatorApplicationTransaction
     */
    public function getApplicationTransactionTemplate()
    {
        return new PhabricatorSurveyTransaction();
    }
}