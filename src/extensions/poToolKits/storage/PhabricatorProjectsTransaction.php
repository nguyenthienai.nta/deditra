<?php

final class PhabricatorProjectsTransaction
    extends PhabricatorModularTransaction
{

    public function getApplicationName()
    {
        return 'po_toolkit';
    }

    public function getApplicationTransactionType()
    {
        return PhabricatorPoToolkitProjectsPHIDType::TYPECONST;
    }

    public function getBaseTransactionClass()
    {
        return 'PhabricatorProjectsTransactionType';
    }

}
