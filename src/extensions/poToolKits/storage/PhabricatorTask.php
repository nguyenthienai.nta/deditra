<?php


class PhabricatorTask extends PhabricatorLiskDAO implements
    PhabricatorPolicyInterface,
    PhabricatorApplicationTransactionInterface
{

    protected $id;
    protected $projectPHID;
    protected $nameTask;
    protected $startDate;
    protected $endDate;
    protected $parentTaskPHID;
    protected $progress;
    protected $dateCreated;
    protected $dateModified;
    protected $isDeleted;
    protected $isParentTask;


    /**
     * @task config
     */
    public function getApplicationName()
    {
        return 'po_toolkit';
    }

    protected function getConfiguration()
    {
        return array(
                self::CONFIG_AUX_PHID => true,
            ) + parent::getConfiguration();
    }

    public static function initializeNewGantChart(PhabricatorUser $actor)
    {
        return id(new PhabricatorTask());
    }

    public function generatePHID()
    {
        return PhabricatorPHID::generateNewPHID(
            PhabricatorPoToolTaskPHIDType::TYPECONST);
    }

    public function getCapabilities()
    {
        // TODO: Implement getCapabilities() method.
        return array(
            PhabricatorPolicyCapability::CAN_VIEW,
            PhabricatorPolicyCapability::CAN_EDIT,
        );
    }

    public function getPolicy($capability)
    {
        // TODO: Implement getPolicy() method.
    }

    public function hasAutomaticCapability($capability, PhabricatorUser $viewer)
    {
        // TODO: Implement hasAutomaticCapability() method.
        return false;
    }

    /**
     * Return a @{class:PhabricatorApplicationTransactionEditor} which can be
     * used to apply transactions to this object.
     *
     * @return PhabricatorApplicationTransactionEditor Editor for this object.
     */
    public function getApplicationTransactionEditor()
    {
        return new PhabricatorTaskEditor();
    }

    /**
     * Return a template transaction for this object.
     *
     * @return PhabricatorApplicationTransaction
     */
    public function getApplicationTransactionTemplate()
    {
        return new PhabricatorTaskTransaction();
    }
}