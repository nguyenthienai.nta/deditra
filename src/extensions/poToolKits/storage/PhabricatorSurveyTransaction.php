<?php

final class PhabricatorSurveyTransaction
    extends PhabricatorModularTransaction
{

    public function getApplicationName()
    {
        return 'po_toolkit';
    }

    public function getApplicationTransactionType()
    {
        return PhabricatorPoToolSurveyPHIDType::TYPECONST;
    }

    public function getBaseTransactionClass()
    {
        return 'PhabricatorSurveyTransactionType';
    }

}
