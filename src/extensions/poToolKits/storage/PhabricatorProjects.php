<?php

final class PhabricatorProjects
    extends PhabricatorPOToolkitDAO
    implements
    PhabricatorPolicyInterface,
    PhabricatorApplicationTransactionInterface,
    PhabricatorDestructibleInterface,
    PhabricatorCustomFieldInterface
{

    protected $name;
    protected $nameProjectCompany;
    protected $viewPolicy;
    protected $editPolicy;
    protected $icon;
    protected $isArchived;
    protected $authorPHID;
    protected $customer;
    protected $who;
    protected $productName;
    protected $classify;
    protected $benefit;
    protected $unlike;
    protected $ourProduct;
    protected $updateTask;
    protected $openDate;
    protected $closeDate;

    private $customFields = self::ATTACHABLE;

    const ITEM_PICTURE = 'projects.picture';
    const ITEM_CHART = 'projects.chart';
    const ITEM_CUSTOMER = 'projects.customer';
    const ITEM_MANAGE = 'projects.manage';
    const ITEM_PROJECTS = 'projects.projects';
    const ITEM_PROFILE = 'projects.profile';
    const ITEM_SURVEY = 'projects.survey';
    const ITEM_PRODUCT_VISION = 'projects.productVision';

    public static function initializeNewProjects(PhabricatorUser $actor)
    {
        $app = id(new PhabricatorApplicationQuery())
            ->setViewer($actor)
            ->withClasses(array('PhabricatorPoToolKitsApplication'))
            ->executeOne();

        $view_policy = $app->getPolicy(
            PhabricatorPoToolkitCapabilityDefaultView::CAPABILITY);
        $edit_policy = $app->getPolicy(
            PhabricatorPoToolkitCapabilityDefaultEdit::CAPABILITY);

        return id(new PhabricatorProjects())
            ->setViewPolicy($view_policy)
            ->setEditPolicy($edit_policy)
            ->setAuthorPHID($actor->getPHID())
            ->setIsArchived(0);
    }

    public function getStatus()
    {
      return $this->isArchived;
    }

    protected function getConfiguration()
    {
        return array(
                self::CONFIG_AUX_PHID => true,
                self::CONFIG_COLUMN_SCHEMA => array(
                    'name' => 'text255',
                    'nameProjectCompany' => 'text255',
                    'isArchived' => 'bool',
                ),
                self::CONFIG_KEY_SCHEMA => array(
                    'key_default' => array(),
                ),
            ) + parent::getConfiguration();
    }

    public function attachProfileImageFile(PhabricatorFile $file)
    {
        $this->profileImageFile = $file;
        return $this;
    }

    public function generatePHID()
    {
        return PhabricatorPHID::generateNewPHID(
            PhabricatorPoToolkitProjectsPHIDType::TYPECONST);
    }

    public function getMonogram()
    {
        return 'P' . $this->getID();
    }

    public function getCapabilities()
    {
        return array(
            PhabricatorPolicyCapability::CAN_VIEW,
            PhabricatorPolicyCapability::CAN_EDIT,
        );
    }

    public function getPolicy($capability)
    {
        switch ($capability) {
            case PhabricatorPolicyCapability::CAN_VIEW:
                return $this->getViewPolicy();
            case PhabricatorPolicyCapability::CAN_EDIT:
                return $this->getEditPolicy();
        }
    }

    public function hasAutomaticCapability($capability, PhabricatorUser $viewer)
    {
        return false;
    }

    public function getApplicationTransactionEditor()
    {
        return new PhabricatorProjectsEditor();
    }

    public function getApplicationTransactionTemplate()
    {
        return new PhabricatorProjectsTransaction();
    }

    public function destroyObjectPermanently(PhabricatorDestructionEngine $engine)
    {
        $this->delete();
    }

    public function getProfileImageURI()
    {
      return $this->getProfileImageFile()->getBestURI();
    }

    public function getProfileImageFile()
    {
      return $this->assertAttached($this->profileImageFile);
    }

    public function getWorkboardURI() {
        return urisprintf('/po-tools/product-vision/edit/%d/', $this->getID());
    }

    public function getProfileURI() {
        $id = $this->getID();
        return "/projects/profile/{$id}/";
    }

    public function isArchived()
    {
        return ($this->getisArchived() == 1);
    }

    public function attachSlugs(array $slugs)
    {
        $this->slugs = $slugs;
        return $this;
    }

    public function getSlugs()
    {
        return $this->assertAttached($this->slugs);
    }

    public function getCustomFieldSpecificationForRole($role)
    {
        return PhabricatorEnv::getEnvConfig('projects.fields');
    }

    public function getCustomFieldBaseClass()
    {
        return 'PhabricatorProjectCustomField';
    }

    public function getCustomFields()
    {
        return $this->assertAttached($this->customFields);
    }

    public function attachCustomFields(PhabricatorCustomFieldAttachment $fields)
    {
        $this->customFields = $fields;
        return $this;
    }

}
