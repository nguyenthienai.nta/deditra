<?php

final class PhabricatorOkrsCapabilityCreateOkrs
    extends PhabricatorPolicyCapability
{

    const CAPABILITY = 'okrs.create';

    public function getCapabilityName()
    {
        return pht('Can Create Okrs');
    }

    public function describeCapabilityRejection()
    {
        return pht('You do not have permission to create Okrs.');
    }

}
