<?php

final class PhabricatorOkrsCapabilityDefaultView
    extends PhabricatorPolicyCapability
{

    const CAPABILITY = 'okrs.default.view';

    public function getCapabilityName()
    {
        return pht('Default View Policy');
    }

    public function shouldAllowPublicPolicySetting()
    {
        return true;
    }

}
