<?php

final class PhabricatorOkrsCapabilityDefaultEdit
    extends PhabricatorPolicyCapability
{

    const CAPABILITY = 'okrs.default.edit';

    public function getCapabilityName()
    {
        return pht('Default Edit Policy');
    }

}
