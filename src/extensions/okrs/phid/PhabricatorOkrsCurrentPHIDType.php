<?php

final class PhabricatorOkrsCurrentPHIDType extends PhabricatorPHIDType {

    const TYPECONST = 'CURR';

    public function getTypeName() {
        return pht('Current');
    }

    public function newObject() {
        return new PhabricatorOkrsCurrent();
    }

    public function getPHIDTypeApplicationClass() {
        return 'PhabricatorOkrsApplication';
    }

    protected function buildQueryForObjects(
        PhabricatorObjectQuery $query,
        array $phids) {

        return id(new PhabricatorOkrsCurrentQuery())
            ->withPHIDs($phids);
    }

    public function loadHandles(
        PhabricatorHandleQuery $query,
        array $handles,
        array $objects) {

        foreach ($handles as $phid => $handle) {
            $current = $objects[$phid];
            $handle->setName($current->getCurrent());
            $handle->setFullName(pht('Kr: ').$current->getCurrent());
            $handle->setURI('/kr'.$current->getID());
        }

    }

}
