<?php

final class PhabricatorOkrsKeyResultPHIDType extends PhabricatorPHIDType {

    const TYPECONST = 'KERS';

    public function getTypeName() {
        return pht('Kr ');
    }

    public function newObject() {
        return new PhabricatorOkrsKeyResult();
    }

    public function getPHIDTypeApplicationClass() {
        return 'PhabricatorOkrsApplication';
    }

    protected function buildQueryForObjects(
        PhabricatorObjectQuery $query,
        array $phids) {

        return id(new PhabricatorOkrsKeyResultQuery())
            ->withPHIDs($phids);
    }

    public function loadHandles(
        PhabricatorHandleQuery $query,
        array $handles,
        array $objects) {

        foreach ($handles as $phid => $handle) {
            $keyResult = $objects[$phid];
            $handle->setName($keyResult->getContent());
            $handle->setFullName(pht('Kr: ').$keyResult->getTitle());
            $handle->setURI('/kr'.$keyResult->getID());
        }

    }

}
