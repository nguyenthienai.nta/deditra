<?php

final class AphrontFormContentKRControl extends AphrontBarView {

  const WIDTH = 100;

  private $value;
  private $manualCheckIn;
  private $target;
  private $current;
  private $baseValue;
  private $max = 100;
  private $alt = '';

  protected function getDefaultColor() {
    return parent::COLOR_AUTO_BADNESS;
  }

  public function setValue($value) {
    $this->value = $value;
    return $this;
  }

  public function setManualCheckin($manualCheckIn){
    $this->manualCheckIn = $manualCheckIn;

    return $this;
  }

  public function setBaseValue($baseValue) {
    $this->baseValue = $baseValue;
    return $this;
  }

  public function setCurrent($current) {
    $this->current = $current;
    return $this;
  }

  public function setTarget($target) {
    $this->target = $target;
    return $this;
  }

  public function setMax($max) {
    $this->max = $max;
    return $this;
  }

  public function setAlt($text) {
    $this->alt = $text;
    return $this;
  }

  public function getTarget(){
    return $this->target;
  }

  public function getValue(){
    return $this->value;
  }

  public function getCurrent(){
    return $this->current;
  }

  public function getBaseValue(){
    return $this->baseValue;
  }

  public function getManualCheckin(){
    return $this->manualCheckIn;
  }

  protected function getRatio() {
    return min($this->value, $this->max) / $this->max;
  }

  public function render() {
    require_celerity_resource('aphront-bars');
    $containManualCheckIns = ['1', '2', '3', '4', '5'];
    $hiddenBaseTarget = true;

    if (in_array($this->getManualCheckin(), $containManualCheckIns)){
      $hiddenBaseTarget = $this->getValue() === null ? 'hiddenProcess' : '';
    }

    $ratio = $this->getRatio();
    $width = self::WIDTH * $ratio;
    $this->getValue() > 20 ? $marginSpan = $width - 10 : $marginSpan = $width + 10;
    $widthTarget = $width - $marginSpan + 10;
    $this->getBaseValue() > 9 ?  $widthCurrent = $this->max/2 -10 : $widthCurrent = $this->max/2 - 5;
    $color = $this->getColor();

    return phutil_tag_div(
      "kr-content-form {$hiddenBaseTarget}",
      array(
        phutil_tag(
          'div',
          array('title' => $this->alt),
          phutil_tag(
            'div',
            array('style' => "width: {$width}px;font-weight: bold"),
            '')),
        phutil_tag(
          'span',
          array('style' => "float : left;font-weight: bold"),
          $this->getBaseValue()),
        phutil_tag(
          'span',
          array('style' => "margin-left: {$widthCurrent}px;font-weight: bold"),
          $this->getCurrent()),
        phutil_tag(
          'span',
          array('style' => "float: right;margin-right: 10px;font-weight: bold"),
          $this->getTarget()),

        ));
  }

}
