<?php

final class AphrontFormNumberControl extends AphrontFormControl
{

  private $disableAutocomplete;
  private $sigil;
  private $placeholder;
  private $autofocus;
  private $readonly;

  public function setDisableAutocomplete($disable)
  {
    $this->disableAutocomplete = $disable;
    return $this;
  }

  private function getDisableAutocomplete()
  {
    return $this->disableAutocomplete;
  }

  public function getPlaceholder()
  {
    return $this->placeholder;
  }

  public function setPlaceholder($placeholder)
  {
    $this->placeholder = $placeholder;
    return $this;
  }

  public function setAutofocus($autofocus)
  {
    $this->autofocus = $autofocus;
    return $this;
  }

  public function getAutofocus()
  {
    return $this->autofocus;
  }

  public function getSigil()
  {
    return $this->sigil;
  }

  public function setSigil($sigil)
  {
    $this->sigil = $sigil;
    return $this;
  }

  public function getReadonly()
  {
    return $this->readonly;
  }

  public function setReadonly($readonly)
  {
    $this->readonly = $readonly;
    return $this;
  }

  protected function getCustomControlClass()
  {
    return 'aphront-form-control-text';
  }

  protected function renderInput()
  {
    return javelin_tag(
      'input',
      array(
        'type' => 'number',
        'step' => 'any',
        'name' => $this->getName(),
        'value' => $this->getValue(),
        'disabled' => $this->getDisabled() ? 'disabled' : null,
        'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
        'id' => $this->getID(),
        'sigil' => $this->getSigil(),
        'placeholder' => $this->getPlaceholder(),
        'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
        'readonly' =>  ($this->getReadonly() ? 'true' : null),
      ));
  }

}
