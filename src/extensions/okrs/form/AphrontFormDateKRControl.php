<?php
final class AphrontFormDateKRControl extends AphrontFormControl {

    private $disableAutocomplete;
    private $sigil;
    private $placeholder;
    private $autofocus;

    public function setDisableAutocomplete($disable) {
        $this->disableAutocomplete = $disable;
        return $this;
    }

    private function getDisableAutocomplete() {
        return $this->disableAutocomplete;
    }

    public function getPlaceholder() {
        return $this->placeholder;
    }

    public function setPlaceholder($placeholder) {
        $this->placeholder = $placeholder;
        return $this;
    }

    public function setAutofocus($autofocus) {
        $this->autofocus = $autofocus;
        return $this;
    }

    public function getAutofocus() {
        return $this->autofocus;
    }

    public function getSigil() {
        return $this->sigil;
    }

    public function setSigil($sigil) {
        $this->sigil = $sigil;
        return $this;
    }

    protected function getCustomControlClass() {
        return 'aphront-form-control-text';
    }

  protected function renderInput()
  {
    return javelin_tag(
      'div',
      array(),
      [
        phutil_tag(
          'input',
          array(
            'type' => 'text',
            'id' => 'startDate',
            'class' => 'startDate',
            'name' => $this->getName(),
            'value' => $this->getValue()[0],
            'disabled' => $this->getDisabled() ? 'disabled' : null,
            'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
            'sigil' => $this->getSigil(),
            'placeholder' => $this->getPlaceholder(),
            'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
            'readonly' => 'true'
          )),
        phutil_tag(
          'span',
          array(
            'class' => 'end-date'
          ),
          '-'
        ),
        phutil_tag(
          'input',
          array(
            'type' => 'text',
            'id' => 'endDate',
            'class' => 'endDate',
            'name' => $this->getName(),
            'value' => $this->getValue()[1],
            'disabled' => $this->getDisabled() ? 'disabled' : null,
            'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
            'sigil' => $this->getSigil(),
            'placeholder' => $this->getPlaceholder(),
            'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
            'readonly' => 'true'
          ))
      ]
    );
  }
}
