<?php

final class AphrontFormDateOfWeekControl extends AphrontFormControl {

  protected function getCustomControlClass() {
    return 'aphront-form-control-checkbox';
  }

  protected function renderInput() {
    return javelin_tag(
      'div',
      array(
        'style' => 'padding-top: 20px; display: none',
        'id' => 'week'),
      javelin_tag(
        'table',
        array('class' => 'table-dayOfWeek'),
        [
          javelin_tag(
            'tr',
            array(),
            [
              javelin_tag(
                'td',
                array(),
                [
                  javelin_tag(
                    'label',
                    array('class' => 'label-container'),
                    [
                      javelin_tag(
                        'input',
                        array(
                          'type' => 'checkbox',
                          'id' => 'sun',
                          'value' => 7,
                          'name' => $this->getName()
                        )
                      ),
                      javelin_tag(
                        'span',
                        array('class' => 'checkmark')
                      ), 'Sun'
                    ]
                  )
                ]
              ),
              javelin_tag(
                'td',
                array(),
                [
                  javelin_tag(
                    'label',
                    array('class' => 'label-container'),
                    [
                      javelin_tag(
                        'input',
                        array(
                          'type' => 'checkbox',
                          'id' => 'mon',
                          'value' => 1,
                          'name' => $this->getName()
                        )
                      ),
                      javelin_tag(
                        'span',
                        array('class' => 'checkmark')
                      ), 'Mon'
                    ]
                  )
                ]
              ),
              javelin_tag(
                'td',
                array(),
                [
                  javelin_tag(
                    'label',
                    array('class' => 'label-container'),
                    [
                      javelin_tag(
                        'input',
                        array(
                          'type' => 'checkbox',
                          'id' => 'tue',
                          'value' => 2,
                          'name' => $this->getName()
                        )
                      ),
                      javelin_tag(
                        'span',
                        array('class' => 'checkmark')
                      ), 'Tue'
                    ]
                  )
                ]
              ),
              javelin_tag(
                'td',
                array(),
                [
                  javelin_tag(
                    'label',
                    array('class' => 'label-container'),
                    [
                      javelin_tag(
                        'input',
                        array(
                          'type' => 'checkbox',
                          'id' => 'wed',
                          'value' => 3,
                          'name' => $this->getName()
                        )
                      ),
                      javelin_tag(
                        'span',
                        array('class' => 'checkmark')
                      ), 'Wed'
                    ]
                  )
                ]
              ),
            ]
          ),
          javelin_tag(
            'tr',
            array(),
            [
              javelin_tag(
                'td',
                array(),
                [
                  javelin_tag(
                    'label',
                    array('class' => 'label-container'),
                    [
                      javelin_tag(
                        'input',
                        array(
                          'type' => 'checkbox',
                          'id' => 'thu',
                          'value' => 4,
                          'name' => $this->getName()
                          )
                      ),
                      javelin_tag(
                        'span',
                        array('class' => 'checkmark')
                      ), 'Thu'
                    ]
                  )
                ]
              ),
              javelin_tag(
                'td',
                array(),
                [
                  javelin_tag(
                    'label',
                    array('class' => 'label-container'),
                    [
                      javelin_tag(
                        'input',
                        array(
                          'type' => 'checkbox',
                          'id' => 'fri',
                          'value' => 5,
                          'name' => $this->getName()
                          )
                      ),
                      javelin_tag(
                        'span',
                        array('class' => 'checkmark')
                      ), 'Fri'
                    ]
                  ),
                ]
              ),
              javelin_tag(
                'td',
                array(),
                [
                  javelin_tag(
                    'label',
                    array('class' => 'label-container'),
                    [
                      javelin_tag(
                        'input',
                        array(
                          'type' => 'checkbox',
                          'id' => 'sat',
                          'value' => 6,
                          'name' => $this->getName()
                          )
                      ),
                      javelin_tag(
                        'span',
                        array('class' => 'checkmark')
                      ),
                      'Sat'
                    ]
                  )
                ]
              ),
              javelin_tag(
                'td',
                array(),
                [
                  javelin_tag(
                    'input',
                    array(
                      'type' => 'hidden',
                      'name' => 'dayOfWeeksValue',
                      'id'   => 'dayOfWeeksValue',
                      'value' => $this->getValue()
                    )
                  )
                ]
              )
            ]
          )
        ]
      )
    );

  }

}
