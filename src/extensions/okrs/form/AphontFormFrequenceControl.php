<?php

final class AphontFormFrequenceControl extends AphrontFormControl
{

  private $disableAutocomplete;
  private $sigil;
  private $placeholder;
  private $autofocus;
  private $readonly;
  private $isNew;
  private $totalDev;
  private $isAutoCheckin;

  public function setDisableAutocomplete($disable)
  {
    $this->disableAutocomplete = $disable;
    return $this;
  }

  private function getDisableAutocomplete()
  {
    return $this->disableAutocomplete;
  }

  public function getPlaceholder()
  {
    return $this->placeholder;
  }

  public function setPlaceholder($placeholder)
  {
    $this->placeholder = $placeholder;
    return $this;
  }

  public function setAutofocus($autofocus)
  {
    $this->autofocus = $autofocus;
    return $this;
  }

  public function getAutofocus()
  {
    return $this->autofocus;
  }

  public function getSigil()
  {
    return $this->sigil;
  }

  public function setSigil($sigil)
  {
    $this->sigil = $sigil;
    return $this;
  }

  public function getReadonly()
  {
    return $this->readonly;
  }

  public function setReadonly($readonly)
  {
    $this->readonly = $readonly;
    return $this;
  }

  public function getIsNew()
  {
    return $this->isNew;
  }

  public function setIsNew($isNew)
  {
    $this->isNew = $isNew;
    return $this;
  }

  public function getIsAutoCheckin()
  {
    return $this->isAutoCheckin;
  }

  public function setIsAutoCheckin($isAutoCheckin)
  {
    $this->isAutoCheckin = $isAutoCheckin;
    return $this;
  }

  public function getTotalDev()
  {
    return $this->totalDev;
  }

  public function setTotalDev($totalDev)
  {
    $this->totalDev = $totalDev;
    return $this;
  }

  protected function getCustomControlClass()
  {
    return 'aphront-form-control-text';
  }

  protected function renderInput()
  {
    return javelin_tag(
      'div',
      array(
        'class' => 'txtSuccessRule'
      ),
    [
      javelin_tag(
        'input',
        array(
          'type' => 'number',
          'step' => 'any',
          'name' => $this->getName(),
          'value' => $this->getValue(),
          'disabled' => $this->getDisabled() ? 'disabled' : null,
          'autocomplete' => $this->getDisableAutocomplete() ? 'off' : null,
          'id' => $this->getID(),
          'class' => 'hephaestos-frequence-time',
          'sigil' => $this->getSigil(),
          'placeholder' => $this->getPlaceholder(),
          'autofocus' => ($this->getAutofocus() ? 'autofocus' : null),
          'readonly' =>  ($this->getReadonly() ? 'true' : null),
        )
      ),
      javelin_tag(
        'div',
        array(
          'style' => 'margin-left: 10px;',
          'id' => 'txtDev'
        ),
        [
          javelin_tag(
            'label',
            array(
              'for' => 'devTotal',
              'style' => 'padding-right: 5px; padding-left: 10px',
              'class' => 'aphront-form-label'
            ),
            'Dev'
          ),
          javelin_tag(
            'input',
            array(
              'type' => 'number',
              'step' => 0.5,
              'style' => 'width: 50px; margin-left: 10px',
              'name' => 'totalDev',
              'value' => $this->getTotalDev(),
              'readonly' =>  ($this->getReadonly() ? 'true' : null),
            ))
        ]
      ),
      javelin_tag(
        'div',
        array(
          'style' => 'margin-top: 4px; padding-left: 10px; padding-right: 10px'
        ),
        [
          javelin_tag(
            'input',
            array(
              'type' => 'checkbox',
              'id' => 'isAutoCheckin',
              'name' => 'isAutoCheckin',
              'value' => 1,
            )),
          javelin_tag(
            'input',
            array(
              'type' => 'hidden',
              'name' => 'autoCheckin',
              'id' => 'chkAutoCheckin',
              'value' => $this->getIsAutoCheckin(),
            )),
          javelin_tag(
            'label',
            array(
              'for' => 'checkin-auto',
              'style' => 'padding-left: 3px',
            ),
            'Check-in automatically'
          ),
        ]
      )



      ]);
  }

}
