<?php

final class AphontFormFrequenceKRMonthlyControl extends AphrontFormControl
{

  private $disableAutocomplete;
  private $sigil;
  private $placeholder;
  private $autofocus;
  private $readonly;
  private $isNew;
  private $totalDev;
  private $isAutoCheckin;

  public function setDisableAutocomplete($disable)
  {
    $this->disableAutocomplete = $disable;
    return $this;
  }

  private function getDisableAutocomplete()
  {
    return $this->disableAutocomplete;
  }

  public function getPlaceholder()
  {
    return $this->placeholder;
  }

  public function setPlaceholder($placeholder)
  {
    $this->placeholder = $placeholder;
    return $this;
  }

  public function setAutofocus($autofocus)
  {
    $this->autofocus = $autofocus;
    return $this;
  }

  public function getAutofocus()
  {
    return $this->autofocus;
  }

  public function getSigil()
  {
    return $this->sigil;
  }

  public function setSigil($sigil)
  {
    $this->sigil = $sigil;
    return $this;
  }

  public function getReadonly()
  {
    return $this->readonly;
  }

  public function setReadonly($readonly)
  {
    $this->readonly = $readonly;
    return $this;
  }

  public function getIsNew()
  {
    return $this->isNew;
  }

  public function setIsNew($isNew)
  {
    $this->isNew = $isNew;
    return $this;
  }

  public function getIsAutoCheckin()
  {
    return $this->isAutoCheckin;
  }

  public function setIsAutoCheckin($isAutoCheckin)
  {
    $this->isAutoCheckin = $isAutoCheckin;
    return $this;
  }

  public function getTotalDev()
  {
    return $this->totalDev;
  }

  public function setTotalDev($totalDev)
  {
    $this->totalDev = $totalDev;
    return $this;
  }

  protected function getCustomControlClass()
  {
    return 'aphront-form-control-text';
  }

  protected function renderInput()
  {
    $days = array();
    $tags = ['first', 'second', 'third', 'fourth', 'last'];
    $dayOfWeek = ['1' => 'Monday', '2' => 'Tuesday', '3' => 'Wednesday', '4' => 'Thursday', '5' => 'Friday', '6' => 'Saturday', '7' => 'Sunday'];
    foreach ($tags as $key => $value) {
      $tags[] = javelin_tag(
        'option',
        array(
          'value' => $value
        ), $value
      );
    };

    foreach ($dayOfWeek as $key => $value) {
      $dayOfWeek[] = javelin_tag(
        'option',
        array(
          'value' => $key
        ), $value
      );
    };

    for ($i = 1; $i <= 31; $i++) {
      $days[] = javelin_tag(
        'option',
        array(
          'value' => $i < 10 ? ('0' . (string) $i) : (string) $i
        ), $i
      );
    };
    return javelin_tag(
      'div',
      array(
        'class' => 'monthly'),
      [
        javelin_tag(
          'div',
          array(),
          javelin_tag(
            'label',
            array(
              'class' => 'radio-container'),
            [
              'On day',
              javelin_tag(
                'input',
                array(
                  'type' => 'radio',
                  'name' => 'month-on',
                  'value' => 'on_day',
                  'checked' => 'checked',
                  'id' => 'on_day'
                )
              ),
              javelin_tag(
                'span',
                array('class' => 'checkmark-radio')
              ),
              javelin_tag(
                'div',
                array('class' => 'dropdown_container'),
                javelin_tag(
                  'select',
                  array(
                    'id' => 'dayOfMonth',
                    'name' => 'dayOfMonth'
                  ),
                  $days
                )
              )
            ]
          )
        ),
        javelin_tag(
          'div',
          array(),
          javelin_tag(
            'label',
            array(
              'class' => 'radio-container'),
            [
              javelin_tag(
                'span',
                array(), 'On the'
              ),
              javelin_tag(
                'input',
                array(
                  'type' => 'radio',
                  'name' => 'month-on',
                  'value' => 'on_the',
                  'id' => 'on_the'
                )
              ),
              javelin_tag(
                'span',
                array('class' => 'checkmark-radio')
              ),
              javelin_tag(
                'div',
                array('class' => 'dropdown_container'),
                [
                  javelin_tag(
                    'select',
                    array(
                      'id' => 'pre_dayOfWeek',
                      'name' => 'pre_dayOfWeek'
                    ),
                    $tags
                  ),
                  javelin_tag(
                    'select',
                    array(
                      'id' => 'dayOfWeek',
                      'name' => 'dayOfWeek'
                    ),
                    $dayOfWeek
                  )
                ]
              )
            ]
          )
        )
      ]
    );
  }

}
