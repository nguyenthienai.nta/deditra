<?php

final class PhabricatorOkrsMailEngineExtension
    extends PhabricatorMailEngineExtension
{

    const EXTENSIONKEY = 'okrs';

    public function supportsObject($object)
    {
        return ($object instanceof PhabricatorOkrsInterface);
    }

    public function newMailStampTemplates($object)
    {
        return array(
            id(new PhabricatorPHIDMailStamp())
                ->setKey('space')
                ->setLabel(pht('Objective')),
        );
    }

    public function newMailStamps($object, array $xactions)
    {
        $editor = $this->getEditor();
        $viewer = $this->getViewer();

        if (!PhabricatorOkrsNamespaceQuery::getOkrsExist()) {
            return;
        }

        $space_phid = PhabricatorOkrsNamespaceQuery::getObjectObjectivePHID(
            $object);

        $this->getMailStamp('objective')
            ->setValue($space_phid);
    }

}
