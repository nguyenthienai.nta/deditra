<?php

final class PhabricatorOkrsSearchEngineExtension
    extends PhabricatorSearchEngineExtension
{

    const EXTENSIONKEY = 'Okrs';

    public function isExtensionEnabled()
    {
        return PhabricatorApplication::isClassInstalled(
            'PhabricatorOkrsApplication');
    }

    public function getExtensionName()
    {
        return pht('Support for Okrs');
    }

    public function getExtensionOrder()
    {
        return 4000;
    }

    public function supportsObject($object)
    {
        return ($object instanceof PhabricatorOkrsInterface);
    }

    public function getSearchFields($object)
    {
        $fields = array();

        if (PhabricatorOkrsNamespaceQuery::getOkrsExist()) {
            $fields[] = id(new PhabricatorOkrsSearchField())
                ->setKey('spacePHIDs')
                ->setConduitKey('Okrs')
                ->setAliases(array('objective', 'Okrs'))
                ->setLabel(pht('Okrs'))
                ->setDescription(
                    pht('Search for objects in certain Okrs.'));
        }

        return $fields;
    }

    public function applyConstraintsToQuery(
        $object,
        $query,
        PhabricatorSavedQuery $saved,
        array $map)
    {

        if (!empty($map['spacePHIDs'])) {
            $query->withObjectivePHIDs($map['spacePHIDs']);
        } else {
            // If the user doesn't search for objects in specific Okrs, we
            // default to "all active Okrs you have permission to view".
            $query->withObjectiveIsArchived(false);
        }
    }

    public function getFieldSpecificationsForConduit($object)
    {
        return array(
            id(new PhabricatorConduitSearchFieldSpecification())
                ->setKey('spacePHID')
                ->setType('phid?')
                ->setDescription(
                    pht('PHID of the policy space this object is part of.')),
        );
    }

    public function getFieldValuesForConduit($object, $data)
    {
        return array(
            'spacePHID' => $object->getObjectivePHID(),
        );
    }

}
