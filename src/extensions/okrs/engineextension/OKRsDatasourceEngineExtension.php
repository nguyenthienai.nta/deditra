<?php

final class OKRsDatasourceEngineExtension
  extends PhabricatorDatasourceEngineExtension {

  public function newQuickSearchDatasources() {
    return array(
      new PhabricatorOkrsDataSource(),
      new PhabricatorOkrsKrDatasource(),
      new PhabricatorOkrsNamespaceDatasource()
    );
  }

  public function newJumpURI($query) {

    return null;
  }
}
