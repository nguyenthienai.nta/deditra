<?php

final class PhabricatorOkrsCurrentEditor
    extends PhabricatorApplicationTransactionEditor
{

    public function getEditorApplicationClass()
    {
        return pht('PhabricatorOkrsApplication');
    }

    public function getEditorObjectsDescription()
    {
        return pht('okrs');
    }

    public function getTransactionTypes()
    {
        $types = parent::getTransactionTypes();

        $types[] = PhabricatorTransactions::TYPE_VIEW_POLICY;
        $types[] = PhabricatorTransactions::TYPE_EDIT_POLICY;

        return $types;
    }

    public function getCreateObjectTitle($author, $object)
    {
        return pht('%s created this current.', $author);
    }

    public function getCreateObjectTitleForFeed($author, $object)
    {
        return pht('%s created current %s.', $author, $object);
    }

}
