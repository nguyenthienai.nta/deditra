<?php

final class PhabricatorOkrsNamespaceEditor
    extends PhabricatorApplicationTransactionEditor
{

    public function getEditorApplicationClass()
    {
        return pht('PhabricatorOkrsApplication');
    }

    public function getEditorObjectsDescription()
    {
        return pht('okrs');
    }

    public function getTransactionTypes()
    {
        $types = parent::getTransactionTypes();

        $types[] = PhabricatorTransactions::TYPE_VIEW_POLICY;
        $types[] = PhabricatorTransactions::TYPE_EDIT_POLICY;

        return $types;
    }

    public function getCreateObjectTitle($author, $object)
    {
        return pht('%s created this objective.', $author);
    }

    public function getCreateObjectTitleForFeed($author, $object)
    {
        return pht('%s created objective %s.', $author, $object);
    }

}
