<?php

final class PhabricatorOkrsKeyResultDayOfWeekTransaction
  extends PhabricatorOkrsKeyResultTransactionType {

  const TRANSACTIONTYPE = 'okrs.key_result.dateOfWeek';

  public function generateOldValue($object) {
    return $object->getDayOfWeeks();
  }

  public function applyInternalEffects($object, $value) {
    $object->setDayOfWeeks((string)$value);
  }

  public function getTitle() {
    return pht(
      '%s updated day of week.',
      $this->renderAuthor());
  }

  public function getTitleForFeed() {
    return pht(
      '%s updated day of week for %s.',
      $this->renderAuthor(),
      $this->renderObject());
  }

  public function validateTransactions($object, array $xactions) {
    return array();
  }

}
