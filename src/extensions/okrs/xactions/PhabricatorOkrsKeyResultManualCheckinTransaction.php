<?php

final class PhabricatorOkrsKeyResultManualCheckinTransaction
  extends PhabricatorOkrsKeyResultTransactionType
{

  const TRANSACTIONTYPE = 'okrs.key_result.manualCheckin';

  public function generateOldValue($object)
  {
    return $object->getManualCheckin();
  }

  public function applyInternalEffects($object, $value)
  {
    $object->setManualCheckin((int)$value);
  }

  public function getTitle()
  {
    $new = $this->getNewValue();
    if ($new) {
      return pht(
        '%s created the manual checkin for this kr.',
        $this->renderAuthor());
    } else {
      return pht(
        '%s changed the manual checkin for this kr.',
        $this->renderAuthor());
    }
  }

  public function getTitleForFeed()
  {
    $new = $this->getNewValue();
    if ($new) {
      return pht(
        '%s created the manual checkin for this kr %s.',
        $this->renderAuthor(),
        $this->renderObject());
    } else {
      return pht(
        '%s changed the manual checkin for this kr %s.',
        $this->renderAuthor(),
        $this->renderObject());
    }
  }

  public function getIcon()
  {
    $new = $this->getNewValue();
    if ($new) {
      return 'fa-ban';
    } else {
      return 'fa-check';
    }
  }

  public function getColor()
  {
    $new = $this->getNewValue();
    if ($new) {
      return 'indigo';
    }
  }

}
