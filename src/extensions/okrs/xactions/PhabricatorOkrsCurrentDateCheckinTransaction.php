<?php

final class PhabricatorOkrsCurrentDateCheckinTransaction
    extends PhabricatorModularTransactionType {

    const TRANSACTIONTYPE = 'okrs.current.datecheckin';

    public function generateOldValue($object) {
        return $object->getDateCheckin();
    }

    public function applyInternalEffects($object, $value) {
        $object->setDateCheckin($value);
    }

    public function getTitle() {
        return pht(
            '%s updated the kr date checkin.',
            $this->renderAuthor());
    }

    public function getTitleForFeed() {
        return pht(
            '%s updated the kr date checkin for %s.',
            $this->renderAuthor(),
            $this->renderObject());
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();

        if ($this->isEmptyTextTransaction($object->getDateCheckin(), $xactions)) {
            $errors[] = $this->newRequiredError(
                pht('KR must have a date checkin.'));
        }
        return $errors;
    }

}
