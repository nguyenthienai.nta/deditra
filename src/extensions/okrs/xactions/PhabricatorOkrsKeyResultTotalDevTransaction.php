<?php

final class PhabricatorOkrsKeyResultTotalDevTransaction
  extends PhabricatorOkrsKeyResultTransactionType
{

  const TRANSACTIONTYPE = 'okrs.key_result.totalDev';

  public function generateOldValue($object)
  {
    return $object->getTotalDev();
  }

  public function applyInternalEffects($object, $value)
  {
    $object->setTotalDev($value == 69.69 ? -1 : $value);
  }

  public function getTitle()
  {
    $new = $this->getNewValue();
    if ($new) {
      return pht(
        '%s created the total dev for this kr.',
        $this->renderAuthor());
    } else {
      return pht(
        '%s changed the total dev for this kr.',
        $this->renderAuthor());
    }
  }

  public function getTitleForFeed()
  {
    $new = $this->getNewValue();
    if ($new) {
      return pht(
        '%s created the total dev for this kr %s.',
        $this->renderAuthor(),
        $this->renderObject());
    } else {
      return pht(
        '%s changed the total dev for this kr %s.',
        $this->renderAuthor(),
        $this->renderObject());
    }
  }

  public function getIcon()
  {
    $new = $this->getNewValue();
    if ($new) {
      return 'fa-ban';
    } else {
      return 'fa-check';
    }
  }

  public function getColor()
  {
    $new = $this->getNewValue();
    if ($new) {
      return 'indigo';
    }
  }

  public function validateTransactions($object, array $xactions)
  {
    $errors = array();
//    $type_frequenceTime =
//      PhabricatorOkrsKeyResultFrequenceTimeTransaction::TRANSACTIONTYPE;
//
//    if ($this->isEmptyTextTransaction($object->getTotalDev(), $xactions) == false) {
//      foreach ($xactions as $xaction) {
//        $new_value = $xaction->getNewValue();
//        if ($new_value <= 0) {
//          $errors[] = new PhabricatorApplicationTransactionValidationError(
//            $type_frequenceTime,
//            pht('Invalid'),
//            pht('Dev total must be greater than 0.'),
//            null);
//        }
//
//        $value = (float)$new_value * 2;
//        if ($new_value != 69.69) {
//          if (strval($value) !== strval(intval($value))) {
//            $errors[] = new PhabricatorApplicationTransactionValidationError(
//              $type_frequenceTime,
//              pht('Invalid'),
//              pht('Dev total value must be valid , Example: 0.5 or 1.'),
//              null);
//          }
//        }
//      }
//    }

    return $errors;

  }
}


