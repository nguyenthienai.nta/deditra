<?php

final class PhabricatorOkrsCurrentCurrentTransaction
    extends PhabricatorOkrsCurrentTransactionType {

    const TRANSACTIONTYPE = 'okrs.current.current';

    public function generateOldValue($object) {
        return $object->getCurrent();
    }

    public function applyInternalEffects($object, $value) {
        $object->setCurrent($value);
        $object->setDateCheckin(strtotime('now'));
    }

    public function getTitle() {
        $old = $this->getOldValue();
        $currentPHID = $this->getObjectPHID();
        $current = id(new PhabricatorOkrsCurrent())->loadOneWhere('PHID = %s', $currentPHID);
        $dateCheckin = date('d/m/Y', $current->getDateCreated());
        if (!strlen($old)) {
        return pht(
            "%s check-in this KR' checkin value %s at %s.",
            $this->renderAuthor(),
            $this->renderNewValue(),
            $dateCheckin);
        } else {
        return pht(
            "%s update this KR' checkin value at %s from %s to %s .",
            $this->renderAuthor(),
            $dateCheckin,
            $this->renderOldValue(),
            $this->renderNewValue());
        }
    }

    public function getTitleForFeed() {
        return pht(
            '%s updated the kr checkin value for %s.',
            $this->renderAuthor(),
            $this->renderObject());
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();

        if ($this->isEmptyTextTransaction($object->getCurrent(), $xactions)) {
            $errors[] = $this->newRequiredError(
                pht('KR must have a Current.'));
        }

        return $errors;
    }


}
