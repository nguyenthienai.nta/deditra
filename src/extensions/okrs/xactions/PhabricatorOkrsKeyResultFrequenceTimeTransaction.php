<?php

final class PhabricatorOkrsKeyResultFrequenceTimeTransaction
    extends PhabricatorOkrsKeyResultTransactionType {

    const TRANSACTIONTYPE = 'okrs.key_result.frequenceTime';

    public function generateOldValue($object) {
        return $object->getFrequenceTime();
    }

    public function applyInternalEffects($object, $value) {
        $object->setFrequenceTime($value);
    }

    public function getTitle() {
        $new = $this->getNewValue();
        if ($new == 0) {
            return pht(
                '%s marked this kr as a time Per.',
                $this->renderAuthor());
        } else if ($new == 1) {
            return pht(
                '%s marked this kr as a week',
                $this->renderAuthor());
        } else {
            return pht(
                '%s marked this kr as a month',
                $this->renderAuthor());
        }
    }

    public function getTitleForFeed() {
        $new = $this->getNewValue();
        if ($new == 0) {
            return pht(
                '%s marked %s as a time per.',
                $this->renderAuthor(),
                $this->renderObject());
        } else if ($new == 1) {
            return pht(
                '%s marked %s as week.',
                $this->renderAuthor(),
                $this->renderObject());
        } else {
            return pht(
                '%s marked %s as month.',
                $this->renderAuthor(),
                $this->renderObject());
        }
    }

    public function getIcon() {
        $new = $this->getNewValue();
        if ($new == PhameConstants::VISIBILITY_PUBLISHED) {
            return 'fa-rss';
        } else if ($new == PhameConstants::VISIBILITY_ARCHIVED) {
            return 'fa-ban';
        } else {
            return 'fa-eye-slash';
        }
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();

      if ($this->isEmptyTextTransaction($object->getFrequenceTime(), $xactions)) {
        $errors[] = $this->newRequiredError(
          pht('KR must have a frequence time.'));
      } else {

        foreach ($xactions as $xaction) {
          $new_value = $xaction->getNewValue();
          if (!is_numeric($new_value) || $new_value < 1) {
            $errors[] = $this->newInvalidError(
              pht('Frequence time must be a number and bigger than 0.'
              ));
          }
        }
      }


      return $errors;
    }
}
