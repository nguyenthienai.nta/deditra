<?php

final class PhabricatorOkrsNamespaceNameTransaction
    extends PhabricatorOkrsNamespaceTransactionType
{

    const TRANSACTIONTYPE = 'okrs:name';

    public function generateOldValue($object)
    {
        return $object->getNamespaceName();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setNamespaceName(trim($value));
    }

    public function getTitle()
    {
        $old = $this->getOldValue();
        if (!strlen($old)) {
            return pht(
                '%s created this objective.',
                $this->renderAuthor());
        } else {
            return pht(
                '%s renamed this objective from %s to %s.',
                $this->renderAuthor(),
                $this->renderOldValue(),
                $this->renderNewValue());
        }
    }

    public function getTitleForFeed()
    {
        return pht(
            '%s renamed objective %s from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

    public function validateTransactions($object, array $xactions)
    {
        $errors = array();

//        if ($this->isEmptyTextTransaction($object->getNamespaceName(), $xactions)) {
//            $errors[] = $this->newRequiredError(
//                pht('Objective must have a name.'));
//        }
        $okr = id(new PhabricatorOkrsOkr())->loadOneWhere('phid = %s', $object->getOkrPHID());
        $objectives = id(new PhabricatorOkrsNamespace())->loadAllWhere('OkrPHID = %s', $okr->getPHID());
        $objective_names = [];
        foreach($objectives as $objective){
            $objective_names[] = $objective->getNameSpaceName();
            $objective_names[] = str_replace(' ', '', $objective->getNameSpaceName());
        }
        $max_length = $object->getColumnMaximumByteLength('namespaceName');
        foreach ($xactions as $xaction) {
            $new_value = trim($xaction->getNewValue());
            $new_length = strlen($new_value);
            if ($new_length == 0) {
                $errors[] = $this->newRequiredError(
                    pht('Objective must have a name.'));
            }
            if ($new_length > $max_length) {
                $errors[] = $this->newInvalidError(
                    pht('The name can be no longer than %s characters.',
                        new PhutilNumber($max_length)));
            }
            if($object->getNamespaceName() == null){
                if (in_array($new_value, $objective_names) || in_array(str_replace(' ', '', $new_value), $objective_names)) {
                    $errors[] = $this->newInvalidError(
                        pht('This name is already taken.'));
                }
            }
            else{
                if($new_value != $object->getNamespaceName() && str_replace(' ', '', $new_value) != str_replace(' ', '', $object->getNamespaceName())){
                    if (in_array($new_value, $objective_names) || in_array(str_replace(' ', '', $new_value), $objective_names)) {
                        $errors[] = $this->newInvalidError(
                            pht('This name is already taken.'));
                    }
                }
            }
        }

//        if(!$object->getNamespaceName()) {
//          $okrPHID = id(new PhabricatorOkrsOkr())->load($_GET['okr'])->getPHID();
//          if (count(id(new PhabricatorOkrsNamespace())->loadAllWhere('okrPHID = %s', $okrPHID)) > 4) {
//            $errors[] = $this->newInvalidError(
//              pht('OKR must have less than 5 objectives.'));
//          }
//        }

        return $errors;
    }

}
