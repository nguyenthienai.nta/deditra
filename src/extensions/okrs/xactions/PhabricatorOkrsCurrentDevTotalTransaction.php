<?php

final class PhabricatorOkrsCurrentDevTotalTransaction
  extends PhabricatorOkrsCurrentTransactionType {

  const TRANSACTIONTYPE = 'okrs.current.dev';

  public function generateOldValue($object) {
    return $object->getTotalDev();
  }

  public function applyInternalEffects($object, $value) {
    $object->setTotalDev($value);
  }

  public function validateTransactions($object, array $xactions) {
    return array();
  }


}
