<?php

final class PhabricatorOkrsCurrentColorTransaction
    extends PhabricatorModularTransactionType {

    const TRANSACTIONTYPE = 'okrs.current.color';

    public function generateOldValue($object) {
        return $object->getColor();
    }

    public function applyInternalEffects($object, $value) {
        $object->setColor($value);
    }

    public function getTitle() {
        return pht(
            '%s updated the current color.',
            $this->renderAuthor());
    }

    public function getTitleForFeed() {
        return pht(
            '%s updated the current color for %s.',
            $this->renderAuthor(),
            $this->renderObject());
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();

        if ($this->isEmptyTextTransaction($object->getDateCheckin(), $xactions)) {
            $errors[] = $this->newRequiredError(
                pht('Current must have a color.'));
        }
        return $errors;
    }

}
