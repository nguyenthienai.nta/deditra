<?php

final class PhabricatorOkrsCurrentVelocityDataTransaction
    extends PhabricatorOkrsCurrentTransactionType {

    const TRANSACTIONTYPE = 'okrs.current.velocitydata';

    public function generateOldValue($object) {
        return $object->getVelocityData();
    }

    public function applyInternalEffects($object, $value) {
        $object->setVelocityData($value);
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();
        return $errors;
    }

}
