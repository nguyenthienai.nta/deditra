<?php

final class PhabricatorOkrsKeyResultEndDateTransaction
  extends PhabricatorOkrsKeyResultTransactionType
{

  const TRANSACTIONTYPE = 'okrs.key_result.endDate';

  public function generateOldValue($object)
  {
    return $object->getEndDate();
  }

  public function applyInternalEffects($object, $value)
  {
    return $object->setEndDate($value);
  }

  public function getTitle()
  {
    return pht(
      '%s changed the end date for this kr.',
      $this->renderAuthor());
  }

  public function getTitleForFeed()
  {
    return pht(
      '%s changed the end date for kr %s.',
      $this->renderAuthor(),
      $this->renderObject());
  }

  public function getIcon()
  {
    $new = $this->getNewValue();
    if ($new == PhameConstants::VISIBILITY_PUBLISHED) {
      return 'fa-rss';
    } else if ($new == PhameConstants::VISIBILITY_ARCHIVED) {
      return 'fa-ban';
    } else {
      return 'fa-eye-slash';
    }
  }

  public function validateTransactions($object, array $xactions)
  {
    $errors = array();

    if ($this->isEmptyTextTransaction($object->getEndDate(), $xactions)) {
      $errors[] = $this->newRequiredError(
        pht('KR must have a end date.'));
    }
//    foreach ($xactions as $xaction) {
//      if ($xaction->getNewValue() <= strtotime(date('d-m-Y'))) {
//        $errors[] = $this->newInvalidError(
//          pht('End date must be greater than the current date.'));
//      }
//    }

    return $errors;
  }
}
