<?php

final class PhabricatorOkrsCurrentHFITransaction
  extends PhabricatorOkrsCurrentTransactionType {

  const TRANSACTIONTYPE = 'okrs.current.hfi';

  public function generateOldValue($object) {
    return $object->getHFI();
  }

  public function applyInternalEffects($object, $value) {
    $object->setHFI($value);
  }

  public function validateTransactions($object, array $xactions) {
    return array();
  }


}
