<?php

final class PhabricatorOkrsKeyResultDayOffsTransaction
    extends PhabricatorOkrsKeyResultTransactionType {

    const TRANSACTIONTYPE = 'okrs.key_result.dayOffs';

    public function generateOldValue($object) {
        return $object->getDayOffs();
    }

    public function applyInternalEffects($object, $value) {
        $object->setDayOffs($value);
    }

    public function getTitle() {
        return pht('%s changed the day off for this kr.', $this->renderAuthor());
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();
      return $errors;
    }
}
