<?php

final class PhabricatorOkrsKeyResultFrequenceTransaction
    extends PhabricatorOkrsKeyResultTransactionType {

    const TRANSACTIONTYPE = 'okrs.key_result.frequence';

    public function generateOldValue($object) {
        return $object->getFrequence();
    }

    public function applyInternalEffects($object, $value) {
        $object->setFrequence($value);
    }

    public function getTitle() {
        $new = $this->getNewValue();
        if ($new == 0) {
            return pht(
                '%s marked this kr as a time Per.',
                $this->renderAuthor());
        } else if ($new == 1) {
            return pht(
                '%s marked this kr as a week',
                $this->renderAuthor());
        } else {
            return pht(
                '%s marked this kr as a month',
                $this->renderAuthor());
        }
    }

    public function getTitleForFeed() {
        $new = $this->getNewValue();
        if ($new == 0) {
            return pht(
                '%s marked %s as a time per.',
                $this->renderAuthor(),
                $this->renderObject());
        } else if ($new == 1) {
            return pht(
                '%s marked %s as week.',
                $this->renderAuthor(),
                $this->renderObject());
        } else {
            return pht(
                '%s marked %s as month.',
                $this->renderAuthor(),
                $this->renderObject());
        }
    }

    public function getIcon() {
        $new = $this->getNewValue();
        if ($new == PhameConstants::VISIBILITY_PUBLISHED) {
            return 'fa-rss';
        } else if ($new == PhameConstants::VISIBILITY_ARCHIVED) {
            return 'fa-ban';
        } else {
            return 'fa-eye-slash';
        }
    }
    public function validateTransactions($object, array $xactions) {
        $errors = array();

        if ($this->isEmptyTextTransaction($object->getFrequence(), $xactions)) {
            $errors[] = $this->newRequiredError(
                pht('KR must have a frequence.'));
        }

        return $errors;
    }
}
