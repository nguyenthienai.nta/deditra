<?php
abstract class PhabricatorOkrsOkrTransactionType
    extends PhabricatorModularTransactionType {}

final class PhabricatorOkrsOkrArchiveTransaction
    extends PhabricatorOkrsOkrTransactionType
{

    const TRANSACTIONTYPE = 'okrs.okr.archive';

    public function generateOldValue($object)
    {
        return $object->getIsArchived();
    }

    public function applyInternalEffects($object, $value)
    {
        $object->setIsArchived((int)$value);
    }

    public function getTitle()
    {
        $new = $this->getNewValue();
        if ($new) {
            return pht(
                '%s archived this space.',
                $this->renderAuthor());
        } else {
            return pht(
                '%s activated this space.',
                $this->renderAuthor());
        }
    }

    public function getTitleForFeed()
    {
        $new = $this->getNewValue();
        if ($new) {
            return pht(
                '%s archived space %s.',
                $this->renderAuthor(),
                $this->renderObject());
        } else {
            return pht(
                '%s activated space %s.',
                $this->renderAuthor(),
                $this->renderObject());
        }
    }

    public function getIcon()
    {
        $new = $this->getNewValue();
        if ($new) {
            return 'fa-ban';
        } else {
            return 'fa-check';
        }
    }

    public function getColor()
    {
        $new = $this->getNewValue();
        if ($new) {
            return 'indigo';
        }
    }

}


final class PhabricatorOkrsOkrContentTransaction
    extends PhabricatorOkrsOkrTransactionType {

    const TRANSACTIONTYPE = 'okrs.okr.content';

    public function generateOldValue($object) {
        return $object->getContent();
    }

    public function applyInternalEffects($object, $value) {
        $object->setContent(trim($value));
    }

    public function getTitle() {
        return pht(
            '%s renamed this kr from %s to %s.',
            $this->renderAuthor(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

    public function getTitleForFeed() {
        return pht(
            '%s renamed %s kr from %s to %s.',
            $this->renderAuthor(),
            $this->renderObject(),
            $this->renderOldValue(),
            $this->renderNewValue());
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();

//        if ($this->isEmptyTextTransaction($object->getContent(), $xactions)) {
//            $errors[] = $this->newRequiredError(
//                pht('OKR must have a name.'));
//        }

        $max_length = $object->getColumnMaximumByteLength('content');
        $okrs = id(new PhabricatorOkrsOkr())->loadAll();
        $okr_names = [];
        foreach($okrs as $okr){
            $okr_names[] = $okr->getContent();
            $okr_names[] = str_replace(' ', '', $okr->getContent());
        }

        foreach ($xactions as $xaction) {
            $new_value = trim($xaction->getNewValue());
            $new_length = strlen($new_value);
            if ($new_length == 0) {
                $errors[] = $this->newRequiredError(
                    pht('OKR must have a name.'));
            }
            if ($new_length > $max_length) {
                $errors[] = $this->newInvalidError(
                    pht('The name can be no longer than %s characters.',
                        new PhutilNumber($max_length)));
            }
            if($object->getContent() == null){
                if (in_array($new_value, $okr_names) || in_array(str_replace(' ', '', $new_value), $okr_names)) {
                    $errors[] = $this->newInvalidError(
                        pht('This name is already taken.'));
                }
            }
            else{
                if($new_value != $object->getContent()  && str_replace(' ', '', $new_value) != str_replace(' ', '', $object->getContent())){
                    if (in_array($new_value, $okr_names) || in_array(str_replace(' ', '', $new_value), $okr_names)) {
                        $errors[] = $this->newInvalidError(
                            pht('This name is already taken.'));
                    }
                }
            }
        }

        return $errors;
    }

}
