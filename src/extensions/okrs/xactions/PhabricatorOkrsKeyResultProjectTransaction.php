<?php

final class PhabricatorOkrsKeyResultProjectTransaction
    extends PhabricatorOkrsKeyResultTransactionType {

    const TRANSACTIONTYPE = 'okrs.key_result.project';

    public function generateOldValue($object) {
        return $object->getProjectPHID();
    }

    public function applyInternalEffects($object, $value) {
        $object->setProjectPHID($value);
    }

    public function getTitle() {
        return pht(
            '%s changed the project for this kr.',
            $this->renderAuthor());
    }

    public function getTitleForFeed() {
        return pht(
            '%s changed the project for kr %s.',
            $this->renderAuthor(),
            $this->renderObject());
    }
}
