<?php

final class PhabricatorOkrsCurrentKrTransaction
    extends PhabricatorModularTransactionType {

    const TRANSACTIONTYPE = 'okrs.current.kr';

    public function generateOldValue($object) {
        return $object->getKrPHID();
    }

    public function applyInternalEffects($object, $value) {
        $object->setKrPHID($value);
    }

    public function getTitle() {
        return pht(
            '%s changed the kr.',
            $this->renderAuthor());
    }

    public function getTitleForFeed() {
        return pht(
            '%s changed the kr for %s.',
            $this->renderAuthor(),
            $this->renderObject());
    }

    public function validateTransactions($object, array $xactions) {
        $errors = array();

        if ($this->isEmptyTextTransaction($object->getKrPHID(), $xactions)) {
            $errors[] = $this->newRequiredError(
                pht('Kr must be attached to a objective.'));
        }

        foreach ($xactions as $xaction) {
            $new_phid = $xaction->getNewValue();

            $keyResult = id(new PhabricatorOkrsKeyResultQuery())
                ->setViewer($this->getActor())
                ->withPHIDs(array($new_phid))
                ->requireCapabilities(
                    array(
                        PhabricatorPolicyCapability::CAN_VIEW,
                        PhabricatorPolicyCapability::CAN_EDIT,
                    ))
                ->execute();

            if ($keyResult) {
                continue;
            }

            $errors[] = $this->newInvalidError(
                pht('The specified objective PHID ("%s") is not valid. You can only '.
                    'create a kr on (or move a kr into) a objective which you '.
                    'have permission to see and edit.',
                    $new_phid));
        }

        return $errors;
    }

}
