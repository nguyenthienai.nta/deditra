<?php

abstract class PhabricatorOkrsBaseArchiveController extends PhabricatorController
{
}


final class PhabricatorOkrsArchiveController
    extends PhabricatorOkrsBaseArchiveController
{

    public function handleRequest(AphrontRequest $request)
    {
        $viewer = $request->getUser();

        $space = id(new PhabricatorOkrsNameQuery())
            ->setViewer($viewer)
            ->withIDs(array($request->getURIData('id')))
            ->requireCapabilities(
                array(
                    PhabricatorPolicyCapability::CAN_VIEW,
                    PhabricatorPolicyCapability::CAN_EDIT,
                ))
            ->executeOne();
        if (!$space) {
            return new Aphront404Response();
        }

        $is_archive = ($request->getURIData('action') == 'archive');
        $cancel_uri = '/okrs';

        if ($request->isFormPost()) {
            $type_archive = PhabricatorOkrsOkrArchiveTransaction::TRANSACTIONTYPE;

            $xactions = array();
            $xactions[] = id(new PhabricatorOkrsOkrTransaction())
                ->setTransactionType($type_archive)
                ->setNewValue($is_archive ? 1 : 0);

            $editor = id(new PhabricatorOkrsNameEditor())
                ->setActor($viewer)
                ->setContinueOnNoEffect(true)
                ->setContinueOnMissingFields(true)
                ->setContentSourceFromRequest($request);

            $editor->applyTransactions($space, $xactions);

            return id(new AphrontRedirectResponse())->setURI($cancel_uri);
        }

        $body = array();
        if ($is_archive) {
            $title = pht('Archive Okr: %s', $space->getContent());
            $body[] = pht(
                'If you archive this Okr, you will no longer be able to create ' .
                'new objects inside it.');
            $body[] = pht(
                'Existing objects in this Okr will be hidden from query results ' .
                'by default.');
            $button = pht('Archive Okr');
        } else {
            $title = pht('Activate Okr: %s', $space->getContent());
            $body[] = pht(
                'If you activate this Okr, you will be able to create objects ' .
                'inside it again.');
            $body[] = pht(
                'Existing objects will no longer be hidden from query results.');
            $button = pht('Activate Okr');
        }


        $dialog = $this->newDialog()
            ->setTitle($title)
            ->addCancelButton($cancel_uri)
            ->addSubmitButton($button);

        foreach ($body as $paragraph) {
            $dialog->appendParagraph($paragraph);
        }

        return $dialog;
    }
}