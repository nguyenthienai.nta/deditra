<?php

abstract class PhabricatorOkrsBaseEditObjController extends PhabricatorController
{
}

final class PhabricatorOkrsEditObjController
  extends PhabricatorOkrsBaseEditObjController
{

  public function handleRequest(AphrontRequest $request)
  {
    require_celerity_resource('krForm-css');

    $viewer = $request->getUser();

    $id = $request->getURIData('id');
    if ($id) {
      $space = id(new PhabricatorOkrsNamespaceQuery())
        ->setViewer($viewer)
        ->withIDs(array($id))
        ->requireCapabilities(
          array(
            PhabricatorPolicyCapability::CAN_VIEW,
            PhabricatorPolicyCapability::CAN_EDIT,
          ))
        ->executeOne();

      $Okrs = id(new PhabricatorOkrsOkr())->loadOneWhere('phid = %s', $space->getOkrPHID());

      $okr = id(new PhabricatorOkrsNameQuery())
        ->setViewer($viewer)
        ->withIDs(array($Okrs->getID()))
        ->requireCapabilities(
          array(
            PhabricatorPolicyCapability::CAN_VIEW,
            PhabricatorPolicyCapability::CAN_EDIT,
          ))
        ->executeOne();

      if (!$space) {
        return new Aphront404Response();
      }

      $okrPHID = $space->getOkrPHID();
      $okr = id(new PhabricatorOkrsOkr())->loadAllWhere('phid = %s', $okrPHID);
      $okr = array_pop(array_reverse($okr));
      $is_new = false;
      $cancel_uri = '/obj/' . $okr->getID();
      $title = pht('Edit Objective');
    } else {
      $okr_id = head($request->getArr('okr'));
      if (!$okr_id) {
        $okr_id = $request->getStr('okr');
      }

      $query = id(new PhabricatorOkrsNameQuery())
        ->setViewer($viewer)
        ->requireCapabilities(
          array(
            PhabricatorPolicyCapability::CAN_VIEW,
            PhabricatorPolicyCapability::CAN_EDIT,
          )
        );

      if (ctype_digit($okr_id)) {
        $query->withIDs(array($okr_id));
      } else {
        $query->withPHIDs(array($okr_id));
      }

      $okr = $query->executeOne();
      if (!$okr) {
        return new Aphront404Response();
      }

      $this->requireApplicationCapability(
        PhabricatorOkrsCapabilityCreateOkrs::CAPABILITY
      );
      $space = PhabricatorOkrsNamespace::initializeNewNamespace($viewer, $okr);

      $is_new = true;
      $cancel_uri = '/obj/' . $okr->getID();
      $title = pht('Create Objective');
    }

    $validation_exception = null;
    $e_name = true;
    $e_point = true;
    $v_name = $space->getNamespaceName();
    $v_edit = $space->getEditPolicy();
    $v_point = explode(',', $space->getPoint());

    if ($request->isFormPost()) {
      $xactions = array();
      $e_name = null;
      $e_point = null;
      $v_name = $request->getStr('name');
      $v_point = $request->getArr('point');
      $v_view = 'users';
      $v_edit = $request->getStr('editPolicy');
      $type_name = PhabricatorOkrsNamespaceNameTransaction::TRANSACTIONTYPE;
      $type_point = PhabricatorOkrsNamespacePointTransaction::TRANSACTIONTYPE;
      $type_view = PhabricatorTransactions::TYPE_VIEW_POLICY;
      $type_edit = PhabricatorTransactions::TYPE_EDIT_POLICY;

      $xactions[] = id(new PhabricatorOkrsNamespaceTransaction())
        ->setTransactionType($type_name)
        ->setNewValue($v_name);

      $xactions[] = id(new PhabricatorOkrsNamespaceTransaction())
        ->setTransactionType($type_point)
        ->setNewValue(implode($v_point, ','));

      $xactions[] = id(new PhabricatorOkrsNamespaceTransaction())
        ->setTransactionType($type_view)
        ->setNewValue($v_view);

      $xactions[] = id(new PhabricatorOkrsNamespaceTransaction())
        ->setTransactionType($type_edit)
        ->setNewValue($v_edit);

      $editor = id(new PhabricatorOkrsNamespaceEditor())
        ->setActor($viewer)
        ->setContinueOnNoEffect(true)
        ->setContentSourceFromRequest($request);

      try {
        $editor->applyTransactions($space, $xactions);

        session_start();
        if (!$is_new) {
          $_SESSION['message'] = 'Objective edited successfully';
        } else {
          $_SESSION['message'] = 'Objective added successfully';
        }

        if ($request->getBool('check')) {
          return id(new AphrontRedirectResponse())
            ->setURI('/obj/edit/?okr=' . $okr->getID());
        } else
          return id(new AphrontRedirectResponse())
            ->setURI('/obj/' . $okr->getID());
      } catch (PhabricatorApplicationTransactionValidationException $ex) {
        $validation_exception = $ex;

        $e_name = $ex->getShortMessage($type_name);
        $e_point = $ex->getShortMessage($type_point);
      }
    }
    $policies = id(new PhabricatorPolicyQuery())
      ->setViewer($viewer)
      ->setObject($space)
      ->execute();
    Javelin::initBehavior(
      'jquery');
    Javelin::initBehavior(
      'submit-objective');
    Javelin::initBehavior(
      'objective-form');

    $objective_police = PhabricatorOkrsNamespace::initializeNewNamespace($viewer, $okr);

    $form = id(new AphrontFormView())
      ->setUser($viewer);

    $form
      ->appendControl(
        id(new AphrontFormStaticKRControl())
          ->setLabel(pht("OKR"))
          ->setID('OKRName')
          ->setValue([$okr->getContent()])
      )
      ->appendChild(
        id(new AphrontFormTextControl())
          ->setLabel(pht('Name'))
          ->setName('name')
          ->setValue($v_name)
          ->setError($e_name))
      ->appendChild(
        id(new AphrontFormSuccessRuleControl())
          ->setLabel(pht('Success Rule'))
          ->setID('point')
          ->setName('point[]')
          ->setValue($v_point)
          ->setError($e_point)
      )
      ->appendChild(
        id(new AphrontFormPolicyControl())
          ->setUser($viewer)
          ->setCapability(PhabricatorPolicyCapability::CAN_EDIT)
          ->setPolicyObject($objective_police)
          ->setPolicies($policies)
          ->setValue($v_edit)
          ->setName('editPolicy'))
      ->addHiddenInput('check', 'false');

    if ($id) {
      $form->appendChild(
        id(new AphrontFormSubmitControl())
          ->addButton(id(new PHUIButtonView())
            ->setTag('a')
            ->setHref($this->getApplicationURI('delete-obj/' . $id . '/'))
            ->setText('Delete')
            ->setWorkflow(true)
            ->setColor(PHUIButtonView::RED))
          ->addButton(id(new PHUIButtonView())
            ->setText('Save'))
          ->addCancelButton($cancel_uri));
    } else {
      $form->appendChild(
        id(new AphrontFormSubmitControl())
          ->addButton(id(new PHUIButtonView())
            ->setText('Save And New')
            ->addSigil('submitObjetiveButton')
            ->setColor(PHUIButtonView::GREEN))
          ->addButton(id(new PHUIButtonView())
            ->setText('Save'))
          ->addCancelButton($cancel_uri)
      );
    }

    $box = id(new PHUIObjectBoxView())
      ->setHeaderText($title)
      ->setBackground(PHUIObjectBoxView::WHITE_CONFIG)
      ->setValidationException($validation_exception)
      ->appendChild($form);

    session_start();
    if (isset($_SESSION['message'])) {
      $box->setFormSaved(true, $_SESSION['message']);
      unset($_SESSION['message']);
    }

    $crumbs = $this->buildApplicationCrumbs();
    $crumbs->addTextCrumb(
      $okr->getContent(),
      $cancel_uri);
    if (!$is_new) {
      $crumbs->addTextCrumb('Edit Objective');
    }
    if ($is_new) {
      $crumbs->addTextCrumb($title);
    }

    $crumbs->setBorder(true);

    $view = id(new PHUITwoColumnView())
      ->setFooter(array(
        $box,
      ));

    return $this->newPage()
      ->setTitle($title)
      ->setCrumbs($crumbs)
      ->appendChild($view);
  }
}
