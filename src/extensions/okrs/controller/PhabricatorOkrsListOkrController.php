<?php
abstract class PhabricatorOkrsBaseViewController extends PhabricatorController
{
}

final class PhabricatorOkrsListOkrController
    extends PhabricatorOkrsBaseViewController
{
    public function shouldAllowPublic()
    {
        return true;
    }

    public function handleRequest(AphrontRequest $request)
    {
        $request = $this->getRequest();
        $controller = id(new PhabricatorApplicationSearchController())
            ->setQueryKey($request->getURIData('queryKey'))
            ->setSearchEngine(new PhabricatorOkrsNamesearchEngine())
            ->setNavigation($this->buildSideNavView());

        return $this->delegateToController($controller);
    }

    public function buildSideNavView($for_app = false)
    {
        $user = $this->getRequest()->getUser();

        $nav = new AphrontSideNavFilterView();
        $nav->setBaseURI(new PhutilURI($this->getApplicationURI()));

        id(new PhabricatorOkrsNamesearchEngine())
            ->setViewer($user)
            ->addNavigationItems($nav->getMenu());

        $nav->selectFilter(null);

        return $nav;
    }

    protected function buildApplicationCrumbs()
    {
        $crumbs = parent::buildApplicationCrumbs();

        $can_create = $this->hasApplicationCapability(
            PhabricatorOkrsCapabilityCreateOkrs::CAPABILITY);

        $crumbs->addAction(
            id(new PHUIListItemView())
                ->setName(pht('New OKR'))
                ->setHref('/okrs/create/')
                ->setIcon('fa-plus-square')
                ->setDisabled(!$can_create)
                ->setWorkflow(!$can_create));

        return $crumbs;
    }


}
