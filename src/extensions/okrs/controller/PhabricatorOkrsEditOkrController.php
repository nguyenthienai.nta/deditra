<?php
abstract class PhabricatorOkrsBaseEditOkrController extends PhabricatorController
{
}

final class PhabricatorOkrsEditOkrController
    extends PhabricatorOkrsBaseEditOkrController
{
    public function handleRequest(AphrontRequest $request)
    {
        $viewer = $request->getUser();


        $id = $request->getURIData('id');
        if ($id) {
            $okr = id(new PhabricatorOkrsNameQuery())
                ->setViewer($viewer)
                ->withIDs(array($id))
                ->requireCapabilities(
                    array(
                        PhabricatorPolicyCapability::CAN_VIEW,
                        PhabricatorPolicyCapability::CAN_EDIT,
                    ))
                ->executeOne();
            if (!$okr) {
                return new Aphront404Response();
            }

            $is_new = false;
            $cancel_uri = '/okrs/';

            $header_text = pht('Edit %s', $okr->getContent());
            $title = pht('Edit OKR');
            $button_text = pht('Save Changes');
        } else {
            $this->requireApplicationCapability(
                PhabricatorOkrsCapabilityCreateOkrs::CAPABILITY);

            $okr = PhabricatorOkrsOkr::initializeNewOkrs($viewer);

            $is_new = true;
            $cancel_uri = $this->getApplicationURI();

            $header_text = pht('Create OKR');
            $title = pht('Create OKR');
            $button_text = pht('Create OKR');

        }

        $validation_exception = null;
        $e_name = true;
        $v_name = $okr->getContent();
        $v_view = $okr->getViewPolicy();
        $v_edit = $okr->getEditPolicy();

        if ($request->isFormPost()) {
            $xactions = array();

            $e_name = null;
            $v_name = $request->getStr('name');
            $v_view = $request->getStr('viewPolicy');
            $v_edit = $request->getStr('editPolicy');

            $type_name =
                PhabricatorOkrsOkrContentTransaction::TRANSACTIONTYPE;
            $type_view = PhabricatorTransactions::TYPE_VIEW_POLICY;
            $type_edit = PhabricatorTransactions::TYPE_EDIT_POLICY;

            $xactions[] = id(new PhabricatorOkrsOkrTransaction())
                ->setTransactionType($type_name)
                ->setNewValue($v_name);
            $xactions[] = id(new PhabricatorOkrsOkrTransaction())
                ->setTransactionType($type_view)
                ->setNewValue($v_view);

            $xactions[] = id(new PhabricatorOkrsOkrTransaction())
                ->setTransactionType($type_edit)
                ->setNewValue($v_edit);


            $editor = id(new PhabricatorOkrsNameEditor())
                ->setActor($viewer)
                ->setContinueOnNoEffect(true)
                ->setContentSourceFromRequest($request);

            try {
                $editor->applyTransactions($okr, $xactions);

                session_start();
                if (!$is_new) {
                    $_SESSION['message'] = 'OKR edited successfully';
                } else{
                    $_SESSION['message'] = 'OKR added successfully';
                }

                if( $request->getBool('check')){
                    return id(new AphrontRedirectResponse())
                        ->setURI('/okrs/create');
                }else
                    return id(new AphrontRedirectResponse())
                        ->setURI('/okrs/');
            } catch (PhabricatorApplicationTransactionValidationException $ex) {
                $validation_exception = $ex;

                $e_name = $ex->getShortMessage($type_name);
            }
        }
        $policies = id(new PhabricatorPolicyQuery())
            ->setViewer($viewer)
            ->setObject($okr)
            ->execute();
        Javelin::initBehavior(
            'submit-objective');
        $okrs_police = PhabricatorOkrsOkr::initializeNewOkrs($viewer);
        $form = id(new AphrontFormView())
            ->setUser($viewer);


        $form
            ->appendChild(
                id(new AphrontFormTextControl())
                    ->setLabel(pht('Name'))
                    ->setName('name')
                    ->setValue($v_name)
                    ->setError($e_name))
            ->appendChild(
                id(new AphrontFormPolicyControl())
                    ->setUser($viewer)
                    ->setCapability(PhabricatorPolicyCapability::CAN_VIEW)
                    ->setPolicyObject($okrs_police)
                    ->setPolicies($policies)
                    ->setValue($v_view)
                    ->setName('viewPolicy'))
            ->appendChild(
                id(new AphrontFormPolicyControl())
                    ->setUser($viewer)
                    ->setCapability(PhabricatorPolicyCapability::CAN_EDIT)
                    ->setPolicyObject($okrs_police)
                    ->setPolicies($policies)
                    ->setValue($v_edit)
                    ->setName('editPolicy'))
            ->addHiddenInput('check', 'false');

        if ($id) {
            if ($okr->getIsArchived()) {
                $form->appendChild(
                    id(new AphrontFormSubmitControl())
                        ->addButton(id(new PHUIButtonView())
                            ->setTag('a')
                            ->setHref($this->getApplicationURI('activate/' . $id . '/'))
                            ->setIcon('fa-check')
                            ->setText('Active')
                            ->setWorkflow(true)
                            ->setColor(PHUIButtonView::RED))
                        ->addButton(id(new PHUIButtonView())
                            ->setText('Save'))
                        ->addCancelButton($cancel_uri));
            }else{
                $form->appendChild(
                    id(new AphrontFormSubmitControl())
                        ->addButton(id(new PHUIButtonView())
                          ->setTag('a')
                          ->setHref($this->getApplicationURI('move/' . $id . '/'))
                          ->setIcon('fa-share')
                          ->setText('Move To Hephaestos')
                          ->setWorkflow(true)
                          ->setColor(PHUIButtonView::RED))
                        ->addButton(id(new PHUIButtonView())
                            ->setTag('a')
                            ->setHref($this->getApplicationURI('archive/' . $id . '/'))
                            ->setIcon('fa-ban')
                            ->setText('Archive')
                            ->setWorkflow(true)
                            ->setColor(PHUIButtonView::GREEN))
                        ->addButton(id(new PHUIButtonView())
                            ->setText('Save'))
                        ->addCancelButton($cancel_uri));
            }
        } else {
            $form->appendChild(
                id(new AphrontFormSubmitControl())
                    ->addButton(id(new PHUIButtonView())
                        ->setText('Save And New')
                        ->addSigil('submitObjetiveButton')
                        ->setColor(PHUIButtonView::GREEN))
                    ->addButton(id(new PHUIButtonView())
                        ->setText('Save'))
                    ->addCancelButton($cancel_uri)
            );
        }

        $box = id(new PHUIObjectBoxView())
            ->setHeaderText($title)
            ->setBackground(PHUIObjectBoxView::WHITE_CONFIG)
            ->setValidationException($validation_exception)
            ->appendChild($form);

        session_start();
        if (isset($_SESSION['message'])) {
            $box->setFormSaved(true, $_SESSION['message']);
            unset($_SESSION['message']);
        }

        $crumbs = $this->buildApplicationCrumbs();
        if (!$is_new) {
            $crumbs->addTextCrumb(
                "Edit OKR");
        }else{
          $crumbs->addTextCrumb($title);
        }

        $crumbs->setBorder(true);

        $view = id(new PHUITwoColumnView())
            ->setFooter(array(
                $box,
            ));

        return $this->newPage()
            ->setTitle($title)
            ->setCrumbs($crumbs)
            ->appendChild($view);
    }
}
