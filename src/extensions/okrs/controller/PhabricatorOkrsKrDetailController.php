<?php

abstract class PhabricatorOkrsBaseKrDetailController extends PhabricatorController
{
}

final class PhabricatorOkrsKrDetailController extends PhabricatorOkrsBaseKrDetailController
{
  public function handleRequest(AphrontRequest $request)
  {
    require_celerity_resource('krForm-css');
    require_celerity_resource('jquery-ui');
    $viewer = $request->getUser();

    $make_default = false;
    $id = $request->getURIData('id');

    require_celerity_resource('chart-css');
    require_celerity_resource('chartMin-css');

    Javelin::initBehavior(
      'chart-bundle'
    );
    Javelin::initBehavior(
      'chart'
    );
    Javelin::initBehavior(
      'chartjs-plugin-datalabels'
    );
    Javelin::initBehavior(
      'jquery'
    );
    Javelin::initBehavior(
      'jquery-ui'
    );
    Javelin::initBehavior(
      'multidatespicker'
    );
    Javelin::initBehavior(
      'kr'
    );
    Javelin::initBehavior(
      'chart-render'
    );

    $keyResult = id(new PhabricatorOkrsKeyResultQuery())
      ->setViewer($viewer)
      ->withIDs(array($id))
      ->requireCapabilities(
        array(
          PhabricatorPolicyCapability::CAN_VIEW,
//          PhabricatorPolicyCapability::CAN_EDIT,
        )
      )
      ->executeOne();

    $obj = id(new PhabricatorOkrsNamespace())->loadOneWhere('phid = %s', $keyResult->getObjectivePHID());
    $okrPHID = $obj->getOkrPHID();
    $okr = id(new PhabricatorOkrsOkr())->loadOneWhere('phid = %s', $okrPHID);

    $query = id(new PhabricatorOkrsNameQuery())
      ->setViewer($viewer)
      ->withIDs(array($okr->getID()))
      ->requireCapabilities(
        array(
          PhabricatorPolicyCapability::CAN_VIEW,
        ))
      ->executeOne();

    $can_edit = PhabricatorPolicyFilter::hasCapability(
      $viewer,
      $query,
      PhabricatorPolicyCapability::CAN_EDIT);
    $check_can_edit = $can_edit === true ? 1 : 0;
    if (!$keyResult) {
      return new Aphront404Response();
    }
    $objective = $keyResult->getObjective();

    $okrPHID = $objective->getOkrPHID();
    $okr = id(new PhabricatorOkrsOkr())->loadAllWhere('phid = %s', $okrPHID);
    $okr = array_pop(array_reverse($okr));

    $is_new = false;
    $cancel_uri = '/obj/' . $okr->getId();
    $edit_uri = '/kr/edit/' . $id;

    $header_text = pht('KR Detail');
    $title = pht('Detail %s', $keyResult->getContent());

    $v_content = $keyResult->getContent();
    $v_meansure = $keyResult->getMeansure();
    $v_baseValue = $keyResult->getBaseValue();
    $v_target = $keyResult->getTarget();
    switch ($keyResult->getManualCheckin()) {
      case 0:
        $v_manualCheckin = 'Manually';
        break;
      case 1:
        $v_manualCheckin = 'Remain of Project\'s point';
        break;
      case 2:
        $v_manualCheckin = 'Velocity';
        break;
      case 3:
        $v_manualCheckin = 'Effective of Develop';
        break;
      case 4:
        $v_manualCheckin = 'DEBIT Bug Rate';
        break;
      case 5:
        $v_manualCheckin = 'Customer Bug Rate';
        break;
    }
    $v_totalDev = $keyResult->getTotalDev();
    $v_typeChart = $keyResult->gettypeChart();
    $v_startDate = $keyResult->getStartDate() ?
      date('d/m/Y', $keyResult->getStartDate()) : $keyResult->getStartDate();
    $v_endDate = $keyResult->getEndDate() ?
      date('d/m/Y', $keyResult->getEndDate()) : $keyResult->getEndDate();
    $v_frequence = $keyResult->getFrequence();
    $v_frequenceTime = round($keyResult->getFrequenceTime());
    $v_confidenceLevel = $keyResult->getConfidenceLevel();
    $v_owner = array();
    $v_project = array();
    $v_date = array();
    $v_dayOffs = $keyResult->getDayOffs();
    $v_confidentScope = explode(',', $keyResult->getConfidentScope());
    $scope_Velocity = $v_confidentScope[0] == '' ? '' : 'Red  |  ' . 'Yellow: ' . $v_confidentScope[0] . ' - ' . $v_confidentScope[1] . '  |  Green';
    $scope_Hi = $v_confidentScope[0] == '' ? '' : 'Green  |  ' . 'Yellow: ' . $v_confidentScope[0] . ' - ' . $v_confidentScope[1] . '  |  Red';
    $scope = $v_manualCheckin == 'DEBIT Bug Rate' || $v_manualCheckin == 'Customer Bug Rate' ? $scope_Hi : $scope_Velocity;
    $v_ownerUserName = id(new PhabricatorUser())->loadOneWhere('PHID = %s', $keyResult->getOwnerPHID())->getUserName();
    $v_ownerRealName = id(new PhabricatorUser())->loadOneWhere('PHID = %s', $keyResult->getOwnerPHID())->getRealName();
    $v_owner = $v_ownerUserName . ' (' . $v_ownerRealName . ')';
    if ($keyResult->getProjectPHID() !== null) {
      $projects = id(new PhabricatorProjectQuery())
          ->setViewer($viewer)
          ->withPHIDs(explode(',', $keyResult->getProjectPHID()))
          ->execute();
      $projectInfo = [];
      foreach($projects as $project){
        $projectInfo[] = $viewer->renderHandle($project->getPHID())->setAsTag(true);
        $projectInfo[] = ' ';
      }
    }
    $projectPHID = $keyResult->getProjectPHID();
    $totalDev = $keyResult->getTotalDev();
    $arrayChart = ['label'=>[]];
    if($keyResult->getManualCheckin() == 0 || $keyResult->getManualCheckin() == 1){
      $arrayChart = $this->dataChart($keyResult);
    }
    if ($keyResult->getManualCheckin() == 2 && $projectPHID !== null) {
      $checkInData = id(new PhabricatorOkrsCurrent())->loadAllWhere('krPHID = %s', $keyResult->getPHID());
      $checkInData ? $lastFeedback = end($checkInData)->getFeedback() : '';
      if($checkInData){
        end($checkInData)->getVelocityData() != null ? $velocityChartData = $this->getVelocityChartData($keyResult) : $velocityChartData = array('label' => [''], 'value' => [0]);
      }
      else{
        $velocityChartData = array('label' => [''], 'value' => [null]);
      }
    }
    if ($keyResult->getManualCheckin() == 3 && $projectPHID !== null) {
      $effectiveOfDevelopChartData = $this->getEffectiveOfDevelopChartData($keyResult);
    }
    if (($keyResult->getManualCheckin() == 4) && $projectPHID !== null) {
      $arrayChart = $this->dataChart($keyResult, 'hii');
      $chartData_HII = array('label' => [''], 'value' => [null]);
    }
    if (($keyResult->getManualCheckin() == 5) && $projectPHID !== null) {
      $arrayChart = $this->dataChart($keyResult, 'hfi');
      $chartData_HII = array('label' => [''], 'value' => [null]);
    }
    $v_date = [$v_startDate, $v_endDate];
    $form = id(new AphrontFormView())
      ->setUser($viewer)
      ->setID('form_edit');

    if ($make_default) {
      $form->appendRemarkupInstructions(
        pht(
          'NOTE: You are creating the **default objective**. All existing ' .
          'objects will be put into this objective. You must create a default ' .
          'objective before you can create other Okrs.'
        )
      );
    }
    $form
      ->appendChild(
        phutil_tag(
          'input',
          array('id' => 'type_chart', 'type' => 'hidden', 'value' => $v_typeChart)
        )
      )
      ->appendControl(
        id(new AphrontFormStaticKRControl())
          ->setLabel(pht('Objective :'))
          ->setValue([$objective->getNamespaceName()])
      )
      ->appendControl(
        id(new AphrontFormHrControl())
      )
      ->appendControl(
        id(new AphrontFormStaticKRControl())
          ->setLabel(pht('KR :'))
          ->setValue($v_content)
      )
      ->appendControl(
        id(new AphrontFormHrControl())
      )
      ->appendControl(
        id(new AphrontFormStaticKRControl())
          ->setID('manual-checkin-detail')
          ->setLabel(pht('Measure by :'))
          ->setValue($v_manualCheckin)
      )
      ->appendControl(
        id(new AphrontFormHrControl())
      )
      ->appendControl(
        id(new AphrontFormStaticKRDateControl())
          ->setLabel(pht('Date :'))
          ->setValue($v_date)
      )
      ->appendControl(
        id(new AphrontFormHrControl())
      );

    if ($v_manualCheckin != 'Velocity') {
      $form->appendChild(
        id(new AphrontFormStaticKRControl())
          ->setLabel(pht('Day-Offs :'))
          ->setValue($v_dayOffs)
      )
        ->appendControl(
        id(new AphrontFormHrControl())
      );
    }
    if(($keyResult->getManualCheckIn() == 4 || $keyResult->getManualCheckIn() == 5) && $keyResult->getAutoCheckin() == 1){
      $form->appendControl(
        id(new AphrontFormStaticKRControl())
          ->setLabel(pht('Dev :'))
          ->setValue((!isset($v_totalDev) || $v_totalDev == 0) ? '' : $v_totalDev)
      )
        ->appendControl(
          id(new AphrontFormHrControl())
        );
    }
    if ($v_manualCheckin == 'Manually') {
      $form
        ->appendControl(
          id(new AphrontFormStaticKRControl())
            ->setLabel(pht('Base :'))
            ->setValue($v_baseValue)
        )
        ->appendControl(
          id(new AphrontFormHrControl())
        )
        ->appendControl(
          id(new AphrontFormStaticKRControl())
            ->setLabel(pht('Target :'))
            ->setValue($v_target)
        );
    } else {
      $form->appendControl(
        id(new AphrontFormStaticKRControl())
          ->setLabel(pht('Project :'))
          ->setValue($projectInfo)
      );
    }
    $form->appendControl(
      id(new AphrontFormHrControl())
    );
    if ($v_manualCheckin == 'Manually' || $v_manualCheckin == 'Remain of Project\'s point') {
      $form->appendControl(
        id(new AphrontFormStaticKRControl())
          ->setLabel(pht('Chart Type :'))
          ->setValue($v_typeChart == 0 ? 'Burnup Chart' : 'Burndown Chart')
      )
        ->appendControl(
          id(new AphrontFormHrControl())
        );
    }
    if ($v_manualCheckin == 'Manually') {
      $form
        ->appendControl(
          id(new AphrontFormStaticKRControl())
            ->setLabel(pht('Description :'))
            ->setValue($v_meansure)
        )
        ->appendControl(
          id(new AphrontFormHrControl())
        );
    }
    $form->appendControl(
      id(new AphrontFormStaticKRControl())
        ->setLabel(pht('Owner :'))
        ->setValue($v_owner)
    )
      ->appendControl(
        id(new AphrontFormHrControl())
      );
    if ($v_manualCheckin != 'Manually' && $v_manualCheckin != 'Remain of Project\'s point') {
      $form
        ->appendControl(
          id(new AphrontFormStaticKRControl())
            ->setLabel(pht('Confidence Scope :'))
            ->setValue($scope)
        )
        ->appendControl(
          id(new AphrontFormHrControl())
        );;
    }
    $form->addHiddenInput('check', 'false')
      ->addHiddenInput
      (
        'data',
        json_encode(array('id' => $id, 'target' => $v_target))
      )
      ->addHiddenInput
      (
        'labelChart',
        isset($arrayChart) ? json_encode($arrayChart['label']) : ''
      )
      ->addHiddenInput
      (
        'labelDateChart',
        isset($arrayChart) ? json_encode($arrayChart['labelDate']) : ''
      )
      ->addHiddenInput
      (
        'dataChart',
        isset($arrayChart) ? json_encode($arrayChart['data']) : ''
      )
      ->addHiddenInput
      (
        'colorChart',
        isset($arrayChart) ? json_encode($arrayChart['color']) : ''
      )
      ->addHiddenInput
      (
        'dataTarget',
        isset($arrayChart) ? json_encode($arrayChart['data2']) : ''
      )
      ->addHiddenInput
      (
        'emotions',
        isset($arrayChart) ? json_encode($arrayChart['emotions']) : ''
      )
      ->addHiddenInput
      (
        'nameCurrent',
        isset($arrayChart) ? json_encode($arrayChart['nameCurrent']) : ''
      )
      ->addHiddenInput
      (
        'scopeLine',
        isset($arrayChart) ? json_encode($arrayChart['data3']) : ''
      )
      ->addHiddenInput
      (
        'velocityChartData',
        isset($velocityChartData) ? (count($velocityChartData['label']) > 0 ? json_encode($velocityChartData) : json_encode(array('label' => [], 'value' => []))) : json_encode(array('null'))
      )
      ->addHiddenInput
      (
        'effectiveOfDevelopChartData',
        isset($effectiveOfDevelopChartData) ? (count($effectiveOfDevelopChartData['label']) > 0 ? json_encode($effectiveOfDevelopChartData) : json_encode(array('label' => [], 'value' => []))) : json_encode(array('null'))
      )
      ->addHiddenInput('chartData_HII',
        isset($chartData_HII) ? (count($chartData_HII['label']) > 0 ? json_encode($chartData_HII) : json_encode(array('label' => [], 'value' => []))) : json_encode(array('null'))
      )
      ->addHiddenInput
      (
        'chartData_HFI',
        isset($chartData_HFI) ? (count($chartData_HFI['label']) > 0 ? json_encode($chartData_HFI) : json_encode(array('label' => [], 'value' => []))) : json_encode(array('null'))
      )
      ->addHiddenInput
      (
        'taskID',
        isset($arrayChart) ? json_encode($arrayChart['taskID']) : ''
      )
      ->addHiddenInput
      (
        'estimation',
        isset($arrayChart) ? json_encode($arrayChart['estimation']) : ''
      )
      ->addHiddenInput
      (
        'dayOffs',
        json_encode(explode(", ",$keyResult->getDayOffs()))
      )
        ->addHiddenInput
      (
        'confidentScope',
        json_encode($v_confidentScope)
      )
      ->addHiddenInput('lastFeedback', isset($lastFeedback) ? $lastFeedback : 6)
      ->addHiddenInput
      (
        'resolvedPoint',
        isset($arrayChart) ? json_encode($arrayChart['resolvedPoint']) : ''
      )
      ->addHiddenInput
      (
        'pointToResolve',
        isset($arrayChart) ? json_encode($arrayChart['pointToResolve']) : ''
      )
      ->addHiddenInput('oldMeasure', $keyResult->getManualCheckin());



    if (isset($arrayChart)) {
      foreach ($arrayChart['task'] as $key => $arrayTask) {
        $form->addHiddenInput
        (
          'task' . $key,
          isset($arrayChart) ? json_encode($arrayTask) : ''
        );
      }
    }

    if ($check_can_edit === 1){
      $form->appendChild(
        id(new AphrontFormSubmitControl())
          ->addEditButton($edit_uri)
          ->addBackButton($cancel_uri)
      );
    }else{
      $form->appendChild(
        id(new AphrontFormSubmitControl())
          ->addBackButton($cancel_uri)
      );
    }

    $crumbs = $this->buildApplicationCrumbs();
    $crumbs->addTextCrumb(pht('Create KRs'));
    $crumbs->setBorder(true);

    $box = id(new PHUIOkrsBoxView())
      ->setHeaderText($header_text)
      ->setBackground(PHUIObjectBoxView::WHITE_CONFIG)
//      ->setValidationException($validation_exception)
      ->appendChild($form);

    session_start();
    if (isset($_SESSION['message'])) {
      $box->setFormSaved(true, $_SESSION['message']);
      unset($_SESSION['message']);
    }

    $crumbs = $this->buildApplicationCrumbs();
    $crumbs->addTextCrumb(
      $okr->getContent(),
      $cancel_uri);


    $crumbs->addTextCrumb($objective->getNamespaceName());
    $viewFooter = [$box];
    if (!$is_new) {
      $crumbs->addTextCrumb(
        "KR Detail"
      );
      $currents = id(new PhabricatorOkrsCurrent())
        ->loadAllWhere('krPHID = %s', $keyResult->getPHID());

      $timeline = $this->buildTransactionTimeline(
        $currents,
        new PhabricatorOkrsCurrentTransactionQuery());
      $timeline->setShouldTerminate(true);
      array_push($viewFooter, $timeline);
    }

    if ($is_new) {
      $crumbs->addTextCrumb($header_text);
    }

    $crumbs->setBorder(true);

    $view = id(new PHUITwoColumnView())
      ->setFooter($viewFooter);

    return $this->newPage()
      ->setTitle($title)
      ->setCrumbs($crumbs)
      ->appendChild($view);
  }

  private function dataChart($keyResult, $type = null)
  {
    $arrays = id(new PhabricatorOkrsCurrent())
      ->loadAllWhere('krPHID = %s', $keyResult->getPHID());
    $dayOffs =  explode(", ",$keyResult->getDayOffs());
    $begin = new DateTime(date('Y-m-d', $keyResult->getStartDate()));
    $end = new DateTime(date('Y-m-d', $keyResult->getEndDate()));
    $v_dayOfWeeks = $keyResult->getDayOfWeeks();
    if($keyResult->getFrequence() == 'Day') {
      $end = $end->modify('+1 day');
    }

    ($keyResult->getManualCheckIn() != 4 && $keyResult->getManualCheckIn() != 5) ?
      $tasks = $this->getTasksFromKr($keyResult) : $tasks = array();

    $interval = new DateInterval(
      'P' .
      (
      ($keyResult->getFrequenceTime() > 1) ? $keyResult->getFrequenceTime() : 1
      ) .
      (
      $keyResult->getFrequence() == 'Day' ? 'D' :
        ($keyResult->getFrequence() == 'Week' ? 'W' : 'M')
      )
    );
    $daterange = new DatePeriod($begin, $interval, $end);
    $arrayDate = [];

    if (($keyResult->getFrequence()) == 'Week' && ($keyResult->getDayOfWeeks() != "")) {
      $start_day = $begin->format('d-m-Y');
      $start_strtotime = strtotime($begin->format('d-m-Y'));
      $end_day = $end->format('d-m-Y');
      $end_strtotime = strtotime($end_day);
      foreach ($daterange as $key => $date) {
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '1', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '2', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '3', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '4', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '5', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '6', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, '7', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key);
      }
      if ($end_strtotime > strtotime($arrayDate['label'][count($arrayDate['label']) - 1])) {
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '1', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '2', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '3', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '4', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '5', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '6', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $end, '7', $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, 1);
      }
      if(!in_array($end_day, $arrayDate['label'])) {
        $arrayDate['label'][] = $end_day;
        $arrayDate['data'][] = null;
        $arrayDate['data2'][] = null;
        $arrayDate['emotions'][] = null;
        $arrayDate['color'][] = null;
        $arrayDate['data3'][] = null;
      }
      if(!in_array($start_day, $arrayDate['label'])) {
        array_unshift($arrayDate['label'],$start_day);
        array_unshift($arrayDate['data'], null);
        array_unshift($arrayDate['data2'], null);
        array_unshift($arrayDate['emotions'], null);
        array_unshift($arrayDate['color'], null);
        array_unshift($arrayDate['data3'], null);
      }
    } else if(($keyResult->getFrequence() == 'Day') && ($v_dayOfWeeks == 'every-weekday')){
      $myDays = array('1', '2', '3', '4', '5');
      $start_day = $begin->format('d-m-Y');
      $end = $end->modify('-1 day');
      $end_day = $end->format('d-m-Y');
      foreach ($daterange as $date) {
        if (in_array($date->format('N'), $myDays) && !in_array($date->format('d/m/Y'), $dayOffs)) {
          $arrayDate['label'][] = $date->format('d-m-Y');
          $arrayDate['data'][] = null;
          $arrayDate['data2'][] = null;
          $arrayDate['emotions'][] = null;
          $arrayDate['color'][] = null;
          $arrayDate['data3'][] = null;
        }
      }
      if(!in_array($end_day, $arrayDate['label'])) {
        $arrayDate['label'][] = $end_day;
        $arrayDate['data'][] = null;
        $arrayDate['data2'][] = null;
        $arrayDate['emotions'][] = null;
        $arrayDate['color'][] = null;
        $arrayDate['data3'][] = null;
      }
      if(!in_array($start_day, $arrayDate['label'])) {
        array_unshift($arrayDate['label'],$start_day);
        array_unshift($arrayDate['data'], null);
        array_unshift($arrayDate['data2'], null);
        array_unshift($arrayDate['emotions'], null);
        array_unshift($arrayDate['color'], null);
        array_unshift($arrayDate['data3'], null);
      }
    } else if(($keyResult->getFrequence()) == 'Month' && $keyResult->getDayOfWeeks()) {
      $start_day = $begin->format('d-m-Y');
      $start_strtotime = strtotime($begin->format('d-m-Y'));
      $end_day = $end->format('d-m-Y');
      $end_strtotime = strtotime($end_day);
      if(strpos($keyResult->getDayOfWeeks(), ',') !== false) {
        $dayOfWeek = $this->getNameDay(explode(',', $keyResult->getDayOfWeeks())[1]);
        $position = explode(',', $keyResult->getDayOfWeeks())[0];
        foreach ($daterange as $key => $date) {
          if (strtotime($date->modify("$position $dayOfWeek of this month")->format('d-m-Y')) >= $start_strtotime &&
            strtotime($date->modify("$position $dayOfWeek of this month")->format('d-m-Y')) <= $end_strtotime &&
            (!in_array($date->modify("$position $dayOfWeek of this month")->format('d-m-Y'), $arrayDate['label']))
          ) {
            if (!in_array($date->modify("$position $dayOfWeek of this month")->format('d/m/Y'), $dayOffs)) {
              $arrayDate['label'][] = $date->modify("$position $dayOfWeek of this month")->format('d-m-Y');
              $arrayDate['data'][] = null;
              $arrayDate['data2'][] = null;
              $arrayDate['emotions'][] = null;
              $arrayDate['color'][] = null;
              $arrayDate['data3'][] = null;
            }
          }
        }
      } else {
        foreach ($daterange as $key => $date) {
          $dayOfMonth = $keyResult->getDayOfWeeks();
          $currentMonth = $date->format('m');
          $currentYear = $date->format('Y');
          $checkDate = checkdate($dayOfMonth , $currentMonth, $currentYear);

          if($checkDate) {
            if (strtotime("$dayOfMonth-$currentMonth-$currentYear") >= $start_strtotime &&
              strtotime(strtotime("$dayOfMonth-$currentMonth-$currentYear")) <= $end_strtotime &&
              (!in_array("$dayOfMonth-$currentMonth-$currentYear", $arrayDate['label']))
            ) {
              if (!in_array("$dayOfMonth-$currentMonth-$currentYear", $dayOffs)) {
                $arrayDate['label'][] = "$dayOfMonth-$currentMonth-$currentYear";
                $arrayDate['data'][] = null;
                $arrayDate['data2'][] = null;
                $arrayDate['emotions'][] = null;
                $arrayDate['color'][] = null;
                $arrayDate['data3'][] = null;
              }
            }
          } else {
            if (strtotime($date->modify('last day of this month')->format('d-m-Y')) >= $start_strtotime &&
              strtotime($date->modify('last day of this month')->format('d-m-Y')) <= $end_strtotime &&
              (!in_array($date->modify('last day of this month')->format('d-m-Y'), $arrayDate['label']))
            ) {
              if (!in_array($date->modify('last day of this month')->format('d/m/Y'), $dayOffs)) {
                $arrayDate['label'][] = $date->modify('last day of this month')->format('d-m-Y');
                $arrayDate['data'][] = null;
                $arrayDate['data2'][] = null;
                $arrayDate['emotions'][] = null;
                $arrayDate['color'][] = null;
                $arrayDate['data3'][] = null;
              }
            }
          }
        }
      }
      if(!in_array($end_day, $arrayDate['label'])) {
        $arrayDate['label'][] = $end_day;
        $arrayDate['data'][] = null;
        $arrayDate['data2'][] = null;
        $arrayDate['emotions'][] = null;
        $arrayDate['color'][] = null;
        $arrayDate['data3'][] = null;
      }
      if(!in_array($start_day, $arrayDate['label'])) {
        array_unshift($arrayDate['label'],$start_day);
        array_unshift($arrayDate['data'], null);
        array_unshift($arrayDate['data2'], null);
        array_unshift($arrayDate['emotions'], null);
        array_unshift($arrayDate['color'], null);
        array_unshift($arrayDate['data3'], null);
      }
    }
    else {
      foreach ($daterange as $date) {
        $arrayDate = $this->dayOfWeekDataChart($arrayDate, $date, null, null, $dayOffs, null, null, 1);
      }
      if (count($arrayDate['label']) === 0) {
        $arrayDate['label'][] = $begin->format('d-m-Y');;
        $arrayDate['data'][] = null;
        $arrayDate['data2'][] = null;
        $arrayDate['emotions'][] = null;
        $arrayDate['color'][] = null;
        $arrayDate['data3'][] = null;
      }
    }

    $arrayChart = $arrayDate;
    $count = 1;
    $arrayChart['color'][0] = 'green';
    foreach ($arrayDate['label'] as $key => $dateChart) {
      foreach ($arrays as $value) {
        if($type == 'hii' && $value->getHII() === null) {
          continue;
        } else if ($type == 'hfi' && $value->getHFI() === null) {
          continue;
        }
        $dateCheckin = strtotime(date('d-m-Y', $value->getDateCreated()));
        $color = $value->getColor() == 1 ? 'green' :
          ($value->getColor() == 2 ? 'yellow' : 'red');
        if ($dateCheckin == (int)strtotime($dateChart)) {
          $arrayChart['data'][$key + $count - 1] = $this->getCurrentWithTypeChart($keyResult, $value);
          $arrayChart['color'][$key + $count - 1] = $color;
          $arrayChart['emotions'][$key + $count - 1] = $value->getFeedBack();
          $arrayChart['data2'][$key + $count - 1] = null;
          $arrayChart['data3'][$key + $count - 1] = null;
        }
        if(!in_array(date('d/m/Y', $value->getDateCreated()), $dayOffs)){
          if (
            $dateCheckin > (int)strtotime($dateChart) &&
            $dateCheckin < (int)strtotime($arrayDate['label'][$key + 1])
          ) {
            array_splice
            (
              $arrayChart['label'],
              $key + $count,
              0,
              array(date('d-m-Y', $value->getDateCreated()))
            );
            array_splice
            (
              $arrayChart['data'],
              $key + $count,
              0,
              array($this->getCurrentWithTypeChart($keyResult, $value))
            );
            array_splice
            (
              $arrayChart['emotions'],
              $key + $count,
              0,
              array($value->getFeedBack())
            );
            array_splice
            (
              $arrayChart['data2'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['data3'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['color'],
              $key + $count,
              0,
              array($color)
            );
            $count++;
          }
        }
      }
    }

    $arrayChart['task'] = [];
    $arrayChart['taskID'] = [];

    if($keyResult->getManualCheckIn() != 4 && $keyResult->getManualCheckIn() != 5) {
      $arrayChart = $this->getTaskEndChartData($tasks, $arrayChart, $dayOffs);
      $target = $this->getTargetFromKrHasProject($keyResult);
    } else {
      $target = 100;
    };

    $arrayChart['estimation'] = [];
    if ($keyResult->getProjectPHID() !== null && ($keyResult->getManualCheckIn() == 1)) {
      $arrayChart = $this->getEstimationChartData($tasks, $arrayChart, $keyResult, $dayOffs);
    }

    if ($arrayChart['data'][0] == null) {
      $arrayChart['data'][0] = ($keyResult->getTypeChart() != 1) ? $keyResult->getBaseValue() : $target;
    }
    $arrayChart['data2'][0] = ($keyResult->getTypeChart() != 1) ? 0 : $target;
    $arrayChart['data2'][count($arrayChart['label']) - 1] = ($keyResult->getTypeChart() != 1) ? $target : 0;
    $arrayChart['data3'][0] = $target;
    $arrayChart['data3'][count($arrayChart['label']) - 1] = $target;

    $arrayChart['nameCurrent'] = "Check-in Value";

    foreach ($arrayChart['label'] as $k => $chart) {
      $arrayChart['labelDate'][$k] = str_replace('-', '/', substr($chart, 0, 5));
    }
    return $arrayChart;
  }

  protected function getTaskEndChartData($tasks, $arrayChart, $dayOffs)
  {
    foreach ($tasks as $task) {
      if ($task->getEndDate() != null) {
        $arrayChart['taskID'][] = $task->getID();
        $endDate = strtotime(date('d-m-Y', $task->getEndDate()));
        foreach ($arrayChart['label'] as $key => $dateChart) {
          $arrayChart['task'][$task->getID()][] = null;
          if ($endDate == (int)strtotime($dateChart)) {
            $arrayChart['task'][$task->getID()][$key] = '0';
          }

          if(!in_array(date('d/m/Y', $task->getEndDate()) , $dayOffs)){
            if (
              $endDate > (int)strtotime($dateChart) &&
              $endDate < (int)strtotime($arrayChart['label'][$key + 1])
            ) {
              array_splice
              (
                $arrayChart['label'],
                $key + 1,
                0,
                array(date('d-m-Y', $task->getEndDate()))
              );

              array_splice
              (
                $arrayChart['data'],
                $key + 1,
                0,
                array(null)
              );
              array_splice
              (
                $arrayChart['emotions'],
                $key + 1,
                0,
                array(null)
              );
              array_splice
              (
                $arrayChart['data2'],
                $key + 1,
                0,
                array(null)
              );
              array_splice
              (
                $arrayChart['data3'],
                $key + 1,
                0,
                array(null)
              );
              array_splice
              (
                $arrayChart['color'],
                $key + 1,
                0,
                array(null)
              );
              foreach ($arrayChart['task'] as $name => $array) {
                if ($name == $task->getID()) {

                  $arrayChart['task'][$task->getID()][] = 0;

                } else {
                  array_splice
                  (
                    $arrayChart['task'][$name],
                    $key + 1,
                    0,
                    array(null)
                  );
                }
              }
            }
          }
        }
      }
    }
    return $arrayChart;
  }

  private function getPointOfDateCheckin($keyResult)
  {
    $arrays = id(new PhabricatorOkrsCurrent())
      ->loadAllWhere('krPHID = %s', $keyResult->getPHID());
    $points = $arrays ? json_decode(end($arrays)->getResolvedPoint()) : null;
    return $points;
  }

  protected function getEstimationChartData($tasks, $arrayChart, $keyResult, $dayOffs)
  {
    $target = $this->getTargetFromKrHasProject($keyResult);
    $point = ($keyResult->getTypeChart() != 1) ? 0 : $target;
    $arr_Point = (array)$this->getPointOfDateCheckin($keyResult);
    $array_resolvedPoint = (array)$arr_Point["resolvedPoint"];
    $array_pointToResolve = (array)$arr_Point["pointToResolve"];
    foreach ($arrayChart['label'] as $key => $dateChart) {
      $arrayChart['estimation'][] = null;
      $arrayChart['resolvedPoint'][] = null;
      $arrayChart['pointToResolve'][] = null;
      $resolvedPoint = 0;
      $pointToResolve = 0;

      foreach ($tasks as $task) {
        if ($task->getEndDate() != null) {
          $endDate = strtotime(date('d-m-Y', $task->getEndDate()));
          $taskStatus = $task->getStatus();
          if ($endDate == (int)strtotime($dateChart)) {
            if (($keyResult->getTypeChart() != 1))
              $point += $task->getPoints() != null ? $task->getPoints() : 0;
            else
              $point -= $task->getPoints() != null ? $task->getPoints() : 0;

            if (!$arr_Point) {
              if ($taskStatus == 'resolved' || $taskStatus == 'closed')
                $resolvedPoint += $task->getPoints() != null ? $task->getPoints() : null;
              elseif ($taskStatus == 'open' || $taskStatus == 'doing')
                $pointToResolve += $task->getPoints() != null ? $task->getPoints() : null;

              $arrayChart['resolvedPoint'][$key] = null;
              $arrayChart['pointToResolve'][$key] = $pointToResolve;
            }

            $arrayChart['estimation'][$key] = $point;
          }
        }
      }

      $date = date_format(date_create($dateChart), 'd/m/Y');
      if(isset($array_pointToResolve[$date]))
        $arrayChart['pointToResolve'][$key] = $array_pointToResolve[$date];
    }
    if ($arrayChart['estimation'][0] == null)
      $arrayChart['estimation'][0] = ($keyResult->getTypeChart() != 1) ? 0 : $target;
    if ($arrayChart['estimation'][$key] == null)
      $arrayChart['estimation'][$key] = $point;

    $count = 1;
    $arrayDate = $arrayChart;
    foreach ($arrayDate['label'] as $key => $dateChart) {
      foreach ($array_resolvedPoint as $dateResolved => $resolvedPoint){
        if(is_null($resolvedPoint)){
          continue;
        }
        $date = str_replace('/', '-', $dateResolved);
        $timeResolved = (int)strtotime($date);
        if ($timeResolved == (int)strtotime($dateChart)) {
          $arrayChart['resolvedPoint'][$key +  $count - 1] = $resolvedPoint;
          continue;
        }
        else if(!in_array(date('d/m/Y', $timeResolved) , $dayOffs)){
          if (
            (int)$timeResolved > (int)strtotime($dateChart) &&
            (int)$timeResolved < (int)strtotime($arrayDate['label'][$key + 1])
          ) {
            array_splice
            (
              $arrayChart['label'],
              $key + $count,
              0,
              array($date)
            );

            array_splice
            (
              $arrayChart['data'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['emotions'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['data2'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['data3'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['color'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['estimation'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['pointToResolve'],
              $key + $count,
              0,
              array(null)
            );
            array_splice
            (
              $arrayChart['resolvedPoint'],
              $key + $count,
              0,
              array($resolvedPoint)
            );
            foreach ($arrayChart['task'] as $name => $array) {
              array_splice
              (
                $arrayChart['task'][$name],
                $key + $count,
                0,
                array(null)
              );
            }
            $count++;
          }
        }
      }
    }
    return $arrayChart;
  }

  public function getVelocityChartData($keyResult)
  {
    $checkInData = id(new PhabricatorOkrsCurrent())->loadAllWhere('krPHID = %s', $keyResult->getPHID());
    return $checkInData == null ? null : json_decode(end($checkInData)->getVelocityData(), TRUE);
  }

  protected function buildTransactionTimeline(
    $objects,
    PhabricatorApplicationTransactionQuery $query = null,
    PhabricatorMarkupEngine $engine = null,
    $view_data = array())
  {

    $request = $this->getRequest();
    $viewer = $this->getViewer();

    $pager = id(new AphrontCursorPagerView())
      ->readFromRequest($request);

    if (empty($objects)) {
      $handle = id(new PhabricatorObjectHandle());
      $event = id(new PHUITimelineEventView())
        ->setUserHandle($handle)
        ->setIcon('fa-exclamation')
        ->setTitle(pht('This KR not have any check-in'))
        ->setUser($viewer);
      $timeline = id(new PHUITimelineView());
      $timeline->setUser($viewer);
      $timeline->addEvent($event);
      return $timeline;
    }
    $object = end($objects);

    $objectPHIDs = mpull($objects, 'getPHID');

    $xactions = $query
      ->setViewer($viewer)
      ->withObjectPHIDs($objectPHIDs)
      ->needComments(true)
      ->executeWithCursorPager($pager);
    $xactions = array_reverse($xactions);

    $timeline_engine = PhabricatorTimelineEngine::newForObject($object)
      ->setViewer($viewer)
      ->setTransactions($xactions)
      ->setViewData($view_data);

    $view = $timeline_engine->buildTimelineView();

    $timeline = $view
      ->setPager($pager)
      ->setQuoteTargetID($this->getRequest()->getStr('quoteTargetID'))
      ->setQuoteRef($this->getRequest()->getStr('quoteRef'));

    return $timeline;
  }

  private function getTargetFromKrHasProject($kr)
  {
    if ($kr->getProjectPHID() == null || ($kr->getManualCheckIn() == 0)) {
      return $kr->getTarget();
    } else {
      $project = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $kr->getProjectPHID());
      $state = id(new PhabricatorOkrsViewState())
        ->setProject($project)
        ->readFromRequest($this->getRequest());
      $tasks = $state->getObjects();
      $totalPoint = 0;
      foreach ($tasks as $taskID => $task) {
        if ($task->getPoints() !== null) {
          $totalPoint += $task->getPoints();
        }
      }
      return $totalPoint;
    }
  }

  private function getTasksFromKr($kr)
  {
    if ($kr->getProjectPHID() == null || ($kr->getManualCheckIn() == 0)) {
      return null;
    } else {
      $project = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $kr->getProjectPHID());
      $state = id(new PhabricatorOkrsViewState())
        ->setProject($project)
        ->readFromRequest($this->getRequest());
      $tasks = $state->getObjects();
      return $tasks;
    }
  }


  protected function getCurrentWithTypeChart($keyResult, $current)
  {
    if ($keyResult->getTypeChart() != 1) {
      return $current->getCurrent();
    } else {
      return $this->getTargetFromKrHasProject($keyResult) - $current->getCurrent();
    }
  }

  protected function getMilestones($project)
  {
    $viewer = $this->getViewer();
    $allows_milestones = $project->supportsMilestones();

    if ($allows_milestones) {
      $milestones = id(new PhabricatorProjectQuery())
        ->setViewer($viewer)
        ->withParentProjectPHIDs(array($project->getPHID()))
        ->needImages(true)
        ->withIsMilestone(true)
        ->setOrderVector(array('milestoneNumber', 'id'))
        ->execute();
    } else {
      $milestones = array();
    }
    return $milestones;
  }

  public function getEffectiveOfDevelopChartData($keyResult)
  {
    $data = [];
    $data['totalTimeDEV'] = [];
    $data['totalTimeBUG'] = [];
    $data['totalTime'] = [];
    $data['label'] = [];
    $data['value'] = [];
    $data['emotion'] = [];

    $currents = id(new PhabricatorOkrsCurrent())
        ->loadAllWhere('krPHID = %s', $keyResult->getPHID());
    $currents = $this->filterArrayCurrentTableEffective($currents);
    $dayOffs =  explode(", ",$keyResult->getDayOffs());
    $startDate = $keyResult->getStartDate();
    $endDate = strtotime(date('d-m-Y 24:59:59', $keyResult->getEndDate()));
    foreach ($currents as $current){
      $dateCreate = $current->getDateCreated();
      if(!in_array(date('d/m/Y', $dateCreate), $dayOffs) && $dateCreate >= $startDate && $dateCreate <= $endDate){
        $effective = json_decode($current->getEffective());
        $confidenceLevel = $current->getColor();
        $emotion = $current->getFeedback();
        $dateCheckin = date("d/m", $current->getDateCreated());
        $currentValue = $current->getCurrent();
        $data['totalTimeDEV'][] = $effective->totalTimeDEV;
        $data['totalTimeBUG'][] = $effective->totalTimeBUG;
        $data['totalTime'][] = $effective->totalTime;
        $data['label'][] = $dateCheckin;
        $data['value'][] = $currentValue;
        $data['emotion'][] = $emotion;
        $data['confidenceLevel'][] = $this->getColorConfidenceLevel($confidenceLevel);
      }
    }
    if(empty($data['label'])){
      $data['label'][] = 'No data checkin';
    }
    return $data;
  }

  private function getColorConfidenceLevel($confidenceLevel){
    $color = null;
    switch ($confidenceLevel) {
      case 1:
        $color = 'green';
        break;
      case 2:
        $color = 'yellow';
        break;
      case 3:
        $color = 'red';
        break;
      default:
        $color = 'green';
    }
    return $color;
  }

  private function filterArrayCurrentTableEffective($arrayCurrentTable){
    $arrayFilter = [];
    foreach($arrayCurrentTable as $current){
      if($current->getEffective() != null){
        $arrayFilter[] = $current;
      }
    }
    return $arrayFilter;
  }



  protected function getIndexChartDataOfMilestones($project , $data, $total_dev, $milestone_names, $index)
  {
    $state = id(new PhabricatorOkrsViewState())->setProject($project)->readFromRequest($this->getRequest());
    $tasks = $state->getObjects();
    $index === 'DEBIT Bug Rate' ? $bugTag = 'Bug Report (QA)' : $bugTag = 'Bug Report (Customer)';

    $bug = id(new PhabricatorProject())->loadOneWhere('name = %s', $bugTag);
    $bugPHID = isset($bug) ? $bug->getPHID() : null;

    $totalBug = 0;
    $type = 'edge';
    foreach ($tasks as $taskID => $task) {
      $index === 'DEBIT Bug Rate' ?  $isBug = $this->checkTaskIsBugQA($task, $bugPHID) :  $isBug = $this->checkTaskIsBugQA($task, $bugPHID);

      if ($isBug == true) {
        $xactions = $this->getXactionsFromTask($task, $type);

        foreach($xactions as $xaction){
          if($xaction->getNewValue()[0]){
            $project_first_xaction = id(new PhabricatorProject())->loadOneWhere('PHID = %s', $xaction->getNewValue()[0]);
            if($project_first_xaction != null){
              $project_tag = $project_first_xaction->getName();
              if (in_array($project_tag, $milestone_names)){
                $data[$project_tag]++;
                break;
              }
            }
          }
          if(array_keys($xaction->getNewValue())[0]){
            $project_first_xaction1 = id(new PhabricatorProject())->loadOneWhere('PHID = %s', array_keys($xaction->getNewValue())[0]);
            if($project_first_xaction1 != null){
              $project_tag1 = $project_first_xaction1->getName();
              if(in_array($project_tag1 ,$milestone_names)){
                $data[$project_tag1]++;
                break;
              }
            }
          }
        }
      }
    }
    return $data;
  }

  protected function getXactionsFromTask($task, $type)
  {
    $request = $this->getRequest();
    $viewer = $this->getViewer();
    $pager = id(new AphrontCursorPagerView())
      ->readFromRequest($request);
    $query = id(new ManiphestTransactionQuery());

    if($type === 'edge'){
      $xactions = $query
        ->setViewer($viewer)
        ->withObjectPHIDs(array($task->getPHID()))
        ->withTransactionTypes(array(PhabricatorTransactions::TYPE_EDGE))
        ->needComments(true)
        ->executeWithCursorPager($pager);
    }
    elseif($type === 'columns'){
      $xactions = $query
        ->setViewer($viewer)
        ->withObjectPHIDs(array($task->getPHID()))
        ->withTransactionTypes(array(PhabricatorTransactions::TYPE_COLUMNS))
        ->needComments(true)
        ->executeWithCursorPager($pager);
    }
    return array_reverse($xactions);
  }

  protected function checkTaskIsBug($task, $bugQAPHID, $bugCustomerPHID = null)
  {
    $tags = $task->getProjectPHIDs();
    $isBug = false;
    if(in_array($bugQAPHID, $tags) || in_array($bugCustomerPHID, $tags)){
      $isBug = true;
    }
    return $isBug;
  }

  protected function checkTaskIsBugQA($task, $bugQAPHID)
  {
    $tags = $task->getProjectPHIDs();
    $isBug = false;
    if(in_array($bugQAPHID, $tags)){
      $isBug = true;
    }
    return $isBug;
  }


  protected function checkTaskIsBugCustomer($task, $bugCustomerPHID)
  {
    $tags = $task->getProjectPHIDs();
    $isBug = false;
    if(in_array($bugCustomerPHID, $tags)){
      $isBug = true;
    }
    return $isBug;
  }

//  protected function getTotalMembership($project){
//    return count($project->getMemberPHIDs());
//  }

  protected function getTotalBug($milestone){
    $state = id(new PhabricatorOkrsViewState())->setProject($milestone)->readFromRequest($this->getRequest());
    $tasks = $state->getObjects();
    $total_bug = 0;

    foreach ($tasks as $taskID => $task) {
      $tags = $this->getProjectTags($task);
      $bug = count(array_filter($tags,function($v, $k){
        return ($v == 'Bug Report (QA)');
      }, ARRAY_FILTER_USE_BOTH));
      $total_bug += $bug;
    }

    return $total_bug;
  }

  public function getIndexChartData($projectPHID, $totalDev, $index){
    $project = id(new PhabricatorProjectQuery())
      ->setViewer($this->getViewer())
      ->withPHIDs(array($projectPHID))
      ->needMembers(true)
      ->needWatchers(true)
      ->needImages(true)
      ->executeOne();
    $total_dev = $totalDev;
    //$totalMember = $this->getTotalMembership($project);

    $milestones = $this->getMilestones($project);
    $data = [];
    if($milestones){
      $milestone_names = [];
      foreach($milestones as $milestone){
        $milestone_names[] = $milestone->getName();
      }
      $data = $this->getIndexChartDataOfMilestones($project, $data, $total_dev, $milestone_names, $index);
    }
    else{
      $milestone_names = [$project->getName()];
      $data = $this->getIndexChartDataOfMilestones($project, $data, $total_dev, $milestone_names, $index);
    }

    if($milestones){
      foreach($milestone_names as $milestone_name){
        if(!in_array($milestone_name, array_keys($data))){
          $data[$milestone_name] = 0;
        }
      }
    }
    else{
      if(count($data) ==  0){
        $data[$milestone_names[0]] = 0;
      }
    }

    $data = $this->sortBySprint($data, $milestone_names, $total_dev);

    return $data;
  }

  protected function getProjectTags($task)
  {
    if (json_encode($task->edgeProjectPHIDs)) {
      $text = json_encode($task->edgeProjectPHIDs);
      $replacements = [
        '"' => "",
        "[" => "",
        "]" => ""
      ];
      $arrTags = explode(',', strtr($text, $replacements));
      $tags = [];
      for ($i = 0; $i < count($arrTags); $i++) {
        $tag = id(new PhabricatorProject())->loadOneWhere('phid = %s', $arrTags[$i])->getName();
        $tags[] = $tag;
      }
      return $tags;
    } else {
      return null;
    }
  }

  public function isInDayOff($key_result)
  {
    $day_offs =  explode(", ",$key_result->getDayOffs());
    $now = date('d/m/Y');

    return in_array($now, $day_offs);
  }

  protected function sortBySprint($data, $milestone_names, $total_dev){
    $chartData = [];
    foreach($milestone_names as $milestone_name){
      foreach($data as $k => $v){
        if($milestone_name == $k){
          $chartData[$milestone_name] = round($v / $total_dev, 2);
        }
      }
    }
    $data1 = [];
    $data1['label'] = array_keys($chartData);
    $data1['value'] = array_values($chartData);
    return $data1;
  }

  protected function getNameDay($day) {
    switch ($day) {
      case 1 : return 'monday';
      case 2 : return 'tuesday';
      case 3 : return 'wednesday';
      case 4 : return 'thursday';
      case 5 : return 'friday';
      case 6 : return 'saturday';
      case 7 : return 'sunday';
        break;
    }
  }

  protected function dayOfWeekDataChart($arrayDate, $date, $numOfDay, $v_dayOfWeeks, $dayOffs, $start_strtotime, $end_strtotime, $key)
  {
    if ($numOfDay === null) {
      if (!in_array($date->format('d/m/Y'), $dayOffs)) {
        $arrayDate['label'][] = $date->format('d-m-Y');
        $arrayDate['data'][] = null;
        $arrayDate['data2'][] = null;
        $arrayDate['emotions'][] = null;
        $arrayDate['color'][] = null;
        $arrayDate['data3'][] = null;
      }
    } else {
      if ((strpos($v_dayOfWeeks, $numOfDay) !== false)) {
        if ($end_strtotime === null) {
          if ($key == 0) {
            if (strtotime($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y')) >= $start_strtotime) {
              if (!in_array($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d/m/Y'), $dayOffs)) {
                $arrayDate['label'][] = $date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y');
                $arrayDate['data'][] = null;
                $arrayDate['data2'][] = null;
                $arrayDate['emotions'][] = null;
                $arrayDate['color'][] = null;
                $arrayDate['data3'][] = null;
              }
            }
          } else {
            if (!in_array($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d/m/Y'), $dayOffs)) {
              $arrayDate['label'][] = $date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y');
              $arrayDate['data'][] = null;
              $arrayDate['data2'][] = null;
              $arrayDate['emotions'][] = null;
              $arrayDate['color'][] = null;
              $arrayDate['data3'][] = null;
            }
          }
        }

        if ($key === 0) {
          if (strtotime($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y')) >= $start_strtotime &&
            (strtotime($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y')) <= $end_strtotime)) {
            if (!in_array($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d/m/Y'), $dayOffs)) {
              $arrayDate['label'][] = $date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y');
              $arrayDate['data'][] = null;
              $arrayDate['data2'][] = null;
              $arrayDate['emotions'][] = null;
              $arrayDate['color'][] = null;
              $arrayDate['data3'][] = null;
            }
          }
        } else if (strtotime($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y')) <= $end_strtotime
          && (!in_array($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y'), $arrayDate['label']))
        ) {
          if (!in_array($date->modify($this->getNameDay($numOfDay) . ' this week')->format('d/m/Y'), $dayOffs)) {
            $arrayDate['label'][] = $date->modify($this->getNameDay($numOfDay) . ' this week')->format('d-m-Y');
            $arrayDate['data'][] = null;
            $arrayDate['data2'][] = null;
            $arrayDate['emotions'][] = null;
            $arrayDate['color'][] = null;
            $arrayDate['data3'][] = null;
          }
        }
      }
    }
    return $arrayDate;
  }

}
