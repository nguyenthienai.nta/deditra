<?php

abstract class PhabricatorOkrsMoveToHephaestosArchiveController extends PhabricatorController
{
}


final class PhabricatorOkrsMoveToHephaestosController
  extends PhabricatorOkrsMoveToHephaestosArchiveController
{
  public function handleRequest(AphrontRequest $request)
  {

    Javelin::initBehavior(
      'jquery'
    );
    Javelin::initBehavior(
      'jquery-ui'
    );
    Javelin::initBehavior(
      'move-okr'
    );

    $viewer = $request->getUser();
    $okr = id(new PhabricatorOkrsNameQuery())
      ->setViewer($viewer)
      ->withIDs(array($request->getURIData('id')))
      ->requireCapabilities(
        array(
          PhabricatorPolicyCapability::CAN_VIEW,
          PhabricatorPolicyCapability::CAN_EDIT,
        ))
      ->executeOne();
    if (!$okr) {
      return new Aphront404Response();
    }
    $cancel_uri = '/okrs/edit/'.$request->getURIData('id');

    if ($request->isFormPost()) {
      if(count(id(new PhabricatorHephaestosOkr())->loadAllWhere('content = %s', $okr->getContent())) > 0) {
        session_start();
        $_SESSION['message'] = 'This name is already taken in Hephaestos. Please rename this OKR.';
        return id(new AphrontRedirectResponse())->setURI($cancel_uri);
      }
      $this->moveOkrToHephaestos($okr, $viewer);
      $okr->delete();
      session_start();
      $_SESSION['message'] = 'OKR moved successfully';
      return id(new AphrontRedirectResponse())->setURI('/hephaestos');
    }

    $body = array();
    $title = pht('Move (%s) To Hephaestos', $okr->getContent());
    $body[] = pht('This Okr will move to Hephaestos.');
    $button = pht('Move this Okr to Hephaestos');
    $dialog = $this->newDialog()
      ->setTitle($title)
      ->addCancelButton($cancel_uri)
      ->addSubmitButton($button);
    foreach ($body as $paragraph) {
      $dialog->appendParagraph($paragraph);
    }
    return $dialog;
  }

  private function moveOkrToHephaestos($okr, $viewer)
  {
    if(id(new PhabricatorHephaestosOkr())->loadOneWhere('phid = %s', $okr->getPHID())){
      $hephaestos = id(new PhabricatorHephaestosOkr())->loadOneWhere('phid = %s', $okr->getPHID());
    } else {
      $hephaestos = PhabricatorHephaestosOkr::initializeNewOkrs($viewer);
      $hephaestos
        ->setPHID($okr->getPHID())
        ->setContent($okr->getContent())
        ->setViewPolicy($okr->getViewPolicy())
        ->setEditPolicy($okr->getEditPolicy());
    }
    $okrObjectives = id(new PhabricatorOkrsNamespace())->loadAllWhere('OkrPHID = %s', $okr->getPHID());
    $this->moveObjToHephaestos($hephaestos, $okrObjectives, $viewer);
    $hephaestos->save();
  }

  private function moveObjToHephaestos($hephaestos, $okrObjectives, $viewer)
  {
    foreach ($okrObjectives as $okrObjective) {
      if(id(new PhabricatorHephaestosObjective())->loadOneWhere('phid = %s', $okrObjective->getPHID())){
        ${"objective" . $okrObjective->getID()} = id(new PhabricatorHephaestosObjective())->loadAllWhere('phid = %s', $okrObjective->getPHID());
      } else {
        ${"objective" . $okrObjective->getID()} = id(new PhabricatorHephaestosObjective());
        ${"objective" . $okrObjective->getID()}
          ->setPHID($okrObjective->getPHID())
          ->setHephaestosPHID($okrObjective->getOkrPHID())
          ->setTitle($okrObjective->getNamespaceName())
          ->setPoint($okrObjective->getPoint())
          ->setDescription('')
          ->setIsArchived(0)
          ->setViewPolicy($okrObjective->getViewPolicy())
          ->setEditPolicy($okrObjective->getEditPolicy())
          ->save();
      }
      $okrKRs = id(new PhabricatorOkrsKeyResult())->loadAllWhere('ObjectivePHID = %s', $okrObjective->getPHID());
      $objective_copy = ${"objective" . $okrObjective->getID()};
      $this->moveKrToHephaestos($okrKRs, $objective_copy, $viewer);
      //$okrObjective->delete();
    }
  }

  private function moveKrToHephaestos($okrKRs, $objective_copy, $viewer)
  {
    foreach ($okrKRs as $okrKR) {
      if(id(new PhabricatorHephaestosKeyResult())->loadOneWhere('phid = %s', $okrKR->getPHID())){
        ${"kr" . $okrKR->getID()} = id(new PhabricatorHephaestosKeyResult())->loadOneWhere('phid = %s', $okrKR->getPHID());
      } else {
        ${"kr" . $okrKR->getID()} = id(new PhabricatorHephaestosKeyResult());
        ${"kr" . $okrKR->getID()}
          ->setPHID($okrKR->getPHID())
          ->setTitle($okrKR->getContent())
          ->setObjectivePHID($okrKR->getObjectivePHID())
          ->setStartDate($okrKR->getStartDate())
          ->setEndDate($okrKR->getEndDate())
          ->setOwnerPHID($okrKR->getOwnerPHID())
          ->setFrequenceTime($okrKR->getFrequenceTime())
          ->setFrequence($okrKR->getFrequence())
          ->setDayOfWeeks($okrKR->getDayOfWeeks())
          ->setTotalDev($okrKR->getTotalDev())
          ->setProjectPHID($okrKR->getProjectPHID())
          ->setTarget($okrKR->getTarget())
          ->setBaseValue($okrKR->getBaseValue())
          ->setMeansure($okrKR->getMeansure())
          ->setConfidenceLevel($okrKR->getConfidenceLevel())
          ->setManualCheckin($okrKR->getManualCheckin())
          ->setTypeChart($okrKR->getTypeChart())
          ->save();
      }
      $okrKR_copy = ${"kr" . $okrKR->getID()};
      $this->moveCheckinToHephaestos($okrKR_copy, $viewer);
      //$okrKR->delete();
    }
  }

  private function moveCheckinToHephaestos($okrKR_copy, $viewer)
  {
    $okrCheckins = id(new PhabricatorOkrsCurrent())->loadAllWhere('krPHID = %s',$okrKR_copy->getPHID());
    foreach ($okrCheckins as $okrCheckin) {
      if(id(new PhabricatorHephaestosCheckin())->loadOneWhere('phid = %s', $okrCheckin->getPHID())){
        ${"kr" . $okrCheckin->getID()} = id(new PhabricatorHephaestosCheckin())->loadOneWhere('phid = %s', $okrCheckin->getPHID());
      } else {
        ${"kr" . $okrCheckin->getID()} = id(new PhabricatorHephaestosCheckin());
        ${"kr" . $okrCheckin->getID()}
          ->setPHID($okrCheckin->getPHID())
          ->setKrPHID($okrCheckin->getKrPHID())
          ->setColor($okrCheckin->getColor())
          ->setFeedback($okrCheckin->getFeedback())
          ->setResolvedPoint($okrCheckin->getResolvedPoint())
          ->setEffective($okrCheckin->getEffective())
          ->setVelocityData($okrCheckin->getVelocityData())
          ->setHII($okrCheckin->getHII())
          ->setHFI($okrCheckin->getHFI())
          ->setTotalDev($okrCheckin->getTotalDev())
          ->setComment($okrCheckin->getComment())
          ->setCheckinValue($okrCheckin->getCurrent())
          ->setDateCheckin($okrCheckin->getDateCheckin())
          ->setComment($okrCheckin->getComment())
          ->setDateCreated($okrCheckin->getDateCreated())
          ->setDateModified($okrCheckin->getDateModified())
          ->save();
      }

      $okrCheckin_copy = ${"kr" . $okrCheckin->getID()};
      $this->moveXactionToHephaestos($okrCheckin_copy, $viewer);
      //$okrCheckin->delete();
    }
  }

  private function moveXactionToHephaestos($okrCheckin_copy, $viewer)
  {
    $checkinXactions = array_reverse($this->getCheckinXactions($okrCheckin_copy, $viewer));
    foreach ($checkinXactions as $checkinXaction) {
      if($checkinXaction->getTransactionType() === 'okrs.current.current') {
        $transactionType = 'checkin.value';
      } elseif ($checkinXaction->getTransactionType() === 'okrs.current.comment') {
        $transactionType = 'hcheckin.comment';
      } else {
        $transactionType = $checkinXaction->getTransactionType();
      }
      if(id(new PhabricatorHephaestosCheckinTransaction())->loadOneWhere('phid = %s', $checkinXaction->getPHID())){
        continue;
      } else {
        id(new PhabricatorHephaestosCheckinTransaction())
          ->setPHID($checkinXaction->getPHID())
          ->setAuthorPHID($checkinXaction->getAuthorPHID())
          ->setObjectPHID($checkinXaction->getObjectPHID())
          ->setViewPolicy($checkinXaction->getViewPolicy())
          ->setEditPolicy($checkinXaction->getEditPolicy())
          ->setCommentPHID($checkinXaction->getCommentPHID())
          ->setCommentVersion($checkinXaction->getCommentVersion())
          ->setTransactionType($transactionType)
          ->setOldValue($checkinXaction->getOldValue())
          ->setNewValue($checkinXaction->getNewValue())
          ->setContentSource($checkinXaction->getContentSource())
          ->setMetaData($checkinXaction->getMetaData())
          ->setDateCreated($checkinXaction->getDateCreated())
          ->setDateModified($checkinXaction->getDateModified())
          ->save();
      }

      //$checkinXaction->delete();
    }
  }

  private function getCheckinXactions($okrCheckin_copy, $viewer)
  {
    $query = id(new PhabricatorOkrsCurrentTransactionQuery());
    return $query
      ->setViewer($viewer)
      ->withObjectPHIDs(array($okrCheckin_copy->getPHID()))
      ->withTransactionTypes(array('okrs.current.current','okrs.current.comment'))
      ->needComments(true)
      ->execute();
  }

}
