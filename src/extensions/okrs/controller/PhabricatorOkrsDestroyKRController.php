<?php
final class PhabricatorOkrsDestroyKRController
    extends PhabricatorController {

    public function handleRequest(AphrontRequest $request) {
        $viewer = $request->getViewer();
        $id = $request->getURIData('id');

        $kr = id(new PhabricatorOkrsKeyResultQuery())
            ->setViewer($viewer)
            ->withIDs(array($id))
            ->requireCapabilities(
                array(
                    PhabricatorPolicyCapability::CAN_VIEW,
                    PhabricatorPolicyCapability::CAN_EDIT,
                ))
            ->executeOne();
        if (!$kr) {
            return new Aphront404Response();
        }
        $obj_id = id(new PhabricatorOkrsNamespace())->loadOneWhere('phid = %s',$kr->getObjectivePHID())->getOkrPHID();
        $okr_id = id(new PhabricatorOkrsOkr())->loadOneWhere('phid = %s',$obj_id)->getID();

        $cancel_uri = ('/kr/edit/'.$kr->getID().'/');

        if ($request->isFormPost()) {

            id(new PhabricatorOkrsKeyResult())->load($kr->getID())->delete();
            session_start();
            $_SESSION['message'] = 'KR deleted successfully';

            return id(new AphrontRedirectResponse())->setURI('/obj/'.$okr_id.'/');
        }

        $title = pht('Really Delete KR?');
        $body = pht('This KR will delete.');
        $button = pht('Delete KR');

        $dialog = id(new AphrontDialogView())
            ->setUser($viewer)
            ->setTitle($title)
            ->appendChild($body)
            ->addCancelButton($cancel_uri)
            ->addSubmitButton($button);

        return id(new AphrontDialogResponse())->setDialog($dialog);
    }


}