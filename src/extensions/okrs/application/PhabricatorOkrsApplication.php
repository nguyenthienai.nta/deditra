<?php

final class PhabricatorOkrsApplication extends PhabricatorApplication
{

  public function getBaseURI()
  {
    return '/okrs/';
  }

  public function getName()
  {
    return pht('OKRs');
  }

  public function getShortDescription()
  {
    return pht('OKR Management');
  }

  public function getIcon()
  {
    return 'fa-bullseye';
  }

  public function getTitleGlyph()
  {
    return "\xE2\x97\x8E";
  }

  public function getFlavorText()
  {
    return pht('Control access to groups of objects.');
  }

  public function getApplicationGroup()
  {
    return self::GROUP_UTILITIES;
  }

  public function canUninstall()
  {
    return false;
  }

  public function getRemarkupRules()
  {
    return array(
      new PhabricatorOkrsRemarkupRule(),
    );
  }

  public function getRoutes()
  {
    return array(
      '/okrs/' => array(
        '(?:query/(?P<queryKey>[^/]+)/)?' => 'PhabricatorOkrsListOkrController',
        'create/' => 'PhabricatorOkrsEditOkrController',
        'edit/(?:(?P<id>\d+)/)?' => 'PhabricatorOkrsEditOkrController',
        'delete-obj/(?P<id>[1-9]\d*)/'
        => 'PhabricatorOkrsDestroyObjController',
        'delete/key&result/(?:(?P<id>\d+)/)' => 'PhabricatorOkrsDestroyKRController',
        '(?P<action>activate|archive)/(?P<id>\d+)/'
        => 'PhabricatorOkrsArchiveController',
        'move/(?:(?P<id>\d+)/)?' => 'PhabricatorOkrsMoveToHephaestosController',
      ),

      '/obj/' => array(
        '(?:query/(?P<queryKey>[^/]+)/)?' => 'PhabricatorOkrsListController',
        '(?P<id>[1-9]\d*)/(?:query/(?P<queryKey>[^/]+)/)?' => 'PhabricatorOkrsListController',

        'create/' => 'PhabricatorOkrsEditObjController',
        'edit/(?:(?P<id>\d+)/)?' => 'PhabricatorOkrsEditObjController',

        '(?P<action>activate|archive)/(?P<id>\d+)/'
        => 'PhabricatorOkrsArchiveController',
        'edit/key&result/(?:(?P<id>\d+)/)' => 'PhabricatorOkrsEditController',
        'kr/' => array(
          $this->getEditRoutePattern('edit/') => 'PhabricatorOkrsEditController',
        ),
      ),

      '/kr/' => array(
        $this->getEditRoutePattern('edit/') => 'PhabricatorOkrsEditController',
        'checkin/(?P<id>[1-9]\d*)/'
        => 'PhabricatorOkrsCheckinController',
        'detail/(?P<id>[1-9]\d*)/' => 'PhabricatorOkrsKrDetailController',
      ),
    );
  }

  public function isPinnedByDefault(PhabricatorUser $viewer)
  {
    return true;
  }

  protected function getCustomCapabilities()
  {
    return array(
      PhabricatorOkrsCapabilityCreateOkrs::CAPABILITY => array(
        'default' => PhabricatorPolicies::POLICY_USER,
      ),
      PhabricatorOkrsCapabilityDefaultView::CAPABILITY => array(
        'caption' => pht('Default view policy for newly created Okrs.'),
        'default' => PhabricatorPolicies::POLICY_USER,
        'template' => PhabricatorOkrsNamespacePHIDType::TYPECONST,
        'capability' => PhabricatorPolicyCapability::CAN_VIEW,
      ),
      PhabricatorOkrsCapabilityDefaultEdit::CAPABILITY => array(
        'caption' => pht('Default edit policy for newly created Okrs.'),
        'default' => PhabricatorPolicies::POLICY_USER,
        'template' => PhabricatorOkrsNamespacePHIDType::TYPECONST,
        'capability' => PhabricatorPolicyCapability::CAN_EDIT,
      ),
    );
  }

}
