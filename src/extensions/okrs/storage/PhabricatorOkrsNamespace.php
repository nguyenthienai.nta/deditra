<?php

final class PhabricatorOkrsNamespace
  extends PhabricatorOkrsDAO
  implements
  PhabricatorPolicyInterface,
  PhabricatorApplicationTransactionInterface,
  PhabricatorDestructibleInterface
{

  protected $namespaceName;
  protected $point;
  protected $viewPolicy;
  protected $editPolicy;
  protected $isDefaultNamespace;
  protected $description;
  protected $isArchived;
  protected $okrPHID;

  private $okr = self::ATTACHABLE;

  public static function initializeNewNamespace(PhabricatorUser $actor, PhabricatorOkrsOkr $okr)
  {
    $app = id(new PhabricatorApplicationQuery())
      ->setViewer($actor)
      ->withClasses(array('PhabricatorOkrsApplication'))
      ->executeOne();

    $view_policy = $app->getPolicy(
      PhabricatorOkrsCapabilityDefaultView::CAPABILITY);
    $edit_policy = $app->getPolicy(
      PhabricatorOkrsCapabilityDefaultEdit::CAPABILITY);

    return id(new PhabricatorOkrsNamespace())
      ->setIsDefaultNamespace(null)
      ->setViewPolicy($view_policy)
      ->setEditPolicy($edit_policy)
      ->setDescription('')
      ->setIsArchived(0)
      ->setOkrPHID($okr->getPHID())
      ->attachOkr($okr);
  }

  public function attachOkr(PhabricatorOkrsOkr $okr)
  {
    $this->okr = $okr;
    return $this;
  }

  public function getOkr()
  {
    return $this->assertAttached($this->okr);
  }

  protected function getConfiguration()
  {
    return array(
        self::CONFIG_AUX_PHID => true,
        self::CONFIG_COLUMN_SCHEMA => array(
          'namespaceName' => 'text255',
          'point' => 'text',
          'isDefaultNamespace' => 'bool?',
          'description' => 'text',
          'isArchived' => 'bool',
          'okrPHID' => 'phid?',
        ),
        self::CONFIG_KEY_SCHEMA => array(
          'key_default' => array(
            'columns' => array('isDefaultNamespace'),
            'unique' => true,
          ),
        ),
      ) + parent::getConfiguration();
  }

  public function generatePHID()
  {
    return PhabricatorPHID::generateNewPHID(
      PhabricatorOkrsNamePHIDType::TYPECONST);
  }

  public function getMonogram()
  {
    return $this->getNamespaceName();
  }


  /* -(  PhabricatorPolicyInterface  )----------------------------------------- */


  public function getCapabilities()
  {
    return array(
      PhabricatorPolicyCapability::CAN_VIEW,
      PhabricatorPolicyCapability::CAN_EDIT,
    );
  }

  public function getPolicy($capability)
  {
    switch ($capability) {
      case PhabricatorPolicyCapability::CAN_VIEW:
        return $this->getViewPolicy();
      case PhabricatorPolicyCapability::CAN_EDIT:
        return $this->getEditPolicy();
    }
  }

  public function hasAutomaticCapability($capability, PhabricatorUser $viewer)
  {
    return false;
  }

  /* -(  PhabricatorApplicationTransactionInterface  )------------------------- */


  public function getApplicationTransactionEditor()
  {
    return new PhabricatorOkrsNamespaceEditor();
  }

  public function getApplicationTransactionTemplate()
  {
    return new PhabricatorOkrsNamespaceTransaction();
  }


  /* -(  PhabricatorDestructibleInterface  )----------------------------------- */


  public function destroyObjectPermanently(
    PhabricatorDestructionEngine $engine)
  {
    $this->openTransaction();

    $okrs = id(new PhabricatorOkrsNameQuery())
      ->setViewer($engine->getViewer())
      ->withSpacePHIDs(array($this->getPHID()))
      ->execute();
    foreach ($okrs as $okrs) {
      $engine->destroyObject($okrs);
    }
    $this->delete();

    $this->saveTransaction();
  }

}
