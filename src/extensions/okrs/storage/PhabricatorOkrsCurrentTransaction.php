<?php

final class PhabricatorOkrsCurrentTransaction
    extends PhabricatorModularTransaction {


    public function getApplicationName() {
        return 'okrs';
    }

    public function getApplicationTransactionType() {
        return PhabricatorOkrsCurrentPHIDType::TYPECONST;
    }

    public function getBaseTransactionClass() {
        return 'PhabricatorOkrsCurrentTransactionType';
    }

}
