<?php

final class PhabricatorOkrsCurrent extends PhabricatorLiskDAO
    implements
    PhabricatorPolicyInterface,
    PhabricatorFlaggableInterface,
    PhabricatorProjectInterface,
    PhabricatorApplicationTransactionInterface,
    PhabricatorSubscribableInterface,
    PhabricatorDestructibleInterface,
    PhabricatorTokenReceiverInterface {

    const MARKUP_FIELD_BODY    = 'markup:body';
    const MARKUP_FIELD_SUMMARY = 'markup:summary';

    protected $current;
    protected $dateCheckin;
    protected $krPHID;
    protected $color;
    protected $feedback;
    protected $comment;
    protected $hii;
    protected $hfi;
    protected $totalDev;
    protected $velocityData;
    protected $effective;
    protected $resolvedPoint;

    private $keyResult = self::ATTACHABLE;

    public function getApplicationName()
    {
        return 'okrs';
    }

    public static function initializeCurrent(
        PhabricatorOkrsKeyResult $keyResult) {

        $current = id(new PhabricatorOkrsCurrent())
            ->setKrPHID($keyResult->getPHID())
            ->setColor($keyResult->getConfidenceLevel())
            ->setDateCheckin(strtotime('now'))
            ->attachKeyResult($keyResult);

        return $current;
    }

    public function attachKeyResult(PhabricatorOkrsKeyResult $keyResult) {
        $this->keyResult = $keyResult;
        return $this;
    }

    public function getKeyResult() {
        return $this->assertAttached($this->keyResult);
    }

    public function getMonogram() {
        return 'CR'.$this->getID();
    }

    public function getEditURI() {
        return '/current/'.$this->getID().'/';
    }

    protected function getConfiguration() {
        return array(
                self::CONFIG_AUX_PHID   => true,
                self::CONFIG_SERIALIZATION => array(
                    'configData' => self::SERIALIZATION_JSON,
                ),
                self::CONFIG_COLUMN_SCHEMA => array(
                    'current' => 'float',
                    'krPHID' => 'phid?',
                    'color' => 'text255',
                    'feedback' => 'uint32',
                    'comment' => 'text',
                    'hii' => 'text',
                    'hfi' => 'text',
                    'totalDev' => 'float',
                    'velocityData' => 'fulltext',
                    'effective' => 'text',
                    'resolvedPoint' => 'text',
                ),
                self::CONFIG_KEY_SCHEMA => array(
                    'phid' => array(
                        'columns' => array('phid'),
                        'unique' => true,
                    ),
                    'key_dateCheckin' => array(
                        'columns' => array('dateCheckin'),
                      ),
                ),
            ) + parent::getConfiguration();
    }


    public function generatePHID() {
        return PhabricatorPHID::generateNewPHID(
            PhabricatorOkrsCurrentPHIDType::TYPECONST);
    }



    /* -(  PhabricatorPolicyInterface Implementation  )-------------------------- */


    public function getCapabilities() {
        return array(
            PhabricatorPolicyCapability::CAN_VIEW,
            PhabricatorPolicyCapability::CAN_EDIT,
        );
    }

    public function getPolicy($capability) {
        // Draft and archived posts are visible only to the author and other
        // users who can edit the blog. Published posts are visible to whoever
        // the blog is visible to.

        switch ($capability) {
            case PhabricatorPolicyCapability::CAN_VIEW:
                if ($this->getKeyResult()) {
                    return $this->getKeyResult()->getObjective()->getViewPolicy();
                }
                break;
            case PhabricatorPolicyCapability::CAN_EDIT:
                if ($this->getKeyResult()) {
                    return $this->getKeyResult()->getObjective()->getEditPolicy();
                } else {
                    return PhabricatorPolicies::POLICY_NOONE;
                }
        }
    }

    public function hasAutomaticCapability($capability, PhabricatorUser $user) {
        // A blog post's author can always view it.

        switch ($capability) {
            case PhabricatorPolicyCapability::CAN_VIEW:
            case PhabricatorPolicyCapability::CAN_EDIT:
                return ($user->getPHID() == $this->getKeyResult()->getOwnerPHID());
        }
    }

    public function describeAutomaticCapability($capability) {
        return pht('The author of a Okr can always view and edit it.');
    }

    /* -(  PhabricatorApplicationTransactionInterface  )------------------------- */


    public function getApplicationTransactionEditor() {
        return new PhabricatorOkrsCurrentEditor();
    }

    public function getApplicationTransactionTemplate() {
        return new PhabricatorOkrsCurrentTransaction();
    }


    /* -(  PhabricatorDestructibleInterface  )----------------------------------- */


    public function destroyObjectPermanently(
        PhabricatorDestructionEngine $engine) {

        $this->openTransaction();
        $this->delete();
        $this->saveTransaction();
    }


    /* -(  PhabricatorTokenReceiverInterface  )---------------------------------- */


    public function getUsersToNotifyOfTokenGiven() {
        return array(
            $this->getKeyResult()->getOwnerPHID(),
        );
    }


    /* -(  PhabricatorSubscribableInterface Implementation  )-------------------- */


    public function isAutomaticallySubscribed($phid) {
        return ($this->getKeyResult()->getOwnerPHID() == $phid);
    }

}
