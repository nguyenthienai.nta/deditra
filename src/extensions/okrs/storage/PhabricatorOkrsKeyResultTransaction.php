<?php

final class PhabricatorOkrsKeyResultTransaction
    extends PhabricatorModularTransaction {


    public function getApplicationName() {
        return 'okrs';
    }

    public function getApplicationTransactionType() {
        return PhabricatorOkrsKeyResultPHIDType::TYPECONST;
    }

    public function getBaseTransactionClass() {
        return 'PhabricatorOkrsKeyResultTransactionType';
    }

}
