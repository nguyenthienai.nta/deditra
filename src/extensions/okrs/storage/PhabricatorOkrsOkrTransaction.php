<?php

final class PhabricatorOkrsOkrTransaction
    extends PhabricatorModularTransaction {


    public function getApplicationName() {
        return 'okrs';
    }

    public function getApplicationTransactionType() {
        return PhabricatorOkrsNamePHIDType::TYPECONST;
    }

    public function getBaseTransactionClass() {
        return 'PhabricatorOkrsOkrTransactionType';
    }

}
