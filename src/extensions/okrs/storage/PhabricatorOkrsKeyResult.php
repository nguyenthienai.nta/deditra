<?php

final class PhabricatorOkrsKeyResult extends PhabricatorOkrsDAO
  implements
  PhabricatorPolicyInterface,
  PhabricatorFlaggableInterface,
  PhabricatorProjectInterface,
  PhabricatorApplicationTransactionInterface,
  PhabricatorSubscribableInterface,
  PhabricatorDestructibleInterface,
  PhabricatorTokenReceiverInterface {

  const MARKUP_FIELD_BODY    = 'markup:body';
  const MARKUP_FIELD_SUMMARY = 'markup:summary';

  protected $ownerPHID;
  protected $content;
  protected $meansure;
  protected $frequence;
  protected $baseValue;
  protected $target;
  protected $confidenceLevel;
  protected $current;
  protected $progress;
  protected $projectPHID;
  protected $objectivePHID;
  protected $frequenceTime;
  protected $startDate;
  protected $endDate;
  protected $manualCheckin;
  protected $typeChart;
  protected $totalDev;
  protected $confidentScope;
  protected $dayOffs;
  protected $autoCheckin;
  protected $dayOfWeeks;



  private $objective = self::ATTACHABLE;

  public static function initializeKeyResult(
    PhabricatorOkrsNamespace $objective) {

    $keyResult = id(new PhabricatorOkrsKeyResult())
      ->setObjectivePHID($objective->getPHID())
      ->attachObjective($objective);

    return $keyResult;
  }

  public function attachObjective(PhabricatorOkrsNamespace $objective) {
    $this->objective = $objective;
    return $this;
  }

  public function getObjective() {
    return $this->assertAttached($this->objective);
  }

  public function getMonogram() {
    return 'KR'.$this->getID();
  }

  public function getEditURI() {
    return '/phame/post/edit/'.$this->getID().'/';
  }

  protected function getConfiguration() {
    return array(
        self::CONFIG_AUX_PHID   => true,
        self::CONFIG_SERIALIZATION => array(
          'configData' => self::SERIALIZATION_JSON,
        ),
        self::CONFIG_COLUMN_SCHEMA => array(
          'content' => 'text255',
          'meansure' => 'text255',
          'frequence' => 'text255',
          'baseValue' => 'float',
          'confidenceLevel' => 'uint32',
          'target' => 'float',
          'current' => 'uint32',
          'progress' => 'uint32',
          'projectPHID' => 'fulltext',
          'frequenceTime' => 'uint32',
          'startDate' => 'uint32',
          'endDate' => 'uint32',
          'manualCheckin' => 'bool',
          'typeChart' => 'bool',
          'totalDev' => 'float',
          'dayOffs' => 'fulltext',
          'autoCheckin' => 'bool',
          'dayOfWeeks' => 'text255',


          // T6203/NULLABILITY
          // These seem like they should always be non-null?
          'objectivePHID' => 'phid?',
          'ownerPHID' => 'phid?',
          'confidentScope' => 'text255',

        ),
        self::CONFIG_KEY_SCHEMA => array(
          'phid' => array(
            'columns' => array('phid'),
            'unique' => true,
          ),
        ),
      ) + parent::getConfiguration();
  }


  public function generatePHID() {
    return PhabricatorPHID::generateNewPHID(
      PhabricatorPhamePostPHIDType::TYPECONST);
  }



  /* -(  PhabricatorPolicyInterface Implementation  )-------------------------- */


  public function getCapabilities() {
    return array(
      PhabricatorPolicyCapability::CAN_VIEW,
      PhabricatorPolicyCapability::CAN_EDIT,
    );
  }

  public function getPolicy($capability) {
    // Draft and archived posts are visible only to the author and other
    // users who can edit the blog. Published posts are visible to whoever
    // the blog is visible to.

    switch ($capability) {
      case PhabricatorPolicyCapability::CAN_VIEW:
        if ($this->getObjective()) {
          return $this->getObjective()->getViewPolicy();
        }
        break;
      case PhabricatorPolicyCapability::CAN_EDIT:
        if ($this->getObjective()) {
          return $this->getObjective()->getEditPolicy();
        } else {
          return PhabricatorPolicies::POLICY_NOONE;
        }
    }
  }

  public function hasAutomaticCapability($capability, PhabricatorUser $user) {
    // A blog post's author can always view it.

    switch ($capability) {
      case PhabricatorPolicyCapability::CAN_VIEW:
      case PhabricatorPolicyCapability::CAN_EDIT:
        return ($user->getPHID() == $this->getOwnerPHID());
    }
  }

  public function describeAutomaticCapability($capability) {
    return pht('The author of a Okr can always view and edit it.');
  }

  /* -(  PhabricatorApplicationTransactionInterface  )------------------------- */


  public function getApplicationTransactionEditor() {
    return new PhabricatorOkrsKeyResultEditor();
  }

  public function getApplicationTransactionTemplate() {
    return new PhabricatorOkrsKeyResultTransaction();
  }


  /* -(  PhabricatorDestructibleInterface  )----------------------------------- */


  public function destroyObjectPermanently(
    PhabricatorDestructionEngine $engine) {

    $this->openTransaction();
    $this->delete();
    $this->saveTransaction();
  }


  /* -(  PhabricatorTokenReceiverInterface  )---------------------------------- */


  public function getUsersToNotifyOfTokenGiven() {
    return array(
      $this->getOwnerPHID(),
    );
  }


  /* -(  PhabricatorSubscribableInterface Implementation  )-------------------- */


  public function isAutomaticallySubscribed($phid) {
    return ($this->ownerPHID == $phid);
  }

}
