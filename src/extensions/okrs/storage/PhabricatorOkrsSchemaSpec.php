<?php

final class PhabricatorOkrsSchemaSpec
    extends PhabricatorConfigSchemaSpec
{

    public function buildSchemata()
    {
        $this->buildEdgeSchemata(new PhabricatorOkrsNamespace());
    }

}
