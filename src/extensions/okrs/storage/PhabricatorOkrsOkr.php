<?php

final class PhabricatorOkrsOkr
  extends PhabricatorOkrsDAO
  implements
  PhabricatorPolicyInterface,
  PhabricatorApplicationTransactionInterface,
  PhabricatorDestructibleInterface,
  PhabricatorFlaggableInterface,
  PhabricatorProjectInterface
{

  protected $content;
  protected $viewPolicy;
  protected $editPolicy;
  protected $phid;
  protected $isArchived;
  private $objective = self::ATTACHABLE;


  public static function initializeNewOkrs(PhabricatorUser $actor)
  {
    $app = id(new PhabricatorApplicationQuery())
      ->setViewer($actor)
      ->withClasses(array('PhabricatorOkrsApplication'))
      ->executeOne();

    $view_policy = $app->getPolicy(
      PhabricatorOkrsCapabilityDefaultView::CAPABILITY);
    $edit_policy = $app->getPolicy(
      PhabricatorOkrsCapabilityDefaultEdit::CAPABILITY);

    return id(new PhabricatorOkrsOkr())
      ->setViewPolicy($view_policy)
      ->setEditPolicy($edit_policy)
      ->setIsArchived(0);
  }

  protected function getConfiguration()
  {
    return array(
        self::CONFIG_AUX_PHID => true,
        self::CONFIG_COLUMN_SCHEMA => array(
          'content' => 'text255',
          'isArchived' => 'bool',
        ),
        self::CONFIG_KEY_SCHEMA => array(
          'phid' => array(
            'columns' => array('phid'),
            'unique' => true,
          ),
        ),
      ) + parent::getConfiguration();
  }

  public function generatePHID()
  {
    return PhabricatorPHID::generateNewPHID(
      PhabricatorOkrsNamespacePHIDType::TYPECONST);
  }

  public function getMonogram()
  {
    return $this->getContent();
  }


  /* -(  PhabricatorPolicyInterface  )----------------------------------------- */


  public function getCapabilities()
  {
    return array(
      PhabricatorPolicyCapability::CAN_VIEW,
      PhabricatorPolicyCapability::CAN_EDIT,
    );
  }

  public function getPolicy($capability)
  {
    switch ($capability) {
      case PhabricatorPolicyCapability::CAN_VIEW:
        return $this->getViewPolicy();
      case PhabricatorPolicyCapability::CAN_EDIT:
        return $this->getEditPolicy();
    }
  }

  public function hasAutomaticCapability($capability, PhabricatorUser $viewer)
  {
    return false;
  }

  /* -(  PhabricatorApplicationTransactionInterface  )------------------------- */


  public function getApplicationTransactionEditor()
  {
    return new PhabricatorOkrsNameEditor();
  }

  public function getApplicationTransactionTemplate()
  {
    return new PhabricatorOkrsOkrTransaction();
  }


  /* -(  PhabricatorDestructibleInterface  )----------------------------------- */


  public function destroyObjectPermanently(
    PhabricatorDestructionEngine $engine)
  {
    $this->delete();
  }

}
