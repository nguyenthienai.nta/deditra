<?php

final class PhabricatorOkrsNamespaceTransaction
    extends PhabricatorModularTransaction
{

    public function getApplicationName()
    {
        return 'okrs';
    }

    public function getApplicationTransactionType()
    {
        return PhabricatorOkrsNamespacePHIDType::TYPECONST;
    }

    public function getBaseTransactionClass()
    {
        return 'PhabricatorOkrsNamespaceTransactionType';
    }

}
