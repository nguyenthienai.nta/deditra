<?php

final class PhabricatorOkrsNamespaceTransactionQuery
    extends PhabricatorApplicationTransactionQuery
{

    public function getTemplateApplicationTransaction()
    {
        return new PhabricatorOkrsNamespaceTransaction();
    }

}
