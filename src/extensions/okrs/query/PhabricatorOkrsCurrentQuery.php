<?php

final class PhabricatorOkrsCurrentQuery extends PhabricatorCursorPagedPolicyAwareQuery {

    private $ids;
    private $phids;
    private $krPHIDs;

    public function withIDs(array $ids) {
        $this->ids = $ids;
        return $this;
    }

    public function withPHIDs(array $phids) {
        $this->phids = $phids;
        return $this;
    }

    public function withKrPHIDs(array $kr_phids) {
        $this->krPHIDs = $kr_phids;
        return $this;
    }


    protected function loadPage() {
        return $this->loadStandardPage(new PhabricatorOkrsCurrent());
    }
    protected function willFilterPage(array $currents) {
        // We require blogs to do visibility checks, so load them unconditionally.
        $kr_phids = mpull($currents, 'getKrPHID');

        $keyResults = id(new PhabricatorOkrsKeyResultQuery())
            ->setViewer($this->getViewer())
            ->withPHIDs($kr_phids)
            ->execute();

        $keyResults = mpull($keyResults, null, 'getPHID');
        foreach ($currents as $key => $current) {
            $kr_phid = $current->getKrPHID();

            $keyResult = idx($keyResults, $kr_phid);
            if (!$keyResult) {
                $this->didRejectResult($current);
                unset($currents[$key]);
                continue;
            }
            $current->attachkeyResult($keyResult);
        }

        return $currents;
    }

    protected function buildWhereClauseParts(AphrontDatabaseConnection $conn) {
        $where = parent::buildWhereClauseParts($conn);

        if ($this->ids !== null) {
            $where[] = qsprintf(
                $conn,
                'id IN (%Ld)',
                $this->ids);
        }

        if ($this->phids !== null) {
            $where[] = qsprintf(
                $conn,
                'phid IN (%Ls)',
                $this->phids);
        }

        if ($this->krPHIDs !== null) {
            $where[] = qsprintf(
                $conn,
                'krPHID IN (%Ls)',
                $this->krPHIDs);
        }
        return $where;
    }

    public function getQueryApplicationClass() {
        return "PhabricatorOkrsApplication";
    }

    protected function getPrimaryTableAlias() {
        return 'current';
    }

}
