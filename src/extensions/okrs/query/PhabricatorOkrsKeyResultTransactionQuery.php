<?php

final class PhabricatorOkrsKeyResultTransactionQuery
    extends PhabricatorApplicationTransactionQuery {

    public function getTemplateApplicationTransaction() {
        return new PhabricatorOkrsKeyResultTransaction();
    }

}
