<?php

final class PhabricatorOkrsCurrentTransactionQuery
    extends PhabricatorApplicationTransactionQuery {

    public function getTemplateApplicationTransaction() {
        return new PhabricatorOkrsCurrentTransaction();
    }

}
