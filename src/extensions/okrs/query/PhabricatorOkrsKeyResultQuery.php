<?php

final class PhabricatorOkrsKeyResultQuery extends PhabricatorCursorPagedPolicyAwareQuery {

  private $ids;
  private $objectivePHIDs;
  private $ownerPHIDs;
  private $projectPHIDs;
  private $phids;

  private $needHeaderImage;

  public function withIDs(array $ids) {
    $this->ids = $ids;
    return $this;
  }

  public function withPHIDs(array $phids) {
    $this->phids = $phids;
    return $this;
  }

  public function withOwnerPHIDs(array $owner_phids) {
    $this->ownerPHIDs = $owner_phids;
    return $this;
  }

  public function withProjectPHIDs(array $project_phids) {
    $this->projectPHIDs = $project_phids;
    return $this;
  }

  public function withObjectivePHIDs(array $objective_phids) {
    $this->objectivePHIDs = $objective_phids;
    return $this;
  }

  protected function loadPage() {
    return $this->loadStandardPage(new PhabricatorOkrsKeyResult());
  }
  protected function willFilterPage(array $keyResults) {
    // We require blogs to do visibility checks, so load them unconditionally.
    $objective_phids = mpull($keyResults, 'getObjectivePHID');

    $objectives = id(new PhabricatorOkrsNamespaceQuery())
      ->setViewer($this->getViewer())
      ->withPHIDs($objective_phids)
      ->execute();

    $objectives = mpull($objectives, null, 'getPHID');
    foreach ($keyResults as $key => $keyResult) {
      $objective_phid = $keyResult->getObjectivePHID();

      $objective = idx($objectives, $objective_phid);
      if (!$objective) {
        $this->didRejectResult($keyResult);
        unset($keyResults[$key]);
        continue;
      }

      $keyResult->attachObjective($objective);
    }

    return $keyResults;
  }

  protected function buildWhereClauseParts(AphrontDatabaseConnection $conn) {
    $where = parent::buildWhereClauseParts($conn);

    if ($this->ids !== null) {
      $where[] = qsprintf(
        $conn,
        'kr.id IN (%Ld)',
        $this->ids);
    }

    if ($this->phids !== null) {
      $where[] = qsprintf(
        $conn,
        'kr.phid IN (%Ls)',
        $this->phids);
    }

    if ($this->ownerPHIDs !== null) {
      $where[] = qsprintf(
        $conn,
        'kr.ownerPHID IN (%Ls)',
        $this->ownerPHIDs);
    }

    if ($this->objectivePHIDs !== null) {
      $where[] = qsprintf(
        $conn,
        'kr.ObjectivePHID in (%Ls)',
        $this->objectivePHIDs);
    }

    if ($this->projectPHIDs !== null) {
      $where[] = qsprintf(
        $conn,
        'kr.projectPHID in (%Ls)',
        $this->objectivePHIDs);
    }

    return $where;
  }

  public function getQueryApplicationClass() {
    return "PhabricatorOkrsApplication";
  }

  protected function getPrimaryTableAlias() {
    return 'kr';
  }

}
