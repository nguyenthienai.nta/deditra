<?php

final class PhabricatorOkrsOkrTransactionQuery
    extends PhabricatorApplicationTransactionQuery {

    public function getTemplateApplicationTransaction() {
        return new PhabricatorOkrsOkrTransaction();
    }

}