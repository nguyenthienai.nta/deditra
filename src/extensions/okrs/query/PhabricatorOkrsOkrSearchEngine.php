<?php

final class PhabricatorOkrsNamesearchEngine
  extends PhabricatorApplicationSearchEngine
{

  public function getApplicationClassName()
  {
    return 'PhabricatorOkrsApplication';
  }

  public function getResultTypeDescription()
  {
    return pht('Okrs');
  }

  public function newQuery()
  {
    return new PhabricatorOkrsNameQuery();
  }

  protected function buildCustomSearchFields()
  {
    return array(
      id(new PhabricatorSearchThreeStateField())
        ->setLabel(pht('Active'))
        ->setKey('active')
        ->setOptions(
          pht('(Show All)'),
          pht('Show Only Active OKR'),
          pht('Hide Active OKR')),
    );
  }

  protected function buildQueryFromParameters(array $map)
  {
    $query = $this->newQuery();

    if ($map['active']) {
      $query->withIsArchived(!$map['active']);
    }

    return $query;
  }

  protected function getURI($path)
  {
    return '/okrs/'.$path;
  }

  protected function getBuiltinQueryNames()
  {
    $names = array(
      'active' => pht('Active OKRs'),
      'all' => pht('All OKRs'),
    );

    return $names;
  }

  public function buildSavedQueryFromBuiltin($query_key)
  {
    $query = $this->newSavedQuery();
    $query->setQueryKey($query_key);

    switch ($query_key) {
      case 'active':
        return $query->setParameter('active', true);
      case 'all':
        return $query;
    }

    return parent::buildSavedQueryFromBuiltin($query_key);
  }

  private function setExecutionQuality($okrs)
  {
    foreach ($okrs as $key => $okr) {
      $objectives = id(new PhabricatorOkrsNamespace())->loadAllWhere('OkrPHID = %s', $okr->getPHID());
      $okr_total_task_green = 0;
      $okr_total_task = 0;
      foreach($objectives as $key => $objective){
        $key_results = id(new PhabricatorOkrsKeyResult())->loadAllWhere('ObjectivePHID = %s', $objective->getPHID());
        $obj_total_task_green = count(array_filter($key_results,function($v, $k){
          return $v->getConfidenceLevel() == 1;
        }, ARRAY_FILTER_USE_BOTH));
        $okr_total_task_green += $obj_total_task_green;
        $okr_total_task += count($key_results);
      }
      $excution_quality = $okr_total_task_green/$okr_total_task;
      $excution_quality < 0.6 ? ($okr->excution_quality = 3) : ($excution_quality < 0.8 ?  $okr->excution_quality = 2 : $okr->excution_quality = 1);
    }

    return $okrs;
  }

  private function getChartData($okrs)
  {
    $data = [];
    foreach ($okrs as $key => $okr) {
      $objectives = id(new PhabricatorOkrsNamespace())->loadAllWhere('OkrPHID = %s', $okr->getPHID());
      $objective_point = [];

      foreach ($objectives as $key => $objective) {
        $key_results = id(new PhabricatorOkrsKeyResult())->loadAllWhere('ObjectivePHID = %s', $objective->getPHID());
        $total_kr_confident = $this->getTotalKrConfident($key_results);
        $success_rule = explode(",", $objective->getPoint());
        $total_key_results = count(id(new PhabricatorOkrsKeyResult())->loadAllWhere('ObjectivePHID = %s', $objective->getPHID()));

        $total_key_results != 0 ? $obj_confident_rate = ($total_kr_confident / $total_key_results)*100 : $obj_confident_rate = 0;

        for ($i = 0; $i < 10; $i++) {
          if ($obj_confident_rate == 0) {
            $objective_point[$objective->getNamespaceName()] = 0;
          }
          else if ($obj_confident_rate >= (int)$success_rule[9]) {
            $objective_point[$objective->getNamespaceName()] = 10;
          }
          else if ($obj_confident_rate < (int)$success_rule[0]) {
            $objective_point[$objective->getNamespaceName()] = 0;
          }
          else if ($obj_confident_rate < (int)$success_rule[$i] && $obj_confident_rate >= (int)$success_rule[$i - 1]) {
            $objective_point[$objective->getNamespaceName()] = $i;
          }
        }
      }
      $objective_point != null ? $data[] = $objective_point : $data[] = array();
    }
    return $data;
  }

  protected function renderResultList(
    array $okrs,
    PhabricatorSavedQuery $query,
    array $handles)
  {
    assert_instances_of($okrs, 'PhabricatorOkrsOkr');
    require_celerity_resource('okrs-css');
    require_celerity_resource('chart-css');
    require_celerity_resource('chartMin-css');

    Javelin::initBehavior(
      'chart-bundle-v2.8'
    );
    Javelin::initBehavior(
      'chart-v2.8'
    );
    Javelin::initBehavior(
      'jquery'
    );
    Javelin::initBehavior(
      'okr'
    );
    $list = [];
    session_start();
    if (isset($_SESSION['message'])) {
      $save = id(new PHUIInfoView())
        ->setSeverity(PHUIInfoView::SEVERITY_NOTICE)
        ->appendChild($_SESSION['message']);
      $list[] = $save;
      unset($_SESSION['message']);
    }

    $okrs1 = $this->setExecutionQuality($okrs);
    $viewer = $this->requireViewer();
    $board = new PHUIOkrsboardView();
    $ids = [];
    $colors = [];
    $contents = [];
    $hidden_icon_editOKR = [];
    foreach ($okrs as $key => $okr) {
      $check_okr = id(new PhabricatorOkrsNameQuery())
        ->setViewer($viewer)
        ->withIDs(array($okr->getID()))
        ->requireCapabilities(
          array(
            PhabricatorPolicyCapability::CAN_VIEW,
          ))
        ->executeOne();

      $can_edit = PhabricatorPolicyFilter::hasCapability(
        $viewer,
        $check_okr,
        PhabricatorPolicyCapability::CAN_EDIT);

      $check_icon_can_edit = $can_edit === true ? 'fa-pencil' : '';
      $hidden_icon_editOKR[] = $check_icon_can_edit;
      $id = $okr->getID();
      $ids[] = $id;
      $contents[] = $okr->getContent();
      $okr->excution_quality == 1 ? $color = 'green' : ($okr->excution_quality == 2 ? $color = 'yellow': $color = 'red');
      $colors[] = $color;
      $item = id(new PHUIOkrsboardItemView())
        ->setObject($okr)
        ->setHeader(mb_strimwidth($okr->getContent(), 0, 33, '...'))
        ->setUser($viewer)
        ->setURI("/obj/{$id}/")
        ->setImageSize(280, 210)
        ->setId($id);
      $href = new PhutilURI('/okrs/edit/' . $okr->getID() . '/');
      $board->addItem($item);
    }

    $data = $this->getChartData($okrs);

    $form = id(new AphrontFormView())
      ->setUser($viewer)
      ->setID('form_edit');
    $form->addHiddenInput('ids', json_encode(array('ids' => $ids)));
    $form->addHiddenInput('hidden_icon_editOKR', json_encode(array('hidden_icon_editOKR' => $hidden_icon_editOKR)));
    $form->addHiddenInput('colors', json_encode(array('colors' => $colors)));
    $form->addHiddenInput('contents', json_encode(array('contents' => $contents)));
    $form->addHiddenInput('data', json_encode(array('data' => $data)));

    $list[] = $form;
    $list[] = $board;
    $result = new PhabricatorApplicationSearchResultView();
    $result->setContent($list);
    $result->setNoDataString(pht('No OKR found.'));

    return $result;
  }

  protected function getNewUserBody()
  {
    $create_button = id(new PHUIButtonView())
      ->setTag('a')
      ->setText(pht('Create a OKRs'))
      ->setHref('create/')
      ->setColor(PHUIButtonView::GREEN);

    $icon = $this->getApplication()->getIcon();
    $app_name = $this->getApplication()->getName();
    $view = id(new PHUIBigInfoView())
      ->setIcon($icon)
      ->setTitle(pht('Welcome to %s', $app_name))
      ->setDescription(
        pht('OKRs Management'))
      ->addAction($create_button);

    return $view;
  }

  private function getTotalKrConfident($key_results)
  {
    $total_kr_confident = 0;
    foreach ($key_results as $key => $kr) {
      if ($kr->getConfidenceLevel() == 1) {
        $total_kr_confident++;
      }
    }
    return $total_kr_confident;
  }
}
