<?php

final class PhabricatorOkrsKrDatasource
  extends PhabricatorTypeaheadDatasource
{

  public function getBrowseTitle()
  {
    return pht('Browse Okrs');
  }

  public function getPlaceholderText()
  {
    return pht('Type a okr name...');
  }

  public function getDatasourceApplicationClass()
  {
    return 'PhabricatorOkrsApplication';
  }

  public function loadResults()
  {
    $query = id(new PhabricatorOkrsKeyResultQuery());

    $viewer = $this->getViewer();
    $krs = $this->executeQuery($query);
    $results = array();

    $query_okr = id(new PhabricatorOkrsNameQuery());
    $Okrs = $this->executeQuery($query_okr);
    $okr_ids = [];

    foreach ($Okrs as $okr){
      $okr_ids[] = $okr->getID();
    }

    foreach ($krs as $space) {
      $full_name = pht(
        '%s %s',
        '',
        $space->getContent());

      $uri = isset($space) ? "/kr/detail/" . $space->getID() : '';

      $obj = id(new PhabricatorOkrsNamespace())->loadOneWhere('phid = %s', $space->getObjectivePHID());
      $okrPHID = $obj->getOkrPHID();
      $Okrs = id(new PhabricatorOkrsOkr())->loadOneWhere('phid = %s', $okrPHID);
      $okr_id = $Okrs ? $Okrs->getID() : '';
      $nameOkr = $Okrs ? $Okrs->getContent() : '';

      if ($okr_ids !== NULL) {
        if (in_array($okr_id, $okr_ids)) {
          $check_objective = id(new PhabricatorOkrsNameQuery())
            ->setViewer($viewer)
            ->withIDs(array($okr_id))
            ->executeOne();
          $can_edit = PhabricatorPolicyFilter::hasCapability(
            $viewer,
            $check_objective,
            PhabricatorPolicyCapability::CAN_VIEW);
          $check_can_search = $can_edit === true ? 1 : 0;

          if ($check_can_search === 1) {
            $result = id(new PhabricatorTypeaheadResult())
              ->setName($full_name)
              ->setDisplayName($space->getContent())
              ->setDisplayType($nameOkr . ' - ' . $obj->getNamespaceName())
              ->setIcon('fa-bull')
              ->setURI($uri)
              ->setImageSprite(
                'phabricator-search-icon phui-font-fa phui-icon-view fa-bullseye')
              ->setPHID($space->getPHID());
            $results[] = $result;
          }
        }
      }
    }

    return $this->filterResultsAgainstTokens($results);
  }

}
