<?php

final class PhabricatorOkrsDataSource
  extends PhabricatorTypeaheadDatasource
{

  public function getBrowseTitle()
  {
    return pht('Browse Okrs');
  }

  public function getPlaceholderText()
  {
    return pht('Type a okr name...');
  }

  public function getDatasourceApplicationClass()
  {
    return 'PhabricatorOkrsApplication';
  }

  public function loadResults()
  {
    $query = id(new PhabricatorOkrsNameQuery());

    $Okrs = $this->executeQuery($query);
    $results = array();
    foreach ($Okrs as $space) {
      $full_name = pht(
        '%s %s',
        '',
        $space->getContent());

      $uri = isset($space) ? "/obj/" . $space->getID() : '';

      $result = id(new PhabricatorTypeaheadResult())
        ->setName($full_name)
        ->setDisplayType("OKRs")
        ->setURI($uri)
        ->setImageSprite(
          'phabricator-search-icon phui-font-fa phui-icon-view fa-bullseye')
        ->setPHID($space->getPHID());

      if ($space->getIsArchived()) {
        $result->setClosed(pht('Archived'));
      }

      $results[] = $result;
    }

    return $this->filterResultsAgainstTokens($results);
  }

}
