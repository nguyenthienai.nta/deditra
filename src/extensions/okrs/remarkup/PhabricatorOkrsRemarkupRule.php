<?php

final class PhabricatorOkrsRemarkupRule
    extends PhabricatorObjectRemarkupRule
{

    protected function getObjectNamePrefix()
    {
        return 'S';
    }

    protected function loadObjects(array $ids)
    {
        $viewer = $this->getEngine()->getConfig('viewer');
        return id(new PhabricatorOkrsNamespaceQuery())
            ->setViewer($viewer)
            ->withIDs($ids)
            ->execute();
    }

}
