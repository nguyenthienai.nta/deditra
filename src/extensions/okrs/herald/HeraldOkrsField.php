<?php

final class HeraldOkrsField extends HeraldField
{

    const FIELDCONST = 'okrs';

    public function getHeraldFieldName()
    {
        return pht('okrs');
    }

    public function getFieldGroupKey()
    {
        return HeraldSupportFieldGroup::FIELDGROUPKEY;
    }

    public function supportsObject($object)
    {
        return ($object instanceof PhabricatorOkrssInterface);
    }

    public function getHeraldFieldValue($object)
    {
        return PhabricatorOkrssNameOkrsQuery::getObjectOkrsPHID($object);
    }

    protected function getHeraldFieldStandardType()
    {
        return self::STANDARD_PHID;
    }

    protected function getDatasource()
    {
        return new PhabricatorOkrssNameOkrsDatasource();
    }

}
